package ch.soreco.webbies.common.db;
import java.lang.reflect.Field;
import java.util.ArrayList;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
import ch.ivyteam.ivy.process.data.persistence.IIvyQuery;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
public class EntityFilter {
	public static java.util.List<?> getEntityResultList(CompositeObject filter, IIvyEntityManager persistenceUnit){
		java.util.List<?> found = null;
		
		StringBuffer whereClauses = new StringBuffer("");
		
		ArrayList<String> allowedClasses = new ArrayList<String>();
		allowedClasses.add("java.lang.String");
		allowedClasses.add("java.lang.Integer");
		allowedClasses.add("java.util.Date");
		
		
		ArrayList<Object> filterValues = new ArrayList<Object>();;
		ArrayList<String> filterParameters = new ArrayList<String>();

		//Loop given Filter employee
		for(Field field:filter.getClass().getDeclaredFields()){
			String fieldName = field.getName();
			if(fieldName!="serialVersionUID"){
				try{
					Object objField = filter.get(fieldName);
					if(objField!=null){
						String fieldClass = objField.getClass().getName();
						//If Object is in list of allowed classes append
						//fieldName and parametername to where clauses
						//if objField returns "java.lang.Object" class ignore
						//current field as it will have no value
						if(fieldClass!="java.lang.Object"
						&&allowedClasses.indexOf(fieldClass)>-1) {
							if(whereClauses.length()>0) {
								whereClauses.append(" and ");
								}
							whereClauses.append("e."+fieldName+" = :"+fieldName);
							filterParameters.add(fieldName);
							filterValues.add(objField);
							}
						}						
					}
				catch (Exception e){
					Ivy.log().info("fieldName:"+fieldName+" exception occurred:"+e.toString(), e);
					}
				}
			}

		try {

			if(whereClauses.length()==0){
				found = persistenceUnit.findAll(filter.getClass());
				}
			else{			
				String select = "select e from "+filter.getClass().getSimpleName()+" e where "+whereClauses;				
				IIvyQuery query = persistenceUnit.createQuery(select);
				for(Integer i=0;i<filterParameters.size();i++){
					query.setParameter(filterParameters.get(i),filterValues.get(i));
					}
				found = query.getResultList();
				}
				
			if(found.size() >0) {

				}
			else {
				String error = "No Data of class "+found.getClass().getSimpleName()+" stored in environment "+Ivy.session().getActiveEnvironment();
				Ivy.log().info(error);
				}
		}
		catch (Exception e)
			{
				Ivy.log().error(e.getMessage(),e);
			}

		return found;
		}
}
