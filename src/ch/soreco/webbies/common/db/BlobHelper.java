package ch.soreco.webbies.common.db;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

import ch.ivyteam.db.jdbc.DatabaseUtil;
import ch.ivyteam.ivy.db.IExternalDatabase;
import ch.ivyteam.ivy.db.IExternalDatabaseApplicationContext;
import ch.ivyteam.ivy.db.IExternalDatabaseRuntimeConnection;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;

public class BlobHelper {
	IExternalDatabase database = null;
	private String ivyDBConnectionName = null; // the user friendly connection name to Database in Ivy

	public BlobHelper(String dbConfigName){
		this.ivyDBConnectionName = dbConfigName;
	}
	/**
	 * used to get Ivy IExternalDatabase object with given user friendly name of Ivy Database configuration
	 * @param _nameOfTheDatabaseConnection: the user friendly name of Ivy Database configuration
	 * @return the IExternalDatabase object
	 * @throws Exception 
	 * @throws EnvironmentNotAvailableException 
	 */
	private IExternalDatabase getDatabase() throws Exception{
		if(database==null){
			final String _nameOfTheDatabaseConnection = this.ivyDBConnectionName;
			database = Ivy.session().getSecurityContext().executeAsSystemUser(new Callable<IExternalDatabase>(){
				public IExternalDatabase call() throws Exception {
					IExternalDatabaseApplicationContext context = (IExternalDatabaseApplicationContext)Ivy.wf().getApplication().getAdapter(IExternalDatabaseApplicationContext.class);
					return context.getExternalDatabase(_nameOfTheDatabaseConnection);
				}
			});
		}
		return database;	
	}
	public void fileUpload(String fileUrl, Integer fileId) throws PersistencyException, SQLException, Exception{
		String query = "UPDATE tbl_Files SET fileBlob = ? WHERE fileId="+fileId;

		//get File Input Stream
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(fileUrl));
		// get a connection from the environment
		IExternalDatabaseRuntimeConnection connection = null;
		try {
			connection = getDatabase().getAndLockConnection();
			Connection jdbcConnection=connection.getDatabaseConnection();

			//now Prepare File Update Statement
			PreparedStatement statement = null;
			try{
				statement = jdbcConnection.prepareStatement(query);
				//set Blob
				statement.setBinaryStream(1, inputStream, inputStream.available());
				statement.executeUpdate();				
			} finally{
				DatabaseUtil.close(statement);
			}
			
		} finally{
			inputStream.close();
			if(connection!=null ){
				database.giveBackAndUnlockConnection(connection);
			}
		}		
	}
	public ch.ivyteam.ivy.scripting.objects.File fileDownload(Integer fileId) throws PersistencyException, SQLException, Exception {
		String query = "SELECT fileBlob, fileName FROM  tbl_Files WHERE fileId="+fileId;
		ch.ivyteam.ivy.scripting.objects.File file = null;
		// get a connection from the environment
		IExternalDatabaseRuntimeConnection connection = null;
		try {
			connection = getDatabase().getAndLockConnection();
			Connection jdbcConnection=connection.getDatabaseConnection();

			//now Prepare File Update Statement
			PreparedStatement statement = null;
			try{
				statement = jdbcConnection.prepareStatement(query);
				//get Blob
				ResultSet rs = statement.executeQuery();
				if (rs.next()) {
					String fileName = rs.getString(2);
					// Get Bytes
					InputStream  in = rs.getBinaryStream(1);
					file = new ch.ivyteam.ivy.scripting.objects.File(fileName);
					if(!file.exists()){
			            int i;
			            byte[] buf = new byte[8192];
			            FileOutputStream out = new FileOutputStream( file.getJavaFile() );
			            while(( i = in.read( buf )) > 0 ) {
			                out.write( buf, 0, i );
			            }
			            in.close();
			            out.flush();
			            out.close();	
		            }
					
				}
			} finally{
				DatabaseUtil.close(statement);
			}
			
		} finally{
			if(connection!=null ){
				database.giveBackAndUnlockConnection(connection);
			}
		}
		return file;
	}
}
