package ch.soreco.webbies.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.eclipse.jetty.util.URIUtil;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.Record;

public class XmlProperties {
	private final String META_NAME = "properties.xml";
	private final String FOLDER_SEPARATOR = "/";
	private final String FOLDER_PROPERTIES = "properties/";
	private Properties properties = null;
	private String comment = "";
	private Class<?> clazz = null;
	private Field field = null;
	private String view="";
	public XmlProperties(Class<?> clazz) {
		this.clazz = clazz;
	}
	public XmlProperties(Class<?> clazz, String view) {
		this.clazz = clazz;
		this.view = view;
	}
	public XmlProperties(Field field, String view) {
		this.clazz = field.getDeclaringClass();
		this.field = field;
		this.view = view;
	}
	public XmlProperties(Record rec) throws ClassNotFoundException
											, SecurityException
											, NoSuchFieldException {
		String className = (String) rec.getField("Class");
		String fieldName = "";
		if(rec.getField("Field")!=null){
			fieldName = (String) rec.getField("Field");
		}
		this.view = (String) rec.getField("View");		
		this.clazz = Class.forName(className);
		if(!fieldName.equals("")){
			this.field = this.clazz.getDeclaredField(fieldName);			
		}
		Properties props = this.getProperties();
		
		for(String key:rec.getKeys()){
			if(rec.getField(key)!=null){
				Object object = rec.getField(key);
				String value = CompositeUtils.getValue(object);
				//String value = (String) rec.getField(key);
				props.put(key, value);
			}
		}
		this.setProperties(props);
		this.save();
	}
	/**
	 * @return the resourceName
	 */
	private String getResourceName() {
		String name = "";
		name = clazz.getSimpleName();
		if(field!=null){
			name = name+"."+field.getName();
		}
		if(!view.equals("")){
			name = name+"."+view;
		}
		name = name+"."+META_NAME;
		return name;
	}
	private String getPropertiesPath(){
		//get the Code location
		String codeLocation = this.clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedLocation = URIUtil.decodePath(codeLocation);
		File location = new File(decodedLocation);
		if(location.exists()){
			codeLocation = location.getAbsolutePath();
		}
		String projectPath = codeLocation.substring(0, codeLocation.toLowerCase().lastIndexOf("classes"));
		String packagePath = this.clazz.getPackage().getName().replace(".",this.FOLDER_SEPARATOR);
		String propertiesFolder = projectPath+FOLDER_PROPERTIES+packagePath+this.FOLDER_SEPARATOR;
		File folder = new File(propertiesFolder);
		Boolean exists = false;
		if(!folder.exists()){
			exists = folder.mkdirs();
		}
		else {
			exists = true;
		}
		if(exists){
			//Ivy.log().debug(folder.getAbsolutePath()+"\nProperties Folder:"+propertiesFolder);
			return folder.getAbsolutePath()+this.FOLDER_SEPARATOR+this.getResourceName();		
			
		}
		else {
			return this.getResourceName();
		}
	}
	/**
	 * Get Properties from an XML as is stored relative to class named Class SimpleName +field name + "view"+properties.xml
	 */
	public Properties getProperties(){
		if(this.properties==null){
			this.properties = new Properties();
			FileInputStream in = null;
			try {
				//Ivy.log().info(this.getClass().getSimpleName()+":Loading Xml Properties from "+this.getPropertiesPath());
				in = new FileInputStream(this.getPropertiesPath());
			} catch (FileNotFoundException e1) {
				//Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e1);
			}
			if(in!=null){
				try {
					this.properties.loadFromXML(in);
				} catch (InvalidPropertiesFormatException e) {
					Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
				} catch (IOException e) {
					Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
				} finally {
					try {
						in.close();
					} catch (IOException e) {
						Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
					}
				}
			}
		}
		return properties;
	}
	public Boolean hasProperties(){
		return this.getProperties().size()>0;
	}
	public void setProperties(Properties properties){
		if(this.properties==null){
			this.properties = new Properties();
			}
		this.properties = properties;
	}
	/**
	 * Get a property by key
	 * @param key
	 * @return String, empty String if not in property map
	 */
	public String getProperty(String key){
		String property = "";
		if(this.getProperties().containsKey(key)){
			property = (String) this.getProperties().get(key);
		}
		return property;
	}
	public void setProperty(String key, String value){
		if(this.properties==null){
			this.properties = new Properties();
			}
		this.properties.put(key, value);
	}
	public void save(){
		if(this.getProperties().size()>0){
			String path = "";
			FileOutputStream out = null;
			try {
				path = this.getPropertiesPath();
				out = new FileOutputStream(path);
				this.getProperties().storeToXML(out, this.comment);
				//Ivy.log().info(this.getClass().getSimpleName()+":Saved Xml Properties to "+this.getPropertiesPath());
			} catch (Exception e) {
				Ivy.log().error(this.getClass().getSimpleName()+":Error on Saving Xml Properties of "+this.getResourceName()+" to "+path, e);				
			} finally {
				if(out!=null){
					try {
						out.close();
					} catch (IOException e) {
						Ivy.log().error(this.getClass().getSimpleName()+":Error on Saving Xml Properties of "+this.getResourceName()+" to "+path, e);
					}
				}
			}
		}
	}
	/**
	 * Define the comment to set to the Beginning of properties xml
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		if(this.comment.equals("")){
			this.comment = "Properties - these values are saved programmatically - do not change this.";
		}
		return comment;
	}
	public String toString(){
		StringBuilder text = new StringBuilder("");
		for(String key:this.properties.stringPropertyNames()){
			text.append(key);
			text.append("=");
			text.append(this.properties.getProperty(key));
			text.append("\n");
		}
		return text.toString();
	}
	public Record toRecord(){
		Record rec = new Record();
		String fieldName = "";
		if(field!=null){
			fieldName = this.field.getName();
		}
		rec.putField("Class", this.clazz.getName());
		rec.putField("Field", fieldName);
		rec.putField("View", this.view);
		for(String key:this.getProperties().stringPropertyNames()){
			String value = this.getProperties().getProperty(key);
			rec.putField(key, value);
		}
		return rec;
	}
}
