package ch.soreco.webbies.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.html.generics.AttributeConstants;

public class ListToRecordset {
	public static Recordset getRecordset(List<CompositeObject> list, Class<?> clazz){
		return ListToRecordset.getRecordset(list, clazz, false, false);
	}
	/**
	 * Converts a List to a ivy Recordset. To skip enclosed CompositeObjects set the @param skipComposites to true.
	 * @param list
	 * @param clazz base class of the list items
	 * @param skipComposites to skip enclosed Composites set to true.
	 * @param useIdFieldOnEnclosedEntity To get the value of the Id Field only set to true here.
	 * @return Recordset or empty Recordset if List is empty or null.
	 */
	public static Recordset getRecordset(List<CompositeObject> list
										, Class<?> clazz
										, Boolean skipComposites
										, Boolean useIdFieldOnEnclosedEntity){
		Recordset rs = new Recordset();
		List<String> fieldKeys = new ArrayList<String>();
		HashMap<String, String> entities  = new HashMap<String, String>();
		
		for(Field field :clazz.getDeclaredFields()){
			if(!field.getName().equals("serialVersionUID")){
				boolean isComposite = CompositeObject.class.isAssignableFrom(field.getType());
				if(skipComposites){
					if(!isComposite){
						fieldKeys.add(field.getName());
					}
				}
				else {
					if(isComposite
							&&useIdFieldOnEnclosedEntity){
						if(PersistenceUtils.isEntityClass(field.getType())){
							String fieldName = field.getName();
							String idFieldName = PersistenceUtils.getIdFieldName(field.getType());
							if(!idFieldName.equals("")){
								fieldName = fieldName+AttributeConstants.SEPARATORCLASS+idFieldName;								
								entities.put(field.getName(), fieldName);							
							}
							fieldKeys.add(fieldName);
						}
						else {
							fieldKeys.add(field.getName());							
						}
					}
					else {
						fieldKeys.add(field.getName());						
					}
				}
			}
		}
		//initialize columns
		for(int col=0;col<fieldKeys.size();col++){
			rs.addColumn(fieldKeys.get(col),  new ArrayList<Object>());
		}		
		if(list!=null){
			for(int i=0;i<list.size();i++){
				CompositeObject item = list.get(i);

				//initialize new row
				ch.ivyteam.ivy.scripting.objects.List<Object> row = ch.ivyteam.ivy.scripting.objects.List.create();
				for(int j=0;j<fieldKeys.size();j++){
					row.add(new Object());
				}
				
				for(Field field:item.getClass().getDeclaredFields()){
					//set the Recordset field key
					String fieldKey = field.getName();
					boolean isComposite = CompositeObject.class.isAssignableFrom(field.getType());
					if(isComposite){
						if(entities.containsKey(field.getName())){
							fieldKey = entities.get(field.getName());
						}
					}
					int fieldIndex = fieldKeys.indexOf(fieldKey);
					if(fieldIndex>-1){
						Object value = null;
						try {
							//get object value from declared field
							value = item.get(field.getName());
						} catch (IllegalArgumentException e) {
							Ivy.log().info(ListToRecordset.class.getSimpleName()+" Error on putting field "+field.getName()+" at index "+i,  e);
						} catch (NoSuchFieldException e) {
							Ivy.log().info(ListToRecordset.class.getSimpleName()+" Error on putting field "+field.getName()+" at index "+i,  e);
						}
						if(isComposite&&useIdFieldOnEnclosedEntity&&value!=null){
							CompositeObject entity = (CompositeObject) value;
							value = PersistenceUtils.getIdFieldValue(entity);
						}
		
						if(skipComposites){
							if(!isComposite){
								row.set(fieldIndex, value);
							}
						}
						else {
							row.set(fieldIndex, value);
						}					
					}
				}
				rs.add(row);
			}			
		}
		return rs;
	}
}
