package ch.soreco.webbies.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import ch.ivyteam.ivy.environment.Ivy;

public class XmlProperties2 {
	private final String META_NAME = "properties";
	private final String FOLDER_SEPARATOR = "/";
	private final String FOLDER_PROPERTIES = "properties/";
	private final String PROPERTY_DELIMINITER = ".";
	private Properties properties = null;
	private Properties currentProperties = null;
	private String comment = "";
	private Class<?> clazz = null;
	private Field field = null;
	private String view="";
	private String prefix="";
	public XmlProperties2(Class<?> clazz) {
		this.clazz = clazz;
	}
	public XmlProperties2(Class<?> clazz, String view) {
		this.clazz = clazz;
		this.view = view;
	}
	public XmlProperties2(Field field, String view) {
		this.clazz = field.getDeclaringClass();
		this.field = field;
		this.view = view;
	}
	/**
	 * @return the resourceName
	 */
	private String getResourceName() {
		String name = "";
		name = clazz.getSimpleName();
		name = name+PROPERTY_DELIMINITER+META_NAME;
		return name;
	}
	private String getPropertyPrefix(){
		if(this.prefix.equals("")){
			this.setPropertyPrefix();
		}
		return prefix;
	}
	private void setPropertyPrefix(){
		this.prefix = this.clazz.getSimpleName();
		if(this.field!=null){
			this.prefix = this.prefix+PROPERTY_DELIMINITER+this.field.getName();
		}
		if(!view.equals("")){
			this.prefix = this.prefix+PROPERTY_DELIMINITER+this.view;
		}		
	}
	private String getPropertiesPath(){
		//get the Code location
		String codeLocation = this.clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
		String projectPath = codeLocation.substring(0, codeLocation.toLowerCase().lastIndexOf("classes"));
		String packagePath = this.clazz.getPackage().getName().replace(PROPERTY_DELIMINITER,this.FOLDER_SEPARATOR);
		String propertiesFolder = projectPath+FOLDER_PROPERTIES+packagePath+this.FOLDER_SEPARATOR;
		File folder = new File(propertiesFolder);
		Boolean exists = false;
		if(!folder.exists()){
			exists = folder.mkdirs();
		}
		else {
			exists = true;
		}
		if(exists){
			return propertiesFolder+this.getResourceName();
		}
		else {
			return this.getResourceName();
		}
	}

	public Properties getProperties(){
		if(this.currentProperties==null){
			this.currentProperties = new Properties();
			for(String key:this.getAllProperties().stringPropertyNames()){
				if(key.startsWith(this.getPropertyPrefix())){
					String currentKey = key.substring(this.getPropertyPrefix().length()+1);
					this.currentProperties.put(currentKey, this.getAllProperties().get(key));
				}
			}
		}
		return this.currentProperties;
	}
	/**
	 * Get Properties from an XML as is stored relative to class named Class SimpleName +field name + "view"+properties.xml
	 */
	private Properties getAllProperties(){
		if(this.properties==null){
			this.properties = new Properties();
			FileInputStream in = null;
			try {
				//Ivy.log().info(this.getClass().getSimpleName()+":Loading Xml Properties from "+this.getPropertiesPath());
				in = new FileInputStream(this.getPropertiesPath());
			} catch (FileNotFoundException e1) {
				//Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e1);
			}
			if(in!=null){
				try {
					this.properties.loadFromXML(in);
				} catch (InvalidPropertiesFormatException e) {
					Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
				} catch (IOException e) {
					Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
				} finally {
					try {
						in.close();
					} catch (IOException e) {
						Ivy.log().error(this.getClass().getSimpleName()+":Error on Loading Xml Properties from "+this.clazz.getName(), e);
					}
				}
			}
		}
		return properties;
	}
	public Boolean hasProperties(){
		return this.getProperties().size()>0;
	}
	public void setProperties(Properties properties){
		if(this.currentProperties==null){
			this.currentProperties = new Properties();
			}
		this.currentProperties = properties;
	}
	/**
	 * Get a property by key
	 * @param key
	 * @return String, empty String if not in property map
	 */
	public String getProperty(String key){
		String property = "";
		if(this.getProperties().containsKey(key)){
			property = (String) this.getProperties().get(key);
		}
		return property;
	}
	public void setProperty(String key, String value){
		if(this.currentProperties==null){
			this.currentProperties = new Properties();
			}
		this.currentProperties.put(key, value);
	}
	public void save(){
		if(this.getProperties().size()>0){
			for(String key:this.getProperties().stringPropertyNames()){
				String name = this.getPropertyPrefix()+PROPERTY_DELIMINITER+key;
				this.getAllProperties().put(name, this.getProperties().get(key));
			}
		}
		if(this.getAllProperties().size()>0){
			String path = "";
			FileOutputStream out = null;
			try {
				path = this.getPropertiesPath();
				out = new FileOutputStream(path);
				this.getAllProperties().storeToXML(out, this.comment);
				//Ivy.log().info(this.getClass().getSimpleName()+":Saved Xml Properties to "+this.getPropertiesPath());
			} catch (Exception e) {
				Ivy.log().error(this.getClass().getSimpleName()+":Error on Saving Xml Properties of "+this.getResourceName()+" to "+path, e);				
			} finally {
				if(out!=null){
					try {
						out.close();
					} catch (IOException e) {
						Ivy.log().error(this.getClass().getSimpleName()+":Error on Saving Xml Properties of "+this.getResourceName()+" to "+path, e);
					}
				}
			}
		}
	}
	/**
	 * Define the comment to set to the Beginning of properties xml
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		if(this.comment.equals("")){
			this.comment = "Properties - these values are saved programmatically - do not change this.";
		}
		return comment;
	}
	public String toString(){
		StringBuilder text = new StringBuilder("");
		for(String key:this.properties.stringPropertyNames()){
			text.append(key);
			text.append("=");
			text.append(this.properties.getProperty(key));
			text.append("\n");
		}
		return text.toString();
	}
}
