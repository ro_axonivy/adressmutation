package ch.soreco.webbies.common.util;
import java.util.Comparator;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Time;
public class IvyListSort implements Comparator<CompositeObject>{
	private String field = "";
	private Boolean descending = false;
	
	public IvyListSort(String fieldName) {
		field = fieldName;
	}
	public IvyListSort(String fieldName, Boolean sortDescending) {
		field = fieldName;
		descending = sortDescending;
	}
	public int compare(CompositeObject a, CompositeObject b){
		int ret = 0;
		if(field.equals("")){
			ret = doCompare(a.toString(), b.toString());
		}
		else {
			Object aObj = null;
			Object bObj = null;
			try {
				aObj = a.get(field);
				bObj = b.get(field);
				if(aObj instanceof Number&&bObj instanceof Number){
					Number first = (Number) aObj;
					Number second = (Number) bObj;
					ret = doCompare(first, second);
				}
				else if(aObj instanceof DateTime&&bObj instanceof DateTime){
					DateTime first = (DateTime) aObj;
					DateTime second = (DateTime) bObj;
					ret = doCompare(first.toNumber(), second.toNumber());
				}
				else if(aObj instanceof Date&&bObj instanceof Date){
					Date first = (Date) aObj;
					Date second = (Date) bObj;
					ret = doCompare(first.toNumber(), second.toNumber());
				}
				else if(aObj instanceof Time&&bObj instanceof Time){
					Time first = (Time) aObj;
					Time second = (Time) bObj;
					ret = doCompare(first.toNumber(), second.toNumber());
				}
				else {
					ret = doCompare(aObj.toString(), bObj.toString());				
				}
			} catch (NoSuchFieldException e) {
				ret = doCompare(a.toString(), b.toString());
			}
		}		
		return ret;	
	}
	private int doCompare(String a, String b){
		int ret = 0;
		if(descending){
			ret = b.compareTo(a);
		}
		else {
			ret = a.compareTo(b);
		}
		return ret;
	}
	private int doCompare(Number first, Number second){
		int ret = 0;
		Number a = first;
		Number b = second;
		/*
		 * the value 0 if the argument is a string lexicographically equal to this string; 
		 * a value less than 0 if the argument is a string lexicographically greater than this string; 
		 * and a value greater than 0 if the argument is a string lexicographically less than this string. 
		 */
		if(descending){
			a = second;
			b = first;
		}
		if(a instanceof Integer && b instanceof Integer){
			Integer aValue = (Integer) a;
			Integer bValue = (Integer) b;
			ret = aValue.compareTo(bValue);
		}
		else if(a instanceof Long && b instanceof Long){
			Long aValue = (Long) a;
			Long bValue = (Long) b;
			ret = aValue.compareTo(bValue);
		}
		else if(a instanceof Double && b instanceof Double){
			Double aValue = (Double) a;
			Double bValue = (Double) b;
			ret = aValue.compareTo(bValue);
		}
		else if(a instanceof Float && b instanceof Float){
			Float aValue = (Float) a;
			Float bValue = (Float) b;
			ret = aValue.compareTo(bValue);
		}
		else {
			ret = doCompare(a.toString(), b.toString());
		}		
		return ret;
	}
}
