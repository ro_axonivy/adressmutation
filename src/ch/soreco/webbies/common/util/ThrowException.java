package ch.soreco.webbies.common.util;
/**
 * Ivy Script Steps do not allow to throw new Exceptions
 * Use methods of this class to test exception handling.
 */
public class ThrowException {
	/**
	 * Throw a new Unsupported Operation Exception
	 * @param message
	 */
	public static void unsupported(String message){
		throw new UnsupportedOperationException(message);
	}
}
