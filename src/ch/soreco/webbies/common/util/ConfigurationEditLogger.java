package ch.soreco.webbies.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.jetty.util.ajax.JSON;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.soreco.orderbook.bo.MailBoxConfiguration;
import ch.soreco.orderbook.bo.ScanShare;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ConfigurationEditLogger {
	private static final String DELIMINITER = ";";
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ConfigurationEditLogger.class);
	private static final ObjectMapper mapper = new ObjectMapper();
	static {
		// Don't throw an exception when json has extra fields you are
		// not serializing on. This is useful when you want to use a pojo
		// for deserialization and only care about a portion of the json
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		// Ignore null values when writing json.
		mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
		mapper.setSerializationInclusion(Include.NON_NULL);

		// Write times as a String instead of a Long so its human readable.
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

	public static String toJson(Object value) {
		if (value != null) {
			try {
				return mapper.writeValueAsString(value);
			} catch (JsonProcessingException e) {
				Ivy.log().error("Cannot write value as string", e);
				return JSON.toString(value);
			}
		}
		return null;
	}

	private static String toCleanJson(CompositeObject object) {
		String json = "undefined";
		if (object != null) {
			if (object instanceof MailBoxConfiguration) {
				MailBoxConfiguration obj = (MailBoxConfiguration) object.deepClone();
				if (obj.getPassword() != null) {
					obj.setPassword(String.valueOf(obj.getPassword().hashCode()));
				}
				json = toJson(obj);
			} else if (object instanceof ScanShare) {
				ScanShare obj = (ScanShare) object.deepClone();
				if (obj.getNetPathPWD() != null) {
					obj.setNetPathPWD(String.valueOf(obj.getNetPathPWD().hashCode()));
				}
				json = toJson(obj);
			} else {
				json = toJson(object);
			}
		}
		return json;
	}

	public static void logConfigurationEdit(Class<?> clazz, CompositeObject oldObject, CompositeObject newObject) {
		String clazzName = clazz.getSimpleName();
		String oldJson = toCleanJson(oldObject);
		String newJson = toCleanJson(newObject);
		writeLogMessage(createLogMessage(clazzName, oldJson, newJson));
	}

	private static String createLogMessage(String clazzName, String oldObject, String newObject) {
		StringBuilder msg = new StringBuilder("");
		String timestamp = SDF.format(new Date());
		msg.append(timestamp);
		msg.append(DELIMINITER);
		msg.append(Ivy.session().getSessionUserName());
		msg.append(DELIMINITER);
		msg.append(clazzName);
		msg.append(DELIMINITER);
		msg.append("{");
		msg.append("\"oldValue\":");
		if (oldObject != null) {
			msg.append(oldObject);
		} else {
			msg.append("null");
		}
		msg.append(", \"newValue\":");
		if (newObject != null) {
			msg.append(newObject);
		} else {
			msg.append("null");
		}
		msg.append("}");

		return msg.toString();
	}

	private static void writeLogMessage(String message) {
		logger.info(message);
		Ivy.log().debug(message);
	}

}
