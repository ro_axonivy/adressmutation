package ch.soreco.webbies.common.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Duration;

import ch.ivyteam.ivy.scripting.objects.Record;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.ivyteam.ivy.scripting.objects.Time;
import ch.soreco.webbies.common.db.PersistenceUtils;

public class CompositeUtils {
	public static final String FIELD_DELIMINITER = ".";
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	public static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
	public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static Boolean isComposite(Field field){
		Boolean isComposite = false;
		if(field!=null){
			if(CompositeObject.class.isAssignableFrom(field.getType())){
				isComposite = true;
			}			
		}
		return isComposite;
	}
	
	public static Boolean isCompositeClass(Class<?> clazz){
		return CompositeObject.class.isAssignableFrom(clazz);
	}
	public static Object getFieldValue(CompositeObject composite, String name){
		Object value=null;
		
		if(!name.equals("")&&composite!=null){
			try {
				value = composite.get(name);
			} catch (NoSuchFieldException e) {
				Ivy.log().info(CompositeUtils.class.getSimpleName()+" Exception",e);
			}
		}
		return value;	
	}
	public static String getValue(Object object){
		String value = "";
	    if (object == null){

	    }
	    else
	    {
	    	// if (propertyType.isPrimitive() || attribute instanceof String) {
	    	if (object.getClass().isPrimitive() || object instanceof String)
	    	{
	    		value = object.toString();
	    	}
	    	else
	    	{
	    		if (object instanceof Date)
	    		{
	    			value = DATE_FORMAT.format(((Date) object).toDate());
	    		}
	    		else if (object instanceof Time)
	    		{
	    			value = TIME_FORMAT.format(((Time) object).toDate());
	    		}
	    		else if (object instanceof DateTime)
	    		{
	    			value = DATE_TIME_FORMAT.format(((DateTime) object).toDate());
	    		}
	    		else if (object instanceof Duration)
	    		{
	    			value = Long.valueOf(((Duration) object).toNumber()).toString();
	    		}
	    		else if (object instanceof Number)
	    		{
	    			value = object.toString();
	    		}
	    		else if (object instanceof Boolean)
	    		{
	    			value = object.toString();	
	    		}
	    		else {
	    			//ivy may auto initialized the value!
	    			//value = object.toString();
	    		}
	    	}
	    }			
	    return value;
	}
	@SuppressWarnings("unchecked")
	public static CompositeObject mapObjectTo(CompositeObject composite, 
											Field field, 
											Object object) throws InstantiationException, IllegalAccessException, SecurityException, NoSuchFieldException{
		Class<?> clazz = composite.getClass();
		if(CompositeObject.class.isAssignableFrom(field.getType())){
			if(object instanceof CompositeObject){
				//Ivy.log().info("Mapped "+field.getName()+" with "+object.toString());
				composite.set(field.getName(),object);
			}
			else if(object instanceof HashMap){
				java.util.HashMap<String, Object> parameterMap = (java.util.HashMap<String, Object>) object;
				CompositeObject attribute = (CompositeObject) field.getType().newInstance();
				Boolean hasValue = false;
				for(String key:parameterMap.keySet()){
					//Ivy.log().info("Try to Map "+key+" to "+attribute.getClass().getName());
					Field mapField = attribute.getClass().getDeclaredField(key);
					Object parameters = parameterMap.get(key);
					String[] values = (String[]) parameters;
					String value = "";
					if(values.length>0){
						value = values[0];
						if(!value.trim().equals("")){
							hasValue = true;
							attribute = CompositeUtils.mapObjectTo(attribute, mapField, value);							
						}
					}
					//Ivy.log().info(field.getName()+".("+key+") values to:"+value);						
				}
				if(hasValue){
					composite.set(field.getName(),attribute);									
				}
			}
			else {				
				throw new ClassCastException(CompositeUtils.class.getSimpleName()+" Can't map "+field.getName()+":Error on Method mapObjectTo with Class:"+clazz.getName()+"."+field.getName()+" from object:"+object.toString()+" to type "+field.getType());
			}
		}
		else {
			String value = CompositeUtils.getValue(object);
			//Ivy.log().info(CompositeUtils.class.getSimpleName()+" try to cast value:"+value+" to "+field.getType().toString());
			
			if(value!=null
				&&!value.equals("")){
				try {
					if(field.getType().equals(String.class)){
						composite.set(field.getName(), value);
					}
					//Boolean representation
					else if(field.getType().equals(Boolean.class)){
						Boolean aBool = Boolean.parseBoolean(value);
						composite.set(field.getName(), aBool);
					}
					//Number representations
					else if(field.getType().equals(Integer.class)){
						Double aDouble = Double.valueOf(value);
						Integer aInteger = aDouble.intValue();
						composite.set(field.getName(), aInteger);							
					}
					else if(field.getType().equals(Long.class)){
						Double aDouble = Double.valueOf(value);
						Long aLong = aDouble.longValue();
						composite.set(field.getName(), aLong);
					}
					else if(field.getType().equals(Short.class)){
						Double aDouble = Double.valueOf(value);
						Short aShort = aDouble.shortValue();
						composite.set(field.getName(), aShort);
					}
					else if(field.getType().equals(Float.class)){
						Float aFloat = Float.parseFloat(value);
						composite.set(field.getName(), aFloat);
					}
					else if(field.getType().equals(Double.class)){
						Double aDouble = Double.parseDouble(value);
						composite.set(field.getName(), aDouble);
					}
					else if(field.getType().equals(Byte.class)){
						Byte aByte = Byte.parseByte(value);
						composite.set(field.getName(), aByte);
					}
					else if(field.getType().equals(BigInteger.class)){
						BigInteger aBigInteger = new BigInteger(value);
						composite.set(field.getName(), aBigInteger);
					}
					else if(field.getType().equals(BigDecimal.class)){
						BigDecimal aBigDecimal = new BigDecimal(value);
						composite.set(field.getName(), aBigDecimal);
					}
					//Date representations
					else if(field.getType().equals(Date.class)){
						Date aDate = new Date(value);
						composite.set(field.getName(), aDate);
					}
					else if(field.getType().equals(java.util.Date.class)){
						java.util.Date aDate = new Date(value).toDate();
						composite.set(field.getName(), aDate);
					}
					else if(field.getType().equals(Time.class)){
						Time aTime = new Time(value);
						composite.set(field.getName(), aTime);
					}
					else if(field.getType().equals(DateTime.class)){
						DateTime aDateTime = new DateTime(value);
						composite.set(field.getName(), aDateTime);
					}
					else if(field.getType().equals(Duration.class)){
						Duration aDuration = new Duration(value);
						composite.set(field.getName(), aDuration);
					}
					else {
						composite.set(field.getName(), field.getType().cast(value));
						Ivy.log().info(CompositeUtils.class.getSimpleName()+" Implicit Cast on mapRecordTo Class:"+clazz.getName()+"."+field.getName()+" of type "+field.getType()+" with "+value);
					}
				} catch (NoSuchFieldException e) {
					Ivy.log().info(CompositeUtils.class.getSimpleName()+" Error on mapRecordTo Class:"+clazz.getName()+"."+field.getName()+" of type "+field.getType()+" with "+value, e);
				} catch(ClassCastException e){
					Ivy.log().error(CompositeUtils.class.getSimpleName()+" Error on mapRecordTo Class:"+clazz.getName()+"."+field.getName()+" of type "+field.getType()+" with "+value, e);
					throw e;
				}					
			}					
		}		
		return composite;
	}
	public static List<CompositeObject> recordsetToList(Recordset rs, Class<?> clazz) throws SecurityException, InstantiationException, IllegalAccessException, ParseException, NoSuchFieldException, EnvironmentNotAvailableException, PersistencyException{
		
		List<CompositeObject> list = new ArrayList<CompositeObject>();
		
		//Ivy.log().info(CompositeUtils.class.getSimpleName()+" Begin RecordMapping for "+clazz.getName());

		for(int i=0;i<rs.size(); i++){
			//Ivy.log().info(CompositeUtils.class.getSimpleName()+" RecordMapping for "+clazz.getName()+" at index "+i+" record:\n"+rs.getAt(i).toString());
			CompositeObject element = CompositeUtils.mapRecordTo(rs.getAt(i), clazz);
			list.add(element);
		}
		return list;
	}
	public static CompositeObject mapRecordTo(Record rec, Class<?> clazz) throws InstantiationException, IllegalAccessException, ParseException, SecurityException, NoSuchFieldException, EnvironmentNotAvailableException, PersistencyException{
		CompositeObject composite = (CompositeObject) clazz.newInstance();
		
		for(Field field:clazz.getDeclaredFields()){
			if(!field.getName().equals("serialVersionUID")){
				//Ivy.log().info(CompositeUtils.class.getSimpleName()+" try RecordMapping for \n"+field.getName()+" to "+field.getType());
				if(rec.getKeys().contains(field.getName())){
					Object cell = rec.getField(field.getName());
					composite = CompositeUtils.mapObjectTo(composite, field, cell);					
				}
				else {
					//as the field is a composite may get the whole composite by Persistence find
					if(CompositeObject.class.isAssignableFrom(field.getType())){						
						if(PersistenceUtils.isEntityClass(field.getType())){
							String idFieldName = PersistenceUtils.getIdFieldName(field.getType());
							String rsFieldName = field.getName()+FIELD_DELIMINITER+idFieldName;
							int rsFieldNameIndex = rec.getKeys().indexOf(rsFieldName);
							if(rsFieldNameIndex>-1){
								//Field entityField = field.getType().getDeclaredField(idFieldName);
								String unit = PersistenceUtils.getPersistenceUnitNameOf(field.getType());
								if(!unit.equals("")){
									//get the Id Field Value from Recordset by getField fails as 
									//by example if the recordset contains fieldname "id" and another field named "parent.id"
									//it is not really predictable which one is get.
									//Object idFieldValue = rec.getField(rsFieldName);
									Object idFieldValue = rec.getValues().get(rsFieldNameIndex);
									
									if(idFieldValue!=null&&!idFieldValue.toString().equals("")){
										//Ivy.log().info(CompositeUtils.class.getSimpleName()+" find id field value for "+rsFieldName+" having value:"+idFieldValue.toString()+"\nin Record:"+rec.toString());
										//find object with the id field
										Field idField = field.getType().getDeclaredField(idFieldName);

										//a direct cat to the type may fails
										//Object id = idField.getType().cast(idFieldValue);
										CompositeObject entity = (CompositeObject) field.getType().newInstance();
										entity = CompositeUtils.mapObjectTo(entity, idField, idFieldValue);
										entity = (CompositeObject) Ivy.persistence().get(unit).refresh(entity);
										//and set to parent composite
										composite = CompositeUtils.mapObjectTo(composite, field, entity);									
									}
								}								
							}
						}
					}
				}
			}
		}
		Ivy.log().info(CompositeUtils.class.getSimpleName()+" RecordMapping:\n"+rec.toString()+" \nmapped to:\n"+composite.toString());
		return composite;
	}
	public static CompositeObject escapeHtml(CompositeObject object){
		Field[] fields = object.getClass().getDeclaredFields();
		for(int i=0;i<fields.length;i++){
			String fieldName = fields[i].getName();
			String fieldType = fields[i].getType().getName();
			if(fieldType.equals("java.lang.String")){
				try {
					String value = (String) object.get(fieldName);
					value = StringEscapeUtils.escapeHtml(value);
					object.set(fieldName, value);				
				} catch (NoSuchFieldException e) {
					Ivy.log().debug("Could not Escape "+fieldName, e);
				}
			}
		}
		return object;
	}
	public static CompositeObject unescapeHtml(CompositeObject object){
		Field[] fields = object.getClass().getDeclaredFields();
		for(int i=0;i<fields.length;i++){
			String fieldName = fields[i].getName();
			String fieldType = fields[i].getType().getName();
			if(fieldType.equals("java.lang.String")){
				try {
					String value = (String) object.get(fieldName);
					value = StringEscapeUtils.unescapeHtml(value);
					object.set(fieldName, value);				
				} catch (NoSuchFieldException e) {
					Ivy.log().debug("Could not Escape "+fieldName, e);
				}
			}
		}
		return object;
	}
}
