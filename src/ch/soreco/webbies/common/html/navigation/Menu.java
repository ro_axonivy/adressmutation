/**
 * Create a HTML Menu using CMS structure
 */
package ch.soreco.webbies.common.html.navigation;


import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.htmldialog.IHtmlDialogContext;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.Tree;
import ch.ivyteam.ivy.page.engine.jsp.IvyJSP;

@SuppressWarnings("restriction")
public class Menu {
	private static String navigationRootUri = "/Navigation";
	private static String currentPageId = "";
	private static IContentObject currentPage = null;
	private static String currentPageName = "";
	private static MenuItem currentMenuItem = null;
	private static String ulExpandedClass = "expanded";
	private static String liActiveClass = "active";
	private static String menuClass ="menu";
	private static IHtmlDialogContext currentHtmlContext = null;
	private static IvyJSP ivyJSP = null;
	public static String getNavigationRootUri() {
		return navigationRootUri;
	}
	public static String setNavigationRootUri(String uri) {
		navigationRootUri = uri;
		
		return navigationRootUri;
	}
	public static String setCurrentPageId(String pageId) throws EnvironmentNotAvailableException, PersistencyException{
		currentPageId = pageId;
		if(!currentPageId.equals("")){
			currentPage = Ivy.cms().getContentObject(currentPageId);
			currentPageName = currentPage.getName().toLowerCase();
		}
		return pageId;
	}
	public static IHtmlDialogContext setHtmlDialogContext(IHtmlDialogContext htmlContext){
		return currentHtmlContext = htmlContext;
	}
	public static IContentObject getCurrentPage(){
		return currentPage;
	}
	public static Tree getMenu() throws EnvironmentNotAvailableException, PersistencyException{
		Tree menu = new Tree();
		
		IContentObject navigationRoot = Ivy.cms().getContentObject(navigationRootUri);

		if(navigationRoot != null) {
			menu = setMenuNodes(navigationRoot);
		}
	return menu;
	}
	public static String getMenuHtml(String pageId,IHtmlDialogContext htmlContext) throws EnvironmentNotAvailableException, PersistencyException{
		setCurrentPageId(pageId);
		setHtmlDialogContext(htmlContext);
		
		return getMenuHtml();	
	}
	public static String getMenuHtml(ch.ivyteam.ivy.page.engine.jsp.IvyJSP ivy) throws EnvironmentNotAvailableException, PersistencyException{
		ivyJSP = ivy;
		setCurrentPageId(ivy.html().getCurrentContentPageUri());
		setHtmlDialogContext(ivy.html());
		return getMenuHtml();
	}
	
	private static String getMenuHtml() throws EnvironmentNotAvailableException, PersistencyException{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("<!-- Menu List -->\n");

		Tree menu = getMenu();
		stringBuffer.append("<div id=\"navigation\">\n");
		if(menu.getChildCount()>0){
			for(Tree child:menu.getChildren()){
				String html = getMenuItemHTML(child, "h3");
				stringBuffer.append(html);
			}
		}
		stringBuffer.append("</div>\n");
		//stringBuffer.append("<div id=\"currentMemuItem\"><a>"+currentMenuItem.path+"</a>\n");
		stringBuffer.append("<!-- /Menu List -->\n");

		return stringBuffer.toString();
	}
	private static String getMenuItemHTML(Tree node, String itemTag) {
		String html = "";
		String htmlAttributes = "";
		String childTag = "li";
		String urlType = "";
		Boolean isExpanded = false;
		Boolean isActive = false;
		Boolean wrapChilds = itemTag.toLowerCase().equals("li");
		MenuItem menuItem = (MenuItem) node.getValue();

		if(currentMenuItem!=null){
			if(currentMenuItem.equals(menuItem)){
				isActive = true;
			}
			if(currentMenuItem.path.indexOf(menuItem.name)>-1){
				isExpanded = true;
			}
		}
		//get Links
		if(menuItem.coref.trim().length()>0){
			try {
				menuItem.url = currentHtmlContext.coref(menuItem.coref);
				urlType = "coref";
			}
			catch (Exception e){
				menuItem.url = "#";
			}
		}
		if(menuItem.wr.trim().length()>0){
			try {
				menuItem.url = currentHtmlContext.wr(menuItem.wr);
				urlType = "wr";
			}
			catch (Exception e){
				menuItem.url = "#";
			}
		}
		if(menuItem.url.toLowerCase().endsWith(".ivp")){
			try {
				menuItem.url = currentHtmlContext.ref(menuItem.url);
				urlType = "ref";
			}
			catch (Exception e){
				menuItem.url = "#";
			}
		}
		if(menuItem.startref.toLowerCase().endsWith(".ivp")){
			try {
				menuItem.url = currentHtmlContext.startref(menuItem.startref);
				urlType = "startref";
			}
			catch (Exception e){
				menuItem.url = "#";
			}
		}
		if(menuItem.url.trim().equals("")){
			menuItem.url = "#";
			urlType = "empty";
		}
		
		//get MenuItem as HTML List Item

		htmlAttributes = "id=\""+menuItem.name+"\""
						+" urlType=\""+urlType+"\""
						+" class=\""+(isActive ? liActiveClass : "")
							+" "+(isExpanded ? ulExpandedClass : "")
							+(menuItem.hasChildren ? "hasChildNodes" : "noChildNodes")
							+"\""
						+" title=\""+menuItem.toolTip+"\"";
		html = "<"+itemTag+" "+htmlAttributes+">\n"
				+"<a id=\""+menuItem.name+"Link\" "
					+"href=\""+menuItem.url+"\">"+menuItem.text+"</a>\n";			
		
		if(!wrapChilds){
			html = html+"</"+itemTag+">\n";
		}
		
		html = html+"<div class=\""+(menuItem.hasChildren ? menuClass : "")+"\">\n";												
		if(menuItem.hasChildren){
			html = html+"<ul class=\""+(isExpanded ? ulExpandedClass : "")+"\">\n";
			//get List of children			
			for(Tree child:node.getChildren()){
				try {
					html = html+getMenuItemHTML(child, childTag);
					}
				catch (Exception e){
					html = html+"<li class=\"error\"><a>"+e.getMessage()+"</a></li>";
				}
			}
			html = html+"</ul>\n";
		}
		html = html+"</div>\n";
		
		if(wrapChilds){
			html = html+"</"+itemTag+">\n";				
		}
		return html;
	}
	private static Tree setMenuNodes(IContentObject co) throws PersistencyException{
		Tree node = new Tree();

		MenuItem menuItem = new MenuItem(co, ivyJSP);
		if(menuItem.isAccessible){
			node.setValue(menuItem);
			node.setInfo(co.getName());
			
			if(!currentPageName.equals("")&&menuItem.name.toLowerCase().equals(currentPageName)){
				currentMenuItem = menuItem;
			}		
			
			if(menuItem.hasChildren) {
				for(IContentObject coChild : co.getChildren()){
					if(coChild.getContentObjectType().getCoType()==CoType.FOLDER) {
						Tree childs = setMenuNodes(coChild);
						//Ivy.log().debug(childs.getInfo());
						if(!childs.getInfo().equals("")){
							node.add(childs);							
						}
					}
				}
			}
			node.setMayHaveChildren(menuItem.hasChildren);			
		}
		return node;
	}
}
