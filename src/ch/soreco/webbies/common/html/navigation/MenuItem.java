package ch.soreco.webbies.common.html.navigation;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.page.engine.jsp.IvyJSP;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.security.IRole;

@SuppressWarnings("restriction")
public class MenuItem {
	public String name = "";
	public String path = "";
	public String text = "";
	public String description = "";
	public String security = "";

	public String ref="";		//local reference
	public String startref="";	//startreference
	public String url="";		//weblink
	public String coref="";		//content link
	public String wr="";		//WebContent link
	
	public String toolTip="";
	public Boolean hasChildren = false;
	public Boolean isAccessible = true;
	public IContentObject navigationContentObject;
	private IvyJSP ivy=null;
	
	public MenuItem(IContentObject navigationNodeContent, IvyJSP ivyJSP) throws PersistencyException{
		name = navigationNodeContent.getName();
		text = name;
		ivy = ivyJSP;
		navigationContentObject = navigationNodeContent;
		path = navigationNodeContent.getUri();
		java.util.Locale locale = Ivy.session().getContentLocale();

		for(IContentObject property:navigationNodeContent.getChildren()){
			if(property.getName().equals("text")){
				text = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("description")){
				description = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("security")){
				security = property.getValue(new java.util.Date(),locale).getContentAsString();
				security = getCleanCode(security);
				if(!security.trim().equals("")){
					this.isAccessible = this.evaluateSecurity(this.security);
				}
			}
			if(property.getName().equals("startref")){
				startref = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("ref")){
				ref = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("url")){
				url = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("wr")){
				wr = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("coref")){
				coref = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
			if(property.getName().equals("toolTip")){
				toolTip = property.getValue(new java.util.Date(),locale).getContentAsString();
			}
		}
		
		if(navigationNodeContent.hasChildren()) {
			for(IContentObject coChild : navigationNodeContent.getChildren()){
				if(coChild.getContentObjectType().getCoType()==CoType.FOLDER) {
					hasChildren = true;
					break;
					}
				}
			}

	}
	private Boolean evaluateSecurity(String security){
		Boolean isValid = true;
		/**
		 * TODO: Evaluation of "security" string should include also user names 
		 * and an evaluation of attributes like "evaluate(in.order.orderType==ORDER.name(), boolean)"
		*/
		if(security!=null&&!security.equals("")){
			if(security.equals("false")){
				isValid=false;
			} if(security.equals("true")){
				isValid=true;
			}else {
				String[] roles = security.split(",");
				for(int i=0;i<roles.length;i++){
					String roleName = roles[i];
					try {
						IRole role = ivy.wf().getSecurityContext().findRole(roleName);
						if(role!=null){
							isValid = ivy.session().hasRole(role, false);									
						}
					} catch (EnvironmentNotAvailableException e) {
						isValid = false;
					} catch (PersistencyException e) {
						isValid = false;
					}
				}				
			}
		}
		return isValid;
	}
	private String getCleanCode(String code)
	{
		//Cleanse java comments like /* text */ or //
		code = code.replaceAll("(//).+", "");
		code = code.replaceAll("(\\n)", "");
		code = code.replaceAll("(/\\*+).+(\\*/)", "");
		return code;
	}
}
