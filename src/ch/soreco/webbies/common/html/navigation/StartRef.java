package ch.soreco.webbies.common.html.navigation;

import java.util.HashMap;
import java.util.List;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.workflow.IProcessStart;
import ch.ivyteam.util.NetworkUtil;
public class StartRef {
	private static List<IProcessStart> starts = null;
	private static HashMap<String, String> startrefs = null;
	public static List<IProcessStart> getStartableProcessStarts() {
		if(starts==null){
			try {
				starts = Ivy.session().getStartableProcessStarts();
				Ivy.session();
			} catch (Exception e) {
				Ivy.log().debug("Could not get Startable Process Starts", e);
			}
		}
		return starts;
	}
	public static String getProcessStartRefByName(String name){
		String ref = "";
		try {
			if(startrefs==null&&getStartableProcessStarts()!=null){
				startrefs = new HashMap<String, String>();
				for(IProcessStart start:starts){
					
					startrefs.put(start.getName(), start.getFullRequestPath());
					Ivy.log().debug("Startable Process Start Name:\n"
							+start.getName()
							+"\nLocalHostName"+NetworkUtil.getLocalHostName()
							+"\nStart getRequestPath:"+start.getRequestPath()
							//+"\nRequest getRequestPath:"+Ivy.html().
							+"\ngetFullRequestPath:"+start.getFullRequestPath()
							+"\ngetFullUserFriendlyRequestPath:"+start.getFullUserFriendlyRequestPath()
							+"\ngetFullUserFriendlyRequestPath:"+start.getRequestPath()
							);
				}
			}
			if(startrefs.containsKey(name)){
				ref = startrefs.get(name);
				Ivy.log().debug("Found Startable Process Start for "+name+" "+ref);
			}
			
		} catch (Exception e) {
			Ivy.log().debug("Could not load process starts", e);
		} 
		return ref;
	}
}
