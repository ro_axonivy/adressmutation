package ch.soreco.webbies.common.html.form;

import java.util.ArrayList;

import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.process.data.persistence.IIvyEntityManager;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.GroupRole;
import ch.soreco.orderbook.util.Authority;

public class RuleHandler {
	//private static RuleHandler rules = new RuleHandler(); //Singleton
	private ArrayList<String> forEdit = new ArrayList<String>();
	private ArrayList<String> forCheck = new ArrayList<String>();
	private ArrayList<String> forDispatch = new ArrayList<String>();
	
	public RuleHandler (){
		//rulesCreator(); 
	//}
	
	//private RuleHandler rulesCreator () {
		//if (rules == null){
		
			forEdit.add(OrderState.Default.toString());//Dispatched
			forEdit.add(OrderState.Dispatched.toString());//Dispatched
			forEdit.add(OrderState.Recording.toString());//Erfassung
			forEdit.add(OrderState.InPendencyRecording.toString());//Pendenzbearbeitung
			forEdit.add(OrderState.Pendant.toString());//Pendent

		
			forCheck.add(OrderState.InCheck.toString());//Teilpr�fung
			forCheck.add(OrderState.InIdCheck.toString());//Id Pr�fung
			forCheck.add(OrderState.In4eCheck.toString());//Endpr�fung
			forCheck.add(OrderState.InAbortCheck.toString());//Abbruchpr�fung
			forCheck.add(OrderState.InPendencyCheck.toString());//pendenzpr�fung
			forCheck.add(OrderState.InPendencyRecordingCheck.toString());//pendenzpr�fung
			forCheck.add(OrderState.InDispatchingAbortCheck.toString());// Dispatching Abbruchpr�fung
			
			forDispatch.add(OrderState.InDispatchingNew.toString());// Dispatching Neu
			forDispatch.add(OrderState.InDispatchingOpen.toString());// Dispatching Openispatch
		
		
		//	return this;
		//}
		//else {
		//	return rules;
		//}
	}
	
	static public Boolean validateEdit(String state, String editor) {
		RuleHandler rules = new RuleHandler();
		
		if (rules.forEdit.indexOf(state)>-1 || rules.forCheck.indexOf(state) > -1) {
			
			//if (validateUser(editor,1)) {
				return true;
			//}

		}
		return false;
	}
	
	static public Boolean validateEdit(String state, String editor, String type, IIvyEntityManager persistenceUnit) {
		

		// everyone can edit, even if the user of the order is different
		// mail from J. Chung 04.08.2011
		//if (RuleHandler.validateEdit(state, editor)) {
			try {
				// changerequest "Leserecht": only permit user to edit file if he is authorized, or the order was assigned to him
				// J. Chung 12.10.2011
				if (RuleHandler.validateAuthority(type) || editor.equals(Ivy.session().getSessionUserName())){
					//Ivy.log().debug("Type: "+type+", State: "+state+", Editor / ivyUser: "+ editor+"/"+Ivy.session().getSessionUserName());
					return true;
				}
			} catch (EnvironmentNotAvailableException e) {
				e.printStackTrace();
			} catch (PersistencyException e) {
				e.printStackTrace();
			}
		//}
		return false;
	}
	
	/**
	 * new check method for because of changerequest "Leseberechtigung":
	 * new param "checker" => user can edit the order if he is directly assigned as checker
	 * @param state
	 * @param editor
	 * @param type
	 * @param persistenceUnit
	 * @return
	 */
	static public Boolean validateEdit(String state, String editor, String checker, String type, IIvyEntityManager persistenceUnit) {
		// changerequest "Leserecht": only permit user to edit file if he is authorized, or the order was assigned to him
		// J. Chung 12.10.2011	
		try {
				if (RuleHandler.validateAuthority(type) || editor.equals(Ivy.session().getSessionUserName()) || checker.equals(Ivy.session().getSessionUserName())){
				
				return true;
			}
		} catch (EnvironmentNotAvailableException e) {
			e.printStackTrace();
		} catch (PersistencyException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	static public Boolean validateCheck(String state, String editor){
		RuleHandler rules = new RuleHandler();
		
		if (rules.forCheck.indexOf(state)>-1) {
			if (validateUser(editor,2)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * new check method for because of changerequest "Leseberechtigung":
	 * new param "checker" => user can check the order if it's directly assigned
	 * @param state
	 * @param editor
	 * @param type
	 * @param persistenceUnit
	 * @return
	 */
	static public Boolean validateCheck(String state, String editor, String checker, String type, IIvyEntityManager persistenceUnit) {
		
		if (RuleHandler.validateCheck(state, editor)) {
			try {
				if (RuleHandler.validateAuthority(type) || checker.equals(Ivy.session().getSessionUserName())){
					return true;
				}
			} catch (EnvironmentNotAvailableException e) {
				Ivy.log().error("validateCheck failed as environment is not available", e);
			} catch (PersistencyException e) {
				Ivy.log().error("validateCheck failed as persistency is not available", e);
			}
		}
		return false;
	}
	
	
	static public Boolean validateCheck(String state, String editor, String type, IIvyEntityManager persistenceUnit) {
		
		if (RuleHandler.validateCheck(state, editor)) {
			try {
				if (RuleHandler.validateAuthority(type) || editor.equals(Ivy.session().getSessionUserName())){
					return true;
				}
			} catch (EnvironmentNotAvailableException e) {
				Ivy.log().error("validateCheck failed as environment is not available", e);
			} catch (PersistencyException e) {
				Ivy.log().error("validateCheck failed as persistency is not available", e);
			}
		}
		return false;
	}
	
	static public Boolean validateDispatch(String state, String editor){
		RuleHandler rules = new RuleHandler();
		
		if (rules.forDispatch.indexOf(state)>-1) {
			//if (validateUser(editor,1)) {
				return true;
			//}
		}
		return false;
	}
	
	static public Boolean validateDispatch(String state, String editor, String type, IIvyEntityManager persistenceUnit) {
		
		if (RuleHandler.validateDispatch(state, editor)) {
			if (Authority.getInstance().typeAllowed(type, GroupRole.Dispatch)){
				return true;
			}
		}
		return false;
	}
	static public Boolean validateTeamDispatch(String state, String editor, String type, IIvyEntityManager persistenceUnit) {
		
		if (RuleHandler.validateDispatch(state, editor) ) {
			try {
				if (Authority.getInstance().typeAllowed(type, GroupRole.TeamDisp) || editor.equals(Ivy.session().getSessionUserName())){
					return true;
				}
			} catch (EnvironmentNotAvailableException e) {
				Ivy.log().error("validateCheck failed as environment is not available", e);
			} catch (PersistencyException e) {
				Ivy.log().error("validateCheck failed as persistency is not available", e);
			}
		}
		return false;
	}
	
	
	/*
	 *  @param editor Name of the current editor
	 *  @param type 1: Edit / 2: Check
	 */
	static public Boolean validateUser(String editor, int type){
		String user;
		//get current user
		try {
			user = Ivy.session().getSessionUserName();
		} catch (Exception e) {
			return false;
		}
		//security code for check
		if (type == 2) {	
			if (!user.equals(editor)) {
				return true;
			}
		}
		//security code for edit
		else {
			if (user.equals(editor)||editor.equals("")) {
				return true;
			}
		}
		return false;
	}
	
	static public Boolean validateUser(String editor) {
		return validateUser(editor, 1);
	}
	
	/**
	 * Edit allowed for this type
	 * @return allowed
	 */
	static public Boolean validateAuthority(String type){
		Authority auth = Authority.getInstance();
		return auth.typeAllowed(type, GroupRole.Write);
	}
	
	static public Boolean validateRead(String type){
		Authority auth = Authority.getInstance();
		return auth.typeAllowed(type, GroupRole.Read);
	}
	
	
}
