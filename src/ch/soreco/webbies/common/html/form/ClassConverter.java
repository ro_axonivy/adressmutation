package ch.soreco.webbies.common.html.form;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Duration;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Time;
import ch.soreco.orderbook.ui.Order;

public class ClassConverter { 
	private static String FIELDUID = "serialVersionUID";	
	private static LinkedHashMap<String, InputType> inputTypes = new LinkedHashMap<String, InputType>();
	private static LinkedHashMap<String, String> optionsClassNamesMap = new LinkedHashMap<String, String>();
	private static LinkedHashMap<String, LinkedHashMap<String,String>> optionsHashMap =  new LinkedHashMap<String, LinkedHashMap<String,String>>();
	private static LinkedHashMap<String, String> valueHashMap =  new LinkedHashMap<String, String>();

	
	private static LinkedHashMap<String, java.util.List<String>> adjacentHashMap = new LinkedHashMap<String, java.util.List<String>>();
	private static LinkedHashMap<String, java.util.List<String>> adjacentToOptionHashMap = new LinkedHashMap<String, java.util.List<String>>();
	private static LinkedHashMap<String, java.util.List<String>> nextToOptionHashMap = new LinkedHashMap<String, java.util.List<String>>();	
	private static java.util.List<String> adjacentedFieldNames = new ArrayList<String>();
	
	private static LinkedHashMap<String, String> resetOnDependantIsEmptyHashMap = new LinkedHashMap<String, String>();
	private static java.util.List<String> hiddenOnEmptyFieldNames = new ArrayList<String>();
	
	private static java.util.List<String> disabledFieldNames = new ArrayList<String>();
	private static java.util.List<String> enabledFieldNames = new ArrayList<String>();
	private static java.util.List<String> enabledFieldNamesByInputName = new ArrayList<String>();
	
	private static LinkedHashMap<String, Integer> indexHashMap =  new LinkedHashMap<String, Integer>();
	

	public static void clear(){	
		inputTypes.clear();
		optionsHashMap.clear();
		optionsClassNamesMap.clear();
		valueHashMap.clear();
		indexHashMap.clear();

		
		disabledFieldNames.clear();
		enabledFieldNames.clear();
		enabledFieldNamesByInputName.clear();
		adjacentHashMap.clear();
		adjacentedFieldNames.clear();
		
		adjacentToOptionHashMap.clear();
		nextToOptionHashMap.clear();
		
		resetOnDependantIsEmptyHashMap.clear();
		hiddenOnEmptyFieldNames.clear();

	}
	/* set object types */
	public static void putInputType(InputType type, String fieldName){
		putInputType(type, fieldName, false);
	}
	public static void putInputType(InputType type, String fieldName, Boolean overwrite){
		putInputType(type, fieldName, null, false);
	}
	public static void putInputType(InputType type, List<String> fieldNames){
		for(String fieldName:fieldNames){
			putInputType(type, fieldName, false);		
		}
	}
	public static void putInputType(InputType type, List<String> fieldNames, List<?> values){
		for(String fieldName:fieldNames){
			putInputType(type, fieldName, values, false);		
		}		
	}
	public static void putInputType(InputType type, String fieldName, List<?> values, Boolean overwrite){
		if(!inputTypes.containsKey(fieldName)||overwrite){
			inputTypes.put(fieldName, type);
			switch (type) {
			case RADIOS_RL:
				putOptionsMap(fieldName,values);
				break;
			case RADIOS_TD:
				putOptionsMap(fieldName,values);
				break;
			case SELECT:
				putOptionsMap(fieldName, values);
				break;
			default:
				inputTypes.put(fieldName, type);
				break;
			}
		}		
	}
	private static void putOptionsMap(String fieldName, List<?> options){
		LinkedHashMap<String,String> selectLabelledOptions = new LinkedHashMap<String,String>();
		for(int i=0;i<options.size();i++){
			String label = "";String icon = "";
			String key = options.get(i).toString();
			String className = options.get(i).getClass().getName();
			AttributeContent attributeContent = new AttributeContent(className, key);
			label = attributeContent.getAttributeContentString(AttributeContentType.NAME);
			if(label.equals("")){
				if(!attributeContent.attributeContentExists(AttributeContentType.NAME)){
					label = attributeContent.setAttributeContent(AttributeContentType.NAME, key);					
				}
			}
			icon = attributeContent.getAttributeContentMakro(AttributeContentType.ICON);
			if(!icon.equals("")){
				label = icon+ "&nbsp;"+label;
			}
			valueHashMap.put(key, label);
			optionsClassNamesMap.put(fieldName, className);
			selectLabelledOptions.put(options.get(i).toString(), label);
		}
		optionsHashMap.put(fieldName, selectLabelledOptions);
			
	}	
	/* Logical Methods */
	public static void putDisabledFields(String fieldName){
		if(disabledFieldNames.indexOf(fieldName)==-1){
			disabledFieldNames.add(fieldName);
		}
	}
	public static void putEnabledFields(String fieldName){
		if(disabledFieldNames.indexOf(fieldName)==-1){
			enabledFieldNames.add(fieldName);
		}	
	}
	public static void putEnabledFieldsByInputName(String inputName){
		if(enabledFieldNamesByInputName.indexOf(inputName)==-1){
			enabledFieldNamesByInputName.add(inputName);
		}	
	}
	public static void putHideenOnEmptyFields(String fieldName){
		if(hiddenOnEmptyFieldNames.indexOf(fieldName)==-1){
			hiddenOnEmptyFieldNames.add(fieldName);
		}
	}
	public static void putDependantObject(String fieldName, String dependant){
		resetOnDependantIsEmptyHashMap.put(fieldName, dependant);
	}
	
	public static void putAdjacentMap(String fieldName, java.util.List<String> adjacents){
		adjacentHashMap.put(fieldName, adjacents);
		for(String adjacentFieldName:adjacents){
			if(adjacentedFieldNames.indexOf(adjacentFieldName)==-1){
				adjacentedFieldNames.add(adjacentFieldName);
			}
		}
		
	}
	public static void putNextToOptionMap(String optionKey, java.util.List<String> adjacents){
		nextToOptionHashMap.put(optionKey.toLowerCase(), adjacents);
		//Ivy.log().debug("Adjacenting to Option "+optionKey+" "+adjacents);
		for(String adjacentFieldName:adjacents){
			if(adjacentedFieldNames.indexOf(adjacentFieldName)==-1){
				adjacentedFieldNames.add(adjacentFieldName);
			}
		}
	}
	public static void putAdjacentToOptionMap(String optionKey, java.util.List<String> adjacents){
		adjacentToOptionHashMap.put(optionKey.toLowerCase(), adjacents);
		//Ivy.log().debug("Adjacenting to Option "+optionKey+" "+adjacents);
		for(String adjacentFieldName:adjacents){
			if(adjacentedFieldNames.indexOf(adjacentFieldName)==-1){
				adjacentedFieldNames.add(adjacentFieldName);
			}
		}
	}
	public static String getClassHTML(
			Order orderUI,
			CompositeObject classToConvert, 
			String inputNamePrefix,  
			Boolean canModify, 
			Boolean asSingleLine) throws EnvironmentNotAvailableException, PersistencyException{
		ClassConverter.clear();
		ClassConverterUtil.setEnablers(orderUI);
		return getClassHTML(classToConvert,inputNamePrefix, canModify, asSingleLine);
	}
	public static String getClassHTML(
			CompositeObject classToConvert, 
			String inputNamePrefix,  
			Boolean canModify, 
			Boolean asSingleLine) throws EnvironmentNotAvailableException, PersistencyException{
		ClassConverterUtil.setUI();
		StringBuffer html = new StringBuffer("");
		String classSimpleName = classToConvert.getClass().getSimpleName();
		
		//Loop given Filter Class
		Integer index = getIndex(inputNamePrefix.replace(Attribute.SEPARATORCSL, Attribute.SEPARATORID));
		if(index>-1){
			if(indexHashMap.containsKey(classToConvert.getClass().getName())){
				if(index>indexHashMap.get(classToConvert.getClass().getName())){
					indexHashMap.put(classToConvert.getClass().getName(), index);					
				}
			}
		}
		html.append("<div id='"+classSimpleName+(index>-1? index:"")+"' class=\""+classSimpleName+"\" "+(index>-1? "rowIndex=\""+index+"\"":"")+">\n");
		html.append("<table class=\"AttributeTable\" width=\"100%\" cellpadding=2 cellspacing=0 border=1><tbody>\n");
		if(asSingleLine){
			html.append("<tr id=\""+inputNamePrefix.replace(Attribute.SEPARATORCSL, Attribute.SEPARATORID)+Attribute.SEPARATORID+"Row\">\n");			
		}

		//TODO: Adjacenting should be implemented by attribute Infos...
		
		for(Field field:classToConvert.getClass().getDeclaredFields()){
			StringBuilder row = new StringBuilder("");
			String fieldName = field.getName();
			if(!fieldName.equals(FIELDUID)){
				try {
					Boolean doRow = true;
					InputType type = InputType.UNDEFINED;
					if(inputTypes.containsKey(fieldName)){
						type = inputTypes.get(fieldName);
					}
					if (adjacentedFieldNames.indexOf(fieldName)>-1
							||type.equals(InputType.SUPPRESSED)
							||type.equals(InputType.HIDDEN)
							||classToConvert.get(fieldName) instanceof Iterable<?>) {
						doRow = false;
					}
					if(doRow){
						row.append(new Attribute(classToConvert,field, asSingleLine, inputNamePrefix, canModify).cells);						
					}
				}
				catch (Exception e){
					Ivy.log().debug("Error on Class Converter with field "+fieldName+": "+e.getMessage(),e);
				}				
			}
			html.append(row);
			}
		if(asSingleLine){
			html.append("</tr>\n");			
		}		
		html.append("</tbody>\n</table>\n");
		StringBuilder hidden = new StringBuilder("");
		for(Field field:classToConvert.getClass().getDeclaredFields()){
			String fieldName = field.getName();
			InputType type = InputType.UNDEFINED;
			if(inputTypes.containsKey(fieldName)){
				type = inputTypes.get(fieldName);
			}
			try {
				if(type.toString().equals(InputType.HIDDEN.toString())){
					hidden.append(new Attribute(classToConvert,field, true, inputNamePrefix, canModify).cells);					
				}
			} catch (Exception e) {
				Ivy.log().debug("Error on Class Converter with field "+fieldName+": "+e.getMessage(),e);
			} 									
		}
		if(hidden.length()>0){	
			html.append(hidden);	
		}
		html.append("</div><!-- End "+classSimpleName+"-->\n");		
		
		return html.toString();
	}
	private static Integer getIndex(String text){
		Integer index = new Integer(-1);
		String items[] = text.split(Attribute.SEPARATORID);
		if(items.length>1){
			for(int i=0;i<items.length;i++){
				try{
					//Ivy.log().debug("Parsing for Integer "+items[i]);
					index = Integer.parseInt(items[i].trim());					
					//Ivy.log().debug("Found index "+index+" for "+text);
					break;
				} catch (Exception e){
					//no Index found
				}
			}				
		}
		return index;
	}
	public static class Attribute {
		public static String SEPARATORCSL = "$";
		public static String SEPARATORID = "At";
		public Field field = null;
		public AttributeContent attributeContent = null;

		public CompositeObject parentObject = null;
		public Object object = null;
		public Boolean isObject = false;
		
		public Object dependantObject = null;
		public String dependantValue = "";
		public String dependantField ="";
		
		public String type = "";
		public String value = "";
		public String valueHtml = "";
		public String fieldName = "";
		
		public String inputId = "";
		public String inputName ="";	
		public String inputNamePrefix ="";
		public Integer index = new Integer(-1);
		public InputType inputType = InputType.UNDEFINED;
		public String inputSelectEnum = "";
		public String inputMeta="";
		public String inputCellAttributes="";
		public String inputHtml = "";
		
		public String labelHtml = "";
		public String label = "";
		public String labelText = "";
		public String labelAttributes="";
		public String labelCellAttributes="";
		public String icon = "";
		
		public String cells = "";
		public String source = "";
		public Boolean hasAdjacents = false;
		public Boolean isAdjacent = false;
		public Boolean isDisabled = false;
		public Boolean fieldsCanBeModified = true;
		public Integer gridx=1;
		public Integer gridy=1;
		

		//public Boolean isIterable = false;
		public Attribute(
				CompositeObject composite,
				Field field, 
				Boolean asSingleLine,
				String inputPrefix, 
				Boolean canModify){
			this.field = field;
			this.parentObject = composite;
			this.fieldsCanBeModified = canModify;
			this.fieldName = field.getName();
			this.inputNamePrefix = inputPrefix;

			this.attributeContent = new AttributeContent(field);
			this.inputType = this.attributeContent.getInputTypeProperty();
			
			if(resetOnDependantIsEmptyHashMap.containsKey(this.fieldName)){
				this.dependantField = resetOnDependantIsEmptyHashMap.get(this.fieldName);
			}
			
			if(composite!=null){
				try {
					//get Field Object an if any dependant object
					this.object = composite.get(this.fieldName);
					if(!this.dependantField.equals("")){
						this.dependantObject = composite.get(this.dependantField);
						if(this.dependantObject!=null){
							this.dependantValue = getValue(this.dependantObject,true);
						}
					}
					
					//Get Value and reset if dependant is null
					if(this.object!=null){
						this.isObject = true;											
						this.value = getValue(this.object,true);
						if(this.dependantField.length()>0&&this.dependantValue.equals("")){
							this.value = "";
						}
					}
				}
				catch (Exception e){
					//Ivy.log().debug(e);
				}
			}
			
			if(!this.fieldName.equals(FIELDUID)){
				this.type = field.getType().getSimpleName();				
				this.inputName= this.inputNamePrefix+SEPARATORCSL+this.fieldName;
				this.inputId = this.inputName.toLowerCase().replace(SEPARATORCSL, SEPARATORID);
				this.index = getIndex(this.inputId);				

				this.isAdjacent = adjacentedFieldNames.indexOf(this.fieldName)>-1;
				if(canModify){
					this.isDisabled = disabledFieldNames.indexOf(this.fieldName)>-1;						
				}
				else {
					this.isDisabled = true;
					if(enabledFieldNames.indexOf(this.fieldName)>-1){
						this.isDisabled = false;
					}
					if (enabledFieldNamesByInputName.indexOf(this.inputName)>-1){
						this.isDisabled = false;						
					}
				}
				
				this.valueHtml = "<%=ivy.html.get(\"in."+this.inputName+"\")%>";
				
				//get or create needed cms objects (name, icon, source)
				this.label = this.attributeContent.getAttributeContentMakro(AttributeContentType.NAME);
				this.labelText =this.attributeContent.getAttributeContentString(AttributeContentType.NAME);

				if(this.label.equals("")){
					//Attribute Content ->Name was not found
					//add needed attribute content objects to CMS
					this.label = this.attributeContent.setAttributeContent(AttributeContentType.NAME, this.fieldName);
				}
				else {
					this.icon = this.attributeContent.getAttributeContentMakro(AttributeContentType.ICON);			
					this.source = this.getAttributeSource();
				}
				
				this.labelHtml = "<label id=\""+this.inputId+SEPARATORID+"Label\" " 
								+(this.labelAttributes.trim().length()>0? this.labelAttributes:"")+" >"
								+(this.icon.trim().length()>0? this.icon+ "&nbsp;" :"")
								+this.label+"</label>";
				
				try {
					getGridProperties();
				} catch (Exception e){
					//Ivy.log().debug(e);
				}
				
				//evaluate InputType by given HashMapping
				this.inputType = getAttributeInputType();
				
				if(hiddenOnEmptyFieldNames.indexOf(this.fieldName)>-1
						&&this.value.equals("")){
					this.inputType = InputType.HIDDEN;
				}
				
				//TODO: Develop all HTML form inputs objects				
				this.inputHtml = getInputObjectHtml(this.inputType);					
				
				if(!this.inputType.equals(InputType.SOURCED)
						&&!this.source.equals("")){
					if(this.inputType.equals(InputType.CHECKBOX)){
						this.labelHtml = this.labelHtml+ this.source;
					}
					else {
						this.inputHtml = this.inputHtml+this.source;
					}
				}

				StringBuilder html = new StringBuilder("");
				String labelFor = this.inputId+SEPARATORID+"Input";
				if(this.inputType.equals(InputType.CHECKBOX)){
					labelFor = this.inputId+SEPARATORID+"InputCheckbox";
				}
				
				String labelAttributes = "valign=\"top\" "
										+"for=\""+labelFor+"\" "
										+(this.labelCellAttributes.trim().length()>0? " "+this.labelCellAttributes+" ":" ")
										+"style=\"white-space: nowrap;"
										+(this.inputType.toString().equals(InputType.CHECKBOX.toString())? "padding-top: 5px;":"")
										+(this.inputName.equals("")? "background-color:red;":"")+"\" ";
				labelAttributes = this.setAttributeParametersTo(this, labelAttributes);
				String inputAttributes = "valign=\"top\" "
										+(this.inputCellAttributes.trim().length()>0? " "+this.inputCellAttributes+" ":" ");
				inputAttributes = this.setAttributeParametersTo(this, inputAttributes);	
				switch (this.inputType){
				case SUPPRESSED:
					break;
				case HIDDEN:
					//html.append(wrap("td", inputAttributes, this.inputHtml));
					html.append(this.inputHtml);
					break;
				case CHECKBOX:
					html.append(wrap("td", inputAttributes, this.inputHtml));
					if(!this.labelText.trim().equals("")){
						html.append(wrap("td", labelAttributes,this.labelHtml));							
					}
					String adjacentHtmlCheck = getAdjacents();
					adjacentHtmlCheck = this.setAttributeParametersTo(this, adjacentHtmlCheck);
					html.append(adjacentHtmlCheck);
					//wrap with row if current attribute fields
					//should be displayed top - down
					if(!asSingleLine){
						html.insert(0, "<tr id=\""+this.inputId+SEPARATORID+"Row\" >\n");
						html.append("</tr>\n");						
						}
					break;
				case RADIOS_RL:
					html.append(this.inputHtml);
					String adjacentHtmlRadiosRL = getAdjacents();
					adjacentHtmlCheck = this.setAttributeParametersTo(this, adjacentHtmlRadiosRL);
					html.append(adjacentHtmlRadiosRL);
					if(!asSingleLine){
						html.insert(0, "<tr id=\""+this.inputId+SEPARATORID+"Row\" >\n");
						html.append("</tr>\n");						
						}
					break;
				case RADIOS_TD:
					html.append(this.inputHtml);
					String adjacentHtmlRadiosTD = getAdjacents();
					adjacentHtmlCheck = this.setAttributeParametersTo(this, adjacentHtmlRadiosTD);
					html.append(adjacentHtmlRadiosTD);					
					break;
				default:
					if(!this.labelText.trim().equals("")){
						html.append(wrap("td", labelAttributes,this.labelHtml));							
					}
					html.append(wrap("td", inputAttributes, this.inputHtml));
					html.append(getAdjacents());
					//wrap with row if current attribute fields
					//should be displayed top - down
					if(!asSingleLine){
						html.insert(0, "<tr id=\""+this.inputId+SEPARATORID+"Row\" >\n");
						html.append("</tr>\n");						
						}
					break;
				}
				html.insert(0, "<!--Begin "+this.fieldName+" -->");
				html.append("<!--End "+this.fieldName+" -->");
				this.cells = html.toString()+"\n";

			}

		}
		private Integer getIndex(String text){
			Integer index = new Integer(-1);
			String items[] = text.split(Attribute.SEPARATORID);
			if(items.length>1){
				for(int i=0;i<items.length;i++){
					try{
						//Ivy.log().debug("Parsing for Integer "+items[i]);
						index = Integer.parseInt(items[i].trim());					
						//Ivy.log().debug("Found index "+index+" for "+text);
						break;
					} catch (Exception e){
						//no Index found
					}
				}				
			}
			return index;
		}
		private String getAdjacents(){
			StringBuilder html = new StringBuilder("");
			if(adjacentHashMap.containsKey(this.fieldName)) {
				java.util.List<String> adjacentedFieldNames = adjacentHashMap.get(this.fieldName);
				for(String adjacentFieldName:adjacentedFieldNames){
					InputType type = InputType.UNDEFINED;
					if(inputTypes.containsKey(this.fieldName)){
						type = inputTypes.get(this.fieldName);
					}
					if(!type.toString().equals(InputType.HIDDEN.toString())){
						try {
							Field adjacentField = this.parentObject.getClass().getDeclaredField(adjacentFieldName);
							Attribute adjacentAttribute = new Attribute(this.parentObject,adjacentField, true, this.inputNamePrefix, this.fieldsCanBeModified);
							this.hasAdjacents = true;
							String adjacentHtml = adjacentAttribute.cells;
							
							html.append(adjacentHtml);															
						} catch (Exception e){
							//Ivy.log().error(e);
						}
					}
				}
			}
			return html.toString();
		}
		private void getGridProperties() throws EnvironmentNotAvailableException, PersistencyException{
			if(this.attributeContent.properties.size()>0){
				
				if(this.attributeContent.properties.containsKey(InputProperties.InputMeta.toString())){
					this.inputMeta = (String) this.attributeContent.properties.get(InputProperties.InputMeta.toString());					
				}
				if(this.attributeContent.properties.containsKey(InputProperties.InputCellAttributes.toString())){
					this.inputCellAttributes = (String) this.attributeContent.properties.get(InputProperties.InputCellAttributes.toString());					
				}
				if(this.attributeContent.properties.containsKey(InputProperties.LabelAttributes.toString())){
					this.labelAttributes = (String) this.attributeContent.properties.get(InputProperties.LabelAttributes.toString());
				}
				if(this.attributeContent.properties.containsKey(InputProperties.LabelCellAttributes.toString())){
					this.labelCellAttributes = (String) this.attributeContent.properties.get(InputProperties.LabelCellAttributes.toString());
				}
				if(this.attributeContent.properties.containsKey(InputProperties.SelectEnum.toString())){
					this.inputSelectEnum = (String) this.attributeContent.properties.get(InputProperties.SelectEnum.toString());
				}				
				
			}			
		}

		private String getAttributeSource(Attribute attribute){
			String html = "";
			html = attribute.attributeContent.getAttributeContentString(AttributeContentType.SOURCE);
			return setAttributeParametersTo(attribute, html);
		}
		private String setAttributeParametersTo(Attribute attribute, String html){
			if(!html.equals("")){
				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String,String>();
				//valueMap.put("#cells", this.cells);
				valueMap.put("#contentUri", attribute.attributeContent.contentUri);
				valueMap.put("#fieldName", attribute.fieldName);
				valueMap.put("#icon", attribute.icon);
				//valueMap.put("#inputHtml", this.inputHtml);
				valueMap.put("#inputName", attribute.inputName);
				String inputObjectPath = "in."+attribute.inputName.replaceAll("\\$", "\\.");
				valueMap.put("#inputObject", inputObjectPath);
				valueMap.put("#inputId", attribute.inputId);
				//valueMap.put("#label", this.label);
				//valueMap.put("#labelHtml", this.labelHtml);
				valueMap.put("#labelText", attribute.labelText);
				valueMap.put("#type", attribute.type);
				valueMap.put("#value", attribute.value);
				//valueMap.put("#valueHtml", this.valueHtml);
				valueMap.put("#labelAttributes", attribute.labelAttributes);
				valueMap.put("#inputMeta", attribute.inputMeta);
				valueMap.put("#inputType", attribute.inputType.toString());	
				valueMap.put("#inputNamePrefix", attribute.inputNamePrefix);
				valueMap.put("#index", attribute.index.toString());
				valueMap.put("#isDisabled", attribute.isDisabled.toString());
				
				for(String key:valueMap.keySet()){
					String attributeValue = valueMap.get(key);
					html = html.replace(key, attributeValue);					
				}
			}
			return html;			
		}
		private String getAttributeSource(){
			return getAttributeSource(this);
		}
		private String getAttributeContentSource(AttributeContent attributeContent){
			String html = "";
			html = attributeContent.getAttributeContentString(AttributeContentType.SOURCE);
			if(!html.equals("")){
				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String,String>();
				//valueMap.put("#cells", this.cells);
				valueMap.put("#contentUri", attributeContent.contentUri);
				valueMap.put("#fieldName", attributeContent.fieldName);
				valueMap.put("#icon", attributeContent.getAttributeContentMakro(AttributeContentType.ICON));
				//valueMap.put("#inputHtml", this.inputHtml);
				valueMap.put("#inputName", this.inputName);
				
				valueMap.put("#inputId", this.inputId);
				valueMap.put("#isDisabled", this.isDisabled.toString());
				//valueMap.put("#label", this.label);
				//valueMap.put("#labelHtml", this.labelHtml);
				valueMap.put("#labelText", attributeContent.getAttributeContentString(AttributeContentType.NAME));
				valueMap.put("#type", attributeContent.getInputTypeProperty().toString());
				valueMap.put("#value", attributeContent.fieldName);
				//valueMap.put("#valueHtml", this.valueHtml);
				if(attributeContent.properties.size()>0){
					if(attributeContent.properties.containsKey(InputProperties.LabelAttributes)){
						valueMap.put("#labelAttributes", attributeContent.properties.getProperty(InputProperties.LabelAttributes.toString()));						
					}
					if(attributeContent.properties.containsKey(InputProperties.InputMeta)){
						valueMap.put("#inputMeta", attributeContent.properties.getProperty(InputProperties.InputMeta.toString()));						
					}
				}
				valueMap.put("#inputType", this.inputType.toString());	
				valueMap.put("#inputNamePrefix", this.inputNamePrefix);
				valueMap.put("#index", this.index.toString());
				
				for(String key:valueMap.keySet()){
					String attributeValue = valueMap.get(key);
					html = html.replace(key, attributeValue);					
				}
			}			
			return html;
		}
		private InputType getAttributeInputType(){			
			//TODO: Cover all inputTypes
			if(inputTypes.containsKey(this.fieldName)) return inputTypes.get(this.fieldName);
			if(!this.inputType.toString().trim().equals("")&&!this.inputType.equals(InputType.UNDEFINED)) return this.inputType;
			if(field.getType().getSimpleName().equals("Boolean")) return InputType.CHECKBOX;
			return InputType.TEXTINPUT;
		}
		private String getInputObjectHtml(InputType inputType){
			//get Input Field html code
			String html = "";
			switch(inputType) {
			case CHECKBOX: html = getCheckboxInputHtml();break;
			case HIDDEN: html = getHiddenInputHtml();break;
			case TEXTINPUT: html = getTextInputHtml();break;
			case LABEL: html = getLabelInputHtml();break;
			case TEXTAREA: html = getTextAreaHtml();break;
			case SELECT: html = getSelectHtml();break;
			case RADIOS_RL: html = getRadioGroup();break;
			case RADIOS_TD: html = getRadioGroup();break;
			case SOURCED: html = getSourcedHtml();break;
			default : html = "to be developped";break;
			}
			return html;
		}
		private String getSourcedHtml(){
			String html = "";
			if(!this.source.equals("")){
				html = getAttributeSource();
				if(html.equals("")){
					//if is empty set a default Text Input object
					html = this.getTextInputHtml();
					this.attributeContent.setAttributeContent(AttributeContentType.SOURCE, html);
				}
				this.source = "";
			}
			return html;
		}
		private String getSelectHtml(){
			StringBuilder html =new StringBuilder("");
			html.append("<select ");
			html.append("id=\""+this.inputId+SEPARATORID+"Input\" ");
			html.append("name=\""+this.inputName+"\" ");
			html.append("class=\""+this.type+" "+fieldName+" "+(this.inputMeta.length()>0 ? this.inputMeta : "")+"\" ");
			html.append("value=\""+this.value+"\" ");	
			if(this.isDisabled){
				html.append("disabled=\"disabled\" ");	
			}
			html.append(">");
			//add options
			LinkedHashMap<String,String> optionsMap = null;
			if(optionsHashMap.containsKey(this.fieldName)){
				optionsMap = optionsHashMap.get(this.fieldName);				
			}
			else {
				if(!this.inputSelectEnum.equals("")){
					optionsMap = new LinkedHashMap<String, String>();
					try {
						Class<?> clazz = Class.forName(this.inputSelectEnum);
						
						Field[] fields =clazz.getFields();
						for(int i=0;i<fields.length;i++){
							String key = fields[i].getName();
							String label = "";
							AttributeContent attributeContent = new AttributeContent(this.inputSelectEnum, key);
							label = attributeContent.getAttributeContentString(AttributeContentType.NAME);
							if(label.equals("")){
								if(!attributeContent.attributeContentExists(AttributeContentType.NAME)){
									label = attributeContent.setAttributeContent(AttributeContentType.NAME, key);					
								}
							}
							String icon = attributeContent.getAttributeContentMakro(AttributeContentType.ICON);
							if(!icon.equals("")){
								label = icon+ "&nbsp;"+label;
							}
							optionsMap.put(key, label);							
						}
					} catch (ClassNotFoundException e) {
						optionsMap.put(this.value, this.value + " (Class Not Found)");
					}
				}
				else {
					optionsMap = new LinkedHashMap<String, String>();
					optionsMap.put(this.value, this.value);					
				}
			}
			for(String key: optionsMap.keySet()){
				String label = optionsMap.get(key);
				html.append("<option value=\""+key+"\" "+(this.value.equals(key)? " selected=\"selected\" ":"")+">"+label+"</option>\n");						
			}					
			html.append("</select>\n");
			return html.toString();
			
		}
		private String getTextAreaHtml(){
			StringBuilder html =new StringBuilder("");
			html.append("<textarea ");
			html.append("rows=3 ");
			html.append("cols=60 ");
			html.append("id=\""+this.inputId+SEPARATORID+"Input\" ");
			html.append("name=\""+this.inputName+"\" ");
			html.append("class=\""+this.type+" "+fieldName+" "+(this.inputMeta.length()>0 ? this.inputMeta : "")+"\" ");
			if(this.isDisabled){
				html.append("readonly=\"readonly\" ");				
				//html.append("value=\""+this.value+"\"");
			}
			html.append(">");
			html.append(this.value);
			html.append("</textarea>\n");
			return html.toString();
		}
		private String getLabelInputHtml(){
			StringBuilder html = new StringBuilder("");
			//get Content Value String if applicable by SELECT/RADIO
			String valueString = this.value;
			if(valueHashMap.containsKey(this.value)){
				valueString = valueHashMap.get(this.value);
			}

			html.append("<label ");			
			html.append("id=\""+this.inputId+SEPARATORID+"Labelled\" ");
			html.append("class=\"FormInputLabel\" ");
			html.append(">");
			html.append(valueString);
			html.append("</label>");
			html.append(getHiddenInputHtml());
			
			return html.toString();
		}
		private String getCheckboxInputHtml(){
			StringBuilder html = new StringBuilder("");
			//html.append("<%=ivy.html.checkbox(\"in."+this.inputName.replace(SEPARATORCSL, ".")+"\",\""+this.inputName+"\",\"ID=\\\""+this.inputId+SEPARATORID+"InputCheckbox\\\" \")%>"); 
			//html.append("<INPUT TYPE=\"hidden\" NAME=\""+this.inputName+"\" VALUE=\""+this.value+"\">");
			html.append("<input ");
			html.append("id=\""+this.inputId+SEPARATORID+"InputCheckbox\" ");
			html.append("forInput=\""+this.inputId+SEPARATORID+"Input\" ");
			html.append("class=\""+this.type+" "+fieldName+"\" ");
			html.append("type=\"checkbox\" ");
			html.append("value=\""+this.value+"\" ");
			if(this.isDisabled){
				html.append("disabled=\"disabled\" ");				
			}
			//html.append("onclick=\"this.form."+this.inputName+".value=this.checked\" ");
			html.append("onclick=\"$('#"+this.inputId+SEPARATORID+"Input').attr('value', this.checked);\" ");
			html.append(this.value.equals("true")? "checked=\"checked\" ":"");
			html.append("/>");
			html.append(getHiddenInputHtml(this.value+"", false));
			return html.toString();			
		}
		private String getHiddenInputHtml(){
			return getHiddenInputHtml("");
		}
		private String getHiddenInputHtml(String defaultValue){
			return getHiddenInputHtml(defaultValue, false);
		}
		private String getHiddenInputHtml(String defaultValue, Boolean skipName){
			StringBuilder html = new StringBuilder("");
			
			String thisValue = this.value.trim();
			if(thisValue.equals("")){
				thisValue = defaultValue;
			}
			
			html.append("<input ");
			html.append("id=\""+this.inputId+SEPARATORID+"Input\" ");
			if(!skipName){
				html.append("name=\""+this.inputName+"\" ");				
			}

			html.append("class=\""+this.fieldName+" "
					+"InputType"+this.inputType.toString()+" "
					//+(this.inputType.equals(InputType.RADIOS_RL)||this.inputType.equals(InputType.RADIOS_TD)? "Radios"+this.type+" ":"")
					+(this.inputMeta.length()>0 ? this.inputMeta : "")
					+"\" ");
			html.append("type=\"hidden\" ");
			html.append("value=\""+thisValue+"\" ");
			html.append("/>");
			return html.toString();
		}
		private String getTextInputHtml(){
			StringBuilder html = new StringBuilder("");
			html.append("<input ");
			html.append("type=\"text\" ");
			html.append("id=\""+this.inputId+SEPARATORID+"Input\" ");
			html.append("name=\""+this.inputName+"\" ");
			html.append("class=\""+this.type+" "+this.fieldName+" "+(this.inputMeta.length()>0 ? this.inputMeta : "")+"\" ");
			if(this.isDisabled){
				html.append("disabled=\"disabled\" ");				
			}
			html.append("value=\""+this.value+"\" ");
			html.append("/>");
			return html.toString();
		}
		private String getRadioInput(String radioValue, String radioInputMeta){
			StringBuilder html = new StringBuilder("");
			if(radioValue.equals("TRUE")){
				radioValue = "true";
			}
			if(radioValue.equals("FALSE")){
				radioValue = "false";
			}

			html.append("<input type=\"radio\" ");			
			html.append("id=\""+this.inputId+SEPARATORID+radioValue+SEPARATORID+"Radio\" ");//with javascript onclick a hidden field will be updated
			html.append("forInput=\""+this.inputId+SEPARATORID+"Input\" ");

			html.append("name=\""+this.inputName.replace(SEPARATORCSL, SEPARATORID).toLowerCase()+SEPARATORID+"Radios\" ");
			html.append("class=\""+this.type+" "+fieldName+" "+(radioInputMeta.trim().length()>0? radioInputMeta :"")+"\" ");
			html.append("value=\""+radioValue+"\" ");
			if(this.isDisabled){
				html.append("disabled=\"disabled\" ");				
			}
			//"+this.inputId+SEPARATORID+"Input\"" +
			html.append("onclick=\"$('#"+this.inputId+SEPARATORID+"Input').attr('name','"+this.inputName+"').val(this.value);\" ");
			
			//html.append("onclick=\"this.form."+this.inputName+".value=this.value\" ");
			html.append(radioValue.toLowerCase().equals(this.value.toLowerCase())? "checked=\"checked\" ":"");
			html.append("/>\n");
			return html.toString();
		}
		private String getRadioGroup(){
			StringBuilder optionGroup =new StringBuilder("");
			LinkedHashMap<String,String> optionsMap = null;
			Boolean toLeft = this.inputType.equals(InputType.RADIOS_RL);
			
			if(optionsHashMap.containsKey(this.fieldName)){
				optionsMap = optionsHashMap.get(this.fieldName);				
			}
			else {
				optionsMap = new LinkedHashMap<String, String>();
			}
			
			if(this.isAdjacent){
				toLeft = true;
			}
			String optionClass = "";
			List<String> keyList = new List<String>();
			Field[] fieldsNew = null;

			if(optionsClassNamesMap.containsKey(this.fieldName)){
				optionClass = optionsClassNamesMap.get(fieldName);
				optionClass = optionClass.trim();
				try {
					Class<?> clazz = Class.forName(optionClass);
					AttributeClass attClass = new AttributeClass(clazz);					
					optionsMap = attClass.getLabelsAndIcons();
					fieldsNew = clazz.getFields();
					for (Field keyname: fieldsNew) {
						keyList.add(keyname.getName());
					}
				} catch (ClassNotFoundException e) {
					//do not overwrite optionsMap
				}
			}
			Boolean noLabel = this.labelText.equals("");
			int line = 0;
			for(String key: optionsMap.keySet()){
			//for(String key: keyList){
				Ivy.log().debug("------------------ option group key: " + key);
				line = line+1;
				String radioInputMeta ="";
				String radioInputCellAttributes ="";
				String radioLabelCellAttributes ="";
				
				String radioSource = "";
				
				if(!optionClass.equals("")){
					AttributeContent radioContent = new AttributeContent(optionClass,key);
					radioSource = getAttributeContentSource(radioContent);
					radioSource=this.setAttributeParametersTo(this, radioSource);
					if(radioContent.properties.containsKey(InputProperties.InputMeta.toString())){
						radioInputMeta = radioContent.properties.getProperty(InputProperties.InputMeta.toString());
						radioInputMeta=this.setAttributeParametersTo(this, radioInputMeta);
					}
					if(radioContent.properties.containsKey(InputProperties.InputCellAttributes.toString())){
						radioInputCellAttributes = radioContent.properties.getProperty(InputProperties.InputCellAttributes.toString());	
						radioInputCellAttributes=this.setAttributeParametersTo(this, radioInputCellAttributes);
						//Ivy.log().debug("Radio Input Cell attributes: "+radioInputCellAttributes);
					}
					if(radioContent.properties.containsKey(InputProperties.LabelCellAttributes.toString())){
						radioLabelCellAttributes = radioContent.properties.getProperty(InputProperties.LabelCellAttributes.toString());		
						radioLabelCellAttributes=this.setAttributeParametersTo(this, radioLabelCellAttributes);
					}
				}
				String optionLabel = optionsMap.get(key);				
				String labelFor = this.inputId+SEPARATORID+key+SEPARATORID+"Radio";
				if(this.inputType.equals(InputType.CHECKBOX)){
					labelFor = this.inputId+SEPARATORID+"InputCheckbox";
				}
				
				radioLabelCellAttributes = "valign=\"top\" "
										+"style=\"white-space:nowrap;padding-top:5px;\" "
										+"for=\""+labelFor+"\" "
										+(radioLabelCellAttributes.trim().length()>0? " "+radioLabelCellAttributes+" ":" ");
				
				

				if(!toLeft){
					optionGroup.append("<tr>\n");							
				}
				if(line>1&&!noLabel&&!toLeft){
					//add empty cell to Option Group Label Column
					optionGroup.append("<td>&nbsp;</td>");
				}
				//Option Group Label Column
				if(line==1&&!noLabel){
					optionGroup.append("<td valign=\"top\" style=\"white-space:nowrap;\" "+(this.labelCellAttributes.trim().length()>0? " "+this.labelCellAttributes: " ")+">");
					optionGroup.append(this.labelHtml);
					optionGroup.append("</td>");
				}
				//Input Column
				optionGroup.append("<td valign=\"top\" style=\"white-space:nowrap;\" align=\"right\" "+(radioInputCellAttributes.trim().length()>0? " "+radioInputCellAttributes+" ":" ")+">");
				optionGroup.append(getRadioInput(key, radioInputMeta));
				optionGroup.append(radioSource);
				optionGroup.append("</td>");
				//Label Column
				optionGroup.append("<td "+(radioLabelCellAttributes.trim().length()>0? " "+radioLabelCellAttributes+" ":" ")+">");
				optionGroup.append(optionLabel);
				optionGroup.append("</td>");
				
				String lookupValue = (fieldName+key).toLowerCase();
				//next to Radio Column
				if(nextToOptionHashMap.containsKey(lookupValue)){

					java.util.List<String> adjacents = nextToOptionHashMap.get(lookupValue);
					//Ivy.log().debug("Next to "+fieldName+key+" "+adjacents);
					for(String adjacent:adjacents){
						try {
							Field adjacentField = this.parentObject.getClass().getDeclaredField(adjacent);	
							//do not wrap with tr
							Attribute adjacentAttribute = new Attribute(this.parentObject,adjacentField, true, this.inputNamePrefix, this.fieldsCanBeModified);
							String adjacentHtml = adjacentAttribute.cells;
							if(adjacentHtml.indexOf("<td")==-1){
								adjacentHtml = wrap("td", "", adjacentHtml);
							}
							optionGroup.append(adjacentHtml);						
						}
						catch (Exception e){
							//Ivy.log().error(e);
						}
					}
				}
				if(!toLeft){
					optionGroup.append("</tr>");							
				}
				
				//Adjacent to option -->next Line starts with 1 or 2 empty columns
				//Ivy.log().debug("Look for Adjacenting to "+lookupValue+ " in "+adjacentToOptionHashMap.entrySet().toString());
				if(adjacentToOptionHashMap.containsKey(lookupValue)){
					java.util.List<String> adjacents = adjacentToOptionHashMap.get(lookupValue);
					//Ivy.log().debug("Adjacenting to "+fieldName+key+" "+adjacents);
					//optionGroup.append("<table cellpadding=2 cellspacing=2 border=0 ><tbody>");
					for(String adjacent:adjacents){
						try {
							Field adjacentField = this.parentObject.getClass().getDeclaredField(adjacent);	
							Attribute adjacentAttribute = new Attribute(this.parentObject,adjacentField,true, this.inputNamePrefix, this.fieldsCanBeModified);
							if(!toLeft){
								optionGroup.append("<tr>");
								line = line+1;
								if(line>1&&!noLabel){
									//add empty cell to Option Group Label Column
									optionGroup.append("<td>&nbsp;</td>");
								}
								optionGroup.append("<td>&nbsp;</td>");								
							}
							optionGroup.append(adjacentAttribute.cells);
							if(!toLeft){
								optionGroup.append("</tr>");															
							}
						} catch(Exception e){
							//Ivy.log().error(e);							
						}
					}
				}			
			}
			if(!this.isObject&&this.isDisabled){
				//as the object is disabled and not initialized
				//no need to map the hidden field. CSL Wrapper would initialize this object.
				//Boolean values could be missleaded.
			}
			else {
				Boolean skipName = false;
				if(this.type.equals("Boolean")&&this.value.trim().equals("")){
					skipName = true;
				}
				if(!toLeft){
					optionGroup.append("<tr>");			
					optionGroup.append("<td>"+getHiddenInputHtml(this.value+"", skipName)+"</td>");
					optionGroup.append("</tr>");							
				}
				else {
					optionGroup.append("<td>"+getHiddenInputHtml(this.value+"", skipName)+"</td>");				
				}				

			}
			optionGroup.insert(0, "<!--Begin Option Group "+this.fieldName+" toLeft:"+toLeft+"-->");
			optionGroup.append("<!--End Option Group "+this.fieldName+"-->");
			return optionGroup.toString();
		}
		private static String getValue(Object attribute, boolean detail){
		    String value;
		    value = "";
		    if (attribute == null){
		    	
		    }
		    else
		    {
		    	// if (propertyType.isPrimitive() || attribute instanceof String) {
		    	if (attribute.getClass().isPrimitive() || attribute instanceof String)
		    	{
		    		value = attribute.toString();
		    	}
		    	else
		    	{
		    		if (attribute instanceof Date)
		    		{
		    			DateFormat format = new SimpleDateFormat(detail ? "dd.MM.yyyy" : "yyyyMMdd");
		    			value = format.format(((Date) attribute).toDate());
		    		}
		    		else if (attribute instanceof Time)
		    		{
		    			DateFormat format = new SimpleDateFormat(detail ? "HH:mm:ss" : "HHmmss");
		    			value = format.format(((Time) attribute).toDate());
		    		}
		    		else if (attribute instanceof DateTime)
		    		{
		    			DateFormat format = new SimpleDateFormat(detail ? "dd.MM.yyyy HH:mm:ss" : "yyyyMMdd-HHmmss");
		    			value = format.format(((DateTime) attribute).toDate());
		    		}
		    		else if (attribute instanceof Duration)
		    		{
		    			value = Long.valueOf(((Duration) attribute).toNumber()).toString();
		    		}
		    		else if (attribute instanceof Number)
		    		{
		    			value = attribute.toString();
		    		}
		    		else if (attribute instanceof Boolean)
		    		{
		    			value = attribute.toString();
		    			
		    		} else {
		    			//value = attribute.toString();		    			
		    		}
		    	}
		    }
		    return value;
		}
		private static String wrap(String tag, String attributes, String html){
			html = "<"+tag+" "+attributes+" >"+html+"</"+tag+">\n";
			return html;			
		}
		public String toString(){
			StringBuilder fields = new StringBuilder("");
			fields.append("[");
			for(Field field: this.getClass().getFields()){
				String value = "null";
				if(fields.length()>1){fields.append(", ");}
				fields.append(field.getName());
				fields.append(":");
				try {
					if(Integer.class.isAssignableFrom(field.getType())){
						value = ""+field.getInt(new Integer(0));
					}
					
				} catch (Exception e){
					
				}
				fields.append(value);
			}
			
			fields.append("]");
			return fields.toString();
		}
		
	}

}
