package ch.soreco.webbies.common.html.form;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.soreco.common.enums.BooleanOption;
import ch.soreco.orderbook.bo.BusinessPartner;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.Correspondence;
import ch.soreco.orderbook.bo.CorrespondenceContainer;
import ch.soreco.orderbook.bo.DomicileOrOwner;
import ch.soreco.orderbook.bo.Paper;
import ch.soreco.orderbook.bo.Recording;
import ch.soreco.orderbook.bo.RecordingLSV;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.enums.CorrespondenceType;
import ch.soreco.orderbook.enums.DoubletteTypes;
import ch.soreco.orderbook.enums.OrderIncomeType;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.enums.PaperType;
import ch.soreco.orderbook.enums.PostalOriginalType;
import ch.soreco.orderbook.enums.ReversType;
import ch.soreco.orderbook.enums.SignatureType;
import ch.soreco.orderbook.ui.Order;
import ch.soreco.orderbook.ui.enums.ViewType;

public class ClassConverterUtil {
	//Enum Lists
	private static List<OrderType> orderTypes = List.create(OrderType.class,OrderType.values());
	private static List<OrderState> orderStates = List.create(OrderState.class,OrderState.values());
	private static List<BooleanOption> booleans = List.create(BooleanOption.class, BooleanOption.values());
	private static List<DoubletteTypes> doubletteTypes = List.create(DoubletteTypes.class, DoubletteTypes.values());
	private static List<SignatureType> signatureTypes = List.create(SignatureType.class, SignatureType.values());
	private static List<PostalOriginalType> postalOriginalTypes = List.create(PostalOriginalType.class, PostalOriginalType.values());
	private static List<ReversType> reversTypes = List.create(ReversType.class, ReversType.values());
	private static List<OrderIncomeType> orderIncomeTypes = List.create(OrderIncomeType.class, OrderIncomeType.values());
	private static List<CorrespondenceType> correspondenceTypes = List.create(CorrespondenceType.class, CorrespondenceType.values());
	private static List<PaperType> paperTypes = List.create(PaperType.class, PaperType.values());
	//Attribute Lists
	private static List<String> suppressed =List.create(String.class, new String[]{"papers","correspondenceContainer"});
	private static List<String> hiddenNoOverwrite =List.create(String.class, new String[]{"order"});
	private static List<String> hiddenOverwrite =List.create(String.class, new String[]{"nextDueDate","lastEdit","pendencyTicketType"});
	private static List<String> hiddenOverwriteOnDispatch =List.create(String.class, new String[]{"orderType","orderState","orderStart","orderEnd"});
	//Adjacentings
	private static List<String> orderIncomeTypea1PostalAdjacents = List.create(String.class, new String[]{"postalSignatureType", "postalOriginalType"});
	private static List<String> orderIncomeTypea2FaxOrMailAdjacents = List.create(String.class, new String[]{"faxOrMailReversType"});
	private static List<String> orderIncomeTypea3FaxAdjacents = List.create(String.class, new String[]{"faxSignatureType", "faxReversType"});
	//Siblings
	private static List<String> paperTypeIsStandardSiblings = List.create(String.class, new String[]{"standardPaperType"});
	private static List<String> paperTypeIsIndividualSiblings = List.create(String.class, new String[]{"individualPaperType"});
	private static List<String> correspondenceTypechangeAddressSiblings = List.create(String.class, new String[]{"changeAddressOrderNo", "changeAddressIsChecked"});
	private static List<String> correspondenceTypenewAddressSiblings = List.create(String.class, new String[]{"newAddressOrderNo","newAddressIsChecked"});
	public static void setUI(){
		init();
	}
	/**
	 * Do not use this Method as you'll get some null pointers.
	 * @param in
	 */
	public static void setEnablers(Order in){
		if(in.getEViewType().equals(ViewType.DISPATCH)){
			ClassConverter.putInputType(InputType.SELECT,"orderType",orderTypes, false);
			ClassConverter.putInputType(InputType.SELECT,"orderState",orderStates, false);
		}		
		if(!in.getEViewType().toString().equals(ch.soreco.orderbook.ui.enums.ViewType.CHECK.toString())){
			setCheckFieldsDisabled(in.getOrder().getOrderState());
		} else {
			setCheckFieldsEnabled(in);
		}
		if(in.getOrder().getOrderState().equals(ch.soreco.orderbook.enums.OrderState.InPendencyRecording.toString())){
			setContactsEnabled(in.getOrderContacts());
		}		
	}
	private static void init(){
		//ClassConverter.clear();
		ClassConverter.putInputType(InputType.SUPPRESSED, suppressed);
		ClassConverter.putInputType(InputType.HIDDEN, hiddenNoOverwrite);
		ClassConverter.putHideenOnEmptyFields("matchedToOrder");
		//ClassConverter.putHideenOnEmptyFields("orderQuantityType");
		for(String name:hiddenOverwrite){
			ClassConverter.putInputType(InputType.HIDDEN,name, true);			
		}		
		initOptions();
		initOptionSiblings();
		initOptionAdjacents();
		initAdjacents();	
		for(String name:hiddenOverwriteOnDispatch){
			ClassConverter.putInputType(InputType.HIDDEN,name, true);				
		}
	}
	private static void initOptions(){
		List<String> isChecked = List.create(String.class);
		isChecked.add("addressIsChecked");
		isChecked.add("newAddressIsChecked");
		isChecked.add("changeAddressIsChecked");
		isChecked.add("containerIsChecked");
		isChecked.add("bpIdIsChecked");
		isChecked.add("signatureIsChecked");
		isChecked.add("contactIsChecked");
		isChecked.add("paperIsChecked");
		isChecked.add("domicileOrOwnerIsChecked");
		isChecked.add("LSVIsChecked");
		isChecked.add("affirmationIsChecked");
		isChecked.add("businessPartnerIsChecked");
		isChecked.add("doubletteIsChecked");
		ClassConverter.putInputType(InputType.RADIOS_RL,isChecked,booleans);

		ClassConverter.putInputType(InputType.RADIOS_RL,"doubletteType",doubletteTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_RL,"postalSignatureType",signatureTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_RL,"postalOriginalType",postalOriginalTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_RL,"faxOrMailReversType",reversTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_RL,"faxSignatureType",signatureTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_RL,"faxReversType",reversTypes,false);

		ClassConverter.putInputType(InputType.RADIOS_TD,"orderIncomeType",orderIncomeTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_TD,"paperType",paperTypes,false);
		ClassConverter.putInputType(InputType.RADIOS_TD,"correspondenceType",correspondenceTypes,false);		
	}
	private static void initOptionSiblings(){
		ClassConverter.putNextToOptionMap("paperTypeIsStandard".toLowerCase(),paperTypeIsStandardSiblings);
		ClassConverter.putNextToOptionMap("paperTypeIsIndividual".toLowerCase(),paperTypeIsIndividualSiblings);

		ClassConverter.putNextToOptionMap("correspondenceTypechangeAddress".toLowerCase(),correspondenceTypechangeAddressSiblings);
		ClassConverter.putNextToOptionMap("correspondenceTypenewAddress".toLowerCase(),correspondenceTypenewAddressSiblings);		
	}
	private static void initOptionAdjacents(){
		ClassConverter.putAdjacentToOptionMap("orderIncomeTypea1Postal".toLowerCase(),orderIncomeTypea1PostalAdjacents);
		ClassConverter.putAdjacentToOptionMap("orderIncomeTypea2FaxOrMail".toLowerCase(),orderIncomeTypea2FaxOrMailAdjacents);
		ClassConverter.putAdjacentToOptionMap("orderIncomeTypea3Fax".toLowerCase(),orderIncomeTypea3FaxAdjacents);
	}
	private static void initAdjacents(){
		ClassConverter.putAdjacentMap("orderId", List.create(String.class, new String[]{"scanFileId", "editorUserId", "orderQuantityType"}));
		ClassConverter.putAdjacentMap("scanId",	List.create(String.class, new String[]{"checkerUserId"}));
		ClassConverter.putAdjacentMap("orderType",List.create(String.class, new String[]{"orderState"}));
		ClassConverter.putAdjacentMap("isDouble",List.create(String.class, new String[]{"archiveId", "doubletteIsChecked"}));
		ClassConverter.putAdjacentMap("BPId",List.create(String.class, new String[]{"FAClanId", "bpIdIsChecked"}));
		ClassConverter.putAdjacentMap("isNewAdress",List.create(String.class, new String[]{"newAdressText"}));
		ClassConverter.putAdjacentMap("bpOrderNo",List.create(String.class, new String[]{"businessPartnerIsChecked"}));
		ClassConverter.putAdjacentMap("orderNo",List.create(String.class, new String[]{"affirmationIsChecked"}));
		ClassConverter.putAdjacentMap("postalSignatureType",List.create(String.class, new String[]{"contactIsChecked"}));
		ClassConverter.putAdjacentMap("isPaper",List.create(String.class, new String[]{"paperRequestDate", "paperIsChecked"}));

	}
	private static void setCheckFieldsDisabled(String orderState){
		if(!orderState.equals(OrderState.InIdCheck.toString())){
			ClassConverter.putDisabledFields("bpIdIsChecked");
			}	

		ClassConverter.putDisabledFields("doubletteIsChecked");
		ClassConverter.putDisabledFields("domicileOrOwnerIsChecked");
		ClassConverter.putDisabledFields("containerIsChecked");
		ClassConverter.putDisabledFields("newAddressIsChecked");
		ClassConverter.putDisabledFields("changeAddressIsChecked");
		ClassConverter.putDisabledFields("isChecked");
		ClassConverter.putDisabledFields("contactIsChecked");	
		ClassConverter.putDisabledFields("paperIsChecked");	
		ClassConverter.putDisabledFields("signatureIsChecked");	
		ClassConverter.putDisabledFields("businessPartnerIsChecked");	
		ClassConverter.putDisabledFields("affirmationIsChecked");	
		ClassConverter.putDisabledFields("LSVIsChecked");	
		ClassConverter.putDisabledFields("addressIsChecked");
	}
	private static void setCheckFieldsEnabled(Order in){
		//General
		ClassConverter.putEnabledFields("bpIdIsChecked");	
		
		if(in.getOrder()!=null
				&&in.getOrder().getIsDouble()!=null
				&&in.getOrder().getIsDouble()){
			ClassConverter.putEnabledFields("doubletteIsChecked");	
		}
		else {
			ClassConverter.putDisabledFields("doubletteIsChecked");		
			}
		if(in.getOrderContacts()!=null&&in.getOrderContacts().size()>0){
			//Contact
			for(int i=0;i<in.getOrderContacts().size();i++){
				Contact contact = in.getOrderContacts().get(i);
				if(contact!=null){
					if(contact.getAddress()!=null){
						if(contact.getAddress().getIsNewAdress() != null){
							if(contact.getAddress().getIsNewAdress()){
								String inputName = "orderContacts$"+i+"$address$addressIsChecked";
								ClassConverter.putEnabledFieldsByInputName(inputName);	
							}
						}
					}
					if(	(contact.getPostalSignatureType()!=null&&contact.getPostalSignatureType().length()<2)
							||(contact.getFaxOrMailReversType()!=null&&contact.getFaxOrMailReversType().length()<2)
							||(contact.getFaxReversType()!=null&&contact.getFaxReversType().length()<2)
							||(contact.getFaxSignatureType()!=null&&contact.getFaxSignatureType().length()<2)
							){
								
						ClassConverter.putEnabledFields("contactIsChecked");
					}					
				}
			}			
		}
		if(in.getOrderContactsSignatureRequests()!=null&&in.getOrderContactsSignatureRequests().size()>0){
			for(int i=0;i<in.getOrderContactsSignatureRequests().size();i++){
				SignatureRequest sign = in.getOrderContactsSignatureRequests().get(i);				
				if(sign!=null&&sign.getIsRequested()!=null&&sign.getIsRequested()) {
					String inputName = "orderContactsSignatureRequests$"+i+"$signatureIsChecked";
					ClassConverter.putEnabledFieldsByInputName(inputName);
				}
			}			
		}
		if(in.getOrderContactPapers()!=null&&in.getOrderContactPapers().size()>0){
			for(int i=0;i<in.getOrderContactPapers().size();i++){
				Paper paper = in.getOrderContactPapers().get(i);
				if(paper!=null&&paper.getIsPaper()!=null&&paper.getIsPaper()) {
					String inputName = "orderContactPapers$"+i+"$paperIsChecked";
					ClassConverter.putEnabledFieldsByInputName(inputName);		
				}
			}			
		}

		//Disable OrderNo
		ClassConverter.putDisabledFields("bpOrderNo");
		ClassConverter.putDisabledFields("orderNo");
		ClassConverter.putDisabledFields("changeAddressOrderNo");
		ClassConverter.putDisabledFields("newAddressOrderNo");
		ClassConverter.putDisabledFields("containerOrderNo");
		ClassConverter.putDisabledFields("lsvOrderNo");
		ClassConverter.putDisabledFields("orderNo");
		
		if(in.getOrder()!=null&&in.getOrder().getRecording()!=null){
			Recording recording = in.getOrder().getRecording();
			if(recording.getBusinessPartner()!=null){
				BusinessPartner bp = recording.getBusinessPartner();
				if((bp.getBP()!=null&&bp.getBP())
						||(bp.getDomicile()!=null&&bp.getDomicile())
						||(bp.getEUSDCountry()!=null&&bp.getEUSDCountry())
						||(bp.getEUSDRetention()!=null&&bp.getEUSDRetention())
						||(bp.getTaxResid()!=null&&bp.getTaxResid())
						||(bp.getTaxStatement()!=null&&bp.getTaxStatement())
						||(bp.getTelNr()!=null&&bp.getTelNr())
						) {
					if(bp.getBpOrderNo().trim().length()>0){
						ClassConverter.putEnabledFields("businessPartnerIsChecked");
					}			
				}				
			}
			if(recording.getCorrespondence()!=null){
				Correspondence correspondence = recording.getCorrespondence();
				if(correspondence!=null){
					String correspondenceType = correspondence.getCorrespondenceType();
					if(correspondenceType!=null
						&&correspondence.getChangeAddressOrderNo()!=null
						&&correspondenceType.equals(CorrespondenceType.changeAddress.toString())
						&&correspondence.getChangeAddressOrderNo().trim().length()>0){
							ClassConverter.putEnabledFields("changeAddressIsChecked");	
						}
					if(correspondenceType.equals(CorrespondenceType.newAddress.toString())
						&&correspondence.getNewAddressOrderNo()!=null
						&&correspondence.getNewAddressOrderNo().trim().length()>0){
							ClassConverter.putEnabledFields("newAddressIsChecked");	
						}
					for(int i=0;i<correspondence.getCorrespondenceContainer().size();i++) {
							CorrespondenceContainer cont = in.getRecordingCorrespondenceContainers().get(i);
							if((cont.getChange()!=null&&cont.getChange())
								||(cont.getSpedit()!=null&&cont.getSpedit())
								){
								if(cont.getContainerOrderNo()!=null
									&&cont.getContainerOrderNo().trim().length()>0) {
									String inputName = "recordingCorrespondenceContainers$"+i+"$containerIsChecked";
									ClassConverter.putEnabledFieldsByInputName(inputName);
								}
							}
						}
				}
			}
			if(recording.getAffirmation()!=null
					&&recording.getAffirmation().getIsAffirmed()!=null
					&&recording.getAffirmation().getOrderNo()!=null
					&&recording.getAffirmation().getIsAffirmed()
					&&recording.getAffirmation().getOrderNo().trim().length()>0){
					ClassConverter.putEnabledFields("affirmationIsChecked");		
			}			

		}
		//Erfassung
		if(in.getRecordingDomicileOrOwners()!=null&&in.getRecordingDomicileOrOwners().size()>0){
			for(int i=0;i<in.getRecordingDomicileOrOwners().size();i++) {
				DomicileOrOwner doo = in.getRecordingDomicileOrOwners().get(i);
				if(doo.getIsAddressmutation()!=null
					&&doo.getOrderNo()!=null
					&&doo.getIsAddressmutation()
					&&doo.getOrderNo().trim().length()>0) {
					String inputName = "recordingDomicileOrOwners$"+i+"$domicileOrOwnerIsChecked";
					ClassConverter.putEnabledFieldsByInputName(inputName);
				}
			}			
		}
		if(in.getRecordingLSV()!=null&&in.getRecordingLSV().size()>0){
			for(int i=0;i<in.getRecordingLSV().size();i++) {
				RecordingLSV lsv = in.getRecordingLSV().get(i);
				if(lsv.getIsLSV()!=null
						&&lsv.getLsvOrderNo()!=null
						&&lsv.getIsLSV()
						&&lsv.getLsvOrderNo().trim().length()>0){
					String inputName = "recordingLSV$"+i+"$LSVIsChecked";
					ClassConverter.putEnabledFieldsByInputName(inputName);
				}
			}			
		}
	}
	private static void setContactsEnabled(List<Contact> orderContacts){

		for(int i=0;i<orderContacts.size();i++){
			//Contact contact = orderContacts.get(i);
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$orderIncomeType");		
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$faxOrMailReversType");		
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$faxReversType");		
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$faxSignatureType");		
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$postalSignatureType");	
			ClassConverter.putEnabledFieldsByInputName("orderContacts$"+i+"$postalOriginalType");		
			}
	}
}
