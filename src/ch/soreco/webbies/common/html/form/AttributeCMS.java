package ch.soreco.webbies.common.html.form;

import java.util.ArrayList;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Record;
import ch.ivyteam.ivy.scripting.objects.Recordset;

public class AttributeCMS {
	public static Recordset getCMSRecordset(List<String> rootPaths){
		Recordset rsCMSAll = new Recordset();
		for(String rootPath:rootPaths){
			Recordset rsCMS = getCMSRecordset(rootPath);
			if(rsCMS!=null){
				rsCMSAll.addAll(rsCMS);
			}
		}
		return rsCMSAll;
	}
	public static Recordset getCMSRecordset(String rootPath){
		IContentObject coRoot = null;
		Recordset rsCMS = new Recordset();
		try {
			coRoot = Ivy.cms().getContentObject(rootPath);
			if(coRoot!=null){
				rsCMS.clear();
				rsCMS = exploreCMSContent(coRoot, rsCMS);				
			}
			else {
				Ivy.log().debug("Can not instantiate "+rootPath);
			}
		} catch (Exception e){
			Ivy.log().debug("Error finding "+rootPath, e);
		}	
		return rsCMS;
	}
	public static Recordset getAttributeRecordset(String rootPath){
		IContentObject coRoot = null;
		Recordset rsAttributes = new Recordset();
		try {
			coRoot = Ivy.cms().getContentObject(rootPath);
			if(coRoot!=null){
				rsAttributes.clear();
				rsAttributes = exploreAttributeContent(coRoot, rsAttributes);				
			}
			else {
				Ivy.log().debug("Can not instantiate "+rootPath);
			}
		} catch (Exception e){
			Ivy.log().debug("Error finding "+rootPath, e);
		}	
		return rsAttributes;
	}
	private static Recordset exploreAttributeContent(IContentObject co, Recordset rsAttributes) throws PersistencyException{
		Record record =null;
		try {
			record = getAttributeContentRecord(co);
		} catch (Exception e){
			Ivy.log().debug("Error on get CMS Record", e);
		}
		try {
			rsAttributes = addRecord(record, rsAttributes);
			} catch (Exception e){
				Ivy.log().debug("Error on add CMS Record", e);
			}		

		for(IContentObject child:co.getChildren()){
			try {
				exploreAttributeContent(child, rsAttributes);
			} catch(Exception e){
				Ivy.log().debug("Error on exploring CMS Record", e);
			}
		}	
		return rsAttributes;
	}
	private static Recordset exploreCMSContent(IContentObject co, Recordset rsCMS) throws PersistencyException{
		Record record =null;
		try {
			record = getCMSRecord(co);
		} catch (Exception e){
			Ivy.log().debug("Error on get CMS Record", e);
		}
		try {
			rsCMS = addRecord(record, rsCMS);
			} catch (Exception e){
				Ivy.log().debug("Error on add CMS Record", e);
			}		

		for(IContentObject child:co.getChildren()){
			try {
				exploreCMSContent(child, rsCMS);
			} catch(Exception e){
				Ivy.log().debug("Error on exploring CMS Record", e);
			}
		}	
		return rsCMS;
	}
	private static Recordset addRecord(Record record, Recordset rs){
		if(record!=null){
			for(String key :record.getKeys()){
				if(rs.getKeys().indexOf(key)==-1){
					java.util.List<String> column = new ArrayList<String>();
					//add empty column
					rs.addColumn(key, column);
				}
			}
			rs.add(record);
		}
		return rs;
	}
	private static Record getAttributeContentRecord(IContentObject co){
		Record record = null;
		if(co!=null){
			
			try {
				AttributeContent coAttribute = new AttributeContent(co.getUri());
				if(coAttribute.isAttributeContent){
					record = new Record();
					record.putField("attributeUri", co.getUri());
					record.putField("className", coAttribute.className);
					record.putField("fieldName", coAttribute.fieldName);
					record.putField("childs", coAttribute.attributeContentFolder.getChildren().size());
					
					for(AttributeContentType type:coAttribute.declaredTypes){
						String value =coAttribute.getAttributeContentString(type);
						if(!value.equals("")){
							record.putField(type.toString(), value);													
						}
					}
					for(String key :coAttribute.properties.stringPropertyNames()){
						String value =  coAttribute.properties.get(key).toString();
						if(!value.equals("")){
							record.putField(key, coAttribute.properties.get(key));													
						}
					}
				}
				
			} catch (PersistencyException e) {
				Ivy.log().debug("Error on building Attribute record", e);
			}			
			
		}
		return record;
	}
	private static Record getCMSRecord(IContentObject co){
		Record record = null;
		if(co!=null){
			try {
				if(co.getContentObjectType().getCoType().equals(CoType.SOURCE)
						||co.getContentObjectType().getCoType().equals(CoType.STRING)
						||co.getContentObjectType().getCoType().equals(CoType.HTML)
						||co.getContentObjectType().getCoType().equals(CoType.TEXT)
						){
					record = new Record();
					record.putField("coUri", co.getUri());
					record.putField("coUriJSP", "<%=ivy.cms.co(\""+co.getUri()+"\")%>");				
					record.putField("coName", co.getName());
					record.putField("coType", co.getContentObjectType().getCoType().getName());
					//record.putField("coValue", Ivy.cms().co(co.getUri()));
					if(co.hasValues()){
						IContentObjectValue defaultValue = null;
						for(IContentObjectValue value:co.getValues()){
							if(value.isDefault()){
								defaultValue = value;
							}
						}
						if(defaultValue!=null){
							record.putField("coValue", defaultValue.getContentAsString());														
						}
					}
				}
								
			} catch (PersistencyException e) {
				Ivy.log().debug("Error on building CMS record", e);
			}			
			
		}
		return record;
	}

}
