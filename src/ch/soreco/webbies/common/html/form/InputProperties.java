package ch.soreco.webbies.common.html.form;

public enum InputProperties {
	gridx,
	gridy,
	InputMeta,
	InputCellAttributes,
	LabelAttributes,
	LabelCellAttributes,
	resetIfDependantIsEmpty, 
	hideIfEmpty, 
	InputType, 
	SelectEnum
}
