package ch.soreco.webbies.common.html.form;

public enum AttributeContentType {
		ICON,
		NAME,
		SOURCE
}
