package ch.soreco.webbies.common.html.form;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;

public class AttributeContent implements Comparable<AttributeContent> {
	public Integer ordinal = 0;
	public Boolean isAttributeContent = false;
	public Field field;
	public String fieldName ="";
	public String className = "";
	public java.util.List<AttributeContentType> declaredTypes = new ArrayList<AttributeContentType>();
	public static String CONTENTPREFIX = "0_";
	public static String SEPARATORCMS = "/";
	public static String SEPARATORCLASS = ".";

	public String contentUri="";
	public String user = "";
	public String description ="";
	public IContentObject attributeContentFolder=null;
	public Properties properties = new Properties();
	private HashMap<AttributeContentType, IContentObject> contents = new HashMap<AttributeContentType, IContentObject>(); 
	private void setUser(){
		try {
			this.user = Ivy.session().getSessionUserName();
			//Ivy.session().getActiveEnvironment();
			//Ivy.session().getSessionUser().getDisplayName();
		} catch (Exception e) {
			Ivy.log().debug("Error at Attribute Content - getSessionUserName", e);
		}		
	}
	
	public AttributeContent(String attributeUri){
		this.contentUri = attributeUri.substring(0, attributeUri.lastIndexOf(SEPARATORCMS));
		this.fieldName = attributeUri.substring(attributeUri.lastIndexOf(SEPARATORCMS)+1);		
		this.className = contentUri.substring(1).replace(SEPARATORCMS, SEPARATORCLASS);
		loadAttributeContentByNames(this.className, this.fieldName);
	}
	public AttributeContent(String className, String fieldName){
		loadAttributeContentByNames(className, fieldName);
	}
	public AttributeContent(Field attributeField){
		loadAttributeContentByField(attributeField);
	}
	private void loadAttributeContentByNames(String className, String fieldName){
		this.className = className;
		this.fieldName = fieldName;
		
		try {
			//if clazz exists get the Field
			Class<?> clazz = Class.forName(this.className);
			this.field = clazz.getDeclaredField(this.fieldName);			
			loadAttributeContentByField(this.field);	
		} catch (ClassNotFoundException e) {
			//Ivy.log().debug("Could not load class for "+this.className+" and field "+this.fieldName, e);
			this.isAttributeContent = false;
		}  catch (NoSuchFieldException e) {
			//Ivy.log().debug("Could not load field for "+this.className+" and field "+this.fieldName, e);
			this.isAttributeContent = false;
		}
	}
	private void loadAttributeContentByField(Field attributeField){
		this.field = attributeField;
		this.fieldName = this.field.getName();
		this.className = field.getDeclaringClass().getName();
		this.contentUri = getAttributeContentUri();
		
		setUser();
		this.attributeContentFolder = this.mkdir();
		if(this.attributeContentFolder!=null){
			try {
				this.isAttributeContent = true;
				this.description= this.attributeContentFolder.getDescription();
				this.ordinal = this.attributeContentFolder.getVisualOrder();
			} catch (PersistencyException e) {
				Ivy.log().debug("Error at Attribute Content - getDescription", e);
			}
			loadTypes();
			loadProperties();
		}
	}
	public Boolean isComposite(){
		Boolean isComposite = false;
		if(CompositeObject.class.isAssignableFrom(this.field.getType())){
			isComposite = true;
		}
		return isComposite;
	}
	public void setAttributeVisualOrder(int ordinal) throws PersistencyException{
		this.ordinal = ordinal;
		if(this.attributeContentFolder!=null){
			this.attributeContentFolder.setVisualOrder(ordinal, this.user);			
		}
		this.attributeContentFolder.touch(this.user);
	}
	public String getAttributeContentLabelAndIcon(){
		String label = this.getAttributeContentString(AttributeContentType.NAME);
		if(label.equals("")){
			if(!this.attributeContentExists(AttributeContentType.NAME)){
				label = this.setAttributeContent(AttributeContentType.NAME, this.fieldName);					
			}
		}
		String icon = this.getAttributeContentMakro(AttributeContentType.ICON);
		if(!icon.equals("")){
			label = icon+ "&nbsp;"+label;
		}
		return label;
	}
	public String getAttributeContentUri(String className, String fieldName){
		String uri = SEPARATORCLASS+className+SEPARATORCLASS+fieldName;
		uri = uri.replace(SEPARATORCLASS, SEPARATORCMS);		
		return uri;
	}
	public String getAttributeContentUri(){
		String uri = SEPARATORCLASS+field.getDeclaringClass().getName()+SEPARATORCLASS+field.getName();
		uri = uri.replace(SEPARATORCLASS, SEPARATORCMS);
		return uri;
	}
	public String getAttributeContentLabel(){
		return getAttributeContentString(AttributeContentType.NAME);
	}
	public String getAttributeContentString(AttributeContentType type){
		String content = "";
		String name = CONTENTPREFIX+type.toString().toLowerCase();
		String contentTypeUri = this.contentUri+SEPARATORCMS+name;
		if(this.contents.containsKey(type)){
			switch (type){
			case ICON: 
				content = getAttributeContentMakro(type);
				break;
			default:
				content = Ivy.cms().co(contentTypeUri);
				break;
			}			
		}
		
		return content;
	}
	private void loadTypes(){
		if(this.attributeContentFolder!=null){
			try {
				for(IContentObject child:this.attributeContentFolder.getChildren()){
					if(child.getName().startsWith(CONTENTPREFIX)){
						String childName = child.getName().replace(CONTENTPREFIX, "").toLowerCase();
						//Ivy.log().debug("search Attribute Content type for "+childName);
						for(AttributeContentType type:AttributeContentType.values()){
							if(type.toString().toLowerCase().equals(childName)){
								this.declaredTypes.add(type);
								this.contents.put(type, child);
								break;
							}
						}						
					}
				}
			} catch (Exception e){
				Ivy.log().debug("Error at Attribute Content - Loading Available Types", e);					
			}
		}
	}
	public Boolean attributeContentExists(AttributeContentType type){
		return this.contents.containsKey(type);
	}
	String getAttributeContentMakro(AttributeContentType type){
		String content = "";
		String contentTypeUri = contentUri+SEPARATORCMS+CONTENTPREFIX+type.toString().toLowerCase();
		
		if(this.contents.containsKey(type)){
			try {
				IContentObject co = Ivy.cms().getContentObject(contentTypeUri);
				
				if(co!=null&&co.hasValues()){
					content = "<%=ivy.cms.co(\""+contentTypeUri+"\")%>";
				}
			} catch (Exception e) {
				Ivy.log().debug(e);
			}			
		}
		return content;
	}
	public String setAttributeContent(AttributeContentType type, String content){
		String contentName = CONTENTPREFIX +type.toString().toLowerCase();
		CoType contentType = CoType.STRING;
		
		if(type.equals(AttributeContentType.SOURCE)){
			contentType = CoType.SOURCE;
		}
		if(!this.contentUri.equals("")
				&&!this.user.equals("")){
			try {
				IContentObject coFolder = Ivy.cms().getContentObject(this.contentUri);
				if(coFolder==null){
					coFolder = this.mkdir();
				}
				if(coFolder!=null){
					IContentObject coType= coFolder.getChild(contentName);
					if(coType==null){
						coType = coFolder.addChild(contentName, "", contentType, this.user);
					}
					if(coType!=null){
						if(coType.hasValues()){
							for(IContentObjectValue coValue:coType.getValues()){
								if(coValue.isDefault()){
									coValue.setContent(content, this.user);
									break;
								}
							}
						}
						else {
							IContentObjectValue coValue =coType.addValue("",new java.util.Date(),null,Ivy.cms().getDefaultLanguage(),this.user,true, "");					
							coValue.setContent(content, this.user);							
						}
					}
					coType.touch(this.user);
				}
			} catch (Exception e) {
				Ivy.log().error(e);
			}
		}
		return this.getAttributeContentString(type);
	}
	private IContentObject mkdir(){
		IContentObject co = null;

		String currentUri = "";
		
		try {
			//try to find attribute content->name object directly
			co = Ivy.cms().getContentObject(this.contentUri);
			
			if(co==null
					&&!this.user.equals("")
					&&!this.contentUri.equals("")){
				//no name content object 
				//-> assume that current field is new and its attribute content has to be added to CMS
				
				String[] uriList = this.contentUri.substring(1).split(SEPARATORCMS);
				IContentObject parentFolder = null;
				//explore content hierarchy and add needed CMS folders to meet
				//attributes declaringClass hierarchy 
				for(int i=0;i<uriList.length;i++){
					StringBuilder coUri = new StringBuilder("");
					String coName = uriList[i];
					for(int j=0;j<=i;j++){
						coUri.append(SEPARATORCMS);
						coUri.append(uriList[j]);
					}
					currentUri = coUri.toString();
					IContentObject folder = Ivy.cms().getContentObject(currentUri);
					if(folder==null){
						if(i==0){
							parentFolder = Ivy.cms().getRootContentObject().addChild(coName, "", CoType.FOLDER, this.user);
						}
						else if (parentFolder!=null){
							parentFolder = parentFolder.addChild(coName, "", CoType.FOLDER, this.user);								
						}
					}
					else {
						parentFolder = folder;
					}
				}
				co = Ivy.cms().getContentObject(this.contentUri);
			}
			
		} catch (Exception e) {
			Ivy.log().error("Error on getContentObject "+currentUri+": "+e.getMessage(), e);	
		}			
		return co;
	}
	private void loadProperties()
	{
		/*
		 * <properties>";
		 * <comment>XML string to properties...</comment>
		 * <entry key=\"hello\">world</entry>
		 * </properties>
		 */
		properties = new Properties();
		if(this.description.trim().length()>0){
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.description.getBytes());
			try
			{
				properties.loadFromXML(byteArrayInputStream);
				if(properties.size()==0){
					this.attributeContentFolder.setDescription("", this.user);
				}
			}
			catch(Exception e)
			{
				Ivy.log().debug("Error on Loading CMS Properties at "+this.contentUri, e);
			}			
		}
	}
	public String getInputMetaProperty(){
		return getProperty(InputProperties.InputMeta);		
	}
	public String getProperty(InputProperties propertyType){
		String property = "";
		if(this.properties.containsKey(propertyType.toString())){
			property = (String) this.properties.get(propertyType.toString());
		}
		return property;
	}
	public InputType getInputTypeProperty(){
		InputType inputType = InputType.UNDEFINED;
		if(this.properties.containsKey(InputProperties.InputType.toString())){
			String aInputType = (String) this.properties.get(InputProperties.InputType.toString());
			if(!aInputType.equals("")){
				try {
					inputType = InputType.valueOf(aInputType);													
				} catch (Exception e){
					//input type could not matched
					inputType = InputType.UNDEFINED;
				}
			}
		}
		return inputType;
	}
	public void setProperty(String key, String value){
		this.properties.put(key, value);	
	}
	public void storeProperties(){
		if(this.properties.size()>0){
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			try
			{
				this.properties.storeToXML(byteArrayOutputStream, "CMS Field Properties");
				String xmlString = byteArrayOutputStream.toString();
				attributeContentFolder.setDescription(xmlString, this.user);
			}
			catch(Exception e)
			{
				Ivy.log().error("AttributeContent: storeProperties failed", e);
			}					
		}
		else {
			try {
				this.attributeContentFolder.setDescription("", this.user);
			} catch (PersistencyException e) {
				//Do nothing
			}
		}
	}
	@Override
	public int compareTo(AttributeContent aco) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    if(this.ordinal==aco.ordinal) return EQUAL;
	    if(this.ordinal<aco.ordinal) return BEFORE;
	    if(this.ordinal>aco.ordinal) return AFTER;
	    return EQUAL;
	}
	
}
