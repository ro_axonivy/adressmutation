package ch.soreco.webbies.common.html.form;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class AttributeClass {
	private static String FIELDUID = "serialVersionUID";
	private List<AttributeContent> contents = null;
	public Class<?> clazz= null;
	private LinkedHashMap<String, String> labelsAndIcons = new LinkedHashMap<String, String>();
	private Field[] fields = null;
	public AttributeClass(String className) throws ClassNotFoundException{
		this.clazz = Class.forName(className);
		this.fields = getFields();
	}
	public AttributeClass(Class<?> clazz){
		this.clazz = clazz;
		this.fields = getFields();
	}
	private Field[] getFields(){
		if(this.fields==null){
			if(this.clazz.isEnum()){
				fields = clazz.getFields();
			}
			else {
				fields = clazz.getDeclaredFields();
			}
		}
		return this.fields;
	}
	public List<AttributeContent> getContents(){
		if(this.contents==null){
			this.contents =  new ArrayList<AttributeContent>();
			for(int i=0;i<this.fields.length;i++){
				String key = this.fields[i].getName();
				if(!key.equals(FIELDUID)){
					AttributeContent content = new AttributeContent(this.fields[i]);
					this.contents.add(content);
				}
			}
			//Collections.sort(this.contents);
			}
		return this.contents;
	}
	public LinkedHashMap<String, String> getLabelsAndIcons(){
		if(labelsAndIcons.size()==0){
			getContents();
			if(contents!=null){
				for(AttributeContent content:contents){
					String key = content.fieldName;
					String label = content.getAttributeContentLabelAndIcon();
					labelsAndIcons.put(key, label);
				}
			}
		}
		return labelsAndIcons;
	}
}
