package ch.soreco.webbies.common.html.form;

public enum InputType {
	UNDEFINED,
	LABEL,
	CHECKBOX,
	TEXTAREA,
	TEXTINPUT,
	PASSWORD,
	HIDDEN,
	RADIOS_RL,
	RADIOS_TD,
	RADIO,
	SELECT, 
	OPTION,
	SOURCED, 
	SUPPRESSED
}
