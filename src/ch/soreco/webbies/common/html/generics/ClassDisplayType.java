package ch.soreco.webbies.common.html.generics;

public enum ClassDisplayType {
	TABLE, 
	LIST, 
	TABLEROW,
	LISTROW
}
