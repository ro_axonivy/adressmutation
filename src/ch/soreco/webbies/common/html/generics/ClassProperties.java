package ch.soreco.webbies.common.html.generics;

public enum ClassProperties {
	PERSISTENCEUNIT, 
	ID_FIELD_NAME, 
	CLASS_VIEWS,
	SORT_BY_LABEL,
	CONFIGURABLE
}
