package ch.soreco.webbies.common.html.generics;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.util.CompositeUtils;

public class AttributeObject {
	private Object object = null;
	private AttributeInput input=null;
	private AttributeLabel label = null;
	private String value="";
	public InputType inputType=null;
	public String inputName="";
	public AttributeContent content = null;
	AttributeObject(CompositeObject composite
					, AttributeContent content
					, String inputName){
		this.content = content;
		this.inputType = content.getInputTypeProperty();
		this.inputName = inputName;
		try {
			this.object = composite.get(this.content.field.getName());			
		} catch (NoSuchFieldException e) {
			Ivy.log().debug("AttributeObject "+this.inputName+ " not available", e);
		}		
	}
	public AttributeLabel getLabel(){
		if(this.label==null&&!inputName.equals("")){ 
			this.label = new AttributeLabel(this.inputName, this.content);
		}
		return this.label;
	}
	public AttributeInput getInput(){
		if(this.input==null&&!inputName.equals("")){ 
			//String inputValue = this.getValue();	
			this.value = getValue();
			Boolean skipName = false;
			this.input = new AttributeInput(this.inputName, this.value, this.content, skipName);
		}
		return this.input;
	}
	public ArrayList<AttributeColumn> toColumns(){
		ArrayList<AttributeColumn> columns = new ArrayList<AttributeColumn>();
		if(!inputType.equals(InputType.SUPPRESSED)){
			switch (this.inputType){
			case HIDDEN:
				columns.add(new AttributeColumn(0,this.getInput(), null));
				break;
			case RADIOS_RL:
				columns.addAll(getRadioColumns());
				break;
			case RADIOS_TD:
				columns.addAll(getRadioColumns());
				break;
			case FORMPART:
				columns.addAll(getFormPart());
				break;
			default:
				columns.add(new AttributeColumn(0, this.getInput(), this.getLabel()));			
				break;
			}
		}
		
		return columns;
	}
	
	public String toHtml(String enclosingTag){
		StringBuilder html = new StringBuilder("");
		
		ArrayList<AttributeColumn> nodes = this.toColumns();
		for(AttributeColumn node:nodes){
			try {
				html.append(node.toHtml(enclosingTag));				
			}
			catch (Exception e){
				Ivy.log().error(this.getClass().getSimpleName()+":toHtml", e);
			}
		}
		return html.toString();
	}
	private ArrayList<AttributeColumn> getFormPart(){
		ArrayList<AttributeColumn> columns = new ArrayList<AttributeColumn>();
		if(this.getLabel().getLabelPosition().equals(LabelPosition.LEFT)){
			columns.add(new AttributeColumn(columns.size(), this.getLabel()));			
		}
		String view = this.content.getView();
		String name = this.inputName;
		String nodeKey = this.content.fieldName;
		String html = "<p><b>Form Part for "+this.content.fieldName+"</b></p>";
		CompositeObject attribute = null;
		if(this.object instanceof CompositeObject){
			attribute = (CompositeObject) this.object;			
		}
		else {
			if(CompositeObject.class.isAssignableFrom(this.content.field.getType())){
				try {
					Object instance = this.content.field.getType().newInstance();
					attribute = (CompositeObject) instance;
				} catch (InstantiationException e) {
					Ivy.log().info(this.getClass().getSimpleName()+" Exception instantiating new "+this.content.field.getType(), e);
					html = html+"<p style='color:red;'>Exception:"+e.getCause()+"</p>";
				} catch (IllegalAccessException e) {
					Ivy.log().info(this.getClass().getSimpleName()+" Exception instantiating new "+this.content.field.getType(), e);
					html = html+"<p style='color:red;'>Exception:"+e.getCause()+"</p>";
				}				
			}
			else {
				html = html+"<p style='color:red;'>Exception: Can not be a FormPart as the field type is not representing an ivyClass</p>";				
			}
		}
		if(attribute!=null){
			AttributeClassHtml formPart = new AttributeClassHtml(view, attribute, name);
			html = formPart.toHtml();			
		}
		AttributeColumn column = new AttributeColumn(columns.size(), nodeKey, html);
		columns.add(column);
		if(this.getLabel().getLabelPosition().equals(LabelPosition.RIGHT)){
			columns.add(new AttributeColumn(columns.size(), this.getLabel()));			
		}
		return columns;
	}
	private ArrayList<AttributeColumn> getRadioColumns(){
		ArrayList<AttributeColumn> columns = new ArrayList<AttributeColumn>();
		if(!this.getInput().enumClass.equals("")){
			//Set Label
			if(this.getLabel().getLabelPosition().equals(LabelPosition.LEFT)){
				columns.add(new AttributeColumn(columns.size(), this.getLabel()));			
			}
			
			//instantiate AttributeClass which will return the keyValue Contents
			AttributeClass enumClass = null;
			try {
				enumClass = new AttributeClass(this.getInput().enumClass, this.content.getView());
			} catch (ClassNotFoundException e) {
				Ivy.log().error(this.getClass().getSimpleName()+" Exeption: Error Loading Enum for Select", e);
			}
			if(enumClass!=null){
				if(enumClass.clazz.isEnum()){
					List<AttributeContent> contents = enumClass.getContents();
					for(AttributeContent content: contents){
						String labelName = this.inputName+"$"+content.fieldName;
						try {
							AttributeInput input = new AttributeInput(this.inputName, this.value, content.fieldName, content, false, InputType.RADIO);
							AttributeLabel label = new AttributeLabel(labelName, content);
							columns.add(new AttributeColumn(columns.size(), input, label));						
						} catch (Exception e){
							Ivy.log().error("Could not set "+labelName+" as Radio Input");
						}
					}
					//now add hidden Field to as Value Holder
					Boolean skipName = false;
					if(this.value.trim().equals("")){
						skipName = true;
					}
					AttributeInput hidden = new AttributeInput(this.inputName, this.value, this.value, this.content, skipName, InputType.HIDDEN);
					columns.add(new AttributeColumn(columns.size(), hidden));
				}
				else if(PersistenceUtils.isEntityClass(enumClass.clazz)){
					LinkedHashMap<String, String> keyLabelMap = enumClass.getKeyLabelMap();
					String idFieldName = PersistenceUtils.getIdFieldName(enumClass.clazz);
					String inputName = this.inputName
										+AttributeConstants.SEPARATORCSL
											+idFieldName;
					for(String key:keyLabelMap.keySet()){
						if(!key.equals("")){
							AttributeInput input = new AttributeInput(inputName, this.value, key, enumClass, false, InputType.RADIO);
							columns.add(new AttributeColumn(columns.size(), input));							
						}
					}
					Boolean skipName = false;
					if(this.value.trim().equals("")){
						skipName = true;
					}
					AttributeInput hidden = new AttributeInput(this.inputName, this.value,this.value, this.content, skipName, InputType.HIDDEN);
					columns.add(new AttributeColumn(columns.size(), hidden));
				}
				//Set Label at the end
				if(this.getLabel().getLabelPosition().equals(LabelPosition.RIGHT)){
					columns.add(new AttributeColumn(columns.size(), this.getLabel()));			
				}
			}
		}
		if(columns.size()==0){
			//as the input enum class does not provide entries
			//reset the inputType to undefined
			this.getInput().content.setProperty(InputProperties.InputType, InputType.UNDEFINED.toString());
			this.getInput().content.storeProperties();
			columns.add(new AttributeColumn(0, this.getInput(), this.getLabel()));			
		}		
		return columns;
		
	}

	public String getValue(){
		if(this.object!=null){
			if(this.content.isComposite()
					&&PersistenceUtils.isEntityClass(this.content.field.getType())){
				CompositeObject entity = (CompositeObject) this.object;
				Object value=PersistenceUtils.getIdFieldValue(entity);
				return CompositeUtils.getValue(value);
			}
			return CompositeUtils.getValue(this.object);			
		}
		else {
			return "";
		}
	}
}
