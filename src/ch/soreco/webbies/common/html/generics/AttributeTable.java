package ch.soreco.webbies.common.html.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AttributeTable{
	private List<AttributeObject> objects = new ArrayList<AttributeObject>();
	private ArrayList<AttributeRow> table = new ArrayList<AttributeRow>();		
	AttributeTable(List<AttributeObject> objects){
		this.objects = objects;
		this.initTable();
	}
	private void initTable(){
		int i = 0;
		for(AttributeObject object:objects){
			if(!object.inputType.equals(InputType.SUPPRESSED)
				&&!object.inputType.equals(InputType.HIDDEN)){
				ArrayList<AttributeColumn> columns = object.toColumns();
				Collections.sort(columns);
				
				String name = object.content.fieldName;
				
				if(!object.inputType.equals(InputType.RADIOS_TD)){
					//String name = object.content.fieldName;
					table.add(new AttributeRow(i,name, columns));					
					i = i+1;
				}
				else {
					for(AttributeColumn column:columns){
						String rowname = name+"At"+column.columnKey;
						table.add(new AttributeRow(i,rowname, column));	
						i = i+1;
					}
				}
			}
		}			
	}
	public List<String> getColumnNames(){
		List<String> columnNames = new ArrayList<String>();
		for(AttributeRow row:table){
			columnNames.addAll(row.getFieldNames());
		}
		return columnNames;
	}
	private int getRowIndex(String rowKey){
		int index = -1;
		for(int i=0;i<table.size();i++){
			if(table.get(i).rowKey.equals(rowKey)){
				index = i;
				break;				
			}
		}
		return index;
	}
	public void rowMoveInto(String sourceName, String targetName){
		int sourceRowIndex = getRowIndex(sourceName);
		if(sourceRowIndex>-1){
			int targetRowIndex = getRowIndex(targetName);
			if(targetRowIndex>-1){
				AttributeRow sourceRow = table.get(sourceRowIndex);
				AttributeRow targetRow = table.get(targetRowIndex);	
				targetRow.columns.addAll(sourceRow.columns);
				table.set(targetRowIndex, targetRow);		
				table.remove(sourceRowIndex);
			}
		}
	}
	public void rowMoveAfter(String sourceRowKey, String targetRowKey){
		rowMove(sourceRowKey, targetRowKey, 1);
	}
	public void rowMoveBefore(String sourceRowKey, String targetRowKey){
		rowMove(sourceRowKey, targetRowKey, -1);
	}
	private void rowMove(String sourceRowKey, String targetRowKey, int offset){
		int sourceRowIndex = getRowIndex(sourceRowKey);
		if(sourceRowIndex>-1){
			int targetRowIndex = getRowIndex(targetRowKey);
			if(targetRowIndex>-1){
				AttributeRow sourceRow = table.get(sourceRowIndex);
				AttributeRow targetRow = table.get(targetRowIndex);	
				sourceRow.rowIndex = targetRow.rowIndex;
				sourceRow.sortIndex = sourceRow.sortIndex+offset;
				table.set(sourceRowIndex, sourceRow);					
			}
		}		
	}
	public void putColumn(String rowKey, AttributeColumn column){
		int rowIndex = getRowIndex(rowKey);
		if(rowIndex>-1){
			table.get(rowIndex).putColumn(column);
		}
		else {
			table.add(new AttributeRow(table.size(), rowKey, column));
		}		
	}
	private AttributeRow getRow(String rowKey){
		AttributeRow row = null;
		int rowIndex = getRowIndex(rowKey);
		if(rowIndex>-1){
			row = table.get(rowIndex);
		}
		return row;
	}
	public void putRow(AttributeRow row){
		int index = getRowIndex(row.rowKey);
		if(index>-1){
			table.set(index, row);
		}
		else {
			table.add(row);
		}
	}
	public void putRow(String rowKey, String columnKey,int columnOrdinal, String html){
		AttributeColumn column = new AttributeColumn(columnOrdinal, columnKey, html);		
		AttributeRow row = this.getRow(rowKey);
		if(row==null){
			 row = new AttributeRow(this.table.size(), rowKey, column);				
		}
		else {
			row.putColumn(column);
		}
		this.putRow(row);
	}
	public ArrayList<AttributeRow> getAttributeTable(){
		Collections.sort(this.table);
		return this.table;
	}
	public String toHtml(){
		return this.toHtml(ClassDisplayType.TABLE);
	}
	public String toHtml(ClassDisplayType type){
		//first sort table rows
		String html = "";
		Collections.sort(this.table);	
		switch(type){
		case LIST:
			html= this.toHtmlList();			
			break;
		case TABLE:
			html= this.toHtmlTable();			
			break;
		case TABLEROW:
			html= this.toHtmlTableRow();			
			break;
		case LISTROW:
			html= this.toHtmlListRow();			
			break;
		default:
			html= this.toHtmlTable();			
			break;
		}
		return html;
	}
	private String toHtmlList(){
		StringBuilder html = new StringBuilder("");
		html.append("<ul class=\"AttributeList\">");
		for(AttributeRow row:this.table){
			html.append("<li id=\""+row.rowKey+"AtRow\" class=\"AttributeListRow\">\n");
			html.append("<ul class=\"AttributeListColumns\">");
			html.append(row.toHtml("li"));
			html.append("</ul>\n");
			html.append("</li>\n");
		}
		html.append("</ul>\n");
		return html.toString();				
	}
	private String toHtmlListRow(){
		StringBuilder html = new StringBuilder("");
		html.append("<ul class=\"AttributeList\">");
		html.append("<li class=\"AttributeListRow\">\n");
		for(AttributeRow row:this.table){
			html.append("<ul class=\"AttributeListColumns\">");
			html.append(row.toHtml("li"));
			html.append("</ul>\n");
		}
		html.append("</li>\n");
		html.append("</ul>\n");
		return html.toString();				
	}
	private String toHtmlTableRow(){
		StringBuilder html = new StringBuilder("");
		html.append("<table class=\"AttributeTable\" width=\"100%\" cellpadding=2 cellspacing=0 border=0><tbody>\n");
		html.append("<tr class=\"AttributeTableRow\">\n");
		for(AttributeRow row:this.table){
			html.append(row.toHtml("td"));
		}
		html.append("</tr>\n");
		html.append("</tbody>\n</table>\n");
		return html.toString();		
		
	}
	private String toHtmlTable(){
		StringBuilder html = new StringBuilder("");
		html.append("<table class=\"AttributeTable\" width=\"100%\" cellpadding=2 cellspacing=0 border=0><tbody>\n");
		for(AttributeRow row:this.table){
			html.append("<tr id=\""+row.rowKey+"AtRow\" class=\"AttributeTableRow\">\n");
			html.append(row.toHtml("td"));
			html.append("</tr>\n");
		}
		html.append("</tbody>\n</table>\n");
		return html.toString();		
	}
}
