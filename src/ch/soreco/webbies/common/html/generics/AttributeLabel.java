package ch.soreco.webbies.common.html.generics;

import ch.ivyteam.ivy.environment.Ivy;


public class AttributeLabel {
	public AttributeContent content=null;
	private String labelId = "";
	private String inputName = "";
	private String labelAttributes="";
	public AttributeLabel(String inputName, AttributeContent content){
		this.content = content;
		this.inputName = inputName;
		init();
	}
	private void init(){
		this.labelId = this.inputName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID)+AttributeConstants.SEPARATORID+"Label";
		//get or create needed cms objects (name, icon, source)		
		this.labelAttributes = "id=\""+this.labelId+"\" "+this.content.getProperty(InputProperties.LabelAttributes);
	}
	public HtmlNode toHtmlNode(){
		String tag = "label";
		String innerHtml = this.content.getLabelAndIcon();
		HtmlNode node = new HtmlNode(tag, innerHtml, this.labelAttributes);
		return node;
	}
	public LabelPosition getLabelPosition(){
		LabelPosition pos = LabelPosition.LEFT;
		String property = this.content.getProperty(InputProperties.LabelPosition);
		if(!property.equals("")){
			try {
				pos = LabelPosition.valueOf(property);
			} catch (Exception e){
				Ivy.log().info(this.getClass().getSimpleName()+" Could'nt get value of "+property+" for Enum LabelPosition", e);
			}
		}
		return pos;
	}
	/*
	public String toHtml(){
		return toHtmlNode().toHtml();
	}
	*/
}
