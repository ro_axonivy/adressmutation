package ch.soreco.webbies.common.html.generics;

public enum InputProperties {
	LabelPosition,
	LabelAttributes,
	LabelCellAttributes,
	Required,
	Length, 
	InputType, 	
	SelectEnum,
	SelectQuery,
	InputMeta,
	InputCellAttributes, 
	useForFilter
}
