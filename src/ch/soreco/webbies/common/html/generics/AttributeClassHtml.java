package ch.soreco.webbies.common.html.generics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;

public class AttributeClassHtml {
	private CompositeObject composite =null;
	private String baseName = "";
	/**
	 * The baseId is useful for javascript sources
	 * and will be get by escapeFieldMakros Method
	 */
	@SuppressWarnings("unused")
	private String baseId = "";
	public List<AttributeObject> objects = null;
	public Class<?> clazz= null;
	private String classSimpleName = "";
	private AttributeClass attributeClass = null;
	private int index = -1;
	private String view = AttributeConstants.CONTENTPREFIX;	
	public AttributeClassHtml(String view, CompositeObject composite, String baseName, int index){
		this.index = index;
		init(view, composite, baseName);
	}	
	public AttributeClassHtml(String view, CompositeObject composite, String baseName){
		init(view, composite, baseName);
	}
	
	private void init(String view, CompositeObject composite, String baseName){
		this.view = view;
		if(this.view.equals("")){
			this.view = AttributeConstants.CONTENTPREFIX;
		}
		this.clazz = composite.getClass();
		this.classSimpleName = this.clazz.getSimpleName();
		this.composite = composite;
		
		this.baseName = baseName;	
		if(!this.baseName.endsWith(AttributeConstants.SEPARATORCSL)){
			this.baseName = this.baseName+AttributeConstants.SEPARATORCSL;
		}
		if(this.index>-1){
			this.baseName = this.baseName+this.index+AttributeConstants.SEPARATORCSL;
		}
		this.baseId = this.baseName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID);
		
		this.attributeClass = new AttributeClass(this.clazz, this.view);

		getObjects();		
	}
	private List<AttributeObject> getObjects() {
		if(objects==null&&this.composite!=null){
			objects = new ArrayList<AttributeObject>();
			if(this.attributeClass.getContents()!=null){
				for(AttributeContent content:this.attributeClass.getContents()){
					String inputName = this.baseName+content.fieldName;

					AttributeObject object= new AttributeObject(this.composite, content, inputName);
					if(!object.inputType.equals(InputType.SUPPRESSED)){
						objects.add(object);	
					}
				}							
			}
		}
		return objects;
	}
	public String getClassLabel(){
		return this.attributeClass.getClassContent().getLabel();
	}
	private ClassDisplayType getDisplayType(){
		ClassDisplayType type = ClassDisplayType.TABLE;
		String value = this.attributeClass.getClassContent().getProperty(ViewProperties.CLASS_DISPLAY_TYPE);
		if(!value.equals("")){
			type = ClassDisplayType.valueOf(value);
		}
		return type;
	}
	public List<Class<?>> getAssociatables(){
		List<Class<?>> list = new ArrayList<Class<?>>();
		String associatables = this.attributeClass.getClassContent().getProperty(ViewProperties.ADD_ASSOCIATED_CLASS);
		if(!associatables.trim().equals("")){
			String[] associates = associatables.split(",");
			for(String className:associates){
				try {
					Class<?> clazz = Class.forName(className);
					list.add(clazz);
				} catch (ClassNotFoundException e) {
					Ivy.log().info(this.getClass().getSimpleName()+" Exception: Can't find class "+className,e);
				}
			}			
		}
		return list;
	}

	public String toHtml(){
		StringBuilder html = new StringBuilder("");
		AttributeTable attributeTable = new AttributeTable(objects);
		
		//attributeTable.rowMoveAfter("faxReversType", "orderIncomeTypeAta3Fax");
		//attributeTable.rowMoveAfter("faxSignatureType", "orderIncomeTypeAta3Fax");
		//attributeTable.rowMoveAfter("faxOrMailReversType", "orderIncomeTypeAta2FaxOrMail");
		//attributeTable.rowMoveAfter("postalSignatureType", "orderIncomeTypeAta1Postal");
		//attributeTable.rowMoveInto("contactIsChecked", "orderIncomeTypeAta1Postal");
		//attributeTable.getColumnNames();
		//attributeTable.putRow("testRow", "testColumn",1, "Ha Ha");
		//attributeTable.rowMoveBefore("testRow", "orderIncomeTypeAta1Postal");
				
		html.append("<div id='"+this.classSimpleName+(this.index>-1? this.index:"")+"' class=\""+this.classSimpleName+"\" "+(this.index>-1? "rowIndex=\""+this.index+"\"":"")+">\n");
		if(this.attributeClass.hasFieldset()){
			html.append("<fieldset>");
			html.append("<legend>");
			html.append(this.attributeClass.getClassContent().getLabelAndIcon());
			html.append("</legend>");
		}
		html.append(attributeTable.toHtml(this.getDisplayType()));

		if(this.attributeClass.hasFieldset()){
			html.append("</fieldset>");
		}
		html.append("<!--Hidden Objects-->\n");
		for(AttributeObject object:objects){
			if(object.inputType.equals(InputType.HIDDEN)){
				try {
					html.append(object.toHtml(""));
				} catch (Exception e){
					Ivy.log().error("Error calling Method toHtml() on AttributeObject "+object.inputName,e);
				}
				html.append("\n");
			}
		}
		html.append("\n<!--/Hidden Objects-->");
		if(this.attributeClass.getClassContent().exists(AttributeContentType.SOURCE)){
			html.append("<!-- Class Source-->");
			html.append(this.attributeClass.getClassContent().getType(AttributeContentType.SOURCE));
			html.append("<!-- /Class Source-->");			
		}
		html.append("</div><!-- End "+this.classSimpleName+"-->\n");	
		//Ivy.log().info(html);
		return escapeFieldMakros(html.toString());
	}
	private String escapeFieldMakros(String html){
		if(this.index<0){
			html = html.replace("${@index}$", "$")
					.replace("{@index}", "")
					.replace("#index", "");
		}
		else {
			String value = ""+this.index;
			String target = "{@index}";
			String target2 = "#index";		
			html = html.replace(target, value);
			html = html.replace(target2, value);
		}
		Field[] fields = this.getClass().getDeclaredFields();
		for ( Field field : fields  ) {
			String value = "";
			String target = "{@"+field.getName()+"}";
			String target2 = "#"+field.getName();
			if(!field.getName().equals("index")){
					if(field.getType().getSimpleName().equals("String")){
					try {
						value = (String) field.get(this);
					} catch (IllegalArgumentException e) {
						Ivy.log().info("Can't access "+field.getName(),e);
					} catch (IllegalAccessException e) {
						Ivy.log().info("Can't access "+field.getName(),e);
					}
				}	
				else if(field.getType().getSimpleName().equals("Boolean")){
					try {
						value = field.get(this).toString();
					} catch (IllegalArgumentException e) {
						Ivy.log().info("Can't access "+field.getName(),e);
					} catch (IllegalAccessException e) {
						Ivy.log().info("Can't access "+field.getName(),e);
					}
				}
				html = html.replace(target, value);
				html = html.replace(target2, value);
			}
		}			 

		return html;
	}
}
