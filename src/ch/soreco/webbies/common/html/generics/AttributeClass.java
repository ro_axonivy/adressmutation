package ch.soreco.webbies.common.html.generics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.util.MapUtils;
import ch.soreco.webbies.common.util.XmlProperties;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.ivyteam.ivy.scripting.objects.Record;
public class AttributeClass {
	public Class<?> clazz= null;
	private Field[] fields = null;
	private String view=AttributeConstants.CONTENTPREFIX;

	private AttributeContent classContent = null;
	private List<AttributeContent> unsuppressedContents = null;
	private List<AttributeContent> allContents = null;
	
	private HashMap<String, AttributeContent> contentMap = null;
	private List<String> keys = null;
	private LinkedHashMap<String, String> keyLabelMap = null;
	private LinkedHashMap<String, String> keyContentLabelMap = null;	
	private List<String> views = null;
	
	public AttributeClass(String className, String view) throws ClassNotFoundException{
		this.clazz = Class.forName(className);
		this.fields = getFields();
		this.setView(view);
	}
	public AttributeClass(Class<?> clazz, String view){
		this.clazz = clazz;
		this.fields = this.getFields();
		this.setView(view);
	}
	public AttributeClass(Class<?> clazz){
		this.clazz = clazz;
		this.fields = this.getFields();
		this.setView(AttributeConstants.CONTENTPREFIX);
	}
	public Recordset getPropertiesRecordset(){
		Recordset rs = new Recordset();

		List<XmlProperties> configs = new ArrayList<XmlProperties>();
		XmlProperties classLevel = new XmlProperties(this.clazz);
		if(classLevel.hasProperties()||this.clazz.isEnum()){
			configs.add(classLevel);
			for(String view:this.getViews()){
				XmlProperties classViewLevel = new XmlProperties(this.clazz, view);
				if(classViewLevel.hasProperties()){
					configs.add(classViewLevel);
				}
				for(Field field:this.fields){
					XmlProperties fieldViewLevel = new XmlProperties(field, view);
					if(fieldViewLevel.hasProperties()||this.clazz.isEnum()){
						configs.add(fieldViewLevel);
					}
				}
			}
		}			
		for(XmlProperties conf:configs){
			Record rec = conf.toRecord();
			for(String key:rec.getKeys()){
				if(rs.getKeys().indexOf(key)==-1){
					List<String> values = new ArrayList<String>();
					rs.addColumn(key, values);
				}
			}
			rs.add(conf.toRecord());
		}
		return rs;
	}
	/**
	 * @param view the view to set
	 */
	public void setView(String view) {
		if(!this.view.equals(view)){
			if(!view.equals(AttributeConstants.CONTENTPREFIX)
					&&this.getViews().indexOf(view)==-1){
				//Present View is not Mapped, if the view starts with ?
				//Try to Map it by "contains"
				String mappedByView = AttributeConstants.CONTENTPREFIX;
				for(String mappedView :this.getViews()){
					if(view.contains(mappedView)){
						mappedByView = mappedView;
						break;
					}
				}
				this.view = mappedByView;
			}
			else {
				this.view = view;				
			}
			this.unsuppressedContents = null;
			this.contentMap = null;
			this.keyLabelMap = null;	
		}
	}
	/**
	 * @return the view
	 */
	public String getView() {
		if(this.view.equals("")){
			this.setView(AttributeConstants.CONTENTPREFIX);
		}
		return view;
	}
	public List<String> getViews(){
		if(this.views==null){
			this.views = new ArrayList<String>();
			this.views.add(AttributeConstants.CONTENTPREFIX);
			String property = getClassContent().getProperty(ClassProperties.CLASS_VIEWS);
			if(!property.equals("")){
				String[] views = property.split(",");
				for(int i=0;i<views.length;i++){
					if(!this.views.contains(views[i])){
						this.views.add(views[i].trim().replace(" ", ""));					
					}
				}				
			}
		}
		return this.views;
	}
	private Field[] getFields(){
		if(this.fields==null){
			if(this.clazz.isEnum()){
				fields = clazz.getFields();
			}
			else {
				fields = clazz.getDeclaredFields();
			}
		}
		return this.fields;
	}
	public AttributeContent getClassContent(){
		if(this.classContent==null){
			this.classContent = new AttributeContent(this.getView(), this.clazz);
		}
		return this.classContent;
	}
	public Boolean hasFieldset(){
		Boolean hasFieldset = true;
		String doFieldset = this.getClassContent().getProperty(ViewProperties.WRAP_FIELDSET);
		if(doFieldset.toLowerCase().equals("false")){
			hasFieldset = false;
		}
		return hasFieldset;
	}
	public List<AttributeContent> getAllContents(){
		if(this.allContents==null){
			this.allContents =  new ArrayList<AttributeContent>();
			for(int i=0;i<this.fields.length;i++){
				String key = this.fields[i].getName();
				if(!key.equals(AttributeConstants.FIELDUID)){
					AttributeContent content = new AttributeContent(this.getView(), this.fields[i]);
					//content.setView(this.getView());
					this.allContents.add(content);
				}
			}
			Collections.sort(this.allContents);
			}
		return this.allContents;
	}
	public List<AttributeContent> getContents(){
		if(this.unsuppressedContents==null){
			this.unsuppressedContents =  new ArrayList<AttributeContent>();
			if(this.getAllContents()!=null){
				for(AttributeContent content:this.getAllContents()){
					if(!content.getInputTypeProperty().equals(InputType.SUPPRESSED)){
						this.unsuppressedContents.add(content);						
						}
					}
				}
			}
		return this.unsuppressedContents;
	}
	public HashMap<String, AttributeContent> getContentMap(){
		if(contentMap==null){
			this.contentMap = new HashMap<String, AttributeContent>();
			if(this.getContents()!=null){
				for(AttributeContent aco:getContents()){
					this.contentMap.put(aco.fieldName, aco);
				}
			}			
		}
		return this.contentMap;
	}
	public List<String> getKeys(){
		if(this.keys==null){
			this.keys = new ArrayList<String>();
			for(String key:this.getContentMap().keySet()){
				this.keys.add(key);
			}
		}
		return keys;
	}
	public LinkedHashMap<String, String> getKeyContentLabelMap(){
		if(this.keyContentLabelMap==null){
			this.keyContentLabelMap = new LinkedHashMap<String, String>();
			if(this.getContents()!=null){
				for(AttributeContent content:this.getContents()){
					String key = content.fieldName;
					String label = content.getLabelAndIcon();
					this.keyContentLabelMap.put(key, label);
				}				
			}
		}
		return this.keyContentLabelMap;
	}
	public LinkedHashMap<String, String> getKeyLabelMap(){
		if(this.keyLabelMap==null){
			this.keyLabelMap = new LinkedHashMap<String, String>();
			boolean addEmptyValue = false;
			addEmptyValue = this.getClassContent().getProperty(ViewProperties.ADD_EMPTY_VALUE).toLowerCase().equals("true");
			if(addEmptyValue){
				this.keyLabelMap.put("", "");				
			}
			
			if(PersistenceUtils.isEntityClass(this.clazz)){
				String unit = this.getClassContent().getProperty(ClassProperties.PERSISTENCEUNIT);
				if(!unit.equals("")){
					List<?> found = Ivy.persistence().get(unit).findAll(this.clazz);
					if(found!=null){
						String fieldName = this.getClassContent().getProperty(ViewProperties.USE_FORLABEL_FIELD);
						
						for(int i=0;i<found.size();i++){
							//as ivyClasses always implement composites
							CompositeObject entity = (CompositeObject) found.get(i);
							Object id = PersistenceUtils.getIdFieldValue(entity);
							Object label = null;
							if(!fieldName.trim().equals("")){
								try {
									label = entity.get(fieldName);
									if(label==null){
										label = id;									
									}
								} catch (Exception e) {
									Ivy.log().info(this.clazz.getSimpleName()+" Exception:"+e);
								}								
							}
							this.keyLabelMap.put(id.toString(), label.toString());
						}
					}
					if(found.size()==0){
						this.keyLabelMap.put("", "No Data");						
					}
				}
			}
			else {
				this.keyLabelMap = getKeyContentLabelMap();				
			}
			Boolean sortByLabel = false;
			sortByLabel = this.getClassContent().getProperty(ClassProperties.SORT_BY_LABEL).toLowerCase().equals("true");
			if(this.keyLabelMap.entrySet().size()>0&&sortByLabel){
				this.keyLabelMap = (LinkedHashMap<String, String>) MapUtils.sortedMapByValue(this.keyLabelMap);
			}
		}
		return keyLabelMap;
	}
}
