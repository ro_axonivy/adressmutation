package ch.soreco.webbies.common.html.generics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import ch.ivyteam.ivy.environment.Ivy;
import ch.soreco.webbies.common.db.PersistenceUtils;

public class AttributeInput {
	//Constructor Members
	public AttributeContent content = null;
	@SuppressWarnings("unused")//only as info for Sourced Html
	private String view = AttributeConstants.CONTENTPREFIX;
	public InputType inputType = InputType.UNDEFINED;
	public String enumClass = "";
	private AttributeClass enumAttributeClass = null;
	private String inputLabel = "";
	private String valueLabel = "";

	public String inputName = "";
	private String objectValue = "";
	private String inputValue = "";
	private String fieldType = "";

	private String inputMeta = "";
	private Boolean skipName = false;
	//Disabled not in use
	private Boolean isDisabled = false;

	//Logical Members
	public String inputId = "";
	public String fieldName = "";
	private HashMap<String, String> inputAttributes = new HashMap<String, String>();

	public AttributeInput(String inputName, 
			String objectValue, 
			AttributeContent content, 
			Boolean skipName){
		
		this.inputName = inputName;
		this.content = content; 		
		this.inputType = this.content.getInputTypeProperty();

		this.objectValue = objectValue;
		this.inputValue = objectValue;//not for radios
		this.skipName = skipName;

		if(!this.inputType.equals(InputType.SUPPRESSED)){
			init();	
		}
	}
	/*
	 * This constructor is mentioned to use with Entity Radio Groups
	 */
	public AttributeInput(String inputName, 
			String objectValue,
			String inputValue,
			AttributeClass mappingClass,
			Boolean skipName, 
			InputType inputType){

		this.view = "UNDEFINED";
		this.inputName = inputName;
		this.objectValue = objectValue;
		this.inputValue = inputValue;
		this.enumAttributeClass = mappingClass;
		this.skipName = skipName;

		this.inputType = inputType;
		if(!this.inputType.equals(InputType.SUPPRESSED)){
			init();
		}
	}
	public AttributeInput(String inputName, 
			String objectValue,
			String inputValue,			
			AttributeContent content, 
			Boolean skipName, 
			InputType inputType){
		
		this.inputName = inputName;
		this.content = content; 

		this.objectValue = objectValue;
		this.inputValue = inputValue;
		this.skipName = skipName;

		this.inputType = inputType;
		if(!this.inputType.equals(InputType.SUPPRESSED)){
			init();
		}
	}
	private void initContent(){
		if(this.content!=null){
			this.view = content.getView();
			this.fieldName = this.content.field.getName();
			this.fieldType = this.content.field.getType().getSimpleName();
			this.inputMeta = this.content.getProperty(InputProperties.InputMeta);
			this.enumClass = this.content.getProperty(InputProperties.SelectEnum);					
			this.initComposite();
		}
	}
	private void initComposite(){
		//as the field is a ivyClass do not support values
		//for wrapping
		if(this.content.isComposite()){
			//need to skip this name - the composite should not be mapped
			//as it can be overwritten by another Form Part
			this.skipName = true;
			//Check if the composite is an entity and therefore
			//mapped back by id - this as the user can choose the entity by a select
			String idFieldName = PersistenceUtils.getIdFieldName(this.content.field.getType());
			if(!idFieldName.equals("")){
				this.skipName = false;
				this.inputName = this.inputName
									+AttributeConstants.SEPARATORCSL
									+idFieldName;
				//Ivy.log().info(this.getClass().getSimpleName()+" Info: "+this.inputName+" is a Composite Mapped by"+idFieldName);
			}
		}		
	}
	
	private void initEnumClass(){
		if(!this.enumClass.equals("")){
			Class<?> clazz =null;
			try {
				clazz = Class.forName(this.enumClass);
			} catch (ClassNotFoundException e) {
				Ivy.log().info(this.getClass().getSimpleName()+" Could'nt find "+this.enumClass,e);
			}
			if(clazz!=null){
				if(PersistenceUtils.isEntityClass(clazz)){
					this.enumAttributeClass = new AttributeClass(clazz, this.content.getView());
				}
				if(clazz.isEnum()){
					this.enumAttributeClass = new AttributeClass(clazz, this.content.getView());				
				}				
			}
		}
		if(this.enumAttributeClass!=null){
			//the value of present html input differs from the object value
			//as the user can choose between a group of html input elements (eg. Radios)
			//which provide its value to a hidden field.

			//May current object is an Select or RadioGroup element get its Label
			//try to find by enumAttributeClass
			HashMap<String, String> labelsAndIcons = this.enumAttributeClass.getKeyLabelMap();
			if(this.objectValue.equals("")){
				this.valueLabel ="undefined";
			}
			else if(labelsAndIcons.containsKey(this.objectValue)){
				this.valueLabel = labelsAndIcons.get(this.objectValue);									
			}
			if(labelsAndIcons.containsKey(this.inputValue)){
				this.inputLabel = labelsAndIcons.get(this.inputValue);									
			}
			else {
				this.inputLabel = this.objectValue;
			}
			//Ivy.log().debug("Mapped Class is set to "+this.enumAttributeClass.clazz.getName()
			//		+"\nValueLabel:"+this.valueLabel
			//		+" InputLabel:"+this.inputLabel
			//		+" ObjectValue:"+this.objectValue);
		} 

	}
	private void init(){
		
		this.initContent();
		this.initEnumClass();
		this.inputAttributes.put("value", this.objectValue);
		//inputName contains SEPARATORCSL=$ which is not allowed in id attributes
		this.inputId = this.inputName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID)
						+AttributeConstants.SEPARATORID+"Input";
		this.inputAttributes.put("id", this.inputId);
		//if is undefined, use Text
		if(this.inputType.equals(InputType.UNDEFINED)){
			this.inputType = InputType.TEXTINPUT;
		}
		//put Class info
		this.inputAttributes.put("class", this.fieldType+" "+this.fieldName+" "+this.inputMeta+" FormInput"+this.inputType.toString());
		if(!this.skipName){
			this.inputAttributes.put("name", this.inputName);			
		}
		String required = this.content.getProperty(InputProperties.Required);
		if(required.equals("true")){
			this.inputAttributes.put("required", "required");
		}
		String length = this.content.getProperty(InputProperties.Length);
		if(!length.equals("")&&!length.equals("0")){
			this.inputAttributes.put("maxlength", length);
		}

	}
	public ArrayList<HtmlNode> toHtmlNodes(){
		ArrayList<HtmlNode> nodes = new ArrayList<HtmlNode>();
		try {
			nodes = this.getHtmlNodes();			
		} catch (Exception e){
			Ivy.log().error(this.getClass().getSimpleName()+" on Method toHtmlNodes", e);
		}
		return nodes;
	}
	private ArrayList<HtmlNode> getHtmlNodes(){
		ArrayList<HtmlNode> nodes = new ArrayList<HtmlNode>();
		switch (this.inputType){
		case SUPPRESSED:
			break;
		case FORMPART:
			break;			
		case SELECT:
			if(this.enumAttributeClass!=null){
				//try to find enumClass
				AttributeHtmlInput select = new AttributeHtmlInput(InputType.SELECT, this.inputValue, this.objectValue);
				select.innerHtml = getSelectOptions(this.objectValue, this.enumAttributeClass.getKeyLabelMap());
				nodes.add(select.toHtmlNode());
			}
			else {
				nodes.add(new AttributeHtmlInput(InputType.TEXTINPUT, this.inputValue, this.objectValue).toHtmlNode());
			}
			break;
		case SOURCED:
			//nothing as the Source part is always appended.
			break;
		default:
			nodes.add(new AttributeHtmlInput(this.inputType, this.inputValue, this.objectValue).toHtmlNode());
			break;
		
		}
		if(this.content!=null){
			HtmlNode source = new HtmlNode(this.getSource());
			nodes.add(source);
		}
		return nodes;
	}
	private String getSelectOptions(String selectedValue, LinkedHashMap<String, String> optionMap){
		StringBuilder options = new StringBuilder("");
		for(String inputValue:optionMap.keySet()){
			String tag ="option";
			String innerHtml = optionMap.get(inputValue);
			HashMap<String, String> tagAttributes = new HashMap<String, String>();
			tagAttributes.put("value", inputValue);				
			if(selectedValue.trim().toLowerCase().equals(inputValue.trim().toLowerCase())){
				tagAttributes.put("selected", "selected");
			}
			options.append(new HtmlNode(tag, innerHtml, tagAttributes).toHtml());			
		}
		return options.toString();		
	}
	private String getSource(){
		String html = "";
		if(this.content!=null){
			if(this.content.exists(AttributeContentType.SOURCE)){
				html = this.content.getType(AttributeContentType.SOURCE);

				Field[] fields = this.getClass().getDeclaredFields();

				if(html.indexOf("{@toInfo}")>-1){
					String info = this.toInfo();
					html = html.replace("{@toInfo}", info);
				}
				for ( Field field : fields  ) {
					String value = "";
					String target = "{@"+field.getName()+"}";
					String target2 = "#"+field.getName();
					if(!field.getName().equals("index")){
						if(field.getName().equals("inputType")){
							value = this.inputType.toString();
						}
						else if(field.getType().getSimpleName().equals("String")){
							try {
								value = (String) field.get(this);
							} catch (IllegalArgumentException e) {
								Ivy.log().info("Can't access "+field.getName(),e);
							} catch (IllegalAccessException e) {
								Ivy.log().info("Can't access "+field.getName(),e);
							}
						}	
						else if(field.getType().getSimpleName().equals("Boolean")){
							try {
								value = field.get(this).toString();
							} catch (IllegalArgumentException e) {
								Ivy.log().info("Can't access "+field.getName(),e);
							} catch (IllegalAccessException e) {
								Ivy.log().info("Can't access "+field.getName(),e);
							}
						}
						html = html.replace(target, value);
						html = html.replace(target2, value);
					}
				}			 
			}			
		}
		return html;
	}
	public String toHtml(){
		StringBuilder html = new StringBuilder("");
		try {
			ArrayList<HtmlNode> nodes = this.toHtmlNodes();
			for(HtmlNode node:nodes){
				html.append(node.toHtml());
			}			
		} catch (Exception e){
			Ivy.log().error(this.getClass().getSimpleName()+" on Method toHtml", e);
		}

		return html.toString();
	}
	private String toInfo(){
		StringBuilder info = new StringBuilder("");
		Field[] fields = this.getClass().getDeclaredFields();
		for ( Field field : fields  ) {
			String name = field.getName();
			String value = "";
			//try to get out the value directly
			try {
				Object obj = field.get(this);
				if(obj instanceof String){
					value = (String) obj;
					value = "\""+value+"\"";
				} else if(obj instanceof Integer){
					value = obj.toString();
				} else if (obj instanceof Boolean){
					Boolean bool = (Boolean) obj;
					if(bool){
						value = "true";
					}
					else {
						value = "false";
					}
				} else if(obj instanceof Class<?>){
					Class<?> clazz = (Class<?>) obj;
					value = "\""+clazz.getName()+"\"";
				} else if(name.equals("inputType")){
					value = "\""+this.inputType.toString()+"\"";
				}
			} catch (IllegalArgumentException e) {
				Ivy.log().info(this.getClass().getSimpleName()+" Exception: Can't access "+field.getName(),e);
			} catch (IllegalAccessException e) {
				Ivy.log().info(this.getClass().getSimpleName()+" Exception: Can't access "+field.getName(),e);
			}
			if(!value.equals("")){
				if(info.length()>1){
					info.append(",");
				}
				info.append(name+":"+value);				
			}
		}
		info.insert(0, "{");
		info.append("}");
		return info.toString();
	}	
	private class AttributeHtmlInput{
		private InputType inputType = InputType.UNDEFINED;
		private String innerHtml = "";
		private String inputValue = "";
		private String objectValue = "";
		
		private AttributeHtmlInput(InputType inputType, String inputValue, String objectValue){
			this.inputType = inputType;
			this.inputValue = inputValue;
			this.objectValue = objectValue;
			
			switch(this.inputType) {
			case LABEL:
				this.innerHtml = valueLabel;
				break;
			case RADIO:
				this.innerHtml = inputLabel;
				break;
			case OPTION:
				this.innerHtml = inputLabel;
				break;		
			default:
				break;
			}
		}
		private HtmlNode toHtmlNode(){
			HtmlInput htmlInput = new HtmlInput(this.inputType
					, inputAttributes
					, inputId
					, inputName
					, isDisabled
					, this.inputValue
					, this.objectValue
					, this.innerHtml
					, skipName);
			return htmlInput.toHtmlNode();
		}
	}
	
}
