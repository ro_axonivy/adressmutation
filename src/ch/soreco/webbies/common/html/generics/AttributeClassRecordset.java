package ch.soreco.webbies.common.html.generics;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.CompositeObject;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.util.ListToRecordset;

public class AttributeClassRecordset {
	public static Recordset getRecordset(Class<?> clazz, 
								String view, 
								List<CompositeObject> composites){
		Recordset rsOut = new Recordset();
		Recordset rsIn = new Recordset();
		ArrayList<List<Object>> columns = new ArrayList<List<Object>>();
		
		rsIn = ListToRecordset.getRecordset(composites, clazz, false, true);
		for(int col=0;col<rsIn.getKeys().size();col++){
			List<Object> column = List.create();
			for(int row=0;row<rsIn.size();row++){
				column.add(rsIn.getField(row, col));
			}
			columns.add(column);
		}
		AttributeClass attributeClass = new AttributeClass(clazz, view);
		
		for(AttributeContent co:attributeClass.getContents()){
			Boolean suppressed = false;
			String columnLabel = co.fieldName;	
			String fieldName = co.fieldName;
			if(co.isAttributeContent){
				columnLabel = co.getLabel();
				if(columnLabel.trim().equals("")){
					columnLabel = fieldName;
					}
				if(co.getInputTypeProperty().equals(InputType.HIDDEN)
				||co.getInputTypeProperty().equals(InputType.SUPPRESSED)
				||co.getInputTypeProperty().equals(InputType.PASSWORD)
				||columnLabel.equals("&nbsp;")){
					suppressed = true;
				}
			}
			if(PersistenceUtils.isEntityClass(co.field.getType())){
				String idFieldName = PersistenceUtils.getIdFieldName(co.field.getType());
				fieldName = fieldName+AttributeConstants.SEPARATORCLASS+idFieldName;
			}
			if(!suppressed
					&&rsIn.getKeys().contains(fieldName)
				){
				List<Object> column = columns.get(rsIn.getKeys().indexOf(fieldName));
				String selectClassName = co.getProperty(InputProperties.SelectEnum);
				if(!selectClassName.trim().equals("")){
					Class<?> selectClass =null;
					try {
						selectClass = Class.forName(selectClassName);
						if(selectClass.isEnum()){
							AttributeClass acSelect = new AttributeClass(selectClass, view);
							LinkedHashMap<String, String> options = acSelect.getKeyLabelMap();
							for(int i=0;i<column.size();i++){
								if(column.get(i)!=null){
									String value = column.get(i).toString();
									if(!value.trim().equals("")&&options.containsKey(value)){
										value = options.get(value).toString();
									}
									column.set(i, value);									
								}
							}
						}
					} catch (ClassNotFoundException e) {
						Ivy.log().info(AttributeClassRecordset.class.getSimpleName()+" Could'nt find "+selectClass,e);
					}
				}
				rsOut.addColumn(columnLabel, column);
			}
		}
		return rsOut;
	}
}
