package ch.soreco.webbies.common.html.generics;

public class AttributeConstants {
	public static final String CONTENTPREFIX = "0_";
	public static final String SEPARATORCMS = "/";
	public static final String SEPARATORCLASS = ".";
	public static final String SEPARATORCSL = "$";
	public static final String FIELDUID = "serialVersionUID";
	public static final String SEPARATORID = "At";
}
