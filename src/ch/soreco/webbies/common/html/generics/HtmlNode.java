package ch.soreco.webbies.common.html.generics;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class HtmlNode {
	private String tag = "";
	private String innerHtml = "";
	private String outerHtml = "";
	private String attributes="";

	private HashMap<String, String> tagAttributes = new HashMap<String, String>();
	private final List<String> toCloseTags = (List<String>) Arrays.asList("textarea", "select", "label");

	public HtmlNode(String html){
		this.innerHtml = html;
	}		
	public HtmlNode(String tag, String innerHtml, String attributes){
		this.tag = tag;
		this.innerHtml = innerHtml;
		this.attributes = attributes;
	}
	public HtmlNode(String tag, String innerHtml, HashMap<String, String> tagAttributes, String outerHtml){
		this.tag = tag;
		this.innerHtml = innerHtml;
		this.tagAttributes = tagAttributes;
		this.outerHtml = outerHtml;
	}
	public HtmlNode(String tag, String innerHtml, HashMap<String, String> tagAttributes){
		this.tag = tag;
		this.innerHtml = innerHtml;
		this.tagAttributes = tagAttributes;
	}
	public String toHtml(){
		//get Input Field html code
		StringBuilder html = new StringBuilder("");

		if(!this.tag.equals("")){
			cleanseAttributes();
			//set Attributes
			for(String key:this.tagAttributes.keySet()){
				String attributeValue = this.tagAttributes.get(key); 
				html.append(" "+key+"=\""+attributeValue+"\"");
			}
			html.append(" "+this.attributes);
			//prepend Tag
			html.insert(0, "<"+this.tag);
			//append tag close
			html.append(">");
			if(!this.innerHtml.equals("")
					||this.toCloseTags.indexOf(this.tag.toLowerCase())>-1){
				html.append(this.innerHtml);
				html.append("</"+this.tag+">");
			}
		}
		else {
			if(!this.innerHtml.equals("")){
				html.append(this.innerHtml);
			}
		}
		if(!this.outerHtml.equals("")){
			html.append(this.outerHtml);
		}
		html.append("\n");
		return html.toString();
	}
	private void cleanseAttributes(){
		if(this.tag.toLowerCase().equals("li")){
			if(this.tagAttributes.containsKey("valign")){
				this.tagAttributes.remove("valign");
			}
			this.attributes = this.attributes.replace("valign=\"top\"", "");
		}
	}
}
