package ch.soreco.webbies.common.html.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AttributeRow implements Comparable<AttributeRow>{
	public int rowIndex = 0;
	public int sortIndex = 0;
	public String rowKey = "";
	public ArrayList<AttributeColumn> columns = new ArrayList<AttributeColumn>();
	AttributeRow(int rowIndex, String rowKey, ArrayList<AttributeColumn> columns){
		this.columns = columns;
		this.rowKey = rowKey;
		this.rowIndex = rowIndex;		
	}
	AttributeRow(int rowIndex, String rowKey, AttributeColumn column){			
		this.columns.add(column);
		this.rowKey = rowKey;
		this.rowIndex = rowIndex;			
	}
	public void putColumn(AttributeColumn column){
		int index = getColumnIndex(column.columnKey);
		if(index>-1){
			this.columns.set(index, column);
		}
		else {
			this.columns.add(column);
		}
	}
	public AttributeColumn getColumn(String columnKey){
		AttributeColumn column = null;
		int index = getColumnIndex(columnKey);
		if(index>-1){
			column =  this.columns.get(index);
		}
		return column;
	}
	private int getColumnIndex(String columnKey){
		int index = -1;
		for(int i=0;i<this.columns.size();i++){
			if(this.columns.get(i).columnKey.equals(columnKey)){
				index = i;
				break;
			}
		}
		return index;
	}
	public List<String> getFieldNames(){
		List<String> columnNames = new ArrayList<String>();
		for(AttributeColumn column:this.columns){
			String columnName = column.columnKey;
			columnNames.add(columnName);
			//Ivy.log().debug("Table Row "+row.rowKey+" contains Column "+columnName);
		}	
		return columnNames;
	}
	public String toHtml(String enclosingTag){
		StringBuilder html = new StringBuilder("");
		Collections.sort(this.columns);
		for(AttributeColumn column:this.columns){
			html.append(column.toHtml(enclosingTag));
			html.append("\n");
		}
		return html.toString();
	}
	@Override
	public int compareTo(AttributeRow row) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    if(this.rowIndex==row.rowIndex) {
	    	if(this.sortIndex==row.sortIndex) return EQUAL;
	    	if(this.sortIndex<row.sortIndex) return BEFORE;
	    	if(this.sortIndex>row.sortIndex) return AFTER;
	    }
	    if(this.rowIndex<row.rowIndex) return BEFORE;
	    if(this.rowIndex>row.rowIndex) return AFTER;
	    return EQUAL;
	}
}

