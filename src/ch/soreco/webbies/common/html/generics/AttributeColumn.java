package ch.soreco.webbies.common.html.generics;

public class AttributeColumn  implements Comparable<AttributeColumn>{
	public String columnKey = "";
	private AttributeInput input= null;
	private HtmlNode inputNode = null;
	private AttributeLabel label=null;
	private HtmlNode labelNode = null;
	private String html="";
	public int ordinal = 0;
	AttributeColumn(int ordinal 
			, AttributeInput input
			, AttributeLabel label){
		//as inputs may contains enum fields add its content fieldname too
		//to make it unique within class
		this.columnKey = input.fieldName;
		this.ordinal = ordinal;
		this.label = label;
		this.input = input;
	}
	AttributeColumn(int ordinal, String nodeKey, String html){
		this.ordinal = ordinal;
		this.columnKey = nodeKey;
		this.html = html;
	}
	AttributeColumn(int ordinal
			, AttributeLabel label){
		this.columnKey = label.content.fieldName;
		this.ordinal = ordinal;
		this.label = label;
	}
	AttributeColumn(int ordinal
			, AttributeInput input){
		this.columnKey = input.fieldName;
		this.ordinal = ordinal;
		this.input = input;
	}
	private HtmlNode getInputCellNode(String enclosingTag){
		HtmlNode inputNode = null;
		String inputHtml = getInputHtml();
		if(enclosingTag.equals("")){
			inputNode = new HtmlNode(inputHtml);
		}
		else {
			String inputCellAttributes = "valign=\"top\" ";
			if(this.input!=null
					&&this.input.content!=null){
				inputCellAttributes = inputCellAttributes+this.input.content.getProperty(InputProperties.InputCellAttributes);
			}
			inputNode = new HtmlNode(enclosingTag, inputHtml, inputCellAttributes);						
		}
		return inputNode;
	}
	private String getInputHtml(){
		String inputHtml ="";
		if(this.input!=null){
			inputHtml =  this.input.toHtml();
		}
		else if(this.inputNode!=null) {
			inputHtml = this.inputNode.toHtml();
		}
		return inputHtml;
	}
 	private HtmlNode getLabelCellNode(String enclosingTag){
		HtmlNode labelNode =null;
		if(enclosingTag.equals("")){
			//no parent defined for the label
			//nod need to apply cell properties
			labelNode = getLabelNode();				
		}
		else {
			String labelHtml = "";
			if(getLabelNode()!=null){
				labelHtml = getLabelNode().toHtml();				
			}
			String labelCellAttributes = "";
			if(this.label!=null){
				labelCellAttributes=this.label.content.getProperty(InputProperties.LabelCellAttributes);
			}
			if(this.input!=null){
				labelCellAttributes = labelCellAttributes
					+"valign=\"top\" "
					+"for=\""+this.input.inputId+"\" "
					+"style=\"white-space: nowrap;"
						+(this.input.inputName.equals("")? "background-color:red;":"")
						+(this.input.inputType.equals(InputType.CHECKBOX)? "padding-top: 5px;":"")
						+(this.input.inputType.equals(InputType.RADIO)? "padding-top: 5px;":"")
					+"\" ";
			}
			labelNode = new HtmlNode(enclosingTag, labelHtml, labelCellAttributes);			
		}
		return labelNode;
	}
	private HtmlNode getLabelNode(){
		HtmlNode labelNode =null;
		if(this.label!=null){
			labelNode = this.label.toHtmlNode();			
		}
		else {
			labelNode = this.labelNode;
		}
		return labelNode;
	}
	public String toHtml(String enclosingTag){
		StringBuilder html = new StringBuilder("");
		HtmlNode inputCell = null;
		if(this.input!=null||this.inputNode!=null){
			inputCell = getInputCellNode(enclosingTag);
		}
		HtmlNode labelCell = null;
		if(this.label!=null||this.labelNode!=null){
			labelCell = getLabelCellNode(enclosingTag);
		}
		
		if(this.label!=null){
			switch (this.label.getLabelPosition()){
			case NONE:
				if(inputCell!=null){
					html.append(inputCell.toHtml());
					html.append("\n");					
				}
				break;
			case RIGHT:
				if(inputCell!=null){
					html.append(inputCell.toHtml());
					html.append("\n");					
				}
				if(labelCell!=null){
					html.append(labelCell.toHtml());
					html.append("\n");					
				}
				break;				
			default:
				if(labelCell!=null){
					html.append(labelCell.toHtml());
					html.append("\n");					
				}
				if(inputCell!=null){
					html.append(inputCell.toHtml());
					html.append("\n");					
				}
				break;
			}
		}
		else if(this.input!=null){
			switch (this.input.inputType){
			case HIDDEN:
				html.append(inputCell.toHtml());	
				html.append("\n");
				break;
			case CHECKBOX:
				html.append(inputCell.toHtml());
				html.append("\n");
				if(labelCell!=null){
					html.append(labelCell.toHtml());
					html.append("\n");					
				}
				break;
			case RADIO:
				html.append(inputCell.toHtml());
				html.append("\n");
				if(labelCell!=null){
					html.append(labelCell.toHtml());
					html.append("\n");					
				}
				break;
			default:
				if(labelCell!=null){
					html.append(labelCell.toHtml());
					html.append("\n");					
				}
				html.append(inputCell.toHtml());
				html.append("\n");
				break;			
			}
						
		}
		else {
			if(labelCell!=null){
				html.append(labelCell.toHtml());
				html.append("\n");				
			}
			if(inputCell!=null){
				html.append(inputCell.toHtml());
				html.append("\n");				
			}
			if(this.html.trim().length()>0){
				HtmlNode node = new HtmlNode(enclosingTag, this.html, "");
				html.append(node.toHtml());
			}	
		}
		return html.toString();
	}
	@Override
	public int compareTo(AttributeColumn column) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    if(this.ordinal==column.ordinal) return EQUAL;
	    if(this.ordinal<column.ordinal) return BEFORE;
	    if(this.ordinal>column.ordinal) return AFTER;
	    return EQUAL;
	}
}
