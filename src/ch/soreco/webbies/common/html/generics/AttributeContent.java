package ch.soreco.webbies.common.html.generics;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.soreco.webbies.common.cms.CMSUtils;
import ch.soreco.webbies.common.db.EntityAssociationType;
import ch.soreco.webbies.common.db.PersistenceMetaClass;
import ch.soreco.webbies.common.db.PersistenceMetaField;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.eclipse.Classes;
import ch.soreco.webbies.common.util.CompositeUtils;
import ch.soreco.webbies.common.util.XmlProperties;

public class AttributeContent implements Comparable<AttributeContent> {
	public Integer ordinal = 0;
	public Boolean isAttributeContent = false;
	public Boolean isClassContent = false;
	public Field field = null;
	public Class<?> clazz = null;
	public String fieldName ="";
	public String className = "";
	
	private String contentUri="";
	private IContentObject attributeContentFolder=null;

	private String viewName = AttributeConstants.CONTENTPREFIX;

	private java.util.List<AttributeContentType> attibuteContentTypes = null;
	private HashMap<AttributeContentType, IContentObject> attributeContentTypesMap = null; 
	
	private XmlProperties viewProperties = null;
	private XmlProperties classProperties = null;
	private AttributeContent compositeContent =null;
	private List<String> views = null;
	//private AttributeContent classContent =null;
	private String getUser(){
		String user = "";
		try {
			user = Ivy.session().getSessionUserName();
			//Ivy.session().getActiveEnvironment();
			//Ivy.session().getSessionUser().getDisplayName();
		} catch (Exception e) {
			Ivy.log().debug("Error at Attribute Content - getSessionUserName", e);
		}		
		return user;
	}
	public String getView(){
		return this.viewName;
	}
	private void setView(String name){
		if(!this.viewName.equals(name)){
			this.viewName = name;
			this.loadTypes();			
		}
	}
	public AttributeContent(String view, Field attributeField){
		this.viewName = view;
		loadAttributeContentByField(attributeField);
	}
	public AttributeContent(Field attributeField){
		loadAttributeContentByField(attributeField);
	}
	public AttributeContent(Class<?> clazz){
		this.loadClassContent(clazz);
	}
	public AttributeContent(String view,Class<?> clazz){
		this.viewName = view;
		this.loadClassContent(clazz);
	}
	private void loadAttributeContentByField(Field attributeField){
		this.field = attributeField;
		this.fieldName = this.field.getName();
		this.clazz = this.field.getDeclaringClass();
		this.className = field.getDeclaringClass().getName();
		this.contentUri = getUri();
		
		this.attributeContentFolder = this.mkdir();

		if(this.attributeContentFolder!=null){
			this.setView(this.viewName);
			if(!this.exists(AttributeContentType.NAME)){
				String defaultName = this.fieldName.substring(0, 1).toUpperCase()+this.fieldName.substring(1);
				this.setType(AttributeContentType.NAME, defaultName);
			}
			
			try {
				this.isAttributeContent = true;
				this.ordinal = this.attributeContentFolder.getVisualOrder();
			} catch (PersistencyException e) {
				Ivy.log().debug("Error at Attribute Content - getDescription", e);
			}
		}
	}
	private void loadClassContent(Class<?> clazz){
		this.clazz = clazz;
		this.className = clazz.getName();
		this.contentUri = this.getUri();
		
		this.attributeContentFolder = this.mkdir();

		if(this.attributeContentFolder!=null){
			this.setView(this.viewName);
			if(!this.exists(AttributeContentType.NAME)){
				this.setType(AttributeContentType.NAME, this.clazz.getSimpleName());
			}
			try {
				this.isAttributeContent = false;
				this.isClassContent = true;
				this.ordinal = this.attributeContentFolder.getVisualOrder();
			} catch (PersistencyException e) {
				Ivy.log().debug("Error at Attribute Content - getDescription", e);
			}
		}
	}
	
	private String getUri(){
		if(this.contentUri.equals("")){
			String uri = AttributeConstants.SEPARATORCLASS
								+this.clazz.getName();
			if(this.field!=null){
				uri = uri
								+AttributeConstants.SEPARATORCLASS
								+this.field.getName();
			}
			this.contentUri = uri.replace(AttributeConstants.SEPARATORCLASS, AttributeConstants.SEPARATORCMS);
		}
		return this.contentUri;
	}
	public Boolean isComposite(){
		Boolean isComposite = false;
		if(this.field!=null){
			isComposite = CompositeUtils.isComposite(this.field);			
		}
		return isComposite;
	}
	public Boolean isEntity(){
		Boolean isEntity = false;
		if(isComposite()){
			isEntity = PersistenceUtils.isEntityClass(this.field.getType());			
		}
		return isEntity;
	}
	public AttributeContent getCompositeContent(){
		if(this.isComposite()
				&&this.compositeContent==null){
			this.compositeContent = new AttributeContent(this.field.getType());
		}
		return this.compositeContent;
	}
	/*
	private AttributeContent getClassContent(){
		if(this.classContent==null){
			this.classContent = new AttributeContent(this.clazz);
		}
		return this.classContent;
	}
	*/
	public void setAttributeVisualOrder(int ordinal) throws PersistencyException{
		this.ordinal = ordinal;
		if(this.attributeContentFolder!=null){
			this.attributeContentFolder.setVisualOrder(ordinal, getUser());			
		}
		this.attributeContentFolder.touch(getUser());
	}
	private void loadTypes(){
		if(this.attributeContentFolder!=null){
			this.attibuteContentTypes = new ArrayList<AttributeContentType>();
			this.attributeContentTypesMap = new HashMap<AttributeContentType, IContentObject>();
			String mappedByView = AttributeConstants.CONTENTPREFIX;
			for(String mappedView :this.getViews()){
				if(getView().contains(mappedView)){
					mappedByView = mappedView;
					break;
				}
			}
			try {
				//Loop all childs and find the appropriate type
				for(IContentObject child:this.attributeContentFolder.getChildren()){
					for(AttributeContentType type:AttributeContentType.values()){
						String viewName = getView()+type.toString().toLowerCase();
						String defaultName = AttributeConstants.CONTENTPREFIX+type.toString().toLowerCase();
						String mappedByViewName = mappedByView+type.toString().toLowerCase();
						if(viewName.equals(child.getName())){
							this.attibuteContentTypes.add(type);
							this.attributeContentTypesMap.put(type, child);
							break;																
						}
						else if(mappedByViewName.equals(child.getName())){
							this.attibuteContentTypes.add(type);
							this.attributeContentTypesMap.put(type, child);
							break;																
						}
						else if(defaultName.equals(child.getName())){
							this.attibuteContentTypes.add(type);
							this.attributeContentTypesMap.put(type, child);
							break;																
						}
					}
				}
			} catch (Exception e){
				Ivy.log().debug("Error at Attribute Content - Loading Available Types", e);					
			}
		}
	}
	private HashMap<AttributeContentType, IContentObject> getTypesMap(){
		if(this.attributeContentTypesMap==null){
			this.loadTypes();
		}
		return this.attributeContentTypesMap;
	}

	public String getLabelAndIcon(){

		if(!this.exists(AttributeContentType.NAME)){
			if(this.isClassContent){
				this.setType(AttributeContentType.NAME, this.clazz.getSimpleName());										
			}
			else {
				this.setType(AttributeContentType.NAME, this.fieldName);										
			}	
		}
		String label = this.getType(AttributeContentType.NAME);
		if(this.exists(AttributeContentType.ICON)){
			String icon = this.getTypeMakro(AttributeContentType.ICON);
			if(!icon.equals("")){
				label = icon+ "&nbsp;"+label;
			}			
		}
		return label;
	}
	public String getLabel(){
		return getType(AttributeContentType.NAME);
	}
	public String getType(AttributeContentType type){
		String content = "";
		if(this.getTypesMap().containsKey(type)){			
			if(type.equals(AttributeContentType.ICON)){
				content = this.getTypeMakro(type);				
			}
			else if(type.equals(AttributeContentType.HELPFILE)){
				content = this.getTypeMakro(type);				
			}
			else if(type.equals(AttributeContentType.HELPFILE2)){
				content = this.getTypeMakro(type);				
			}
			else {
				String uri = "";
				try {
					uri = this.getTypesMap().get(type).getUri();
				} catch (PersistencyException e) {
					Ivy.log().error(this.getClass().getSimpleName()+" Error:Could'nt get "+type+" for "+this.getUri());
				}
				content = Ivy.cms().co(uri);
			}
		}		
		return content;
	}

	public Boolean exists(AttributeContentType type){
		return this.getTypesMap().containsKey(type);
	}
	private String getTypeMakro(AttributeContentType type){
		String content = "";
		if(this.exists(type)){
			try {
				//String contentTypeUri = this.getTypesMap().get(type).getUri();
				IContentObject co = this.getTypesMap().get(type);//Ivy.cms().getContentObject(contentTypeUri);				
				if(co!=null&&co.hasValues()){
					content = "<%=ivy.cms.co(\""+co.getUri()+"\")%>";
				}
			} catch (Exception e) {
				Ivy.log().debug(e);
			}			
		}
		return content;
	}
	public String setType(AttributeContentType type, File contentFile){
		String contentName = getView()+type.toString().toLowerCase();
		try {
			CMSUtils.setContent(this.contentUri, contentName, contentFile);
			this.loadTypes();
		} catch (EnvironmentNotAvailableException e) {
			Ivy.log().error(e);
		} catch (PersistencyException e) {
			Ivy.log().error(e);
		} catch (FileNotFoundException e) {
			Ivy.log().error(e);
		}
		
		return this.getType(type);
	}
	public String setType(AttributeContentType type, String content){
		String contentName = getView()+type.toString().toLowerCase();
		CoType contentType = CoType.STRING;
		if(type.equals(AttributeContentType.SOURCE)){
			contentType = CoType.SOURCE;
		}
		try {
			CMSUtils.setContent(contentType, this.contentUri, contentName, content);
			this.loadTypes();
		} catch (EnvironmentNotAvailableException e) {
			Ivy.log().error(e);
		} catch (PersistencyException e) {
			Ivy.log().error(e);
		}
		
		return this.getType(type);
	}
	private IContentObject mkdir(){
		IContentObject co = null;		
		if(!this.getUri().equals("")){
			try {
				//try to find attribute content->name object directly
				co = CMSUtils.mkdirs(this.getUri());		
			} catch (Exception e) {
				Ivy.log().error("Error on getContentObject "+this.getUri()+": "+e.getMessage(), e);	
			}						
		}
		return co;
	}

	private XmlProperties getViewProperties(){
		if(this.viewProperties==null){
			if(this.isClassContent){
				this.viewProperties = new XmlProperties(this.clazz, this.getView());
				if(this.getView()!=AttributeConstants.CONTENTPREFIX
						&&this.viewProperties.getProperties().size()==0){
					Ivy.log().info(this.getClass().getSimpleName()+" Properties for view "+this.getView()+" and item "+this.contentUri+" are undefined, using default instead.");
					XmlProperties defaultViewProperties = new XmlProperties(this.clazz, AttributeConstants.CONTENTPREFIX);
					if(defaultViewProperties.getProperties().size()>0){
						this.viewProperties.setProperties(defaultViewProperties.getProperties());											
						this.viewProperties.setComment("AttributeContent View Properties - Default View");
						this.viewProperties.save();
					}
					else {
						this.viewProperties.setProperties(this.getDefaultProperties());
						this.viewProperties.setComment("AttributeContent View Properties - Defaults");
						this.viewProperties.save();
					}
				}
			}
			else {
				this.viewProperties = new XmlProperties(this.field, this.getView());
				if(this.getView()!=AttributeConstants.CONTENTPREFIX
						&&this.viewProperties.getProperties().size()==0){
					Ivy.log().info(this.getClass().getSimpleName()+" Properties for view "+this.getView()+" and item "+this.contentUri+" are undefined, using default instead.");
					XmlProperties defaultViewProperties = new XmlProperties(this.field, AttributeConstants.CONTENTPREFIX);
					if(defaultViewProperties.getProperties().size()>0){
						this.viewProperties.setProperties(defaultViewProperties.getProperties());											
						this.viewProperties.setComment("AttributeContent View Properties - From Default View");
						this.viewProperties.save();
					}
					else {
						this.viewProperties.setProperties(this.getDefaultProperties());
						this.viewProperties.setComment("AttributeContent View Properties - Defaults");
						this.viewProperties.save();
					}
				}
			}
		}
		return this.viewProperties;
	}
	private XmlProperties getClassProperties(){
		if(this.classProperties==null){
			this.classProperties = new XmlProperties(this.clazz);
		}
		return this.classProperties;
	}
	public List<String> getViews(){
		if(this.views==null){
			this.views = new ArrayList<String>();
			this.views.add(AttributeConstants.CONTENTPREFIX);
			String property = this.getProperty(ClassProperties.CLASS_VIEWS);
			if(!property.equals("")){
				String[] views = property.split(",");
				for(int i=0;i<views.length;i++){
					if(!this.views.contains(views[i])){
						this.views.add(views[i].trim().replace(" ", ""));					
					}
				}				
			}
		}
		return this.views;
	}
	public void resetProperties(){
		this.resetViewProperties();
	}
	private void resetViewProperties(){
		this.viewProperties.setProperties(this.getDefaultProperties());
		this.viewProperties.setComment("AttributeContent View Properties - Defaults");
		this.viewProperties.save();		
	}
	private Properties getDefaultProperties(){
		Properties properties = new Properties();
		PersistenceMetaClass entityMeta = null;
		PersistenceMetaField entityField = null;
		HashMap<String, String> enums = new HashMap<String, String>();
		for(Class<?> clazz:Classes.getAllEnums()){
			enums.put(clazz.getSimpleName().toLowerCase(), clazz.getName());
		}
		
		if(PersistenceUtils.isEntityClass(this.clazz)){
			entityMeta = PersistenceUtils.getMetaClass(this.clazz);
			entityField = entityMeta.getMetaField(this.fieldName);
		}	
		if(this.isAttributeContent){
			for(InputProperties property:InputProperties.values()){
				switch (property){
				case LabelPosition:
					if(Boolean.class.isAssignableFrom(this.field.getType())){
						properties.setProperty(property.toString(), LabelPosition.RIGHT.toString());
					}
					else {
						properties.setProperty(property.toString(), LabelPosition.LEFT.toString());
					}
					break;
				case InputType:
					if(Boolean.class.isAssignableFrom(this.field.getType())){
						properties.setProperty(property.toString(), InputType.CHECKBOX.toString());
					}
					//Present enclosingClass is a Entity Class
					if(entityField!=null){
						if(entityField.isId()){
							properties.setProperty(property.toString(), InputType.HIDDEN.toString());							
						}
						else if(entityField.isGenerated()){
							properties.setProperty(property.toString(), InputType.LABEL.toString());														
						}
						else {
							if(entityField.isAssociated()&&entityField.getAssociationType().equals(EntityAssociationType.OneToMany)){
								properties.setProperty(property.toString(), InputType.SELECT.toString());							
							}
							else if(entityField.isAssociated()&&entityField.getAssociationType().equals(EntityAssociationType.OneToOne)){
								//TODO: Develop a Composite Form expander
								properties.setProperty(property.toString(), InputType.FORMPART.toString());//Map to FormPart		
							}
							else if(entityField.isAssociated()&&entityField.getAssociationType().equals(EntityAssociationType.ManyToOne)){
								//TODO: Develop a Composite List expander
								properties.setProperty(property.toString(), InputType.SELECT.toString());						
							}
							else if(entityField.isAssociated()&&entityField.getAssociationType().equals(EntityAssociationType.ManyToMany)){
								//TODO: Develop a Composite List expander
								properties.setProperty(property.toString(), InputType.SUPPRESSED.toString());//Map to new Form							
							}
							else if(String.class.isAssignableFrom(this.field.getType())){
								if(enums.containsKey(this.fieldName.toLowerCase())){
									properties.setProperty(property.toString(), InputType.SELECT.toString());							
								}
								else if(entityField.getLength()>255){
									properties.setProperty(property.toString(), InputType.TEXTAREA.toString());															
								}

							}							
						}
					}
					else if(this.isEntity()){
						//the Field is of Type Entity Class
						properties.setProperty(property.toString(), InputType.SELECT.toString());
					}
					//Present field is composite class which probably will be overwritten by other class form.
					else if(this.isComposite()){
						properties.setProperty(property.toString(), InputType.SUPPRESSED.toString());						
					}

					break;
				case Required:
					if(entityField!=null){
						if(!entityField.isGenerated()
								&&!entityField.isNullable()){
							properties.setProperty(property.toString(), "true");							
						}						
					}
					break;
				case SelectEnum:
					if(this.isEntity()){
						properties.setProperty(property.toString(), this.field.getType().getName());
					}
					else if(String.class.isAssignableFrom(this.field.getType())){
						if(enums.containsKey(this.fieldName.toLowerCase())){
							String enumClass = enums.get(this.fieldName.toLowerCase());
							properties.setProperty(property.toString(), enumClass);							
						}
					}
					break;
				case Length:
					if(entityField!=null){
						if(entityField.getLength()>0){
							properties.setProperty(property.toString(), entityField.getLength().toString());							
						}						
					}
					break;
				default:
					break;
				}
			}
		}
		return properties;
	}
	public String getProperty(ClassProperties propertyType){
		return this.getClassProperties().getProperty(propertyType.toString());
	}
	public String getProperty(InputProperties propertyType){
		return this.getViewProperties().getProperty(propertyType.toString());
	}
	public String getProperty(ViewProperties propertyType){
		return this.getViewProperties().getProperty(propertyType.toString());
	}
	public void setProperty(ClassProperties propertyType, String value){
		this.getClassProperties().setProperty(propertyType.toString(), value);			
	}
	public void setProperty(InputProperties propertyType, String value){
		this.getViewProperties().setProperty(propertyType.toString(), value);
	}
	public void setProperty(ViewProperties propertyType, String value){
		this.getViewProperties().setProperty(propertyType.toString(), value);
	}
	public InputType getInputTypeProperty(){
		InputType inputType = InputType.UNDEFINED;
		String aInputType = (String) this.getProperty(InputProperties.InputType);
		if(!aInputType.equals("")){
			try {
				inputType = InputType.valueOf(aInputType);													
			} catch (Exception e){
				//input type could not matched
				inputType = InputType.UNDEFINED;
			}
		}
		return inputType;
	}
	
	public void storeProperties(){
		this.getViewProperties().save();
		this.getClassProperties().save();
	}
	@Override
	public int compareTo(AttributeContent aco) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    if(this.ordinal==aco.ordinal) return EQUAL;
	    if(this.ordinal<aco.ordinal) return BEFORE;
	    if(this.ordinal>aco.ordinal) return AFTER;
	    return EQUAL;
	}
	
}
