package ch.soreco.webbies.common.html.generics;

public enum LabelPosition {
	NONE, 
	LEFT, 
	RIGHT
}
