package ch.soreco.webbies.common.html.generics;

import java.util.HashMap;

import ch.ivyteam.ivy.scripting.objects.List;


public class HtmlInput {
	private InputType inputType = InputType.UNDEFINED;
	private String tag = "input";
	private String inputId = "";
	private String inputName = "";
	private Boolean isDisabled = false;
	private String inputValue = "";
	private String objectValue  = "";		
	private String innerHtml = "";//used for textarea and options
	private String outerHtml = "";
	private Boolean skipName = false;
	
	private HashMap<String, String> tagAttributes = new HashMap<String, String>();
	public HtmlInput(InputType inputType, String inputName, String objectValue){
		this.inputType = inputType;		
		this.inputName = inputName;		
		this.inputValue = objectValue;
		this.objectValue = objectValue;
		this.init();
	}
	public HtmlInput( String inputName
			, String objectValue
			, List<?> optionValues){
		this.inputType = InputType.SELECT;		
		this.inputName = inputName;		
		this.objectValue = objectValue;
		this.init();
		HashMap<String, String> options = new HashMap<String, String>();
		for(int i=0;i<optionValues.size();i++){
			String key = optionValues.get(i).toString();
			options.put(key, key);
		}
		this.innerHtml = getSelectOptions(options);
	}
	public HtmlInput( String inputName
			, String objectValue
			, HashMap<String, String> options){
		this.inputType = InputType.SELECT;		
		this.inputName = inputName;		
		this.objectValue = objectValue;
		this.init();
		this.innerHtml = getSelectOptions(options);
	}
	public HtmlInput(InputType inputType, 
			HashMap<String, String> inputAttributes
			, String inputId
			, String inputName
			, Boolean isDisabled
			, String inputValue
			, String objectValue 
			, String innerHtml //Option: labelAndIcon, Label:enumValueLabel or objectValue
			, Boolean skipName
			){
		this.inputType = inputType;
		this.tagAttributes.putAll(inputAttributes);
		this.inputId = inputId;
		this.inputName = inputName;		
		this.isDisabled = isDisabled;
		this.inputValue = inputValue;
		this.objectValue = objectValue;
		this.innerHtml = innerHtml;	
		this.skipName = skipName;
		this.init();
	}
	private String getSelectOptions(HashMap<String, String> optionMap){
		StringBuilder options = new StringBuilder("");
		for(String inputValue:optionMap.keySet()){
			String tag ="option";
			String innerHtml = optionMap.get(inputValue);
			HashMap<String, String> tagAttributes = new HashMap<String, String>();
			tagAttributes.put("value", inputValue);				
			if(this.objectValue.trim().toLowerCase().equals(inputValue.trim().toLowerCase())){
				tagAttributes.put("selected", "selected");
			}
			options.append(new HtmlNode(tag, innerHtml, tagAttributes).toHtml());			
		}
		return options.toString();		
	}
	private void initTagAttributes(){
		if(!this.tagAttributes.containsKey("id")
				&&!this.inputId.equals("")){
			this.tagAttributes.put("id", this.inputId);			
		}
		if(!this.tagAttributes.containsKey("class")){
			this.tagAttributes.put("class", this.inputType.toString());			
		}
		if(!this.tagAttributes.containsKey("value")){
			this.tagAttributes.put("value", this.objectValue);			
		}
		if(!this.skipName){
			this.tagAttributes.put("name", this.inputName);			
		}
	}
	private void init(){
		this.initTagAttributes();
		//we may need the disabling tag, try not to use this
		//as it results in wrapping inconsistencies in List Objects
		if(this.isDisabled&&!this.inputType.equals(InputType.HIDDEN)){
			if(this.inputType.equals(InputType.TEXTAREA)){
				this.tagAttributes.put("disabled", "disabled");								
			}
			else {
				this.tagAttributes.put("readonly", "readonly");
			}
		}		
		//now choose the html input type
		switch(this.inputType) {
			case HIDDEN: 
				this.tagAttributes.put("type", "hidden");				
				break;
			case PASSWORD: 
				this.tagAttributes.put("type", "password");				
				break;
			case SELECT:
				this.tag = "select";
				if(this.innerHtml.equals("")){
					this.innerHtml = "<option value=\"\"></option>";
				}
				//the inner HTML would by set by Parent AttributeInput class
				break;
			case OPTION:
				this.tag ="option";
				this.tagAttributes.put("value", this.inputValue);
				if(this.tagAttributes.containsKey("name")){
					this.tagAttributes.remove("name");			
				}
				if(this.objectValue.trim().toLowerCase().equals(this.inputValue.trim().toLowerCase())){
					this.tagAttributes.put("selected", "selected");
				}
				if(this.innerHtml.trim().equals("")){
					this.innerHtml = this.inputValue;
				}
				break;
			case RADIO:
				this.tagAttributes.put("type", "radio");
				this.tagAttributes.put("value", this.inputValue);
				//overwrite id as the radio does needs the name attribute for grouping
				//a hidden input is appended which will be filled by jQuery
				if(this.inputId.trim().equals("")){
					this.inputId = this.inputName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID);
				}
				this.tagAttributes.put("id", this.inputId+AttributeConstants.SEPARATORID+this.inputValue+AttributeConstants.SEPARATORID+inputType.toString());
				//overwrite name on same reason
				this.tagAttributes.put("name", this.inputId.toLowerCase()+AttributeConstants.SEPARATORID+inputType.toString()+"GROUP");
				
				this.tagAttributes.put("forInput", this.inputId);
				//use with jQuery
				this.tagAttributes.put("onclick", "$('#"+this.inputId+"').attr('name','"+this.inputName+"').val(this.value);");
				//use with JavaScript				
				//this.tagAttributes.put("onclick", "this.form."+this.inputName+".value=this.value;");

				//as on radios the values are sent to a hidden field
				//the field name is used as value not the object value which represents
				//only the actual choosen value

				//we added an Enum Class containing TRUE, FALSE
				//to use AttributeContent for the case that Boolean values 
				//result to a Triple state: true, false or null (as long the 
				//Boolean not has been touched or initialised)
				//we can use this within a visum workflow								
				if(this.objectValue.trim().toLowerCase().equals(this.inputValue.trim().toLowerCase())){
					this.tagAttributes.put("checked", "checked");
				}
				break;
				
			case CHECKBOX: 
				//as this input need to give back a value
				//to the server add a hidden field
				this.outerHtml = this.getHiddenField();

				this.tagAttributes.put("type", "checkbox");
				if(this.inputId.trim().equals("")){
					this.inputId = this.inputName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID);
				}				
				if(this.tagAttributes.containsKey("name")){
					this.tagAttributes.remove("name");			
				}
				//overwrite id as the checkbox does not allow value attribute
				//a hidden input is appended which will be filled by jQuery
				this.tagAttributes.put("id", this.inputId+AttributeConstants.SEPARATORID+inputType.toString());
				this.tagAttributes.put("forInput", this.inputId);
				if(this.objectValue.trim().equals("true")){
					this.tagAttributes.put("checked", "checked");
				}
				if(this.tagAttributes.containsKey("value")){
					this.tagAttributes.remove("value");						
				}
				//choosen to use jQuery code, as the javascript code did'nt work fine on long names
				this.tagAttributes.put("onclick", "$('#"+this.inputId+"').attr('value', this.checked);");
				//use with JavaScript				
				//this.tagAttributes.put("onclick", "this.form."+this.inputName+".value=this.checked;");
				
				break;
			case LABEL:
				//as a label need to give back a value
				//to the server add a hidden field
				this.outerHtml = this.getHiddenField();
				//now set up the displayed label
				this.tag = "label";
				if(this.tagAttributes.containsKey("name")){
					this.tagAttributes.remove("name");					
				}
				if(this.tagAttributes.containsKey("value")){
					this.tagAttributes.remove("value");					
				}
				if(this.inputId.trim().equals("")){
					this.inputId = this.inputName.replace(AttributeConstants.SEPARATORCSL, AttributeConstants.SEPARATORID);
				}
				this.tagAttributes.put("id", this.inputId+AttributeConstants.SEPARATORID+inputType.toString());
				if(this.innerHtml.trim().equals("")){
					this.innerHtml=this.inputValue;
				}

				
				break;
			case TEXTAREA: 
				this.tag ="textarea";
				this.tagAttributes.put("rows", "3");
				this.tagAttributes.put("cols", "80");
				if(this.tagAttributes.containsKey("value")){
					this.tagAttributes.remove("value");						
				}
				this.innerHtml = this.objectValue;
				break;
			default: 
				this.tagAttributes.put("type", "text");
				break;
		}		
		
	}
	private String getHiddenField(){
		String tag = "input";
		HashMap<String, String> attributes = this.tagAttributes;
		this.tagAttributes.put("type", "hidden");
		if(this.isDisabled){
			if(attributes.containsKey("disabled")){
				attributes.remove("disabled");
			}
			else if(attributes.containsKey("readonly")){
				attributes.remove("readonly");				
			}
		}
		return new HtmlNode(tag, "", attributes).toHtml();
	}
	public HtmlNode toHtmlNode(){
		return new HtmlNode(this.tag
							, this.innerHtml
							, this.tagAttributes
							, this.outerHtml);
	}
}
