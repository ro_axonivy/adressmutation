package ch.soreco.webbies.common.html.smarttable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import ch.ivyteam.ivy.cm.CoType;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.persistence.PersistencyException;
import ch.ivyteam.ivy.scripting.objects.Recordset;

@SuppressWarnings("restriction")
public class SmartTableFilter {	
	HttpServletRequest request = null;
	private static String FILTER_SOURCE = "TABLE_SOURCE";
	private static String FILTER_NAME = "TABLE_NAME";
	private String[] currentTableFilters = null;
	private String[] currentFilterNames = null;
	private String NAME_DELIMINITER = "$";
	private static SmartTableContext currentContext = null;
	private static HashMap<String, SmartTableContext> contexts = new HashMap<String, SmartTableContext>();
	
 	private class SmartTableContext{
		String contextUri = "";
		HashMap<String, SmartTableTable> tables = new HashMap<String, SmartTableTable>();
		SmartTableContext() throws EnvironmentNotAvailableException, PersistencyException{
			this.contextUri = Ivy.html().getCurrentContentPageUri();
			IContentObject currentPage = Ivy.cms().getContentObject(Ivy.html().getCurrentContentPageUri());			
			for(IContentObject pageChild:currentPage.getChildren()){
				if(pageChild.getContentObjectType().getCoType().equals(CoType.PANEL)){
					for(IContentObject panelChild:pageChild.getChildren()){
						loadFromContentObject(panelChild);
					}
				}
			}
		}
		private void loadFromContentObject(IContentObject co) throws EnvironmentNotAvailableException, PersistencyException{
			if(co.getContentObjectType().getCoType().equals(CoType.SMART_TABLE)){
				for(IContentObjectValue value :co.getValues()){
					Properties properties = new Properties();
					try {
						properties.load(value.getContentAsCharacterStream());
					} catch (UnsupportedOperationException e) {
						Ivy.log().debug("Error on loading SmartTable Properties:"+e.getMessage(), e);
					} catch (IOException e) {
						Ivy.log().debug("Error on loading SmartTable Properties:"+e.getMessage(), e);
					}
					SmartTableTable smartTableTable = new SmartTableTable(co.getName(), properties);
					this.tables.put(co.getName(), smartTableTable);
				}
			}			
		}
	}
 	@SuppressWarnings("unused")
	private class SmartTableTable{
		Boolean headerShow = false;
		String defaultPageSize = "";
		Long defaultPageSizeNumber = new Long(-1);
		String tableEmptyValue = "";
		String defaultSort="";
		String tableCaption="";
		String tableSource = "";
		Boolean footerShow = false;
		Boolean autoHeader = false;
		String name = "";
		Recordset rsByVal = null;
		Recordset rsByRef = null;
		Boolean isFiltered = false;
		Boolean hasFilters = true;
		Boolean loadByVal = false;
		int found = 0;
		
		HashMap<String, SmartTableColumn> filters = new HashMap<String, SmartTableColumn>();
		HashMap<String, SmartTableColumn> colums = new HashMap<String, SmartTableColumn>();
		SmartTableTable(String name, Properties properties){
			this.name = name;
			loadFromCMSProperties(properties);
		}
		public void loadRecordset(){
			if(!this.tableSource.equals("")){
				Object obj = Ivy.html().getObject(this.tableSource);
				
				if (obj instanceof Recordset) {
					Recordset rs = (Recordset) obj;
					this.rsByRef = rs;
					this.rsByVal = rs.clone();
					//Ivy.log().debug("Set initial Recordset with size:"+this.size);
				}
			}			
		}
		private void loadFromCMSProperties(Properties properties){
			
		    for (String key :properties.stringPropertyNames()) {
		    	if(key.startsWith("col")){
		    		Properties columnProperties = new Properties();
		    		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(properties.getProperty(key).getBytes());
		    		try {
						columnProperties.load(byteArrayInputStream);
					} catch (IOException e) {
						Ivy.log().debug("Error on loading SmartTable Column Properties:"+e.getMessage(), e);
					}
					SmartTableColumn column = new SmartTableColumn(columnProperties, key);
					if(column.hasName){
						this.colums.put(column.name, column);						
					}
					//Ivy.log().debug("Loaded Smart Table Column Class:"+column.name+" "+column.ordinal);
		    	} else {
		    		if(key.equals("header.show.value")){
		    			this.headerShow = Boolean.parseBoolean(properties.getProperty(key));
		    		}
		    		else if(key.equals("footer.show.value")){
		    			this.footerShow = Boolean.parseBoolean(properties.getProperty(key));
		    		}
		    		else if(key.equals("auto.header.value")){
		    			this.autoHeader = Boolean.parseBoolean(properties.getProperty(key));
		    		}
		    		else if(key.equals("table.default.page.size.value")){
		    			this.defaultPageSize = properties.getProperty(key);
		    			try {
		    			this.defaultPageSizeNumber = Long.parseLong(this.defaultPageSize);
		    			} catch (Exception e){
		    				this.defaultPageSizeNumber = new Long(-1);
		    			}
		    		}
		    		else if(key.equals("default.sort.value")){
		    			this.defaultSort = properties.getProperty(key);
		    		}
		    		else if(key.equals("table.source")){
		    			this.tableSource = properties.getProperty(key);
		    		}
		    		else if(key.equals("table.empty.value")){
		    			this.tableEmptyValue = properties.getProperty(key);
		    		}
		    		else if(key.equals("table.caption.value")){
		    			this.tableCaption = properties.getProperty(key);
		    		} 
		    		else {
			    		Ivy.log().debug("Unmapped SmartTable Property:"+key + "=" + properties.getProperty(key));		    			
		    		}
		    	}
		    }				
		}
		public void resetFilterValues(){
			for(String columnName:this.colums.keySet()){
				SmartTableColumn column = this.colums.get(columnName);
				column.filterValues = null;
			}
			this.filters.clear();
			this.hasFilters = false;
			this.isFiltered = false;
		}
		public String getForm(Boolean onlySortables){			
			StringBuilder html = new StringBuilder("");
			html.append("<form id='"+this.name+"FilterForm' title='"+this.tableCaption+"' isFiltered='"+this.hasFilters+"' class='SmartTableFilter' method='POST' action='#'>");
			html.append("<INPUT TYPE='HIDDEN' NAME='"+FILTER_SOURCE+"' VALUE='"+this.tableSource+"'>");
			html.append("<INPUT TYPE='HIDDEN' NAME='"+FILTER_NAME+"' VALUE='"+this.name+"'>");
			
			html.append("<table cellpadding='2' cellspacing='2'>");
			//cast columns to a sortable list
			List<SmartTableColumn> sortcolumns = new ArrayList<SmartTableColumn>();
			for(SmartTableColumn column:this.colums.values()){
				sortcolumns.add(column);
			}
			//do sort
			Collections.sort(sortcolumns, new SmartTableColumnSorter());
			
			for(SmartTableColumn column:sortcolumns){
				Boolean doAdd = false;
				if(onlySortables){
					doAdd = column.sort;
				}
				else {
					doAdd = true;
				}
				if(doAdd){
					String name = this.name+NAME_DELIMINITER+column.name;
					String value = "";
					if(column.filterValues!=null){
						value = column.filterValues[0];
					}
					//Ivy.log().debug("Filter "+name+ " with value:"+value);
					if(!column.header.contains("em")){
						html.append("<tr>");
						html.append("<td class='SmartTableFilterLabel'>"+column.header+"</td>");
						html.append("<td><input name='"+name+"' type='text' value='"+value+"'></td>");			
						html.append("</tr>");
					}
										
				}
			}
			html.append("<tr>");
			html.append("<td ><input type='submit' value='Filter'></td>");
			if(this.isFiltered){
				html.append("<td class='SmartTableFilterResults'><span class='foundTitle'>Found</span><span class='foundValue'>"+this.found+"/"+this.rsByVal.size()+"</span></td>");				
			}
			else {
				html.append("<td class='SmartTableFilterResults'></td>");				
			}
			html.append("</tr>");
						
			html.append("</table>");
			html.append("</form>");
			return html.toString();
		}
	}
 	@SuppressWarnings("unused")
	private class SmartTableColumn {
		String key = "";
		String name = "";
		Boolean hasName = false;
		String header = "";
		String body = "";
		String link = "";
		Boolean sort = false;
		Boolean hasCondition = false;
		String condition = "";
		Integer ordinal = 0;
		String[] filterValues = null;
		private void loadFromProperties(Properties properties){
			if(properties.containsKey("name.value")){
				this.name = properties.getProperty("name.value");
				if(!this.name.trim().equals("")){
					this.hasName = true;
				}	
			}
			if(properties.containsKey("condition.value")){
				this.condition = properties.getProperty("condition.value");
				if(!this.condition.trim().equals("")){
					this.hasCondition = true;
				}	
			}			
			if(properties.containsKey("header.value")){
				this.header = properties.getProperty("header.value");
				if(this.header.trim().equals("")){
					this.header = this.name;
				}
				if(this.header.contains("<%=ivy.cms.co")){
					StringBuffer sb = new StringBuffer();
					Pattern coPattern = Pattern.compile("<%=ivy.cms.co\\(\"[^>]*\"\\)%>");
					Matcher coMatcher = coPattern.matcher(this.header);
					boolean result = coMatcher.find();
					while (result) {
						String group = coMatcher.group();
						String path = group.substring(group.indexOf("\"")+1, group.lastIndexOf("\""));
						String content = Ivy.cms().co(path);
						if(content.equals("")){
							content = "<em>Content Object "+path+" does not exist</em>";
						}
						coMatcher.appendReplacement(sb, content);
						result = coMatcher.find();
					}
					coMatcher.appendTail(sb);
					this.header = sb.toString();
				}
				
			}
			if(properties.containsKey("sort.value")){
				this.sort = Boolean.parseBoolean(properties.getProperty("sort.value"));
			}
			if(properties.containsKey("link.value")){
				this.link = properties.getProperty("link.value");
			}
			if(properties.containsKey("body.value")){
				this.body = properties.getProperty("body.value");
			}
			//for (String columnPropertyKey :properties.stringPropertyNames()) {
			//	Ivy.log().debug(key+" Column Property:"+columnPropertyKey + "=" + properties.getProperty(columnPropertyKey));
			//}			
		}
		SmartTableColumn(Properties properties, String key){
			this.key = key;
			this.ordinal = Integer.parseInt(key.substring(3));
			loadFromProperties(properties);
		}
	}
	private class SmartTableColumnSorter implements Comparator<SmartTableColumn> {
		public int compare(SmartTableColumn a, SmartTableColumn b){
			return a.ordinal.compareTo(b.ordinal);
		}		
	}

	public SmartTableFilter(HttpServletRequest request) throws EnvironmentNotAvailableException, PersistencyException{	
		this.request = request;
		String uri = Ivy.html().getCurrentContentPageUri();
		//Load SmartTable Configurations
		if(!contexts.containsKey(uri)){
			//set up context
			SmartTableContext smartTableContext = new SmartTableContext();
			contexts.put(smartTableContext.contextUri, smartTableContext);			
			currentContext = smartTableContext;
		}
		else {
			currentContext = contexts.get(uri);
		}
		doMapRequestParameters();
		doFilter();

	}
	
	private void doMapRequestParameters(){
		Map<String, String[]> parameters = this.request.getParameterMap();
		for(String tableName:currentContext.tables.keySet()){
			SmartTableTable table = currentContext.tables.get(tableName);
			
			if(parameters.containsKey(FILTER_NAME)){
				currentFilterNames = parameters.get(FILTER_NAME);				
			}
			if(parameters.containsKey(FILTER_SOURCE)){
				//find out if current form is calling for this table source
				currentTableFilters = parameters.get(FILTER_SOURCE);
				//Ivy.log().debug("Try to find out if source is already filtered in Parameter "+FILTER_SOURCE+" value was "+target[0]);
				for(int i=0;i<currentTableFilters.length;i++){
					if(currentTableFilters[i].equals(table.tableSource)){
						table.loadByVal = true;
					}					
				}
			}
			//map filter values to table columns
			table.filters.clear();
			for(String key:table.colums.keySet()){
				String name = table.name+NAME_DELIMINITER+key;
				SmartTableColumn column = table.colums.get(key);
				if(parameters.containsKey(name)){
					column.filterValues = parameters.get(name);
					if(column.filterValues[0].trim().length()>0){
						table.filters.put(key, column);
						//Ivy.log().debug("Adding "+key+" to "+table.name+" filters");						
					}
				}
				else {
					column.filterValues = null;
				}
			}
			if(table.filters.size()>0){
				table.hasFilters = true;
			}
			else {
				table.hasFilters = false;
			}
		}
	}
	public String getForm(String name){
		return getForm(name, true);
	}
	public String getForm(String name, Boolean onlySortables){
		String form = "";
		if(currentContext.tables.containsKey(name)){
			SmartTableTable table = currentContext.tables.get(name);		
			form = table.getForm(onlySortables);
		}
		return form;
	}
	public void doReset(String name){
		if(currentContext.tables.containsKey(name)){
			SmartTableTable table = currentContext.tables.get(name);
			doReset(table);
		}
	}
	private void doReset(SmartTableTable table){
		table.resetFilterValues();
		doFilter();
	}
	private void doFilter(){
		if(currentFilterNames!=null){
			for(int index=0;index<currentFilterNames.length;index++){
				String tableName = currentFilterNames[index];
				if(currentContext.tables.containsKey(tableName)){
					SmartTableTable table = currentContext.tables.get(tableName);
					//get recordset which we can manipulate
					Recordset rsByRef = (Recordset) Ivy.html().getObject(table.tableSource);
					//first load of recordset base data
					if(table.rsByVal==null){
						table.loadRecordset();
						//Ivy.log().debug(table.tableSource +" Copy is null");
					}
					else {
						//if Request Parameter contains this table
						//use stored recordset
						if(table.loadByVal){
							rsByRef.clear();
							rsByRef.addAll(table.rsByVal);
							//Ivy.log().debug("Recordset by Ref is resetted to size "+rsByRef.size());
						}
						else {
							table.loadRecordset();
						}
					}

					if(table.hasFilters){
						//remove unmatching records
						for(int i=0;i<rsByRef.size();i++){	
							for(String columnName:table.filters.keySet()){
								SmartTableColumn column = table.filters.get(columnName);
								if (column.filterValues!=null){
									String filterValue = column.filterValues[0];
									Object value = rsByRef.getField(i, column.name);
									if (value!=null){
										Boolean remove = false;
										if (value instanceof String){
											String text = (String) value;
											if(!text.contains(filterValue))	{ remove = true; }																			
										} else if (value instanceof Number){
											String text = value.toString();
											if(!text.equals(filterValue))	{ remove = true; }																																						
										}
										if(remove){
											rsByRef.removeAt(i);
											i= i-1;
											break;
										}
									}
									else {
										rsByRef.removeAt(i);
										i= i-1;
										break;
									}
								}
							}
						}
						table.isFiltered = true;
						table.found = rsByRef.size();
					}
					else {
						table.isFiltered = false;
					}				
				}
			}			
		}
	}
}
