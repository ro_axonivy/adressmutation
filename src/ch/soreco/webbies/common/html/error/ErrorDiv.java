package ch.soreco.webbies.common.html.error;



public class ErrorDiv {
	public static String ErrorToDiv(Object errorAttribute){
		StringBuffer html = new StringBuffer("");
		html.append("");
		String value = getValue(errorAttribute);
		if(value!=null&&!value.equals("")){
			html.append(getValue(errorAttribute));
		}
		//reset value
		errorAttribute = null;
		return html.toString().trim();
	}
	private static String getValue(Object attribute){
		String value;
	    value = null;

	    if (attribute == null) {
	    	
	    }
	    else {
		      if (attribute.getClass().isPrimitive() || attribute instanceof String)
		      {
		        value = attribute.toString();
		      }
		      else
		      {
			        if (attribute instanceof Exception)
			        {
			        	Exception e = (Exception) attribute;
			        	value = e.getMessage();
			        }		    	  
		      }
	    }
		return value;
	}
	
	
}
