package ch.soreco.webbies.common.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.Recordset;

public class ExcelReader {
	public static Recordset getExcelSheet(File excelFile) throws IOException{
		return getExcelSheet(excelFile, 0, 0, true, true);
	}
	public static Recordset getExcelSheet(File excelFile
											, int sheetindex
											, int startAt
											, Boolean startsWithHeader
											, Boolean skipEmptyRows) throws IOException{
		Recordset rs = new Recordset();
		Workbook wb = null;
		String path = excelFile.getAbsolutePath();
		InputStream inp = null;
        if(path.toLowerCase().endsWith(".xlsx")){
        	wb = new XSSFWorkbook(path);
        }
        else {
        	inp = new FileInputStream(path);
        	wb = new HSSFWorkbook(new POIFSFileSystem(inp));
        }		
		
		Ivy.log().info(ExcelReader.class+" import from "+excelFile.getAbsolutePath()+" started.");
		rs = ExcelReader.getRecordset(wb, sheetindex, startAt, startsWithHeader, skipEmptyRows);
		Ivy.log().info(ExcelReader.class+" import from "+excelFile.getAbsolutePath()+" finished.");

		if(inp!=null){
			inp.close();			
		}
		
		return rs;
	}
	private static Recordset getRecordset(Workbook wb, int sheetindex, int startAt, Boolean startsWithHeader, Boolean skipEmptyRows){
		Recordset rs = new Recordset();
		Sheet sheet = wb.getSheetAt(sheetindex);

		int rowIndex = 0;
		List<String> fieldNames = null;
		for (Row row : sheet) {
			if(rowIndex>=startAt){
				if(fieldNames==null
						&&startsWithHeader){
					fieldNames = ExcelReader.getHeaderRow(row);	
					for(String key:fieldNames){
						rs.addColumn(key, new ArrayList<Object>());
					}
				}
				else {
					ch.ivyteam.ivy.scripting.objects.List<Object> cells = ExcelReader.getCells(row);
					rs.add(cells);
				}				
			}
			rowIndex++;
		}
		if(skipEmptyRows){
			for(int i=rs.size()-1;i>=0;i--){
				if(rs.getAt(i).isEmpty()){
					rs.removeAt(i);
				}
			}
		}
		return rs;
	}
	private static ch.ivyteam.ivy.scripting.objects.List<Object> getCells(Row row){
		ch.ivyteam.ivy.scripting.objects.List<Object> cells = ch.ivyteam.ivy.scripting.objects.List.create();
		for (Cell cell : row) {
			switch(cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				cells.add(cell.getRichStringCellValue().getString());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if(DateUtil.isCellDateFormatted(cell)) {
					cells.add(cell.getDateCellValue());
				} else {
					cells.add(cell.getNumericCellValue());
				}
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				cells.add(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				cells.add( cell.getCellFormula());
				break;
			default:
				cells.add(null);
				//rec.putField(fieldName, cell);
			}			
		}
		return cells;
	}
	private static List<String> getHeaderRow(Row row){
		List<String> fieldNames = new ArrayList<String>();			
		for (Cell cell : row) {
			String fieldName = ""+cell.getColumnIndex();
			switch(cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					fieldName = cell.getRichStringCellValue().getString().trim();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if(DateUtil.isCellDateFormatted(cell)) {
						fieldName = cell.getDateCellValue().toString();
					} else {
						fieldName = ""+cell.getNumericCellValue();
					}
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					Boolean bool = cell.getBooleanCellValue();
					fieldName = bool.toString();
					break;
				case Cell.CELL_TYPE_FORMULA:
					fieldName = cell.getCellFormula();
					break;
				default:
					//rec.putField(fieldName, cell);
			}
			fieldNames.add(fieldName);
		}
		return fieldNames;
	}

}
