package ch.soreco.webbies.common.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.Date;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.File;
import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.ivyteam.ivy.scripting.objects.Time;



public class ExcelWriter {
	public static String name = "Export.xls";
	private static File excel = null;
	//Workbook objects
	private static Workbook wb = null;
	private static String sheetName = "Sheet";
    private static int serialVersionUIDColumn = -1;
    private static HashMap<String, CellStyle> styles = new HashMap<String, CellStyle>();
    private static Boolean isWritten = false;
    public ExcelWriter(String fileName){
    	name = fileName;
    	if(name.endsWith(".xlsx")||name.endsWith("xls")){
    		//name is properly defined
    	}
    	else {
    		name = name+".xls";
    	}  	
    	isWritten = false;
    }

	public void export(SheetMeta sheetMeta) throws IOException{
		// create workbook objects (workbook, sheet and cellstyles)
        initWorkbook();  
        // map cellstyles
        for(String key:sheetMeta.styles.keySet()){
        	styles.put(key, sheetMeta.styles.get(key));
        }
        //export Recordset Data to workbook
        exportSheet(sheetMeta);
            
        writeWorkbook();
      
    }
	public void export(Recordset rs) throws IOException{
		// create workbook objects (workbook, sheet and cellstyles)
        initWorkbook();  
        //export Recordset Data to workbook
        exportSheet(rs, sheetName);       
        writeWorkbook();
      
    }
	public void export(List<Recordset> recordsets) throws IOException{
		// create workbook objects (workbook, sheet and cellstyles)
        initWorkbook();  
        //export Recordset Data to workbook
        for(int i=0;i<recordsets.size();i++){
        	String name = sheetName+(i+1);
            exportSheet(recordsets.get(i), name);        	
        }
        writeWorkbook();
	}
    private static void initWorkbook(){
        if(name.toLowerCase().endsWith(".xlsx")){
        	wb = new XSSFWorkbook();
        }
        else {
        	wb = new HSSFWorkbook();
        }        
        // Set font head to 12 point type, black and bold
        Font fCaption = wb.createFont();
        fCaption.setFontHeightInPoints((short) 14);
        fCaption.setColor( IndexedColors.BLACK.getIndex() );
        fCaption.setBoldweight(Font.BOLDWEIGHT_BOLD);

        // Set font head to 12 point type, black and bold
        Font fHead = wb.createFont();
        fHead.setFontHeightInPoints((short) 12);
        fHead.setColor( IndexedColors.WHITE.getIndex() );
        fHead.setBoldweight(Font.BOLDWEIGHT_BOLD);
        
        // Set font data to 10 point type
        Font fData = wb.createFont();
        fData.setFontHeightInPoints((short) 10);
        //fData.setColor( IndexedColors.BLACK.getIndex() );
        
        // create  cell styles
        CreationHelper createHelper = wb.getCreationHelper();
        DataFormat df = wb.createDataFormat();
    	CellStyle csCaption = null;
    	CellStyle csHead = null;
    	CellStyle csData = null;
    	CellStyle csDate = null;
        CellStyle csTime = null;
        CellStyle csDateTime = null;
        CellStyle csNumeric = null;
        CellStyle csInteger = null;
        
        csCaption = wb.createCellStyle();
        csCaption.setFont(fCaption);
        styles.put("Caption", csCaption);
        
        csHead = wb.createCellStyle();
        csHead.setFont(fHead);
        csHead.setFillForegroundColor(HSSFColor.AQUA.index);
        csHead.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        csHead.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csHead.setBottomBorderColor(HSSFColor.AQUA.index);
        csHead.setBorderTop(HSSFCellStyle.BORDER_THIN);
        csHead.setTopBorderColor(HSSFColor.AQUA.index);        
        styles.put("Head", csHead);
        
        csData = wb.createCellStyle();
        csData.setFont(fData);
        csData.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csData.setBottomBorderColor(HSSFColor.AQUA.index);
        styles.put("Data", csData);
        
        csNumeric = wb.createCellStyle();
        csNumeric.setFont(fData);
        csNumeric.setDataFormat(df.getFormat("#,##0.00"));
        csNumeric.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csNumeric.setBottomBorderColor(HSSFColor.AQUA.index);
        styles.put("Numeric", csNumeric);
        
        csInteger = wb.createCellStyle();
        csInteger.setFont(fData);
        csInteger.setDataFormat(df.getFormat("#,##0"));
        csInteger.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csInteger.setBottomBorderColor(HSSFColor.AQUA.index);
        styles.put("Integer", csInteger);
        styles.put("Long", csInteger);
        
        csDate = wb.createCellStyle();
        csDate.setFont(fData);
        csDate.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy"));
        csDate.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csDate.setBottomBorderColor(HSSFColor.AQUA.index);
        styles.put("Date", csDate);
        
        csDateTime = wb.createCellStyle();
        csDateTime.setFont(fData);
        csDateTime.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy HH:mm:ss"));
        csDateTime.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csDateTime.setBottomBorderColor(HSSFColor.AQUA.index);
        styles.put("DateTime", csDateTime);
        
        csTime = wb.createCellStyle();
        csTime.setFont(fData);
        csTime.setDataFormat(createHelper.createDataFormat().getFormat("HH:mm:ss"));
        csTime.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        csTime.setBottomBorderColor(HSSFColor.AQUA.index);        
        styles.put("Time", csTime);
        
    }
	private void writeWorkbook()  throws IOException {
        if(wb instanceof XSSFWorkbook){
        	name = name + "x";
        }
        //You may create a temporary File
        excel = new File(name);
        // write the workbook to the output stream
        // close our file 
        FileOutputStream out = new FileOutputStream(excel.getAbsolutePath());
        wb.write(out);
        out.close();
        isWritten = true;
        Ivy.log().info("Excel exported to "+excel.getAbsolutePath());
	}
	public File getExcelFile() throws IOException{
		if(!isWritten){
			writeWorkbook();
		}
		return excel;
	}
	private static void exportSheet(SheetMeta sheetMeta){
        // create a new sheet
    	Sheet sheet = wb.createSheet(sheetMeta.sheetName);
    	CellStyle csCaption = null;
		CellStyle csData = null;
		CellStyle csHead = null;
		int headerRow = 0;
		if(styles.containsKey("Head")){
			csHead = styles.get("Head");			
		}
		if(styles.containsKey("Data")){
			csData = styles.get("Data");			
		}
		if(styles.containsKey("Caption")){
			csCaption = styles.get("Caption");
		}
		
		if(sheetMeta.caption.trim().length()>0){
			java.util.List<Object> caption = new ArrayList<Object>();
			caption.add(sheetMeta.caption);
			appendRow(sheet, caption, csCaption, headerRow);
			headerRow = 2;
		}
		//export recordset header
		appendRow(sheet, sheetMeta.getHeaders(),csHead, headerRow);
		
		//sheet.createFreezePane( 0, headerRow, 0, headerRow );
		//export rows		
		for(int row=0;row<sheetMeta.rs.size();row++){
        	appendRow(sheet, sheetMeta.getRow(row),csData, row+headerRow+1);
        }	
    }
	private static void exportSheet(Recordset rs, String sheetname){
		SheetMeta sheetMeta = new SheetMeta(rs);
		sheetMeta.sheetName = sheetname;
		exportSheet(sheetMeta);
    }
	private static void appendRow(Sheet sheet,java.util.List<Object> record, CellStyle cs,int row){
        // declare a row object reference
        Row r = null;
        // declare a cell object reference
        Cell c = null;           
        // create header row
        r = sheet.createRow(row);
        //set row
        int j = 0;
        for (int i=0;i<record.size();i++){
        	if(serialVersionUIDColumn!=i){
            	c = r.createCell((short)j);
            	if(cs!=null){
                    c.setCellStyle(cs);            		
            	}
                Object obj = record.get(i);
                if(obj!=null){
                    String objString = obj.toString();
                	try {
    		    		if (obj instanceof Date)
    		    		{
    		    			java.util.Date value = (java.util.Date)((Time) obj).toDate();
    		    			double excelDateValue = HSSFDateUtil.getExcelDate(value);
                			c.setCellValue(excelDateValue);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("Date")){
                    			c.setCellStyle(styles.get("Date"));                				
                			}
    		    		} 
    		    		else if (obj instanceof Time)
    		    		{
    		    			java.util.Date value = (java.util.Date)((Time) obj).toDate();
    		    			double excelDateValue = HSSFDateUtil.getExcelDate(value);
                			c.setCellValue(excelDateValue);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("Time")){
                    			c.setCellStyle(styles.get("Time"));                				
                			}
    		    		}
    		    		else if (obj instanceof DateTime)
    		    		{
    		    			java.util.Date value = (java.util.Date)((Time) obj).toDate();
    		    			double excelDateValue = HSSFDateUtil.getExcelDate(value);
                			c.setCellValue(excelDateValue);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("DateTime")){
                    			c.setCellStyle(styles.get("DateTime"));                				
                			}
     		    		}
    		    		else if(obj instanceof Calendar){
    		    			Calendar value = (Calendar) obj;
                			c.setCellValue(value);
                			c.setCellType(Cell.CELL_TYPE_STRING);
                			if(styles.containsKey("Data")){
                    			c.setCellStyle(styles.get("Data"));                				
                			} 
                		}
    		    		else if(obj instanceof Boolean){
    		    			Boolean value = (Boolean) obj;
                			c.setCellValue(value);
                			c.setCellType(Cell.CELL_TYPE_BOOLEAN);
                		}
    		    		else if(objString.startsWith("=")){
                			c.setCellFormula(objString);
                			c.setCellType(Cell.CELL_TYPE_FORMULA);
                		}
    		    		else if(obj instanceof Integer){
                			// write as double value 
    		    			Double value = Double.valueOf(objString).doubleValue();
                			c.setCellValue(value);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("Integer")){
                    			c.setCellStyle(styles.get("Integer"));                				
                			}                 		}
    		    		else if(obj instanceof Long){
                			// write as double value 
    		    			Double value = Double.valueOf(objString).doubleValue();
                			c.setCellValue(value);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("Long")){
                    			c.setCellStyle(styles.get("Long"));                				
                			}
    		    		}
                		else if(!Double.valueOf(objString).isNaN()) {
                			// write as double value 
    		    			Double value = Double.valueOf(objString).doubleValue();
                			c.setCellValue(value);
                			c.setCellType(Cell.CELL_TYPE_NUMERIC);
                			if(styles.containsKey("Numeric")){
                    			c.setCellStyle(styles.get("Numeric"));                				
                			}
                		} 
                	}
                	catch(Exception ex) {
                		// write as string value
                		c.setCellValue(objString);
            			c.setCellType(Cell.CELL_TYPE_STRING);
                	}
                }
                j++;
        	}
        }
	}
}
