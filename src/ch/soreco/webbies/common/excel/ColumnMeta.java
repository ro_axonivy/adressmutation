package ch.soreco.webbies.common.excel;

import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Recordset;

public class ColumnMeta {
	public String name = "";
	public String label = "";
	public Boolean suppressed = false;
	public Integer ordinal = 0;
	public Integer index = 0;
	public List<Object> values = null;
	public String className = "";
	public ColumnMeta(Recordset rs, int i) {
		this.ordinal = i;
		this.index = i;
		this.name = rs.getKeys().get(i);
		this.label = this.name;
		this.values = rs.getColumn(this.name);
		if(this.name.equals("serialVersionUID")){
			this.suppressed = true;
		}
	}
	public ColumnMeta(Recordset rs, String key) {
		this.ordinal = rs.getKeys().indexOf(key);
		this.index = this.ordinal;
		this.name = key;
		this.label = this.name;
		this.values = rs.getColumn(this.name);

		if(this.name.equals("serialVersionUID")){
			this.suppressed = true;
		}
	}
}
