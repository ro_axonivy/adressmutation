package ch.soreco.webbies.common.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.CellStyle;

import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Recordset;

public class SheetMeta {
	public Recordset rs = null;
	public String caption = "";
	public String sheetName = "";
    public HashMap<String, CellStyle> styles = new HashMap<String, CellStyle>();
    public java.util.List <ColumnMeta>  columns = new ArrayList <ColumnMeta>() ;
    public SheetMeta(Recordset rs){
    	initColumnMeta(rs);
    }
    public java.util.List<ColumnMeta> getColumns(){
    	ColumnMetaSorter sort = new ColumnMetaSorter();
    	Collections.sort(this.columns, sort);
    	return columns;
    }
    public java.util.List<Object> getHeaders(){
		java.util.List<Object> headers = new ArrayList<Object>();
		for(ColumnMeta column:columns){
			if(!column.suppressed){
				headers.add(column.label);				
			}
		}
		return headers;
    }

    public java.util.List<Object> getRow(int row){
		java.util.List<Object> record = new ArrayList<Object>();
		for(ColumnMeta column:columns){
			if(!column.suppressed){
				record.add(this.getCell(row, column.index));				
			}
		}
		return record;
    }
    private Object getCell(int row, int column){
    	return this.rs.getField(row, column);
    }
    private void initColumnMeta(Recordset rs){
    	this.rs = rs;
    	List<String> keys = rs.getKeys();
    	for(int i=0;i<keys.size();i++){
    		ColumnMeta column = new ColumnMeta(this.rs,i);
    		this.columns.add(column);
    	}
    }
	public void setSuppressedColumns(List<String> suppressednames){
		for(String name:suppressednames){
			for(ColumnMeta column:this.columns){
				if(column.name.equals(name)){
					column.suppressed = true;
					break;
				}
			}
		}
	}
    private class ColumnMetaSorter implements Comparator<ColumnMeta>{
		@Override
		public int compare(ColumnMeta a, ColumnMeta b) {
			return a.ordinal.compareTo(b.ordinal);
		}
    }
}
