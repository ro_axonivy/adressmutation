package ch.soreco.orderbook.authority.util;

import java.util.Comparator;

public class OrderTypeInfoComparator implements Comparator<OrderTypeInfo> {
	@Override
	public int compare(OrderTypeInfo o1, OrderTypeInfo o2) {
		return o1.getOrderTypeLabel().compareTo(o2.getOrderTypeLabel());
	}

}
