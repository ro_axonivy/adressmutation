package ch.soreco.orderbook.authority.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import ch.ivyteam.ivy.scripting.objects.List;
import ch.ivyteam.ivy.scripting.objects.Record;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.soreco.orderbook.bo.OrderBook;
import ch.soreco.orderbook.enums.OrderQuantityType;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.util.Authority;
import ch.soreco.webbies.common.html.generics.AttributeClass;

public class OrderTypeUtil {
	private static final String SERIAL_VERSION = "serialVersionUID";
	/**
	 * Gets the order types associated to Official Team Order Books and returns 
	 * an HashMap containing {@link OrderTypeInfo} by {@link OrderType}.<br>
	 * The {@link OrderTypeInfo} indicates if the type is a member of an {@link OrderBook}
	 * having {@link OrderBook#getRequiresOrderQuantityType()} set to true.
	 * @return HashMap of OrderTypeInfos, never null
	 */
	private static HashMap<OrderType, OrderTypeInfo> getOfficialTeamOrderTypeMap(){
    	HashMap<OrderType, OrderTypeInfo> map = new HashMap<OrderType, OrderTypeInfo>();
    	AttributeClass ac = new AttributeClass(OrderType.class);
    	HashMap<String, String> labels = ac.getKeyLabelMap();
    	/**
    	 * Loop all Order Books as given by Authority
    	 * as books define if its associated order types are 
    	 * requiring the OrderQuantityType and are "Official"
    	 */
    	for(OrderBook book : Authority.getInstance().getBooks()){
    		if(book.getOfficialTeam()!=null
    				&&book.getOfficialTeam()){
    			for(OrderType orderType:book.getOrderTypeList()){
    				if(orderType.equals(OrderType.Default)
    						&&orderType.equals(OrderType.Order)){
    					//Default and Order are excluded
    				} else if(map.containsKey(orderType)){
    					OrderTypeInfo info = map.get(orderType);				
    					if(!info.getRequiresOrderQuantityType()
    							&&book.getRequiresOrderQuantityType()){
    						info.setRequiresOrderQuantityType(true);
    					}
    				} else {
    					OrderTypeInfo info = new OrderTypeInfo();
    					info.setOrderTypeName(orderType.name());
    					String label = orderType.name();
    					if(labels.containsKey(orderType.name())){
    						label = labels.get(orderType.name());
    					}
    					/*
    					if(book.getShortname()!=null){
    						label = book.getShortname()+" - "+label;
    					}
    					*/
						info.setOrderTypeLabel(label);
						info.setRequiresOrderQuantityType(book.getRequiresOrderQuantityType());
    					map.put(orderType, info);
    				}
    			}
    		}
    	}
    	return map;
	}
	/**
	 * Gets the order types associated to Official Team Order Books and returns 
	 * an Ivy list of {@link OrderTypeInfo}.<br>
	 * Which contains the used Order Types within order books sorted by its Label name.
	 * The {@link OrderTypeInfo} indicates if the type is a member of an {@link OrderBook}
	 * having {@link OrderBook#getRequiresOrderQuantityType()} set to true.
	 * @return Ivy List of OrderTypeInfos, never null
	 */
    public static List<OrderTypeInfo> getOfficialTeamOrderTypeInfos(){
    	HashMap<OrderType, OrderTypeInfo> map = getOfficialTeamOrderTypeMap();
    	
    	java.util.List<OrderTypeInfo> infos = new ArrayList<OrderTypeInfo>(map.values());
    	Collections.sort(infos, new OrderTypeInfoComparator());
    	return List.create(OrderTypeInfo.class, infos);
    }
    /**
     * Get the order types associated to Official Team Order Books into a Recordset.<br>
     * <br>
	 * It contains the used Order Types within order books sorted by its Label name.
	 * The {@link OrderTypeInfo} indicates if the type is a member of an {@link OrderBook}
	 * having {@link OrderBook#getRequiresOrderQuantityType()} set to true.
     * @return Recordset, never null
     */
    public static Recordset getOfficialTeamOrderTypes(){
    	HashMap<OrderType, OrderTypeInfo> map = getOfficialTeamOrderTypeMap();

    	//collect field names by reflection as we need them
    	//for instantiation of Recordset
    	Field[] fields = OrderTypeInfo.class.getDeclaredFields();
    	ArrayList<String> keys = new ArrayList<String>();
    	for(Field field:fields){
    		//serialVersionUID should not be part of recordset.
    		if(!field.getName().equals(SERIAL_VERSION)){
    			keys.add(field.getName());
    		}
    	}
    	Recordset rs = new Recordset(keys);
    	java.util.List<OrderTypeInfo> infos = new ArrayList<OrderTypeInfo>(map.values());
    	Collections.sort(infos, new OrderTypeInfoComparator());
    	//append the sorted OrderTypeInfos to Recordset
    	rs.addAll(infos);
    	return rs;
    }
    /**
     * Get the {@link OrderQuantityType} Enumeration values as a Recordset 
     * containing the Enumeration value in "Key"-Field and the "Label"-Field 
     * contains the associated label as given by "Generics".
     * 
     * @return Recordset, never null;
     */
    public static Recordset getOrderQuantityTypes(){
    	AttributeClass acOfOrderQuantityType = new AttributeClass(OrderQuantityType.class);    	
    	HashMap<String, String> mapOfOrderQuantityType = acOfOrderQuantityType.getKeyLabelMap();
    	Recordset rs = new Recordset(new String[]{"Key", "Label"});

    	for(String key:mapOfOrderQuantityType.keySet()){
    		Record record = new Record();
    		record.putField("Key", key);
    		record.putField("Label", mapOfOrderQuantityType.get(key).toString());
    		rs.add(record);
    	}
    	return rs;
    }
}
