package ch.soreco.orderbook.authority.util;

import ch.soreco.orderbook.bo.OrderBook;
/**
* The OrderTypeInfo indicates if the type is a member of an {@link OrderBook}
* having {@link OrderBook#getRequiresOrderQuantityType()} set to true.
*/
public class OrderTypeInfo extends ch.ivyteam.ivy.scripting.objects.CompositeObject {
	/** SerialVersionUID */
	private static final long serialVersionUID = 1L;
	private java.lang.String orderTypeName;
	private java.lang.String orderTypeLabel;
	private java.lang.Boolean requiresOrderQuantityType;

	public java.lang.String getOrderTypeName() {
		return orderTypeName;
	}
	public void setOrderTypeName(java.lang.String orderTypeName) {
		this.orderTypeName = orderTypeName;
	}
	public java.lang.String getOrderTypeLabel() {
		return orderTypeLabel;
	}
	public void setOrderTypeLabel(java.lang.String orderTypeLabel) {
		this.orderTypeLabel = orderTypeLabel;
	}
	public java.lang.Boolean getRequiresOrderQuantityType() {
		return requiresOrderQuantityType;
	}
	public void setRequiresOrderQuantityType(
			java.lang.Boolean requiresOrderQuantityType) {
		this.requiresOrderQuantityType = requiresOrderQuantityType;
	}
}
