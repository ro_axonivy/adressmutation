package ch.soreco.orderbook.ui.enums;

public enum ViewType {
	READ,
	EDIT,
	CHECK,
	DISPATCH
}
