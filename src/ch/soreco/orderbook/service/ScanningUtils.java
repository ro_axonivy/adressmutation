package ch.soreco.orderbook.service;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import ch.soreco.common.bo.File;
import ch.soreco.orderbook.bo.Attachment;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.enums.ArchivingState;
import ch.soreco.orderbook.mail.dao.AttachmentDao;

public class ScanningUtils {
	private ScanningUtils() {

	}

	public static String buildScanningTabs(List<Scanning> scanningList, String uiType) {
		StringBuilder html = new StringBuilder("");
		StringBuilder tabLinks = new StringBuilder("");
		StringBuilder tabContent = new StringBuilder("");
		AttachmentDao attachmentDao = new AttachmentDao();
		int i = 0;

		tabLinks.append("<ul>");
		for (Scanning scan : scanningList) {
			String docType = scan.getDocType() != null ? scan.getDocType() : "";
			String pageNoStart = scan.getPageNoStart() != null ? String.valueOf(scan.getPageNoStart()) : "0";
			List<Attachment> attachments = attachmentDao.findByScanningId(scan.getScanningId());
			String attachmentInfo = null;
			if (attachments != null && attachments.size() > 0) {
				attachmentInfo = buildAttachmentInfo(attachments);
			}
			tabLinks.append("<li><a href=\"#tab" + i + "\">" + docType + "-" + pageNoStart + "</a></li>");
			String archivingState = scan.getArchivingState() != null ? scan.getArchivingState() : "";

			if (uiType.equalsIgnoreCase("SCANINFO")) {
				boolean checked = !archivingState.equalsIgnoreCase(ArchivingState.IMTF_ARCHIVED.toString());
				String scanInfo = buildScanInfo(i, checked, attachmentInfo);
				tabContent.append(scanInfo);
			} else {
				if (attachmentInfo != null) {
					tabContent.append("<div id=\"tab" + i + "\" style=\"height: auto\">");
					tabContent.append(attachmentInfo);
					tabContent.append("</div>");
				} else {
					tabContent.append("<div id=\"tab" + i + "\" style=\"display: none; height: auto\"></div>");
				}
			}
			i++;
		}
		tabLinks.append("</ul>");

		html.append(tabLinks);
		html.append(tabContent);

		html.append("<div id=\"iframeDiv\" height=\"100%\">");
		html.append("<div id=\"scanFile\" height=\"100%\"><iframe id=\"tFrame\" name=\"tFrameName\" src=\"\" width=\"100%\" height=\"100%\"></iframe></div>");
		html.append("</div>");
		return html.toString();
	}

	private static String buildAttachmentInfo(List<Attachment> attachments) {
		StringBuilder attachmentContent = new StringBuilder("");
		if (attachments != null && attachments.size() > 0) {
			attachmentContent.append("<div>");
			attachmentContent.append("<ul class='AttachmentsOfScanning'>");
			int unknownNameCount = 0;
			for (Attachment attachment : attachments) {
				File file = attachment.getFile();
				String fileName = file.getFileName();
				if (fileName == null) {
					unknownNameCount++;
					fileName = "Attachment(" + String.valueOf(unknownNameCount) + ")";
				} else {
					fileName = StringEscapeUtils.escapeHtml(fileName);
				}
				String extension = file.getFileExtension() != null ? file.getFileExtension() : "default";
				if(!extension.equalsIgnoreCase("pdf")){
					attachmentContent.append("<li>");
					attachmentContent.append("<a class=\"AttachmentLink\" fileId='" + String.valueOf(file.getFileId())
							+ "' target='_blank' >");
					attachmentContent.append("<span class='fileExtension " + extension + "'></span>");
					attachmentContent.append("<span class='fileName'>" + fileName + "</span>");
					attachmentContent.append("</a>");
					attachmentContent.append("</li>");					
				}
			}
			attachmentContent.append("</ul>");
			attachmentContent.append("</div>");
		}
		return attachmentContent.toString();
	}

	private static String buildScanInfo(int o, boolean checked, String attachmentInfo) {
		StringBuilder htmlTabs = new StringBuilder("");
		htmlTabs.append("<div id=\"tab" + o + "\">");
		if (attachmentInfo != null) {
			htmlTabs.append(attachmentInfo);
		}
		htmlTabs.append("<table width=\"100%\" border=\"0\">");
		htmlTabs.append("<tr><td  width=\"40%\"  valign=\"top\">");
		if (checked) {
			htmlTabs.append("<INPUT TYPE=\"CHECKBOX\" NAME=\"currentOrder$scanning$archivingState_" + o
					+ "\" VALUE=\"true\" checked>Dokument bereits archiviert<br><br>");
		} else {
			htmlTabs.append("<INPUT TYPE=\"CHECKBOX\" NAME=\"currentOrder$scanning$archivingState_" + o
					+ "\" VALUE=\"false\" >Dokument bereits archiviert<br><br>");

		}
		htmlTabs.append("<div id=\"scanInfo" + o + "\" ></div>");
		htmlTabs.append("</td>");
		htmlTabs.append("<td width=\"60%\">");
		htmlTabs.append("<div id=\"scanFile\"><iframe id=\"scanFileFrame" + o
				+ "\" src=\"\" style=\"width:100%; height:100%;\"></iframe></div>");
		htmlTabs.append("</td>");
		htmlTabs.append("</tr>");
		htmlTabs.append("</table>");
		htmlTabs.append("</div>");
		return htmlTabs.toString();
	}
}
