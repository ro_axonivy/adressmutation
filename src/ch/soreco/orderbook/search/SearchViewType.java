package ch.soreco.orderbook.search;

public enum SearchViewType {
	OrderBookPendant, 
	OrderBookToEdit, 
	OrderBookToCheck,
	OrderBookRecent, 
	Search,
	MassUpdate;
}
