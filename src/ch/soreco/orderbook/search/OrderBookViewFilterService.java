package ch.soreco.orderbook.search;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.Record;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.ivyteam.ivy.security.IUser;
import ch.soreco.common.bo.AuthorityGroup;
import ch.soreco.orderbook.enums.GroupRole;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.util.Authority;

public class OrderBookViewFilterService {
	private Date twoWorkDaysBefore;
	private Authority authority = Authority.getInstance();
	private HashMap<GroupRole, ArrayList<OrderType>> userRole2TypeMap=new HashMap<GroupRole, ArrayList<OrderType>>();
	private int inSize;
	private int outSize;
	private int notAllowedToWrite;
	private int notAllowedToRead;
	private int excludedByState;
	private int isBeforeTwoWorkDays;
	
	public OrderBookViewFilterService() {
		twoWorkDaysBefore = getStartOfPreviousWorkDay(2);
	}

	private Date getStartOfPreviousWorkDay(int offset) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		cal.add(Calendar.DAY_OF_WEEK, (offset * -1));
		// step back 2 days
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			cal.add(Calendar.DAY_OF_WEEK, -2);
		}
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			cal.add(Calendar.DAY_OF_WEEK, -1);
		}

		return cal.getTime();
	}

	private String truncate(String text, int len) {
		if (text != null && text.length() > len) {
			text = text.substring(0, len);
			text = text.concat("...");
		}
		return text;
	}
	private String getOrderTypeLabel(String orderType) {
		String label = "";
		if (orderType != null && orderType.length() > 0) {
			label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/OrderType/" + orderType+ "/0_name"), 16);
		}
		return label;
	}
	private String getOrderStateLabel(String orderState) {
		String label = "";
		if (orderState != null && orderState.length() > 0) {
			label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/OrderState/"+ orderState + "/0_name"), 16);
		}
		return label;
	}
	private String getPendencyTicketTypeLabel(String pendencyTicketType) {
		String label = "";
		if (pendencyTicketType != null && pendencyTicketType.length() > 0) {
			label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/PendencyTicketType/"+ pendencyTicketType + "/0_name"), 16);
		}
		return label;
	}
	private List<OrderState> getDefaultOrderStatesToExclude(){
		// declare default excludes for Book
		List<OrderState> excludesBook = new ArrayList<OrderState>();
		excludesBook.add(OrderState.Default);
		excludesBook.add(OrderState.Finished);
		excludesBook.add(OrderState.Unidentified);
		excludesBook.add(OrderState.Aborted);
		excludesBook.add(OrderState.Archived);
		excludesBook.add(OrderState.Deleted);
		excludesBook.add(OrderState.InDispatchingNew);
		excludesBook.add(OrderState.InDispatchingOpen);
		excludesBook.add(OrderState.Merged);
		return excludesBook;
	}
	private List<OrderState> getOrderStatesToExclude(SearchViewType orderBookViewType, boolean archiveFlag) {
		List<OrderState> excludesBook = new ArrayList<OrderState>();
		
		// declare exludes for OrderBookViewType
		switch(orderBookViewType){
		case Search:
		case MassUpdate:
			if (!archiveFlag) {
				excludesBook.add(OrderState.Archived);
				excludesBook.add(OrderState.Aborted);
			}			
			break;
		case OrderBookPendant:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.Dispatched);
			excludesBook.add(OrderState.Recording);
			excludesBook.add(OrderState.InIdCheck);
			excludesBook.add(OrderState.InCheck);
			excludesBook.add(OrderState.In4eCheck);
			excludesBook.add(OrderState.InAbortCheck);
			excludesBook.add(OrderState.InDispatchingAbortCheck);
			break;
		case OrderBookToEdit:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.InIdCheck);
			excludesBook.add(OrderState.InCheck);
			excludesBook.add(OrderState.In4eCheck);
			excludesBook.add(OrderState.InAbortCheck);
			excludesBook.add(OrderState.InPendencyCheck);
			excludesBook.add(OrderState.InPendencyRecordingCheck);
			excludesBook.add(OrderState.InDispatchingAbortCheck);
			break;
		case OrderBookToCheck:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.Dispatched);
			excludesBook.add(OrderState.Recording);
			excludesBook.add(OrderState.Pendant);
			excludesBook.add(OrderState.InPendencyRecording);			
			break;
		case OrderBookRecent:
			excludesBook.add(OrderState.Default);
			excludesBook.add(OrderState.Unidentified);
			excludesBook.add(OrderState.Deleted);
			excludesBook.add(OrderState.Archived);
			break;
		default:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			break;
		}
		Ivy.log().debug("Excluded States:"+StringUtils.join(excludesBook.toArray(), ","));
		return excludesBook;
	}
	private String getTrimmedString(Record record, String key){
		String value = "";
		if(record.getField(key)!=null){
			value = record.getField(key).toString().trim();
		}
		return value;
	}
	private OrderType getOrderType(String orderTypeName){
		OrderType orderType = null;
		try {
			orderType = OrderType.valueOf(orderTypeName);			
		} catch (IllegalArgumentException e){
			Ivy.log().warn("String \""+orderTypeName+"\" is not an OrderType", e);
		}
		return orderType;
	}
	private OrderState getOrderState(String orderStateName){
		OrderState orderState = null;
		try {
			orderState = OrderState.valueOf(orderStateName);			
		} catch (IllegalArgumentException e){
			Ivy.log().warn("String \""+orderStateName+"\" is not an OrderType", e);
		}
		return orderState;
	}
	public Recordset filter(SearchViewType orderBookViewType, boolean archiveFlag, Recordset orderBook) {
		
		List<OrderState> excludedStates = getOrderStatesToExclude(orderBookViewType, archiveFlag);

		List<String> keys = orderBook.getKeys();
		keys.add("orderTypeCO");
		keys.add("orderStateCO");
		keys.add("pendencyTicketTypeCO");
		Recordset rsOrders = new Recordset(keys);
		
		this.inSize = orderBook.size();
		
		for (Record rec : orderBook.getRecords()) {
			if(rec.getField("orderType")!=null&&rec.getField("orderState")!=null){
				String orderTypeName = getTrimmedString(rec, "orderType");
				String orderStateName =  getTrimmedString(rec, "orderState");
				OrderType orderType = getOrderType(orderTypeName);
				OrderState orderState = getOrderState(orderStateName);
				if(orderType!=null&&!orderStateName.isEmpty()){
					// check if user can edit
					boolean doAdd = this.typeAllowed(orderType, GroupRole.Write);
					if(doAdd == false){
						this.notAllowedToWrite++;
						// check if user can read
						doAdd = this.typeAllowed(orderType, GroupRole.Read);
						if(!doAdd){
							this.notAllowedToRead++;
						}
					}

					if (doAdd&&excludedStates.contains(orderState)) {
						doAdd = false;
						this.excludedByState++;
					}

					if (doAdd) {
						// set name of orderType and cut if needed
						rec.putField("orderTypeCO", getOrderTypeLabel(orderTypeName));
						// set name of orderState and cut if needed
						rec.putField("orderStateCO", getOrderStateLabel(orderStateName));
						// set name of pendencyTicketType and cut if needed
						String pendencyTicketType = getTrimmedString(rec, "pendencyTicketType");
						
						rec.putField("pendencyTicketTypeCO",getPendencyTicketTypeLabel(pendencyTicketType));

						// display archived orders only if is within last 2 days, consider weekends
						if (
								(orderBookViewType.equals(SearchViewType.OrderBookRecent) && orderState.equals(OrderState.Archived))
								|| (orderState.equals(OrderState.Merged) && !archiveFlag)
								|| (orderState.equals(OrderState.Aborted) && !archiveFlag)
							) {
							DateTime recDT = (DateTime) rec.getField("orderEnd");
							if (recDT.toJavaDate().compareTo(twoWorkDaysBefore) == -1) {
								// date of rec before the day before yesterday
								this.isBeforeTwoWorkDays++;
								continue;
							}
						}
						rsOrders.add(rec);
					}				
					
				}
			}
		}
		this.outSize = rsOrders.size();
		Ivy.log().debug("In Size is:"+inSize+" outSize:"+outSize+" notAllowedToWrite:"+notAllowedToWrite+" notAllowedToRead:"+notAllowedToRead+ " excludedByState:"+excludedByState+" isBeforeTwoWorkDays:"+isBeforeTwoWorkDays);
		return rsOrders;
	}
    private Boolean typeAllowed(OrderType orderType, GroupRole role){
		if (role == null || orderType == null){
			return false;
		}
		//get all allowed types
		ArrayList<OrderType> myTypes;
		if(userRole2TypeMap.containsKey(role)){
			myTypes = userRole2TypeMap.get(role);
		} else {
			try {
				IUser user = Ivy.session().getSessionUser();
				myTypes = this.getTypes(user, role.name());
			} catch (Exception e) {
				return false;
			}
			userRole2TypeMap.put(role, myTypes);
		}

		return myTypes.contains(orderType);
	}
    private ArrayList<OrderType> getTypes(IUser user, String role){
    	ArrayList<OrderType> types = new ArrayList<OrderType>();

    	for (AuthorityGroup group : authority.getGroups()){
    		debug("--Check AuthorityGroup:"+group.getGroupName(), role);
    		if (group.getGroupRole().equals(role) && group.getUserList().contains(user)){
    			debug("--+User "+user.getName()+ " is in Group:"+group.getGroupName()+" with role "+role+ " OrderBook:"+group.getOrderBook().getName(), role);
    			for (OrderType type : group.getOrderBook().getOrderTypeList()){
    				if (!types.contains(type)){
    					types.add(type);
    					debug("User "+user.getName()+" has "+role+" permission for "+type.name(), role);
    				}
    			}
    		} else {
    			debug("---User "+user.getName()+ " is not in "+group.getGroupName()+" with role "+role+ " OrderBook:"+group.getOrderBook().getName(), role);
    		}
    	}
    	
    	return types;
    }
    private void debug(String message, String role){
//    	if(role.equals("Write")){
//        	Ivy.log().debug(message);    		
//    	}
    }
}