package ch.soreco.orderbook.search;



import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.Recordset;
import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.enums.PendencyTicketType;
import ch.soreco.orderbook.functional.SearchParameters;
import ch.soreco.orderbook.view.dao.OrderDao;
import ch.soreco.orderbook.view.dao.OrderDao.OrderDaoResult;

public class SearchViewService {
	private OrderDao orderDao;
	private HashMap<String, String> orderTypeLabelMap;
	private HashMap<String, String> orderStateLabelMap;
	private HashMap<String, String> pendencyTicketTypeMap;
	public SearchViewService(){
		orderDao = new OrderDao();
		buildOrderTypeLabelMap();
		buildOrderStateLabelMap();
		buildPendencyTicketTypeLabelMap();
	}
	private List<OrderState> getDefaultOrderStatesToExclude(){
		// declare default excludes for Book
		List<OrderState> excludesBook = new ArrayList<OrderState>();
		excludesBook.add(OrderState.Default);
		excludesBook.add(OrderState.Finished);
		excludesBook.add(OrderState.Unidentified);
		excludesBook.add(OrderState.Aborted);
		excludesBook.add(OrderState.Archived);
		excludesBook.add(OrderState.Deleted);
		excludesBook.add(OrderState.InDispatchingNew);
		excludesBook.add(OrderState.InDispatchingOpen);
		excludesBook.add(OrderState.Merged);
		return excludesBook;
	}
	private List<OrderState> getOrderStatesToExclude(SearchViewType searchViewType) {
		List<OrderState> excludesBook = new ArrayList<OrderState>();
		
		// declare exludes for OrderBookViewType
		switch(searchViewType){
		case Search:
		case MassUpdate:	
			excludesBook.add(OrderState.Deleted);
			break;
		case OrderBookPendant:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.Dispatched);
			excludesBook.add(OrderState.Recording);
			excludesBook.add(OrderState.InIdCheck);
			excludesBook.add(OrderState.InCheck);
			excludesBook.add(OrderState.In4eCheck);
			excludesBook.add(OrderState.InAbortCheck);
			excludesBook.add(OrderState.InDispatchingAbortCheck);
			break;
		case OrderBookToEdit:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.InIdCheck);
			excludesBook.add(OrderState.InCheck);
			excludesBook.add(OrderState.In4eCheck);
			excludesBook.add(OrderState.InAbortCheck);
			excludesBook.add(OrderState.InPendencyCheck);
			excludesBook.add(OrderState.InPendencyRecordingCheck);
			excludesBook.add(OrderState.InDispatchingAbortCheck);
			break;
		case OrderBookToCheck:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			
			excludesBook.add(OrderState.Dispatched);
			excludesBook.add(OrderState.Recording);
			excludesBook.add(OrderState.Pendant);
			excludesBook.add(OrderState.InPendencyRecording);			
			break;
		case OrderBookRecent:
			excludesBook.add(OrderState.Default);
			excludesBook.add(OrderState.Unidentified);
			excludesBook.add(OrderState.Deleted);
			excludesBook.add(OrderState.Archived);
			break;
		default:
			excludesBook.addAll(getDefaultOrderStatesToExclude());
			break;
		}
		return excludesBook;
	}
	public SearchViewServiceResult findBySearchParameters(SearchViewType searchViewType, SearchParameters parameters) {
		List<OrderState> excludes = this.getOrderStatesToExclude(searchViewType);
		if(parameters.getIncludeArchive()==null||!parameters.getIncludeArchive()){
			excludes.add(OrderState.Archived);
			excludes.add(OrderState.Aborted);	
		}
		parameters.setExcludedOrderStates(excludes);

		OrderDaoResult orderDaoResult = orderDao.findBySearchParameters(Ivy.session().getSessionUserName(), parameters);
		List<Order> orders = orderDaoResult.getOrders();

		List<String> keys = new ArrayList<String>();
		for(Field field:Order.class.getDeclaredFields()){
			if(!field.getName().equals("serialVersionUID")){
				keys.add(field.getName());
			}
		}
		keys.add("orderTypeCO");
		keys.add("orderStateCO");
		keys.add("pendencyTicketTypeCO");
		Recordset orderBook = new Recordset(keys);
		int i=0;
		for(Order order:orders){
			orderBook.add(order);
			orderBook.putField(i, "orderTypeCO", orderTypeLabelMap.get(order.getOrderType()));
			orderBook.putField(i, "orderStateCO", orderStateLabelMap.get(order.getOrderState()));
			orderBook.putField(i, "pendencyTicketTypeCO", pendencyTicketTypeMap.get(order.getPendencyTicketType()));
			i++;
		}

		SearchViewServiceResult searchViewServiceResult = new SearchViewServiceResult();
		searchViewServiceResult.setCountOfQueryResults(orderDaoResult.getCountOfQueryResults());
		searchViewServiceResult.setOrderBook(orderBook);
		return searchViewServiceResult;
	}
	private String truncate(String text, int len) {
		if (text != null && text.length() > len) {
			text = text.substring(0, len);
			text = text.concat("...");
		}
		return text;
	}
	private void buildOrderTypeLabelMap(){
		 orderTypeLabelMap = new HashMap<String, String>();
		 for(OrderType orderType:OrderType.values()){
			 String label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/OrderType/" + orderType.name()+ "/0_name"), 16);
			 orderTypeLabelMap.put(orderType.name(), label);
		 }
	}
	private void buildOrderStateLabelMap(){
		 orderStateLabelMap = new HashMap<String, String>();
		 for(OrderState orderState:OrderState.values()){
			 String label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/OrderState/" + orderState.name()+ "/0_name"), 16);
			 orderStateLabelMap.put(orderState.name(), label);
		 }
	}
	private void buildPendencyTicketTypeLabelMap(){
		pendencyTicketTypeMap = new HashMap<String, String>();
		 for(PendencyTicketType pendencyTicketType:PendencyTicketType.values()){
			 String label = truncate(Ivy.cms().co("/ch/soreco/orderbook/enums/PendencyTicketType/" + pendencyTicketType.name()+ "/0_name"), 16);
			 pendencyTicketTypeMap.put(pendencyTicketType.name(), label);
		 }
	}
}
