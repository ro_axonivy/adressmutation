package ch.soreco.orderbook.search;

import ch.ivyteam.ivy.scripting.objects.Recordset;

public class SearchViewServiceResult {
	private Recordset orderBook;
	private int countOfQueryResults;
	protected void setOrderBook(Recordset orderBook){
		this.orderBook = orderBook;
	}
	public Recordset getOrderBook(){
		return this.orderBook;
	}
	protected void setCountOfQueryResults(int countOfQueryResults){
		this.countOfQueryResults = countOfQueryResults;
	}
	public int getCountOfQueryResults(){
		return this.countOfQueryResults;
	}
	
}
