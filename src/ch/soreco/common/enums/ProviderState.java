package ch.soreco.common.enums;

public enum ProviderState {
	New,
	Pendant,
	ReadyForTesting,
	Completed,
	NotApplicable, 
	OpenQuestions
}
