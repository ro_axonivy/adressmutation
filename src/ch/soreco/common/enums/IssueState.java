package ch.soreco.common.enums;

public enum IssueState {
	Issue,
	ToBeClarified,
	ChangeRequest,
	Deferred,
	ReOpen
}
