<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache"); 
response.addHeader("Cache-Control","no-store"); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%
	String jQueryVersion = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion");
	String jQueryUIVersion =ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUIVersion");
	String jQueryUITheme = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUITheme");
	%>
	<meta http-equiv="expires" content="0">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta http-equiv="cache-control" content="no-cache, must-revalidate"> 
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-"+jQueryVersion+".min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-ui-"+jQueryUIVersion+".custom.min.js")%>"></script>
	<link title="<%=jQueryUITheme%>" type="text/css" href="<%=ivy.html.wr("css/"+jQueryUITheme+"/jquery-ui-"+jQueryUIVersion+".custom.css")%>" rel="stylesheet" >
<style type="text/css">
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/Styles/Layout")%>
</style>	
	<title></title>
	
<script type="text/javascript">
function getTitle(){ return "Scanning"; }
function getPageId() {return "ScanningTabs"}
$.ajaxSetup ({cache: false });
</script>
</head>

<body>	
<p>Link To Show Files<a href="<%=ivy.html.ref("showFile.ivp")%>">Target</a></p>
	<div id="CompletePage" class="ui-helper-reset ui-widget ui-widget-content ui-corner-all"></div>
	<!--Loading Dialog-->
	<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/PageLoading")%>		
	<!--/Loading Dialog-->
	<div id="docTabs">
	<%=ivy.html.getObject("in.html")%>
	</div>
<script language="JavaScript">
$(document).ready(function(){
	$(window).resize(function() {
		$('#iframeDiv').height($(window).height()-65);
		$('#tFrame').height($(window).height()-65);
	});
	$('body').css('overflow','hidden');
	$("#docTabs" ).tabs();
	$('#iframeDiv').height($(window).height()-65);
	$('#tFrame').height($(window).height()-65);
	$('#tFrame').attr('src','<%=ivy.html.ref("showFile.ivp")%>&time='+new Date().getTime()+'&tabIndex=0');

	$('#docTabs').bind('tabsselect', function(event,ui){
		var cIndex = ui.index;
		$('#tFrame').attr('src','');
		$('#tFrame').attr('src','<%=ivy.html.ref("showFile.ivp")%>&time='+new Date().getTime()+'&tabIndex='+cIndex);
	});	
});
</script>
</body>
</html>