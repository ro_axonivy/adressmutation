// Global settings 
var fontface = "Verdana";
// Initialize 
var ggWindowCall;
isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;

function iWindow(p_WindowCall, p_uri, p_title) {
	if (p_WindowCall == null)
		this.gWinCal = ggWindowCall;
	else
		this.gWinCal = p_WindowCall;
	this.g_uri = p_uri;
	this.g_title = p_title;
}
iWindow.print = iWindow_print;
function iWindow_print() {
	ggWindowCall.print();
}

// This is for compatibility with Navigator 3, we have to create and discard one object before the prototype object exists.
new iWindow();

iWindow.prototype.show = function() {
	this.gWinCal.document.open();
	// Setup the page...
	this.wwrite("<html>");
	this.wwrite("<head><title>"+this.g_title+"</title>");
	this.wwrite("<script type='text/javascript'>");	
	this.wwrite("window.setTimeout(\"location=('"+this.g_uri+"');\",1);")
	this.wwrite("</script>");
	this.wwrite("</head>");
	this.wwrite("<body>");
	this.wwrite("</body></html>");
	this.gWinCal.document.close();
}
iWindow.prototype.wwrite = function(wtext) {
	this.gWinCal.document.writeln(wtext);
}

iWindow.prototype.wwriteA = function(wtext) {
	this.gWinCal.document.write(wtext);
}

function showIWindow() {
	/* 
		p_uri : uri to load in iframe
		p_title: title
	*/
	var windowposy=window.screenTop;
	var windowposx=window.screenLeft;
	windowposy =windowposy+200;
	windowposx =windowposx+200;

	p_uri = arguments[0];
	if (arguments[1] == null)
		p_title = "Customize the title";
	else
		p_title = arguments[1];
	
	if(windowposx+225>screen.width)
	{
		// 2006-02-12 bb: respect margins of a second screen
		if(windowposx+225>screen.width*2)
		{
			windowposx=(screen.width*2)-228;	
		}
		else if(windowposx<screen.width)
		{
			windowposx=(screen.width)-228;	
		}
	}
	
	if(windowposy+220>screen.height)
	{
		windowposy=screen.height-228;	
	}	
	vWindowCall = window.open("", p_title, "width=220, height=220,status=no,resizable=no,top="+windowposy+",left="+windowposx+", dependent=yes");
	vWindowCall.opener = self;
	vWindowCall.focus();
	ggWindowCall = vWindowCall;
	//build iWindow Object
	gWindow = new iWindow(ggWindowCall, p_uri, p_title);
	// Customize your Window Dialog here..

	gWindow.show();
}
