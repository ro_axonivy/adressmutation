/*jslint browser: true */
/*global jQuery: true */
/**
 * jQuery Anchorify plugin
 *
 * example:
 * $(function(){
 * 	$.anchorify.debug = true;
 *	$('<selector>').anchorify({
 *		href:"http://www.google.com",
 *		title: function(){ return "Query for "+$(this).val()},
 *		enabled: function () { return $(this).val()!==""; },
 *		text: 'Link'
 *	});
 * });
 *
 */
;(function ($) {
	$.extend({
		anchorify : {
			debug: false,
			defaults : {
				containerName: 'AnchoredInput',
				text: '',
				href: '',
				target: '_blank',
				enabled: true,
				enabledClassName: 'AnchorEnabled',
				anchor: {
					template: '<a></a>',
					style: {
						position: 'absolute',
						top: 0,
						bottom: 0,
						width: '18px'
					}
				}
			},
			/**
			 * helper method for console logging
			 * set $.anchorify.debug to true to enable debug logging
			 */
			log: function (message, object) {
				if ($.anchorify.debug && window.console && window.console.log) {
					window.console.log('[jquery.anchorify] ' + message, object);
				}
			},
			build: function (elem, opts) {
				var $this = $(elem),
					options = $.extend({}, this.defaults, opts),
					data = $.data(elem, 'anchorify'),
					$anchor = $(options.anchor.template);
				if (data) {
					$.anchorify.log('anchor is initialized.', data);
					return $this;
				}
				$anchor.css(options.anchor.style);
				
				//wrap this with a container having position set to relative
				//and append the input anchor to the container
				$this
					.wrap('<div class="' + options.containerName + '" style="display: inline-block;position: relative;"></div>')
					.parent()
					.append($anchor);
				//add the new anchor to options data
				options = $.extend({}, {'$anchor': $anchor}, options);
				$.data(elem, 'anchorify', options);
				//bind the update anchor event
				$this
					.addClass('AnchorInput')
					.bind('updateAnchor', function () {
						//get the value and meta data from input
						var $this = $(this),
							options = $.data(this, 'anchorify'),
							settings = {
								enabled: true,
								title: '',
								href: ''
							};
						if (options && options.$anchor) {
							$.extend(settings, options);
							//check link enabling
							if (options.enabled && $.isFunction(options.enabled)) {
								settings.enabled = options.enabled.call(this, options);
							}
							//build the anchors text
							if (options.text && $.isFunction(options.text)) {
								$.anchorify.log('updateAnchor options.text.call', settings);
								settings.text = options.text.call(this, options);
							}
							//build the anchors title
							if (options.title && $.isFunction(options.title)) {
								$.anchorify.log('updateAnchor options.title.call', settings);
								settings.title = options.title.call(this, options);
							}

							if (settings.enabled) {							
								//build the anchors href
								if (options.href && $.isFunction(options.href)) {
									settings.href = options.href.call(this, options);
								}
								//log values
								$.anchorify.log('updateAnchor options', options);
								//set the href and title to anchor
								options.$anchor
									.addClass(settings.enabledClassName)
									.attr('title', settings.title)
									.attr('target', settings.target)
									.attr('href', settings.href)
									.text(settings.text);
									
								if ($this.is(':visible')) {
									$this.next('.InputAnchor').show();
								}
							} else {
								//remove target & href and set disabled
								options.$anchor
									.removeClass(options.enabledClassName)
									.attr('title', settings.title)
									.removeAttr('target')
									.removeAttr('href')
									.text(settings.text);
								$.anchorify.log('updateAnchor anchor is disabled.', options);
							}
						} else {
							$.anchorify.log('updateAnchor options are not set.', $this);
						}
					})
					.blur(function () {
						$(this).trigger('updateAnchor');
					})
					.keyup(function () {
						$(this).trigger('updateAnchor');
					})
					.trigger('updateAnchor');
				$.anchorify.log('anchor is built', $this);

				return $this;
			}
		}
	});

	/**
	 * extend jQuery with this plugin
	*/
	$.fn.anchorify = function (opts) {
		return this.each(function () {
			$.anchorify.build(this, opts);
		});
	};
})(jQuery);