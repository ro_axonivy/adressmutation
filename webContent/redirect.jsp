<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/> 
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache"); 
response.addHeader("Cache-Control","no-store"); 
String uri = ivy.cms.cr(ivy.html.get("in.redirectUrl"));
%>
<html>
<head>
</head>
<body>
<script type="text/javascript">
// wait a second to ensure the stupid IE is ready
setTimeout(function() {
	document.location = '<%=uri%>';
	},1000);
</script>

</body>
</html>