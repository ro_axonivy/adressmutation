package adressmutation;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Data", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Data extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -1852124775574952945L;

  private ch.ivyteam.ivy.scripting.objects.Tree menu;

  /**
   * Gets the field menu.
   * @return the value of the field menu; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Tree getMenu()
  {
    return menu;
  }

  /**
   * Sets the field menu.
   * @param _menu the new value of the field menu.
   */
  public void setMenu(ch.ivyteam.ivy.scripting.objects.Tree _menu)
  {
    menu = _menu;
  }

  private java.lang.String navigationPath;

  /**
   * Gets the field navigationPath.
   * @return the value of the field navigationPath; may be null.
   */
  public java.lang.String getNavigationPath()
  {
    return navigationPath;
  }

  /**
   * Sets the field navigationPath.
   * @param _navigationPath the new value of the field navigationPath.
   */
  public void setNavigationPath(java.lang.String _navigationPath)
  {
    navigationPath = _navigationPath;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.String user;

  /**
   * Gets the field user.
   * @return the value of the field user; may be null.
   */
  public java.lang.String getUser()
  {
    return user;
  }

  /**
   * Sets the field user.
   * @param _user the new value of the field user.
   */
  public void setUser(java.lang.String _user)
  {
    user = _user;
  }

  private java.lang.String passwort;

  /**
   * Gets the field passwort.
   * @return the value of the field passwort; may be null.
   */
  public java.lang.String getPasswort()
  {
    return passwort;
  }

  /**
   * Sets the field passwort.
   * @param _passwort the new value of the field passwort.
   */
  public void setPasswort(java.lang.String _passwort)
  {
    passwort = _passwort;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> liEnvironments;

  /**
   * Gets the field liEnvironments.
   * @return the value of the field liEnvironments; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getLiEnvironments()
  {
    return liEnvironments;
  }

  /**
   * Sets the field liEnvironments.
   * @param _liEnvironments the new value of the field liEnvironments.
   */
  public void setLiEnvironments(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _liEnvironments)
  {
    liEnvironments = _liEnvironments;
  }

  private java.lang.String currentEnvironment;

  /**
   * Gets the field currentEnvironment.
   * @return the value of the field currentEnvironment; may be null.
   */
  public java.lang.String getCurrentEnvironment()
  {
    return currentEnvironment;
  }

  /**
   * Sets the field currentEnvironment.
   * @param _currentEnvironment the new value of the field currentEnvironment.
   */
  public void setCurrentEnvironment(java.lang.String _currentEnvironment)
  {
    currentEnvironment = _currentEnvironment;
  }

  private java.lang.String redirectUrl;

  /**
   * Gets the field redirectUrl.
   * @return the value of the field redirectUrl; may be null.
   */
  public java.lang.String getRedirectUrl()
  {
    return redirectUrl;
  }

  /**
   * Sets the field redirectUrl.
   * @param _redirectUrl the new value of the field redirectUrl.
   */
  public void setRedirectUrl(java.lang.String _redirectUrl)
  {
    redirectUrl = _redirectUrl;
  }

}
