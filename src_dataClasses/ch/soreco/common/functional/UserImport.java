package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class UserImport", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class UserImport extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 5572427934933397324L;

  private ch.ivyteam.ivy.scripting.objects.Recordset userRS;

  /**
   * Gets the field userRS.
   * @return the value of the field userRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getUserRS()
  {
    return userRS;
  }

  /**
   * Sets the field userRS.
   * @param _userRS the new value of the field userRS.
   */
  public void setUserRS(ch.ivyteam.ivy.scripting.objects.Recordset _userRS)
  {
    userRS = _userRS;
  }

}
