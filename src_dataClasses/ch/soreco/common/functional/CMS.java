package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class CMS", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class CMS extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -4534725281821046054L;

  private java.lang.String cmsPath;

  /**
   * Gets the field cmsPath.
   * @return the value of the field cmsPath; may be null.
   */
  public java.lang.String getCmsPath()
  {
    return cmsPath;
  }

  /**
   * Sets the field cmsPath.
   * @param _cmsPath the new value of the field cmsPath.
   */
  public void setCmsPath(java.lang.String _cmsPath)
  {
    cmsPath = _cmsPath;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsCMS;

  /**
   * Gets the field rsCMS.
   * @return the value of the field rsCMS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsCMS()
  {
    return rsCMS;
  }

  /**
   * Sets the field rsCMS.
   * @param _rsCMS the new value of the field rsCMS.
   */
  public void setRsCMS(ch.ivyteam.ivy.scripting.objects.Recordset _rsCMS)
  {
    rsCMS = _rsCMS;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.String attributeUri;

  /**
   * Gets the field attributeUri.
   * @return the value of the field attributeUri; may be null.
   */
  public java.lang.String getAttributeUri()
  {
    return attributeUri;
  }

  /**
   * Sets the field attributeUri.
   * @param _attributeUri the new value of the field attributeUri.
   */
  public void setAttributeUri(java.lang.String _attributeUri)
  {
    attributeUri = _attributeUri;
  }

  private ch.soreco.webbies.common.html.form.AttributeContent attribute;

  /**
   * Gets the field attribute.
   * @return the value of the field attribute; may be null.
   */
  public ch.soreco.webbies.common.html.form.AttributeContent getAttribute()
  {
    return attribute;
  }

  /**
   * Sets the field attribute.
   * @param _attribute the new value of the field attribute.
   */
  public void setAttribute(ch.soreco.webbies.common.html.form.AttributeContent _attribute)
  {
    attribute = _attribute;
  }

  private java.lang.String attributeSource;

  /**
   * Gets the field attributeSource.
   * @return the value of the field attributeSource; may be null.
   */
  public java.lang.String getAttributeSource()
  {
    return attributeSource;
  }

  /**
   * Sets the field attributeSource.
   * @param _attributeSource the new value of the field attributeSource.
   */
  public void setAttributeSource(java.lang.String _attributeSource)
  {
    attributeSource = _attributeSource;
  }

  private java.lang.String attributeLabel;

  /**
   * Gets the field attributeLabel.
   * @return the value of the field attributeLabel; may be null.
   */
  public java.lang.String getAttributeLabel()
  {
    return attributeLabel;
  }

  /**
   * Sets the field attributeLabel.
   * @param _attributeLabel the new value of the field attributeLabel.
   */
  public void setAttributeLabel(java.lang.String _attributeLabel)
  {
    attributeLabel = _attributeLabel;
  }

  private java.lang.String attributeLabelCellAttributes;

  /**
   * Gets the field attributeLabelCellAttributes.
   * @return the value of the field attributeLabelCellAttributes; may be null.
   */
  public java.lang.String getAttributeLabelCellAttributes()
  {
    return attributeLabelCellAttributes;
  }

  /**
   * Sets the field attributeLabelCellAttributes.
   * @param _attributeLabelCellAttributes the new value of the field attributeLabelCellAttributes.
   */
  public void setAttributeLabelCellAttributes(java.lang.String _attributeLabelCellAttributes)
  {
    attributeLabelCellAttributes = _attributeLabelCellAttributes;
  }

  private java.lang.String attributeLabelAttributes;

  /**
   * Gets the field attributeLabelAttributes.
   * @return the value of the field attributeLabelAttributes; may be null.
   */
  public java.lang.String getAttributeLabelAttributes()
  {
    return attributeLabelAttributes;
  }

  /**
   * Sets the field attributeLabelAttributes.
   * @param _attributeLabelAttributes the new value of the field attributeLabelAttributes.
   */
  public void setAttributeLabelAttributes(java.lang.String _attributeLabelAttributes)
  {
    attributeLabelAttributes = _attributeLabelAttributes;
  }

  private java.lang.String attributeInputCellAttributes;

  /**
   * Gets the field attributeInputCellAttributes.
   * @return the value of the field attributeInputCellAttributes; may be null.
   */
  public java.lang.String getAttributeInputCellAttributes()
  {
    return attributeInputCellAttributes;
  }

  /**
   * Sets the field attributeInputCellAttributes.
   * @param _attributeInputCellAttributes the new value of the field attributeInputCellAttributes.
   */
  public void setAttributeInputCellAttributes(java.lang.String _attributeInputCellAttributes)
  {
    attributeInputCellAttributes = _attributeInputCellAttributes;
  }

  private java.lang.String attributeInputMeta;

  /**
   * Gets the field attributeInputMeta.
   * @return the value of the field attributeInputMeta; may be null.
   */
  public java.lang.String getAttributeInputMeta()
  {
    return attributeInputMeta;
  }

  /**
   * Sets the field attributeInputMeta.
   * @param _attributeInputMeta the new value of the field attributeInputMeta.
   */
  public void setAttributeInputMeta(java.lang.String _attributeInputMeta)
  {
    attributeInputMeta = _attributeInputMeta;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsAttributes;

  /**
   * Gets the field rsAttributes.
   * @return the value of the field rsAttributes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsAttributes()
  {
    return rsAttributes;
  }

  /**
   * Sets the field rsAttributes.
   * @param _rsAttributes the new value of the field rsAttributes.
   */
  public void setRsAttributes(ch.ivyteam.ivy.scripting.objects.Recordset _rsAttributes)
  {
    rsAttributes = _rsAttributes;
  }

  private java.lang.String contentType;

  /**
   * Gets the field contentType.
   * @return the value of the field contentType; may be null.
   */
  public java.lang.String getContentType()
  {
    return contentType;
  }

  /**
   * Sets the field contentType.
   * @param _contentType the new value of the field contentType.
   */
  public void setContentType(java.lang.String _contentType)
  {
    contentType = _contentType;
  }

  private java.lang.String attributeInputType;

  /**
   * Gets the field attributeInputType.
   * @return the value of the field attributeInputType; may be null.
   */
  public java.lang.String getAttributeInputType()
  {
    return attributeInputType;
  }

  /**
   * Sets the field attributeInputType.
   * @param _attributeInputType the new value of the field attributeInputType.
   */
  public void setAttributeInputType(java.lang.String _attributeInputType)
  {
    attributeInputType = _attributeInputType;
  }

  private java.lang.String filterUriContains;

  /**
   * Gets the field filterUriContains.
   * @return the value of the field filterUriContains; may be null.
   */
  public java.lang.String getFilterUriContains()
  {
    return filterUriContains;
  }

  /**
   * Sets the field filterUriContains.
   * @param _filterUriContains the new value of the field filterUriContains.
   */
  public void setFilterUriContains(java.lang.String _filterUriContains)
  {
    filterUriContains = _filterUriContains;
  }

  private java.lang.String filterClass;

  /**
   * Gets the field filterClass.
   * @return the value of the field filterClass; may be null.
   */
  public java.lang.String getFilterClass()
  {
    return filterClass;
  }

  /**
   * Sets the field filterClass.
   * @param _filterClass the new value of the field filterClass.
   */
  public void setFilterClass(java.lang.String _filterClass)
  {
    filterClass = _filterClass;
  }

  private java.lang.String filterContentType;

  /**
   * Gets the field filterContentType.
   * @return the value of the field filterContentType; may be null.
   */
  public java.lang.String getFilterContentType()
  {
    return filterContentType;
  }

  /**
   * Sets the field filterContentType.
   * @param _filterContentType the new value of the field filterContentType.
   */
  public void setFilterContentType(java.lang.String _filterContentType)
  {
    filterContentType = _filterContentType;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsCMSClone;

  /**
   * Gets the field rsCMSClone.
   * @return the value of the field rsCMSClone; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsCMSClone()
  {
    return rsCMSClone;
  }

  /**
   * Sets the field rsCMSClone.
   * @param _rsCMSClone the new value of the field rsCMSClone.
   */
  public void setRsCMSClone(ch.ivyteam.ivy.scripting.objects.Recordset _rsCMSClone)
  {
    rsCMSClone = _rsCMSClone;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsAttributesClone;

  /**
   * Gets the field rsAttributesClone.
   * @return the value of the field rsAttributesClone; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsAttributesClone()
  {
    return rsAttributesClone;
  }

  /**
   * Sets the field rsAttributesClone.
   * @param _rsAttributesClone the new value of the field rsAttributesClone.
   */
  public void setRsAttributesClone(ch.ivyteam.ivy.scripting.objects.Recordset _rsAttributesClone)
  {
    rsAttributesClone = _rsAttributesClone;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> liClasses;

  /**
   * Gets the field liClasses.
   * @return the value of the field liClasses; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getLiClasses()
  {
    return liClasses;
  }

  /**
   * Sets the field liClasses.
   * @param _liClasses the new value of the field liClasses.
   */
  public void setLiClasses(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _liClasses)
  {
    liClasses = _liClasses;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> liContentTypes;

  /**
   * Gets the field liContentTypes.
   * @return the value of the field liContentTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getLiContentTypes()
  {
    return liContentTypes;
  }

  /**
   * Sets the field liContentTypes.
   * @param _liContentTypes the new value of the field liContentTypes.
   */
  public void setLiContentTypes(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _liContentTypes)
  {
    liContentTypes = _liContentTypes;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> liFolders;

  /**
   * Gets the field liFolders.
   * @return the value of the field liFolders; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getLiFolders()
  {
    return liFolders;
  }

  /**
   * Sets the field liFolders.
   * @param _liFolders the new value of the field liFolders.
   */
  public void setLiFolders(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _liFolders)
  {
    liFolders = _liFolders;
  }

  private java.lang.String filterFolder;

  /**
   * Gets the field filterFolder.
   * @return the value of the field filterFolder; may be null.
   */
  public java.lang.String getFilterFolder()
  {
    return filterFolder;
  }

  /**
   * Sets the field filterFolder.
   * @param _filterFolder the new value of the field filterFolder.
   */
  public void setFilterFolder(java.lang.String _filterFolder)
  {
    filterFolder = _filterFolder;
  }

  private java.lang.String filterValue;

  /**
   * Gets the field filterValue.
   * @return the value of the field filterValue; may be null.
   */
  public java.lang.String getFilterValue()
  {
    return filterValue;
  }

  /**
   * Sets the field filterValue.
   * @param _filterValue the new value of the field filterValue.
   */
  public void setFilterValue(java.lang.String _filterValue)
  {
    filterValue = _filterValue;
  }

  private java.lang.Integer tabIndex;

  /**
   * Gets the field tabIndex.
   * @return the value of the field tabIndex; may be null.
   */
  public java.lang.Integer getTabIndex()
  {
    return tabIndex;
  }

  /**
   * Sets the field tabIndex.
   * @param _tabIndex the new value of the field tabIndex.
   */
  public void setTabIndex(java.lang.Integer _tabIndex)
  {
    tabIndex = _tabIndex;
  }

  private java.lang.String attributeSelectEnum;

  /**
   * Gets the field attributeSelectEnum.
   * @return the value of the field attributeSelectEnum; may be null.
   */
  public java.lang.String getAttributeSelectEnum()
  {
    return attributeSelectEnum;
  }

  /**
   * Sets the field attributeSelectEnum.
   * @param _attributeSelectEnum the new value of the field attributeSelectEnum.
   */
  public void setAttributeSelectEnum(java.lang.String _attributeSelectEnum)
  {
    attributeSelectEnum = _attributeSelectEnum;
  }

  private java.lang.Integer attributeVisualOrder;

  /**
   * Gets the field attributeVisualOrder.
   * @return the value of the field attributeVisualOrder; may be null.
   */
  public java.lang.Integer getAttributeVisualOrder()
  {
    return attributeVisualOrder;
  }

  /**
   * Sets the field attributeVisualOrder.
   * @param _attributeVisualOrder the new value of the field attributeVisualOrder.
   */
  public void setAttributeVisualOrder(java.lang.Integer _attributeVisualOrder)
  {
    attributeVisualOrder = _attributeVisualOrder;
  }

}
