package ch.soreco.common.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ScanningCleanUp", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ScanningCleanUp extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 5950473007913211247L;

  private transient java.lang.String backupFolder;

  /**
   * Gets the field backupFolder.
   * @return the value of the field backupFolder; may be null.
   */
  public java.lang.String getBackupFolder()
  {
    return backupFolder;
  }

  /**
   * Sets the field backupFolder.
   * @param _backupFolder the new value of the field backupFolder.
   */
  public void setBackupFolder(java.lang.String _backupFolder)
  {
    backupFolder = _backupFolder;
  }

  private transient java.lang.Number cleanUpPeriod;

  /**
   * Gets the field cleanUpPeriod.
   * @return the value of the field cleanUpPeriod; may be null.
   */
  public java.lang.Number getCleanUpPeriod()
  {
    return cleanUpPeriod;
  }

  /**
   * Sets the field cleanUpPeriod.
   * @param _cleanUpPeriod the new value of the field cleanUpPeriod.
   */
  public void setCleanUpPeriod(java.lang.Number _cleanUpPeriod)
  {
    cleanUpPeriod = _cleanUpPeriod;
  }

  private transient ch.ivyteam.ivy.scripting.objects.DateTime cleanUpTime;

  /**
   * Gets the field cleanUpTime.
   * @return the value of the field cleanUpTime; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.DateTime getCleanUpTime()
  {
    return cleanUpTime;
  }

  /**
   * Sets the field cleanUpTime.
   * @param _cleanUpTime the new value of the field cleanUpTime.
   */
  public void setCleanUpTime(ch.ivyteam.ivy.scripting.objects.DateTime _cleanUpTime)
  {
    cleanUpTime = _cleanUpTime;
  }

  private transient java.lang.String periodType;

  /**
   * Gets the field periodType.
   * @return the value of the field periodType; may be null.
   */
  public java.lang.String getPeriodType()
  {
    return periodType;
  }

  /**
   * Sets the field periodType.
   * @param _periodType the new value of the field periodType.
   */
  public void setPeriodType(java.lang.String _periodType)
  {
    periodType = _periodType;
  }

  private transient ch.ivyteam.ivy.scripting.objects.Recordset ordersToCleanUpRS;

  /**
   * Gets the field ordersToCleanUpRS.
   * @return the value of the field ordersToCleanUpRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrdersToCleanUpRS()
  {
    return ordersToCleanUpRS;
  }

  /**
   * Sets the field ordersToCleanUpRS.
   * @param _ordersToCleanUpRS the new value of the field ordersToCleanUpRS.
   */
  public void setOrdersToCleanUpRS(ch.ivyteam.ivy.scripting.objects.Recordset _ordersToCleanUpRS)
  {
    ordersToCleanUpRS = _ordersToCleanUpRS;
  }

  private java.lang.String qry;

  /**
   * Gets the field qry.
   * @return the value of the field qry; may be null.
   */
  public java.lang.String getQry()
  {
    return qry;
  }

  /**
   * Sets the field qry.
   * @param _qry the new value of the field qry.
   */
  public void setQry(java.lang.String _qry)
  {
    qry = _qry;
  }

  private java.lang.Number i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Number getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Number _i)
  {
    i = _i;
  }

  private java.lang.Number currentFileId;

  /**
   * Gets the field currentFileId.
   * @return the value of the field currentFileId; may be null.
   */
  public java.lang.Number getCurrentFileId()
  {
    return currentFileId;
  }

  /**
   * Sets the field currentFileId.
   * @param _currentFileId the new value of the field currentFileId.
   */
  public void setCurrentFileId(java.lang.Number _currentFileId)
  {
    currentFileId = _currentFileId;
  }

  private java.lang.String mode;

  /**
   * Gets the field mode.
   * @return the value of the field mode; may be null.
   */
  public java.lang.String getMode()
  {
    return mode;
  }

  /**
   * Sets the field mode.
   * @param _mode the new value of the field mode.
   */
  public void setMode(java.lang.String _mode)
  {
    mode = _mode;
  }

  private java.util.Date nextInvocation;

  /**
   * Gets the field nextInvocation.
   * @return the value of the field nextInvocation; may be null.
   */
  public java.util.Date getNextInvocation()
  {
    return nextInvocation;
  }

  /**
   * Sets the field nextInvocation.
   * @param _nextInvocation the new value of the field nextInvocation.
   */
  public void setNextInvocation(java.util.Date _nextInvocation)
  {
    nextInvocation = _nextInvocation;
  }

  private java.lang.Boolean canCleanUp;

  /**
   * Gets the field canCleanUp.
   * @return the value of the field canCleanUp; may be null.
   */
  public java.lang.Boolean getCanCleanUp()
  {
    return canCleanUp;
  }

  /**
   * Sets the field canCleanUp.
   * @param _canCleanUp the new value of the field canCleanUp.
   */
  public void setCanCleanUp(java.lang.Boolean _canCleanUp)
  {
    canCleanUp = _canCleanUp;
  }

  private java.lang.Number maxFilesToCleanUp;

  /**
   * Gets the field maxFilesToCleanUp.
   * @return the value of the field maxFilesToCleanUp; may be null.
   */
  public java.lang.Number getMaxFilesToCleanUp()
  {
    return maxFilesToCleanUp;
  }

  /**
   * Sets the field maxFilesToCleanUp.
   * @param _maxFilesToCleanUp the new value of the field maxFilesToCleanUp.
   */
  public void setMaxFilesToCleanUp(java.lang.Number _maxFilesToCleanUp)
  {
    maxFilesToCleanUp = _maxFilesToCleanUp;
  }

  private java.lang.Number ordersToCleanUpSize;

  /**
   * Gets the field ordersToCleanUpSize.
   * @return the value of the field ordersToCleanUpSize; may be null.
   */
  public java.lang.Number getOrdersToCleanUpSize()
  {
    return ordersToCleanUpSize;
  }

  /**
   * Sets the field ordersToCleanUpSize.
   * @param _ordersToCleanUpSize the new value of the field ordersToCleanUpSize.
   */
  public void setOrdersToCleanUpSize(java.lang.Number _ordersToCleanUpSize)
  {
    ordersToCleanUpSize = _ordersToCleanUpSize;
  }

  private java.lang.Number foundOrdersToCleanUpSize;

  /**
   * Gets the field foundOrdersToCleanUpSize.
   * @return the value of the field foundOrdersToCleanUpSize; may be null.
   */
  public java.lang.Number getFoundOrdersToCleanUpSize()
  {
    return foundOrdersToCleanUpSize;
  }

  /**
   * Sets the field foundOrdersToCleanUpSize.
   * @param _foundOrdersToCleanUpSize the new value of the field foundOrdersToCleanUpSize.
   */
  public void setFoundOrdersToCleanUpSize(java.lang.Number _foundOrdersToCleanUpSize)
  {
    foundOrdersToCleanUpSize = _foundOrdersToCleanUpSize;
  }

}
