package ch.soreco.common.webbies.cms.ui.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Upload", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Upload extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -8791516939485817696L;

  private java.lang.String targetUri;

  /**
   * Gets the field targetUri.
   * @return the value of the field targetUri; may be null.
   */
  public java.lang.String getTargetUri()
  {
    return targetUri;
  }

  /**
   * Sets the field targetUri.
   * @param _targetUri the new value of the field targetUri.
   */
  public void setTargetUri(java.lang.String _targetUri)
  {
    targetUri = _targetUri;
  }

  private ch.ivyteam.ivy.cm.CoType coType;

  /**
   * Gets the field coType.
   * @return the value of the field coType; may be null.
   */
  public ch.ivyteam.ivy.cm.CoType getCoType()
  {
    return coType;
  }

  /**
   * Sets the field coType.
   * @param _coType the new value of the field coType.
   */
  public void setCoType(ch.ivyteam.ivy.cm.CoType _coType)
  {
    coType = _coType;
  }

  private ch.ivyteam.ivy.scripting.objects.File contentFile;

  /**
   * Gets the field contentFile.
   * @return the value of the field contentFile; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getContentFile()
  {
    return contentFile;
  }

  /**
   * Sets the field contentFile.
   * @param _contentFile the new value of the field contentFile.
   */
  public void setContentFile(ch.ivyteam.ivy.scripting.objects.File _contentFile)
  {
    contentFile = _contentFile;
  }

}
