package ch.soreco.common.webbies.cms.ui.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ContentEdit", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ContentEdit extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -3623193904498819081L;

  private java.lang.String uri;

  /**
   * Gets the field uri.
   * @return the value of the field uri; may be null.
   */
  public java.lang.String getUri()
  {
    return uri;
  }

  /**
   * Sets the field uri.
   * @param _uri the new value of the field uri.
   */
  public void setUri(java.lang.String _uri)
  {
    uri = _uri;
  }

  private java.lang.String type;

  /**
   * Gets the field type.
   * @return the value of the field type; may be null.
   */
  public java.lang.String getType()
  {
    return type;
  }

  /**
   * Sets the field type.
   * @param _type the new value of the field type.
   */
  public void setType(java.lang.String _type)
  {
    type = _type;
  }

  private java.lang.String value;

  /**
   * Gets the field value.
   * @return the value of the field value; may be null.
   */
  public java.lang.String getValue()
  {
    return value;
  }

  /**
   * Sets the field value.
   * @param _value the new value of the field value.
   */
  public void setValue(java.lang.String _value)
  {
    value = _value;
  }

}
