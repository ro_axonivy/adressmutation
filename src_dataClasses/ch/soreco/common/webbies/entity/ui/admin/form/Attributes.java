package ch.soreco.common.webbies.entity.ui.admin.form;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Attributes", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Attributes extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3237308056993182294L;

  private java.lang.Class clazz;

  /**
   * Gets the field clazz.
   * @return the value of the field clazz; may be null.
   */
  public java.lang.Class getClazz()
  {
    return clazz;
  }

  /**
   * Sets the field clazz.
   * @param _clazz the new value of the field clazz.
   */
  public void setClazz(java.lang.Class _clazz)
  {
    clazz = _clazz;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rs;

  /**
   * Gets the field rs.
   * @return the value of the field rs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRs()
  {
    return rs;
  }

  /**
   * Sets the field rs.
   * @param _rs the new value of the field rs.
   */
  public void setRs(ch.ivyteam.ivy.scripting.objects.Recordset _rs)
  {
    rs = _rs;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private ch.soreco.webbies.common.html.generics.AttributeContent attributeContent;

  /**
   * Gets the field attributeContent.
   * @return the value of the field attributeContent; may be null.
   */
  public ch.soreco.webbies.common.html.generics.AttributeContent getAttributeContent()
  {
    return attributeContent;
  }

  /**
   * Sets the field attributeContent.
   * @param _attributeContent the new value of the field attributeContent.
   */
  public void setAttributeContent(ch.soreco.webbies.common.html.generics.AttributeContent _attributeContent)
  {
    attributeContent = _attributeContent;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsEdit;

  /**
   * Gets the field rsEdit.
   * @return the value of the field rsEdit; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsEdit()
  {
    return rsEdit;
  }

  /**
   * Sets the field rsEdit.
   * @param _rsEdit the new value of the field rsEdit.
   */
  public void setRsEdit(ch.ivyteam.ivy.scripting.objects.Recordset _rsEdit)
  {
    rsEdit = _rsEdit;
  }

  private java.lang.String classLabel;

  /**
   * Gets the field classLabel.
   * @return the value of the field classLabel; may be null.
   */
  public java.lang.String getClassLabel()
  {
    return classLabel;
  }

  /**
   * Sets the field classLabel.
   * @param _classLabel the new value of the field classLabel.
   */
  public void setClassLabel(java.lang.String _classLabel)
  {
    classLabel = _classLabel;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.webbies.common.db.PersistenceMetaField> persistenceMetaFields;

  /**
   * Gets the field persistenceMetaFields.
   * @return the value of the field persistenceMetaFields; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.webbies.common.db.PersistenceMetaField> getPersistenceMetaFields()
  {
    return persistenceMetaFields;
  }

  /**
   * Sets the field persistenceMetaFields.
   * @param _persistenceMetaFields the new value of the field persistenceMetaFields.
   */
  public void setPersistenceMetaFields(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.webbies.common.db.PersistenceMetaField> _persistenceMetaFields)
  {
    persistenceMetaFields = _persistenceMetaFields;
  }

  private java.lang.Boolean isPersistentClass;

  /**
   * Gets the field isPersistentClass.
   * @return the value of the field isPersistentClass; may be null.
   */
  public java.lang.Boolean getIsPersistentClass()
  {
    return isPersistentClass;
  }

  /**
   * Sets the field isPersistentClass.
   * @param _isPersistentClass the new value of the field isPersistentClass.
   */
  public void setIsPersistentClass(java.lang.Boolean _isPersistentClass)
  {
    isPersistentClass = _isPersistentClass;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsAssociations;

  /**
   * Gets the field rsAssociations.
   * @return the value of the field rsAssociations; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsAssociations()
  {
    return rsAssociations;
  }

  /**
   * Sets the field rsAssociations.
   * @param _rsAssociations the new value of the field rsAssociations.
   */
  public void setRsAssociations(ch.ivyteam.ivy.scripting.objects.Recordset _rsAssociations)
  {
    rsAssociations = _rsAssociations;
  }

  private java.lang.String associationHtml;

  /**
   * Gets the field associationHtml.
   * @return the value of the field associationHtml; may be null.
   */
  public java.lang.String getAssociationHtml()
  {
    return associationHtml;
  }

  /**
   * Sets the field associationHtml.
   * @param _associationHtml the new value of the field associationHtml.
   */
  public void setAssociationHtml(java.lang.String _associationHtml)
  {
    associationHtml = _associationHtml;
  }

  private java.lang.String associatedClasses;

  /**
   * Gets the field associatedClasses.
   * @return the value of the field associatedClasses; may be null.
   */
  public java.lang.String getAssociatedClasses()
  {
    return associatedClasses;
  }

  /**
   * Sets the field associatedClasses.
   * @param _associatedClasses the new value of the field associatedClasses.
   */
  public void setAssociatedClasses(java.lang.String _associatedClasses)
  {
    associatedClasses = _associatedClasses;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> persistenceUnits;

  /**
   * Gets the field persistenceUnits.
   * @return the value of the field persistenceUnits; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getPersistenceUnits()
  {
    return persistenceUnits;
  }

  /**
   * Sets the field persistenceUnits.
   * @param _persistenceUnits the new value of the field persistenceUnits.
   */
  public void setPersistenceUnits(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _persistenceUnits)
  {
    persistenceUnits = _persistenceUnits;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> views;

  /**
   * Gets the field views.
   * @return the value of the field views; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getViews()
  {
    return views;
  }

  /**
   * Sets the field views.
   * @param _views the new value of the field views.
   */
  public void setViews(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _views)
  {
    views = _views;
  }

  private java.lang.String view;

  /**
   * Gets the field view.
   * @return the value of the field view; may be null.
   */
  public java.lang.String getView()
  {
    return view;
  }

  /**
   * Sets the field view.
   * @param _view the new value of the field view.
   */
  public void setView(java.lang.String _view)
  {
    view = _view;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsExport;

  /**
   * Gets the field rsExport.
   * @return the value of the field rsExport; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsExport()
  {
    return rsExport;
  }

  /**
   * Sets the field rsExport.
   * @param _rsExport the new value of the field rsExport.
   */
  public void setRsExport(ch.ivyteam.ivy.scripting.objects.Recordset _rsExport)
  {
    rsExport = _rsExport;
  }

  private java.io.File excel;

  /**
   * Gets the field excel.
   * @return the value of the field excel; may be null.
   */
  public java.io.File getExcel()
  {
    return excel;
  }

  /**
   * Sets the field excel.
   * @param _excel the new value of the field excel.
   */
  public void setExcel(java.io.File _excel)
  {
    excel = _excel;
  }

  private ch.ivyteam.ivy.scripting.objects.File contentFile;

  /**
   * Gets the field contentFile.
   * @return the value of the field contentFile; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getContentFile()
  {
    return contentFile;
  }

  /**
   * Sets the field contentFile.
   * @param _contentFile the new value of the field contentFile.
   */
  public void setContentFile(ch.ivyteam.ivy.scripting.objects.File _contentFile)
  {
    contentFile = _contentFile;
  }

  private ch.soreco.webbies.common.html.generics.AttributeContentType currentType;

  /**
   * Gets the field currentType.
   * @return the value of the field currentType; may be null.
   */
  public ch.soreco.webbies.common.html.generics.AttributeContentType getCurrentType()
  {
    return currentType;
  }

  /**
   * Sets the field currentType.
   * @param _currentType the new value of the field currentType.
   */
  public void setCurrentType(ch.soreco.webbies.common.html.generics.AttributeContentType _currentType)
  {
    currentType = _currentType;
  }

  private java.lang.String contentView;

  /**
   * Gets the field contentView.
   * @return the value of the field contentView; may be null.
   */
  public java.lang.String getContentView()
  {
    return contentView;
  }

  /**
   * Sets the field contentView.
   * @param _contentView the new value of the field contentView.
   */
  public void setContentView(java.lang.String _contentView)
  {
    contentView = _contentView;
  }

}
