package ch.soreco.common.webbies.entity.ui.admin;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ClassesAdmin", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ClassesAdmin extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 7759269280421543560L;

  private ch.ivyteam.ivy.scripting.objects.Recordset rsClasses;

  /**
   * Gets the field rsClasses.
   * @return the value of the field rsClasses; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsClasses()
  {
    return rsClasses;
  }

  /**
   * Sets the field rsClasses.
   * @param _rsClasses the new value of the field rsClasses.
   */
  public void setRsClasses(ch.ivyteam.ivy.scripting.objects.Recordset _rsClasses)
  {
    rsClasses = _rsClasses;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Class clazz;

  /**
   * Gets the field clazz.
   * @return the value of the field clazz; may be null.
   */
  public java.lang.Class getClazz()
  {
    return clazz;
  }

  /**
   * Sets the field clazz.
   * @param _clazz the new value of the field clazz.
   */
  public void setClazz(java.lang.Class _clazz)
  {
    clazz = _clazz;
  }

  private ch.soreco.webbies.common.html.generics.AttributeContent attributeContent;

  /**
   * Gets the field attributeContent.
   * @return the value of the field attributeContent; may be null.
   */
  public ch.soreco.webbies.common.html.generics.AttributeContent getAttributeContent()
  {
    return attributeContent;
  }

  /**
   * Sets the field attributeContent.
   * @param _attributeContent the new value of the field attributeContent.
   */
  public void setAttributeContent(ch.soreco.webbies.common.html.generics.AttributeContent _attributeContent)
  {
    attributeContent = _attributeContent;
  }

  private java.io.File excel;

  /**
   * Gets the field excel.
   * @return the value of the field excel; may be null.
   */
  public java.io.File getExcel()
  {
    return excel;
  }

  /**
   * Sets the field excel.
   * @param _excel the new value of the field excel.
   */
  public void setExcel(java.io.File _excel)
  {
    excel = _excel;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsExport;

  /**
   * Gets the field rsExport.
   * @return the value of the field rsExport; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsExport()
  {
    return rsExport;
  }

  /**
   * Sets the field rsExport.
   * @param _rsExport the new value of the field rsExport.
   */
  public void setRsExport(ch.ivyteam.ivy.scripting.objects.Recordset _rsExport)
  {
    rsExport = _rsExport;
  }

  private ch.ivyteam.ivy.scripting.objects.File importFile;

  /**
   * Gets the field importFile.
   * @return the value of the field importFile; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getImportFile()
  {
    return importFile;
  }

  /**
   * Sets the field importFile.
   * @param _importFile the new value of the field importFile.
   */
  public void setImportFile(ch.ivyteam.ivy.scripting.objects.File _importFile)
  {
    importFile = _importFile;
  }

}
