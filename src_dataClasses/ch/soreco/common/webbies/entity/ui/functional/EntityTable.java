package ch.soreco.common.webbies.entity.ui.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class EntityTable", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class EntityTable extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -4251097373674188673L;

  private ch.ivyteam.ivy.scripting.objects.Recordset rs;

  /**
   * Gets the field rs.
   * @return the value of the field rs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRs()
  {
    return rs;
  }

  /**
   * Sets the field rs.
   * @param _rs the new value of the field rs.
   */
  public void setRs(ch.ivyteam.ivy.scripting.objects.Recordset _rs)
  {
    rs = _rs;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Class clazz;

  /**
   * Gets the field clazz.
   * @return the value of the field clazz; may be null.
   */
  public java.lang.Class getClazz()
  {
    return clazz;
  }

  /**
   * Sets the field clazz.
   * @param _clazz the new value of the field clazz.
   */
  public void setClazz(java.lang.Class _clazz)
  {
    clazz = _clazz;
  }

  private ch.ivyteam.ivy.scripting.objects.CompositeObject entity;

  /**
   * Gets the field entity.
   * @return the value of the field entity; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getEntity()
  {
    return entity;
  }

  /**
   * Sets the field entity.
   * @param _entity the new value of the field entity.
   */
  public void setEntity(ch.ivyteam.ivy.scripting.objects.CompositeObject _entity)
  {
    entity = _entity;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.CompositeObject> entities;

  /**
   * Gets the field entities.
   * @return the value of the field entities; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.CompositeObject> getEntities()
  {
    return entities;
  }

  /**
   * Sets the field entities.
   * @param _entities the new value of the field entities.
   */
  public void setEntities(ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.CompositeObject> _entities)
  {
    entities = _entities;
  }

  private java.lang.String classLabel;

  /**
   * Gets the field classLabel.
   * @return the value of the field classLabel; may be null.
   */
  public java.lang.String getClassLabel()
  {
    return classLabel;
  }

  /**
   * Sets the field classLabel.
   * @param _classLabel the new value of the field classLabel.
   */
  public void setClassLabel(java.lang.String _classLabel)
  {
    classLabel = _classLabel;
  }

  private java.lang.StringBuilder listHtml;

  /**
   * Gets the field listHtml.
   * @return the value of the field listHtml; may be null.
   */
  public java.lang.StringBuilder getListHtml()
  {
    return listHtml;
  }

  /**
   * Sets the field listHtml.
   * @param _listHtml the new value of the field listHtml.
   */
  public void setListHtml(java.lang.StringBuilder _listHtml)
  {
    listHtml = _listHtml;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.List<java.lang.Object>> listEntities;

  /**
   * Gets the field listEntities.
   * @return the value of the field listEntities; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.List<java.lang.Object>> getListEntities()
  {
    return listEntities;
  }

  /**
   * Sets the field listEntities.
   * @param _listEntities the new value of the field listEntities.
   */
  public void setListEntities(ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.scripting.objects.List<java.lang.Object>> _listEntities)
  {
    listEntities = _listEntities;
  }

  private java.lang.String view;

  /**
   * Gets the field view.
   * @return the value of the field view; may be null.
   */
  public java.lang.String getView()
  {
    return view;
  }

  /**
   * Sets the field view.
   * @param _view the new value of the field view.
   */
  public void setView(java.lang.String _view)
  {
    view = _view;
  }

  private java.lang.Boolean canEdit;

  /**
   * Gets the field canEdit.
   * @return the value of the field canEdit; may be null.
   */
  public java.lang.Boolean getCanEdit()
  {
    return canEdit;
  }

  /**
   * Sets the field canEdit.
   * @param _canEdit the new value of the field canEdit.
   */
  public void setCanEdit(java.lang.Boolean _canEdit)
  {
    canEdit = _canEdit;
  }

  private java.lang.Boolean canDelete;

  /**
   * Gets the field canDelete.
   * @return the value of the field canDelete; may be null.
   */
  public java.lang.Boolean getCanDelete()
  {
    return canDelete;
  }

  /**
   * Sets the field canDelete.
   * @param _canDelete the new value of the field canDelete.
   */
  public void setCanDelete(java.lang.Boolean _canDelete)
  {
    canDelete = _canDelete;
  }

  private java.lang.Boolean canExport;

  /**
   * Gets the field canExport.
   * @return the value of the field canExport; may be null.
   */
  public java.lang.Boolean getCanExport()
  {
    return canExport;
  }

  /**
   * Sets the field canExport.
   * @param _canExport the new value of the field canExport.
   */
  public void setCanExport(java.lang.Boolean _canExport)
  {
    canExport = _canExport;
  }

  private java.lang.Boolean canImport;

  /**
   * Gets the field canImport.
   * @return the value of the field canImport; may be null.
   */
  public java.lang.Boolean getCanImport()
  {
    return canImport;
  }

  /**
   * Sets the field canImport.
   * @param _canImport the new value of the field canImport.
   */
  public void setCanImport(java.lang.Boolean _canImport)
  {
    canImport = _canImport;
  }

  private java.io.File excel;

  /**
   * Gets the field excel.
   * @return the value of the field excel; may be null.
   */
  public java.io.File getExcel()
  {
    return excel;
  }

  /**
   * Sets the field excel.
   * @param _excel the new value of the field excel.
   */
  public void setExcel(java.io.File _excel)
  {
    excel = _excel;
  }

  private ch.ivyteam.ivy.scripting.objects.File importFile;

  /**
   * Gets the field importFile.
   * @return the value of the field importFile; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getImportFile()
  {
    return importFile;
  }

  /**
   * Sets the field importFile.
   * @param _importFile the new value of the field importFile.
   */
  public void setImportFile(ch.ivyteam.ivy.scripting.objects.File _importFile)
  {
    importFile = _importFile;
  }

  private ch.ivyteam.ivy.scripting.objects.CompositeObject filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.ivyteam.ivy.scripting.objects.CompositeObject _filter)
  {
    filter = _filter;
  }

  private java.lang.Boolean canFilter;

  /**
   * Gets the field canFilter.
   * @return the value of the field canFilter; may be null.
   */
  public java.lang.Boolean getCanFilter()
  {
    return canFilter;
  }

  /**
   * Sets the field canFilter.
   * @param _canFilter the new value of the field canFilter.
   */
  public void setCanFilter(java.lang.Boolean _canFilter)
  {
    canFilter = _canFilter;
  }

  private java.lang.String filterFormHtml;

  /**
   * Gets the field filterFormHtml.
   * @return the value of the field filterFormHtml; may be null.
   */
  public java.lang.String getFilterFormHtml()
  {
    return filterFormHtml;
  }

  /**
   * Sets the field filterFormHtml.
   * @param _filterFormHtml the new value of the field filterFormHtml.
   */
  public void setFilterFormHtml(java.lang.String _filterFormHtml)
  {
    filterFormHtml = _filterFormHtml;
  }

  private java.lang.Integer filterLotSize;

  /**
   * Gets the field filterLotSize.
   * @return the value of the field filterLotSize; may be null.
   */
  public java.lang.Integer getFilterLotSize()
  {
    return filterLotSize;
  }

  /**
   * Sets the field filterLotSize.
   * @param _filterLotSize the new value of the field filterLotSize.
   */
  public void setFilterLotSize(java.lang.Integer _filterLotSize)
  {
    filterLotSize = _filterLotSize;
  }

  private java.lang.String filterView;

  /**
   * Gets the field filterView.
   * @return the value of the field filterView; may be null.
   */
  public java.lang.String getFilterView()
  {
    return filterView;
  }

  /**
   * Sets the field filterView.
   * @param _filterView the new value of the field filterView.
   */
  public void setFilterView(java.lang.String _filterView)
  {
    filterView = _filterView;
  }

}
