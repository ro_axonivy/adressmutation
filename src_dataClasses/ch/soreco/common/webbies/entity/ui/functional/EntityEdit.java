package ch.soreco.common.webbies.entity.ui.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class EntityEdit", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class EntityEdit extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -4158615171172889658L;

  private java.lang.String className;

  /**
   * Gets the field className.
   * @return the value of the field className; may be null.
   */
  public java.lang.String getClassName()
  {
    return className;
  }

  /**
   * Sets the field className.
   * @param _className the new value of the field className.
   */
  public void setClassName(java.lang.String _className)
  {
    className = _className;
  }

  /**
   * By given class name, persistant composite object can be loaded.
   */
  private java.lang.Class clazz;

  /**
   * Gets the field clazz.
   * @return the value of the field clazz; may be null.
   */
  public java.lang.Class getClazz()
  {
    return clazz;
  }

  /**
   * Sets the field clazz.
   * @param _clazz the new value of the field clazz.
   */
  public void setClazz(java.lang.Class _clazz)
  {
    clazz = _clazz;
  }

  /**
   * if null and write mode, an "Add" Form will be displayed.
   */
  private java.lang.Integer key;

  /**
   * Gets the field key.
   * @return the value of the field key; may be null.
   */
  public java.lang.Integer getKey()
  {
    return key;
  }

  /**
   * Sets the field key.
   * @param _key the new value of the field key.
   */
  public void setKey(java.lang.Integer _key)
  {
    key = _key;
  }

  /**
   * Pass True to get a Form
   */
  private java.lang.String write;

  /**
   * Gets the field write.
   * @return the value of the field write; may be null.
   */
  public java.lang.String getWrite()
  {
    return write;
  }

  /**
   * Sets the field write.
   * @param _write the new value of the field write.
   */
  public void setWrite(java.lang.String _write)
  {
    write = _write;
  }

  /**
   * Entity of by the Form will be displayed.
   */
  private ch.ivyteam.ivy.scripting.objects.CompositeObject entity;

  /**
   * Gets the field entity.
   * @return the value of the field entity; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getEntity()
  {
    return entity;
  }

  /**
   * Sets the field entity.
   * @param _entity the new value of the field entity.
   */
  public void setEntity(ch.ivyteam.ivy.scripting.objects.CompositeObject _entity)
  {
    entity = _entity;
  }

  /**
   * Html Text
   */
  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private java.lang.String classLabel;

  /**
   * Gets the field classLabel.
   * @return the value of the field classLabel; may be null.
   */
  public java.lang.String getClassLabel()
  {
    return classLabel;
  }

  /**
   * Sets the field classLabel.
   * @param _classLabel the new value of the field classLabel.
   */
  public void setClassLabel(java.lang.String _classLabel)
  {
    classLabel = _classLabel;
  }

  private java.lang.String associateClassName;

  /**
   * Gets the field associateClassName.
   * @return the value of the field associateClassName; may be null.
   */
  public java.lang.String getAssociateClassName()
  {
    return associateClassName;
  }

  /**
   * Sets the field associateClassName.
   * @param _associateClassName the new value of the field associateClassName.
   */
  public void setAssociateClassName(java.lang.String _associateClassName)
  {
    associateClassName = _associateClassName;
  }

  private ch.ivyteam.ivy.scripting.objects.CompositeObject associateEntity;

  /**
   * Gets the field associateEntity.
   * @return the value of the field associateEntity; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getAssociateEntity()
  {
    return associateEntity;
  }

  /**
   * Sets the field associateEntity.
   * @param _associateEntity the new value of the field associateEntity.
   */
  public void setAssociateEntity(ch.ivyteam.ivy.scripting.objects.CompositeObject _associateEntity)
  {
    associateEntity = _associateEntity;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> associatableClasses;

  /**
   * Gets the field associatableClasses.
   * @return the value of the field associatableClasses; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> getAssociatableClasses()
  {
    return associatableClasses;
  }

  /**
   * Sets the field associatableClasses.
   * @param _associatableClasses the new value of the field associatableClasses.
   */
  public void setAssociatableClasses(ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> _associatableClasses)
  {
    associatableClasses = _associatableClasses;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Boolean isSet;

  /**
   * Gets the field isSet.
   * @return the value of the field isSet; may be null.
   */
  public java.lang.Boolean getIsSet()
  {
    return isSet;
  }

  /**
   * Sets the field isSet.
   * @param _isSet the new value of the field isSet.
   */
  public void setIsSet(java.lang.Boolean _isSet)
  {
    isSet = _isSet;
  }

  private java.lang.String view;

  /**
   * Gets the field view.
   * @return the value of the field view; may be null.
   */
  public java.lang.String getView()
  {
    return view;
  }

  /**
   * Sets the field view.
   * @param _view the new value of the field view.
   */
  public void setView(java.lang.String _view)
  {
    view = _view;
  }

  /**
   * The cloned entity before saving the form.
   */
  private ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld;

  /**
   * Gets the field entityOld.
   * @return the value of the field entityOld; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getEntityOld()
  {
    return entityOld;
  }

  /**
   * Sets the field entityOld.
   * @param _entityOld the new value of the field entityOld.
   */
  public void setEntityOld(ch.ivyteam.ivy.scripting.objects.CompositeObject _entityOld)
  {
    entityOld = _entityOld;
  }

}
