package ch.soreco.common.template.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class SplitPane", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class SplitPane extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -710604744646897857L;

  private java.lang.String orientation;

  /**
   * Gets the field orientation.
   * @return the value of the field orientation; may be null.
   */
  public java.lang.String getOrientation()
  {
    return orientation;
  }

  /**
   * Sets the field orientation.
   * @param _orientation the new value of the field orientation.
   */
  public void setOrientation(java.lang.String _orientation)
  {
    orientation = _orientation;
  }

  private java.lang.String panel1;

  /**
   * Gets the field panel1.
   * @return the value of the field panel1; may be null.
   */
  public java.lang.String getPanel1()
  {
    return panel1;
  }

  /**
   * Sets the field panel1.
   * @param _panel1 the new value of the field panel1.
   */
  public void setPanel1(java.lang.String _panel1)
  {
    panel1 = _panel1;
  }

  private java.lang.String panel2;

  /**
   * Gets the field panel2.
   * @return the value of the field panel2; may be null.
   */
  public java.lang.String getPanel2()
  {
    return panel2;
  }

  /**
   * Sets the field panel2.
   * @param _panel2 the new value of the field panel2.
   */
  public void setPanel2(java.lang.String _panel2)
  {
    panel2 = _panel2;
  }

  private java.lang.String balance;

  /**
   * Gets the field balance.
   * @return the value of the field balance; may be null.
   */
  public java.lang.String getBalance()
  {
    return balance;
  }

  /**
   * Sets the field balance.
   * @param _balance the new value of the field balance.
   */
  public void setBalance(java.lang.String _balance)
  {
    balance = _balance;
  }

  private java.lang.String generatedHTML;

  /**
   * Gets the field generatedHTML.
   * @return the value of the field generatedHTML; may be null.
   */
  public java.lang.String getGeneratedHTML()
  {
    return generatedHTML;
  }

  /**
   * Sets the field generatedHTML.
   * @param _generatedHTML the new value of the field generatedHTML.
   */
  public void setGeneratedHTML(java.lang.String _generatedHTML)
  {
    generatedHTML = _generatedHTML;
  }

}
