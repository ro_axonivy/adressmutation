package ch.soreco.postscanning.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class PostScanning", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class PostScanning extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 6074650744703938517L;

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orderBook;

  /**
   * Gets the field orderBook.
   * @return the value of the field orderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrderBook()
  {
    return orderBook;
  }

  /**
   * Sets the field orderBook.
   * @param _orderBook the new value of the field orderBook.
   */
  public void setOrderBook(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orderBook)
  {
    orderBook = _orderBook;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrderBook;

  /**
   * Gets the field rsOrderBook.
   * @return the value of the field rsOrderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrderBook()
  {
    return rsOrderBook;
  }

  /**
   * Sets the field rsOrderBook.
   * @param _rsOrderBook the new value of the field rsOrderBook.
   */
  public void setRsOrderBook(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrderBook)
  {
    rsOrderBook = _rsOrderBook;
  }

  private ch.soreco.orderbook.bo.Order currentOrder;

  /**
   * Gets the field currentOrder.
   * @return the value of the field currentOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getCurrentOrder()
  {
    return currentOrder;
  }

  /**
   * Sets the field currentOrder.
   * @param _currentOrder the new value of the field currentOrder.
   */
  public void setCurrentOrder(ch.soreco.orderbook.bo.Order _currentOrder)
  {
    currentOrder = _currentOrder;
  }

}
