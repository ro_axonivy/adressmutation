package ch.soreco.archive.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderRepair", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderRepair extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -1874058270605437946L;

  private ch.soreco.orderbook.bo.Order currentOrder;

  /**
   * Gets the field currentOrder.
   * @return the value of the field currentOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getCurrentOrder()
  {
    return currentOrder;
  }

  /**
   * Sets the field currentOrder.
   * @param _currentOrder the new value of the field currentOrder.
   */
  public void setCurrentOrder(ch.soreco.orderbook.bo.Order _currentOrder)
  {
    currentOrder = _currentOrder;
  }

  private ch.soreco.orderbook.ui.enums.ViewType viewType;

  /**
   * Gets the field viewType.
   * @return the value of the field viewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getViewType()
  {
    return viewType;
  }

  /**
   * Sets the field viewType.
   * @param _viewType the new value of the field viewType.
   */
  public void setViewType(ch.soreco.orderbook.ui.enums.ViewType _viewType)
  {
    viewType = _viewType;
  }

  private java.lang.String panelCaption;

  /**
   * Gets the field panelCaption.
   * @return the value of the field panelCaption; may be null.
   */
  public java.lang.String getPanelCaption()
  {
    return panelCaption;
  }

  /**
   * Sets the field panelCaption.
   * @param _panelCaption the new value of the field panelCaption.
   */
  public void setPanelCaption(java.lang.String _panelCaption)
  {
    panelCaption = _panelCaption;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset preparedOrders;

  /**
   * Gets the field preparedOrders.
   * @return the value of the field preparedOrders; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getPreparedOrders()
  {
    return preparedOrders;
  }

  /**
   * Sets the field preparedOrders.
   * @param _preparedOrders the new value of the field preparedOrders.
   */
  public void setPreparedOrders(ch.ivyteam.ivy.scripting.objects.Recordset _preparedOrders)
  {
    preparedOrders = _preparedOrders;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset inRepairOrders;

  /**
   * Gets the field inRepairOrders.
   * @return the value of the field inRepairOrders; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getInRepairOrders()
  {
    return inRepairOrders;
  }

  /**
   * Sets the field inRepairOrders.
   * @param _inRepairOrders the new value of the field inRepairOrders.
   */
  public void setInRepairOrders(ch.ivyteam.ivy.scripting.objects.Recordset _inRepairOrders)
  {
    inRepairOrders = _inRepairOrders;
  }

  private java.lang.Number index;

  /**
   * Gets the field index.
   * @return the value of the field index; may be null.
   */
  public java.lang.Number getIndex()
  {
    return index;
  }

  /**
   * Sets the field index.
   * @param _index the new value of the field index.
   */
  public void setIndex(java.lang.Number _index)
  {
    index = _index;
  }

  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scanningList;

  /**
   * Gets the field scanningList.
   * @return the value of the field scanningList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScanningList()
  {
    return scanningList;
  }

  /**
   * Sets the field scanningList.
   * @param _scanningList the new value of the field scanningList.
   */
  public void setScanningList(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scanningList)
  {
    scanningList = _scanningList;
  }

  private java.lang.Number tabCount;

  /**
   * Gets the field tabCount.
   * @return the value of the field tabCount; may be null.
   */
  public java.lang.Number getTabCount()
  {
    return tabCount;
  }

  /**
   * Sets the field tabCount.
   * @param _tabCount the new value of the field tabCount.
   */
  public void setTabCount(java.lang.Number _tabCount)
  {
    tabCount = _tabCount;
  }

  private java.lang.Number tabIndex;

  /**
   * Gets the field tabIndex.
   * @return the value of the field tabIndex; may be null.
   */
  public java.lang.Number getTabIndex()
  {
    return tabIndex;
  }

  /**
   * Sets the field tabIndex.
   * @param _tabIndex the new value of the field tabIndex.
   */
  public void setTabIndex(java.lang.Number _tabIndex)
  {
    tabIndex = _tabIndex;
  }

  private ch.soreco.orderbook.bo.Order filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Order _filter)
  {
    filter = _filter;
  }

  private java.lang.String uiType;

  /**
   * Gets the field uiType.
   * @return the value of the field uiType; may be null.
   */
  public java.lang.String getUiType()
  {
    return uiType;
  }

  /**
   * Sets the field uiType.
   * @param _uiType the new value of the field uiType.
   */
  public void setUiType(java.lang.String _uiType)
  {
    uiType = _uiType;
  }

  private java.lang.Integer attachmentFileId;

  /**
   * Gets the field attachmentFileId.
   * @return the value of the field attachmentFileId; may be null.
   */
  public java.lang.Integer getAttachmentFileId()
  {
    return attachmentFileId;
  }

  /**
   * Sets the field attachmentFileId.
   * @param _attachmentFileId the new value of the field attachmentFileId.
   */
  public void setAttachmentFileId(java.lang.Integer _attachmentFileId)
  {
    attachmentFileId = _attachmentFileId;
  }

}
