package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderScannings", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderScannings extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -5413025617174358133L;

  private java.lang.Integer orderId;

  /**
   * Gets the field orderId.
   * @return the value of the field orderId; may be null.
   */
  public java.lang.Integer getOrderId()
  {
    return orderId;
  }

  /**
   * Sets the field orderId.
   * @param _orderId the new value of the field orderId.
   */
  public void setOrderId(java.lang.Integer _orderId)
  {
    orderId = _orderId;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private java.lang.Integer tabCount;

  /**
   * Gets the field tabCount.
   * @return the value of the field tabCount; may be null.
   */
  public java.lang.Integer getTabCount()
  {
    return tabCount;
  }

  /**
   * Sets the field tabCount.
   * @param _tabCount the new value of the field tabCount.
   */
  public void setTabCount(java.lang.Integer _tabCount)
  {
    tabCount = _tabCount;
  }

  private java.lang.Integer tabIndex;

  /**
   * Gets the field tabIndex.
   * @return the value of the field tabIndex; may be null.
   */
  public java.lang.Integer getTabIndex()
  {
    return tabIndex;
  }

  /**
   * Sets the field tabIndex.
   * @param _tabIndex the new value of the field tabIndex.
   */
  public void setTabIndex(java.lang.Integer _tabIndex)
  {
    tabIndex = _tabIndex;
  }

}
