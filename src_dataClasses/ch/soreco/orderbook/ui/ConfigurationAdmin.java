package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ConfigurationAdmin", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ConfigurationAdmin extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 4258763715307341231L;

  private java.lang.Class selectedClass;

  /**
   * Gets the field selectedClass.
   * @return the value of the field selectedClass; may be null.
   */
  public java.lang.Class getSelectedClass()
  {
    return selectedClass;
  }

  /**
   * Sets the field selectedClass.
   * @param _selectedClass the new value of the field selectedClass.
   */
  public void setSelectedClass(java.lang.Class _selectedClass)
  {
    selectedClass = _selectedClass;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset configs;

  /**
   * Gets the field configs.
   * @return the value of the field configs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getConfigs()
  {
    return configs;
  }

  /**
   * Sets the field configs.
   * @param _configs the new value of the field configs.
   */
  public void setConfigs(ch.ivyteam.ivy.scripting.objects.Recordset _configs)
  {
    configs = _configs;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> classes;

  /**
   * Gets the field classes.
   * @return the value of the field classes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> getClasses()
  {
    return classes;
  }

  /**
   * Sets the field classes.
   * @param _classes the new value of the field classes.
   */
  public void setClasses(ch.ivyteam.ivy.scripting.objects.List<java.lang.Class> _classes)
  {
    classes = _classes;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

}
