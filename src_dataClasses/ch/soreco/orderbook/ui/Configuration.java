package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Configuration", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Configuration extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6065614253416995375L;

  private ch.ivyteam.ivy.scripting.objects.Recordset recordset;

  /**
   * Gets the field recordset.
   * @return the value of the field recordset; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRecordset()
  {
    return recordset;
  }

  /**
   * Sets the field recordset.
   * @param _recordset the new value of the field recordset.
   */
  public void setRecordset(ch.ivyteam.ivy.scripting.objects.Recordset _recordset)
  {
    recordset = _recordset;
  }

  private ch.ivyteam.ivy.scripting.objects.CompositeObject object;

  /**
   * Gets the field object.
   * @return the value of the field object; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.CompositeObject getObject()
  {
    return object;
  }

  /**
   * Sets the field object.
   * @param _object the new value of the field object.
   */
  public void setObject(ch.ivyteam.ivy.scripting.objects.CompositeObject _object)
  {
    object = _object;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private java.lang.Class clazz;

  /**
   * Gets the field clazz.
   * @return the value of the field clazz; may be null.
   */
  public java.lang.Class getClazz()
  {
    return clazz;
  }

  /**
   * Sets the field clazz.
   * @param _clazz the new value of the field clazz.
   */
  public void setClazz(java.lang.Class _clazz)
  {
    clazz = _clazz;
  }

  private java.lang.String caption;

  /**
   * Gets the field caption.
   * @return the value of the field caption; may be null.
   */
  public java.lang.String getCaption()
  {
    return caption;
  }

  /**
   * Sets the field caption.
   * @param _caption the new value of the field caption.
   */
  public void setCaption(java.lang.String _caption)
  {
    caption = _caption;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> found;

  /**
   * Gets the field found.
   * @return the value of the field found; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> getFound()
  {
    return found;
  }

  /**
   * Sets the field found.
   * @param _found the new value of the field found.
   */
  public void setFound(ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> _found)
  {
    found = _found;
  }

  private java.util.HashMap columnMap;

  /**
   * Gets the field columnMap.
   * @return the value of the field columnMap; may be null.
   */
  public java.util.HashMap getColumnMap()
  {
    return columnMap;
  }

  /**
   * Sets the field columnMap.
   * @param _columnMap the new value of the field columnMap.
   */
  public void setColumnMap(java.util.HashMap _columnMap)
  {
    columnMap = _columnMap;
  }

  private java.lang.String exitButtonText;

  /**
   * Gets the field exitButtonText.
   * @return the value of the field exitButtonText; may be null.
   */
  public java.lang.String getExitButtonText()
  {
    return exitButtonText;
  }

  /**
   * Sets the field exitButtonText.
   * @param _exitButtonText the new value of the field exitButtonText.
   */
  public void setExitButtonText(java.lang.String _exitButtonText)
  {
    exitButtonText = _exitButtonText;
  }

  private java.lang.String exitButtonImage;

  /**
   * Gets the field exitButtonImage.
   * @return the value of the field exitButtonImage; may be null.
   */
  public java.lang.String getExitButtonImage()
  {
    return exitButtonImage;
  }

  /**
   * Sets the field exitButtonImage.
   * @param _exitButtonImage the new value of the field exitButtonImage.
   */
  public void setExitButtonImage(java.lang.String _exitButtonImage)
  {
    exitButtonImage = _exitButtonImage;
  }

  private java.lang.Boolean hasExitButton;

  /**
   * Gets the field hasExitButton.
   * @return the value of the field hasExitButton; may be null.
   */
  public java.lang.Boolean getHasExitButton()
  {
    return hasExitButton;
  }

  /**
   * Sets the field hasExitButton.
   * @param _hasExitButton the new value of the field hasExitButton.
   */
  public void setHasExitButton(java.lang.Boolean _hasExitButton)
  {
    hasExitButton = _hasExitButton;
  }

  private java.lang.String exitButton;

  /**
   * Gets the field exitButton.
   * @return the value of the field exitButton; may be null.
   */
  public java.lang.String getExitButton()
  {
    return exitButton;
  }

  /**
   * Sets the field exitButton.
   * @param _exitButton the new value of the field exitButton.
   */
  public void setExitButton(java.lang.String _exitButton)
  {
    exitButton = _exitButton;
  }

  private java.lang.Integer primaryKey;

  /**
   * Gets the field primaryKey.
   * @return the value of the field primaryKey; may be null.
   */
  public java.lang.Integer getPrimaryKey()
  {
    return primaryKey;
  }

  /**
   * Sets the field primaryKey.
   * @param _primaryKey the new value of the field primaryKey.
   */
  public void setPrimaryKey(java.lang.Integer _primaryKey)
  {
    primaryKey = _primaryKey;
  }

  /**
   * set "" for normal layout "empty" for plain view
   */
  private java.lang.String layout;

  /**
   * Gets the field layout.
   * @return the value of the field layout; may be null.
   */
  public java.lang.String getLayout()
  {
    return layout;
  }

  /**
   * Sets the field layout.
   * @param _layout the new value of the field layout.
   */
  public void setLayout(java.lang.String _layout)
  {
    layout = _layout;
  }

  /**
   * if set false, forms come only in read mode, default is true.
   */
  private java.lang.Boolean write;

  /**
   * Gets the field write.
   * @return the value of the field write; may be null.
   */
  public java.lang.Boolean getWrite()
  {
    return write;
  }

  /**
   * Sets the field write.
   * @param _write the new value of the field write.
   */
  public void setWrite(java.lang.Boolean _write)
  {
    write = _write;
  }

  private java.lang.String submitButtonText;

  /**
   * Gets the field submitButtonText.
   * @return the value of the field submitButtonText; may be null.
   */
  public java.lang.String getSubmitButtonText()
  {
    return submitButtonText;
  }

  /**
   * Sets the field submitButtonText.
   * @param _submitButtonText the new value of the field submitButtonText.
   */
  public void setSubmitButtonText(java.lang.String _submitButtonText)
  {
    submitButtonText = _submitButtonText;
  }

  /**
   * if set true Configuration submit Form will have an Add Button (only on list call)
   */
  private java.lang.Boolean canAdd;

  /**
   * Gets the field canAdd.
   * @return the value of the field canAdd; may be null.
   */
  public java.lang.Boolean getCanAdd()
  {
    return canAdd;
  }

  /**
   * Sets the field canAdd.
   * @param _canAdd the new value of the field canAdd.
   */
  public void setCanAdd(java.lang.Boolean _canAdd)
  {
    canAdd = _canAdd;
  }

}
