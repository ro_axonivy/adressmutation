package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderStatistics", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderStatistics extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -1836059757130335565L;

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrderStatistics;

  /**
   * Gets the field rsOrderStatistics.
   * @return the value of the field rsOrderStatistics; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrderStatistics()
  {
    return rsOrderStatistics;
  }

  /**
   * Sets the field rsOrderStatistics.
   * @param _rsOrderStatistics the new value of the field rsOrderStatistics.
   */
  public void setRsOrderStatistics(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrderStatistics)
  {
    rsOrderStatistics = _rsOrderStatistics;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrderBooks;

  /**
   * Gets the field rsOrderBooks.
   * @return the value of the field rsOrderBooks; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrderBooks()
  {
    return rsOrderBooks;
  }

  /**
   * Sets the field rsOrderBooks.
   * @param _rsOrderBooks the new value of the field rsOrderBooks.
   */
  public void setRsOrderBooks(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrderBooks)
  {
    rsOrderBooks = _rsOrderBooks;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Integer orderBookId;

  /**
   * Gets the field orderBookId.
   * @return the value of the field orderBookId; may be null.
   */
  public java.lang.Integer getOrderBookId()
  {
    return orderBookId;
  }

  /**
   * Sets the field orderBookId.
   * @param _orderBookId the new value of the field orderBookId.
   */
  public void setOrderBookId(java.lang.Integer _orderBookId)
  {
    orderBookId = _orderBookId;
  }

  private ch.ivyteam.ivy.scripting.objects.Date forDate;

  /**
   * Gets the field forDate.
   * @return the value of the field forDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getForDate()
  {
    return forDate;
  }

  /**
   * Sets the field forDate.
   * @param _forDate the new value of the field forDate.
   */
  public void setForDate(ch.ivyteam.ivy.scripting.objects.Date _forDate)
  {
    forDate = _forDate;
  }

  private java.util.HashMap orderTypeLabelMap;

  /**
   * Gets the field orderTypeLabelMap.
   * @return the value of the field orderTypeLabelMap; may be null.
   */
  public java.util.HashMap getOrderTypeLabelMap()
  {
    return orderTypeLabelMap;
  }

  /**
   * Sets the field orderTypeLabelMap.
   * @param _orderTypeLabelMap the new value of the field orderTypeLabelMap.
   */
  public void setOrderTypeLabelMap(java.util.HashMap _orderTypeLabelMap)
  {
    orderTypeLabelMap = _orderTypeLabelMap;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> totals;

  /**
   * Gets the field totals.
   * @return the value of the field totals; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> getTotals()
  {
    return totals;
  }

  /**
   * Sets the field totals.
   * @param _totals the new value of the field totals.
   */
  public void setTotals(ch.ivyteam.ivy.scripting.objects.List<java.lang.Object> _totals)
  {
    totals = _totals;
  }

  private java.io.File excel;

  /**
   * Gets the field excel.
   * @return the value of the field excel; may be null.
   */
  public java.io.File getExcel()
  {
    return excel;
  }

  /**
   * Sets the field excel.
   * @param _excel the new value of the field excel.
   */
  public void setExcel(java.io.File _excel)
  {
    excel = _excel;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset years;

  /**
   * Gets the field years.
   * @return the value of the field years; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getYears()
  {
    return years;
  }

  /**
   * Sets the field years.
   * @param _years the new value of the field years.
   */
  public void setYears(ch.ivyteam.ivy.scripting.objects.Recordset _years)
  {
    years = _years;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset months;

  /**
   * Gets the field months.
   * @return the value of the field months; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getMonths()
  {
    return months;
  }

  /**
   * Sets the field months.
   * @param _months the new value of the field months.
   */
  public void setMonths(ch.ivyteam.ivy.scripting.objects.Recordset _months)
  {
    months = _months;
  }

  private java.lang.Integer forYear;

  /**
   * Gets the field forYear.
   * @return the value of the field forYear; may be null.
   */
  public java.lang.Integer getForYear()
  {
    return forYear;
  }

  /**
   * Sets the field forYear.
   * @param _forYear the new value of the field forYear.
   */
  public void setForYear(java.lang.Integer _forYear)
  {
    forYear = _forYear;
  }

  private java.lang.Integer forMonth;

  /**
   * Gets the field forMonth.
   * @return the value of the field forMonth; may be null.
   */
  public java.lang.Integer getForMonth()
  {
    return forMonth;
  }

  /**
   * Sets the field forMonth.
   * @param _forMonth the new value of the field forMonth.
   */
  public void setForMonth(java.lang.Integer _forMonth)
  {
    forMonth = _forMonth;
  }

  private java.lang.Integer forDay;

  /**
   * Gets the field forDay.
   * @return the value of the field forDay; may be null.
   */
  public java.lang.Integer getForDay()
  {
    return forDay;
  }

  /**
   * Sets the field forDay.
   * @param _forDay the new value of the field forDay.
   */
  public void setForDay(java.lang.Integer _forDay)
  {
    forDay = _forDay;
  }

  private java.lang.String statisticType;

  /**
   * Gets the field statisticType.
   * @return the value of the field statisticType; may be null.
   */
  public java.lang.String getStatisticType()
  {
    return statisticType;
  }

  /**
   * Sets the field statisticType.
   * @param _statisticType the new value of the field statisticType.
   */
  public void setStatisticType(java.lang.String _statisticType)
  {
    statisticType = _statisticType;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> statistics;

  /**
   * Gets the field statistics.
   * @return the value of the field statistics; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getStatistics()
  {
    return statistics;
  }

  /**
   * Sets the field statistics.
   * @param _statistics the new value of the field statistics.
   */
  public void setStatistics(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _statistics)
  {
    statistics = _statistics;
  }

  private java.lang.String additionalFieldName;

  /**
   * Gets the field additionalFieldName.
   * @return the value of the field additionalFieldName; may be null.
   */
  public java.lang.String getAdditionalFieldName()
  {
    return additionalFieldName;
  }

  /**
   * Sets the field additionalFieldName.
   * @param _additionalFieldName the new value of the field additionalFieldName.
   */
  public void setAdditionalFieldName(java.lang.String _additionalFieldName)
  {
    additionalFieldName = _additionalFieldName;
  }

  private java.util.HashMap additionalFieldsMap;

  /**
   * Gets the field additionalFieldsMap.
   * @return the value of the field additionalFieldsMap; may be null.
   */
  public java.util.HashMap getAdditionalFieldsMap()
  {
    return additionalFieldsMap;
  }

  /**
   * Sets the field additionalFieldsMap.
   * @param _additionalFieldsMap the new value of the field additionalFieldsMap.
   */
  public void setAdditionalFieldsMap(java.util.HashMap _additionalFieldsMap)
  {
    additionalFieldsMap = _additionalFieldsMap;
  }

  private java.util.HashMap orderStatesKeyLabelMap;

  /**
   * Gets the field orderStatesKeyLabelMap.
   * @return the value of the field orderStatesKeyLabelMap; may be null.
   */
  public java.util.HashMap getOrderStatesKeyLabelMap()
  {
    return orderStatesKeyLabelMap;
  }

  /**
   * Sets the field orderStatesKeyLabelMap.
   * @param _orderStatesKeyLabelMap the new value of the field orderStatesKeyLabelMap.
   */
  public void setOrderStatesKeyLabelMap(java.util.HashMap _orderStatesKeyLabelMap)
  {
    orderStatesKeyLabelMap = _orderStatesKeyLabelMap;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsAdditionalFields;

  /**
   * Gets the field rsAdditionalFields.
   * @return the value of the field rsAdditionalFields; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsAdditionalFields()
  {
    return rsAdditionalFields;
  }

  /**
   * Sets the field rsAdditionalFields.
   * @param _rsAdditionalFields the new value of the field rsAdditionalFields.
   */
  public void setRsAdditionalFields(ch.ivyteam.ivy.scripting.objects.Recordset _rsAdditionalFields)
  {
    rsAdditionalFields = _rsAdditionalFields;
  }

  private java.lang.String selectAdditionalField;

  /**
   * Gets the field selectAdditionalField.
   * @return the value of the field selectAdditionalField; may be null.
   */
  public java.lang.String getSelectAdditionalField()
  {
    return selectAdditionalField;
  }

  /**
   * Sets the field selectAdditionalField.
   * @param _selectAdditionalField the new value of the field selectAdditionalField.
   */
  public void setSelectAdditionalField(java.lang.String _selectAdditionalField)
  {
    selectAdditionalField = _selectAdditionalField;
  }

  private java.util.HashMap pendencyTicketTypeKeyLabelMap;

  /**
   * Gets the field pendencyTicketTypeKeyLabelMap.
   * @return the value of the field pendencyTicketTypeKeyLabelMap; may be null.
   */
  public java.util.HashMap getPendencyTicketTypeKeyLabelMap()
  {
    return pendencyTicketTypeKeyLabelMap;
  }

  /**
   * Sets the field pendencyTicketTypeKeyLabelMap.
   * @param _pendencyTicketTypeKeyLabelMap the new value of the field pendencyTicketTypeKeyLabelMap.
   */
  public void setPendencyTicketTypeKeyLabelMap(java.util.HashMap _pendencyTicketTypeKeyLabelMap)
  {
    pendencyTicketTypeKeyLabelMap = _pendencyTicketTypeKeyLabelMap;
  }

}
