package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderBook", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderBook extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 5183835049369625676L;

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orderBook;

  /**
   * Gets the field orderBook.
   * @return the value of the field orderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrderBook()
  {
    return orderBook;
  }

  /**
   * Sets the field orderBook.
   * @param _orderBook the new value of the field orderBook.
   */
  public void setOrderBook(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orderBook)
  {
    orderBook = _orderBook;
  }

  private ch.soreco.orderbook.bo.Order filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Order _filter)
  {
    filter = _filter;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private ch.soreco.orderbook.bo.Order currentOrder;

  /**
   * Gets the field currentOrder.
   * @return the value of the field currentOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getCurrentOrder()
  {
    return currentOrder;
  }

  /**
   * Sets the field currentOrder.
   * @param _currentOrder the new value of the field currentOrder.
   */
  public void setCurrentOrder(ch.soreco.orderbook.bo.Order _currentOrder)
  {
    currentOrder = _currentOrder;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrderBook;

  /**
   * Gets the field rsOrderBook.
   * @return the value of the field rsOrderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrderBook()
  {
    return rsOrderBook;
  }

  /**
   * Sets the field rsOrderBook.
   * @param _rsOrderBook the new value of the field rsOrderBook.
   */
  public void setRsOrderBook(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrderBook)
  {
    rsOrderBook = _rsOrderBook;
  }

  private java.lang.Boolean modify;

  /**
   * Gets the field modify.
   * @return the value of the field modify; may be null.
   */
  public java.lang.Boolean getModify()
  {
    return modify;
  }

  /**
   * Sets the field modify.
   * @param _modify the new value of the field modify.
   */
  public void setModify(java.lang.Boolean _modify)
  {
    modify = _modify;
  }

  private ch.soreco.orderbook.ui.enums.ViewType eViewType;

  /**
   * Gets the field eViewType.
   * @return the value of the field eViewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getEViewType()
  {
    return eViewType;
  }

  /**
   * Sets the field eViewType.
   * @param _eViewType the new value of the field eViewType.
   */
  public void setEViewType(ch.soreco.orderbook.ui.enums.ViewType _eViewType)
  {
    eViewType = _eViewType;
  }

  private java.lang.String OrderBookType;

  /**
   * Gets the field OrderBookType.
   * @return the value of the field OrderBookType; may be null.
   */
  public java.lang.String getOrderBookType()
  {
    return OrderBookType;
  }

  /**
   * Sets the field OrderBookType.
   * @param _OrderBookType the new value of the field OrderBookType.
   */
  public void setOrderBookType(java.lang.String _OrderBookType)
  {
    OrderBookType = _OrderBookType;
  }

  private java.io.File ExcelFile;

  /**
   * Gets the field ExcelFile.
   * @return the value of the field ExcelFile; may be null.
   */
  public java.io.File getExcelFile()
  {
    return ExcelFile;
  }

  /**
   * Sets the field ExcelFile.
   * @param _ExcelFile the new value of the field ExcelFile.
   */
  public void setExcelFile(java.io.File _ExcelFile)
  {
    ExcelFile = _ExcelFile;
  }

  private java.lang.String OrderBookCaption;

  /**
   * Gets the field OrderBookCaption.
   * @return the value of the field OrderBookCaption; may be null.
   */
  public java.lang.String getOrderBookCaption()
  {
    return OrderBookCaption;
  }

  /**
   * Sets the field OrderBookCaption.
   * @param _OrderBookCaption the new value of the field OrderBookCaption.
   */
  public void setOrderBookCaption(java.lang.String _OrderBookCaption)
  {
    OrderBookCaption = _OrderBookCaption;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset OrderTypes;

  /**
   * Gets the field OrderTypes.
   * @return the value of the field OrderTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrderTypes()
  {
    return OrderTypes;
  }

  /**
   * Sets the field OrderTypes.
   * @param _OrderTypes the new value of the field OrderTypes.
   */
  public void setOrderTypes(ch.ivyteam.ivy.scripting.objects.Recordset _OrderTypes)
  {
    OrderTypes = _OrderTypes;
  }

  private java.lang.String searchMessage;

  /**
   * Gets the field searchMessage.
   * @return the value of the field searchMessage; may be null.
   */
  public java.lang.String getSearchMessage()
  {
    return searchMessage;
  }

  /**
   * Sets the field searchMessage.
   * @param _searchMessage the new value of the field searchMessage.
   */
  public void setSearchMessage(java.lang.String _searchMessage)
  {
    searchMessage = _searchMessage;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> RemainingConfig;

  /**
   * Gets the field RemainingConfig.
   * @return the value of the field RemainingConfig; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> getRemainingConfig()
  {
    return RemainingConfig;
  }

  /**
   * Sets the field RemainingConfig.
   * @param _RemainingConfig the new value of the field RemainingConfig.
   */
  public void setRemainingConfig(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.TimeRemainingConfig> _RemainingConfig)
  {
    RemainingConfig = _RemainingConfig;
  }

  private java.lang.Boolean archiveFlag;

  /**
   * Gets the field archiveFlag.
   * @return the value of the field archiveFlag; may be null.
   */
  public java.lang.Boolean getArchiveFlag()
  {
    return archiveFlag;
  }

  /**
   * Sets the field archiveFlag.
   * @param _archiveFlag the new value of the field archiveFlag.
   */
  public void setArchiveFlag(java.lang.Boolean _archiveFlag)
  {
    archiveFlag = _archiveFlag;
  }

  private ch.soreco.orderbook.ui.enums.ViewType viewType;

  /**
   * Gets the field viewType.
   * @return the value of the field viewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getViewType()
  {
    return viewType;
  }

  /**
   * Sets the field viewType.
   * @param _viewType the new value of the field viewType.
   */
  public void setViewType(ch.soreco.orderbook.ui.enums.ViewType _viewType)
  {
    viewType = _viewType;
  }

  private java.lang.Integer lotSize;

  /**
   * Gets the field lotSize.
   * @return the value of the field lotSize; may be null.
   */
  public java.lang.Integer getLotSize()
  {
    return lotSize;
  }

  /**
   * Sets the field lotSize.
   * @param _lotSize the new value of the field lotSize.
   */
  public void setLotSize(java.lang.Integer _lotSize)
  {
    lotSize = _lotSize;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> editors;

  /**
   * Gets the field editors.
   * @return the value of the field editors; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getEditors()
  {
    return editors;
  }

  /**
   * Sets the field editors.
   * @param _editors the new value of the field editors.
   */
  public void setEditors(ch.ivyteam.ivy.scripting.objects.List<Object> _editors)
  {
    editors = _editors;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> checkers;

  /**
   * Gets the field checkers.
   * @return the value of the field checkers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getCheckers()
  {
    return checkers;
  }

  /**
   * Sets the field checkers.
   * @param _checkers the new value of the field checkers.
   */
  public void setCheckers(ch.ivyteam.ivy.scripting.objects.List<Object> _checkers)
  {
    checkers = _checkers;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> dispatchers;

  /**
   * Gets the field dispatchers.
   * @return the value of the field dispatchers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getDispatchers()
  {
    return dispatchers;
  }

  /**
   * Sets the field dispatchers.
   * @param _dispatchers the new value of the field dispatchers.
   */
  public void setDispatchers(ch.ivyteam.ivy.scripting.objects.List<Object> _dispatchers)
  {
    dispatchers = _dispatchers;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> excludes;

  /**
   * Gets the field excludes.
   * @return the value of the field excludes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getExcludes()
  {
    return excludes;
  }

  /**
   * Sets the field excludes.
   * @param _excludes the new value of the field excludes.
   */
  public void setExcludes(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _excludes)
  {
    excludes = _excludes;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> priorities;

  /**
   * Gets the field priorities.
   * @return the value of the field priorities; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getPriorities()
  {
    return priorities;
  }

  /**
   * Sets the field priorities.
   * @param _priorities the new value of the field priorities.
   */
  public void setPriorities(ch.ivyteam.ivy.scripting.objects.List<Object> _priorities)
  {
    priorities = _priorities;
  }

  private java.lang.String orderIds;

  /**
   * Gets the field orderIds.
   * @return the value of the field orderIds; may be null.
   */
  public java.lang.String getOrderIds()
  {
    return orderIds;
  }

  /**
   * Sets the field orderIds.
   * @param _orderIds the new value of the field orderIds.
   */
  public void setOrderIds(java.lang.String _orderIds)
  {
    orderIds = _orderIds;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset orderStates;

  /**
   * Gets the field orderStates.
   * @return the value of the field orderStates; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrderStates()
  {
    return orderStates;
  }

  /**
   * Sets the field orderStates.
   * @param _orderStates the new value of the field orderStates.
   */
  public void setOrderStates(ch.ivyteam.ivy.scripting.objects.Recordset _orderStates)
  {
    orderStates = _orderStates;
  }

  private java.lang.String roleView;

  /**
   * Gets the field roleView.
   * @return the value of the field roleView; may be null.
   */
  public java.lang.String getRoleView()
  {
    return roleView;
  }

  /**
   * Sets the field roleView.
   * @param _roleView the new value of the field roleView.
   */
  public void setRoleView(java.lang.String _roleView)
  {
    roleView = _roleView;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset orderBookQuantityTypes;

  /**
   * Gets the field orderBookQuantityTypes.
   * @return the value of the field orderBookQuantityTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrderBookQuantityTypes()
  {
    return orderBookQuantityTypes;
  }

  /**
   * Sets the field orderBookQuantityTypes.
   * @param _orderBookQuantityTypes the new value of the field orderBookQuantityTypes.
   */
  public void setOrderBookQuantityTypes(ch.ivyteam.ivy.scripting.objects.Recordset _orderBookQuantityTypes)
  {
    orderBookQuantityTypes = _orderBookQuantityTypes;
  }

  private transient ch.soreco.orderbook.functional.SearchParameters searchParameters;

  /**
   * Gets the field searchParameters.
   * @return the value of the field searchParameters; may be null.
   */
  public ch.soreco.orderbook.functional.SearchParameters getSearchParameters()
  {
    return searchParameters;
  }

  /**
   * Sets the field searchParameters.
   * @param _searchParameters the new value of the field searchParameters.
   */
  public void setSearchParameters(ch.soreco.orderbook.functional.SearchParameters _searchParameters)
  {
    searchParameters = _searchParameters;
  }

  private transient java.lang.Integer countOfQueryResults;

  /**
   * Gets the field countOfQueryResults.
   * @return the value of the field countOfQueryResults; may be null.
   */
  public java.lang.Integer getCountOfQueryResults()
  {
    return countOfQueryResults;
  }

  /**
   * Sets the field countOfQueryResults.
   * @param _countOfQueryResults the new value of the field countOfQueryResults.
   */
  public void setCountOfQueryResults(java.lang.Integer _countOfQueryResults)
  {
    countOfQueryResults = _countOfQueryResults;
  }

  private transient ch.soreco.orderbook.ui.SearchViewResult searchViewResult;

  /**
   * Gets the field searchViewResult.
   * @return the value of the field searchViewResult; may be null.
   */
  public ch.soreco.orderbook.ui.SearchViewResult getSearchViewResult()
  {
    return searchViewResult;
  }

  /**
   * Sets the field searchViewResult.
   * @param _searchViewResult the new value of the field searchViewResult.
   */
  public void setSearchViewResult(ch.soreco.orderbook.ui.SearchViewResult _searchViewResult)
  {
    searchViewResult = _searchViewResult;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset fdls;

  /**
   * Gets the field fdls.
   * @return the value of the field fdls; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getFdls()
  {
    return fdls;
  }

  /**
   * Sets the field fdls.
   * @param _fdls the new value of the field fdls.
   */
  public void setFdls(ch.ivyteam.ivy.scripting.objects.Recordset _fdls)
  {
    fdls = _fdls;
  }

}
