package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Order", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Order extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2621197918595168574L;

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.Boolean modify;

  /**
   * Gets the field modify.
   * @return the value of the field modify; may be null.
   */
  public java.lang.Boolean getModify()
  {
    return modify;
  }

  /**
   * Sets the field modify.
   * @param _modify the new value of the field modify.
   */
  public void setModify(java.lang.Boolean _modify)
  {
    modify = _modify;
  }

  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private ch.soreco.orderbook.enums.OrderType eOrderType;

  /**
   * Gets the field eOrderType.
   * @return the value of the field eOrderType; may be null.
   */
  public ch.soreco.orderbook.enums.OrderType getEOrderType()
  {
    return eOrderType;
  }

  /**
   * Sets the field eOrderType.
   * @param _eOrderType the new value of the field eOrderType.
   */
  public void setEOrderType(ch.soreco.orderbook.enums.OrderType _eOrderType)
  {
    eOrderType = _eOrderType;
  }

  private ch.soreco.orderbook.ui.enums.ViewType eViewType;

  /**
   * Gets the field eViewType.
   * @return the value of the field eViewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getEViewType()
  {
    return eViewType;
  }

  /**
   * Sets the field eViewType.
   * @param _eViewType the new value of the field eViewType.
   */
  public void setEViewType(ch.soreco.orderbook.ui.enums.ViewType _eViewType)
  {
    eViewType = _eViewType;
  }

  private java.lang.Exception exception;

  /**
   * Gets the field exception.
   * @return the value of the field exception; may be null.
   */
  public java.lang.Exception getException()
  {
    return exception;
  }

  /**
   * Sets the field exception.
   * @param _exception the new value of the field exception.
   */
  public void setException(java.lang.Exception _exception)
  {
    exception = _exception;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> orderContacts;

  /**
   * Gets the field orderContacts.
   * @return the value of the field orderContacts; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> getOrderContacts()
  {
    return orderContacts;
  }

  /**
   * Sets the field orderContacts.
   * @param _orderContacts the new value of the field orderContacts.
   */
  public void setOrderContacts(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> _orderContacts)
  {
    orderContacts = _orderContacts;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> orderContactPapers;

  /**
   * Gets the field orderContactPapers.
   * @return the value of the field orderContactPapers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> getOrderContactPapers()
  {
    return orderContactPapers;
  }

  /**
   * Sets the field orderContactPapers.
   * @param _orderContactPapers the new value of the field orderContactPapers.
   */
  public void setOrderContactPapers(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Paper> _orderContactPapers)
  {
    orderContactPapers = _orderContactPapers;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.SignatureRequest> orderContactsSignatureRequests;

  /**
   * Gets the field orderContactsSignatureRequests.
   * @return the value of the field orderContactsSignatureRequests; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.SignatureRequest> getOrderContactsSignatureRequests()
  {
    return orderContactsSignatureRequests;
  }

  /**
   * Sets the field orderContactsSignatureRequests.
   * @param _orderContactsSignatureRequests the new value of the field orderContactsSignatureRequests.
   */
  public void setOrderContactsSignatureRequests(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.SignatureRequest> _orderContactsSignatureRequests)
  {
    orderContactsSignatureRequests = _orderContactsSignatureRequests;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingLSV> recordingLSV;

  /**
   * Gets the field recordingLSV.
   * @return the value of the field recordingLSV; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingLSV> getRecordingLSV()
  {
    return recordingLSV;
  }

  /**
   * Sets the field recordingLSV.
   * @param _recordingLSV the new value of the field recordingLSV.
   */
  public void setRecordingLSV(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingLSV> _recordingLSV)
  {
    recordingLSV = _recordingLSV;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.DomicileOrOwner> recordingDomicileOrOwners;

  /**
   * Gets the field recordingDomicileOrOwners.
   * @return the value of the field recordingDomicileOrOwners; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.DomicileOrOwner> getRecordingDomicileOrOwners()
  {
    return recordingDomicileOrOwners;
  }

  /**
   * Sets the field recordingDomicileOrOwners.
   * @param _recordingDomicileOrOwners the new value of the field recordingDomicileOrOwners.
   */
  public void setRecordingDomicileOrOwners(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.DomicileOrOwner> _recordingDomicileOrOwners)
  {
    recordingDomicileOrOwners = _recordingDomicileOrOwners;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private java.lang.Integer size;

  /**
   * Gets the field size.
   * @return the value of the field size; may be null.
   */
  public java.lang.Integer getSize()
  {
    return size;
  }

  /**
   * Sets the field size.
   * @param _size the new value of the field size.
   */
  public void setSize(java.lang.Integer _size)
  {
    size = _size;
  }

  private java.lang.StringBuilder htmlBuild;

  /**
   * Gets the field htmlBuild.
   * @return the value of the field htmlBuild; may be null.
   */
  public java.lang.StringBuilder getHtmlBuild()
  {
    return htmlBuild;
  }

  /**
   * Sets the field htmlBuild.
   * @param _htmlBuild the new value of the field htmlBuild.
   */
  public void setHtmlBuild(java.lang.StringBuilder _htmlBuild)
  {
    htmlBuild = _htmlBuild;
  }

  private ch.soreco.orderbook.bo.Contact currentContact;

  /**
   * Gets the field currentContact.
   * @return the value of the field currentContact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getCurrentContact()
  {
    return currentContact;
  }

  /**
   * Sets the field currentContact.
   * @param _currentContact the new value of the field currentContact.
   */
  public void setCurrentContact(ch.soreco.orderbook.bo.Contact _currentContact)
  {
    currentContact = _currentContact;
  }

  private ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrdersMatch;

  /**
   * Gets the field rsOrdersMatch.
   * @return the value of the field rsOrdersMatch; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrdersMatch()
  {
    return rsOrdersMatch;
  }

  /**
   * Sets the field rsOrdersMatch.
   * @param _rsOrdersMatch the new value of the field rsOrdersMatch.
   */
  public void setRsOrdersMatch(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrdersMatch)
  {
    rsOrdersMatch = _rsOrdersMatch;
  }

  private ch.soreco.orderbook.bo.Order orderFilter;

  /**
   * Gets the field orderFilter.
   * @return the value of the field orderFilter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrderFilter()
  {
    return orderFilter;
  }

  /**
   * Sets the field orderFilter.
   * @param _orderFilter the new value of the field orderFilter.
   */
  public void setOrderFilter(ch.soreco.orderbook.bo.Order _orderFilter)
  {
    orderFilter = _orderFilter;
  }

  private ch.soreco.orderbook.bo.Order matchedOrder;

  /**
   * Gets the field matchedOrder.
   * @return the value of the field matchedOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getMatchedOrder()
  {
    return matchedOrder;
  }

  /**
   * Sets the field matchedOrder.
   * @param _matchedOrder the new value of the field matchedOrder.
   */
  public void setMatchedOrder(ch.soreco.orderbook.bo.Order _matchedOrder)
  {
    matchedOrder = _matchedOrder;
  }

  private java.lang.String newObject;

  /**
   * Gets the field newObject.
   * @return the value of the field newObject; may be null.
   */
  public java.lang.String getNewObject()
  {
    return newObject;
  }

  /**
   * Sets the field newObject.
   * @param _newObject the new value of the field newObject.
   */
  public void setNewObject(java.lang.String _newObject)
  {
    newObject = _newObject;
  }

  private java.lang.Integer showChild;

  /**
   * Gets the field showChild.
   * @return the value of the field showChild; may be null.
   */
  public java.lang.Integer getShowChild()
  {
    return showChild;
  }

  /**
   * Sets the field showChild.
   * @param _showChild the new value of the field showChild.
   */
  public void setShowChild(java.lang.Integer _showChild)
  {
    showChild = _showChild;
  }

  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  private java.lang.String orderState;

  /**
   * Gets the field orderState.
   * @return the value of the field orderState; may be null.
   */
  public java.lang.String getOrderState()
  {
    return orderState;
  }

  /**
   * Sets the field orderState.
   * @param _orderState the new value of the field orderState.
   */
  public void setOrderState(java.lang.String _orderState)
  {
    orderState = _orderState;
  }

  private ch.soreco.orderbook.enums.OrderState eOrderState;

  /**
   * Gets the field eOrderState.
   * @return the value of the field eOrderState; may be null.
   */
  public ch.soreco.orderbook.enums.OrderState getEOrderState()
  {
    return eOrderState;
  }

  /**
   * Sets the field eOrderState.
   * @param _eOrderState the new value of the field eOrderState.
   */
  public void setEOrderState(ch.soreco.orderbook.enums.OrderState _eOrderState)
  {
    eOrderState = _eOrderState;
  }

  private java.lang.Integer currentChild;

  /**
   * Gets the field currentChild.
   * @return the value of the field currentChild; may be null.
   */
  public java.lang.Integer getCurrentChild()
  {
    return currentChild;
  }

  /**
   * Sets the field currentChild.
   * @param _currentChild the new value of the field currentChild.
   */
  public void setCurrentChild(java.lang.Integer _currentChild)
  {
    currentChild = _currentChild;
  }

  private ch.ivyteam.ivy.scripting.objects.File xmlFile;

  /**
   * Gets the field xmlFile.
   * @return the value of the field xmlFile; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.File getXmlFile()
  {
    return xmlFile;
  }

  /**
   * Sets the field xmlFile.
   * @param _xmlFile the new value of the field xmlFile.
   */
  public void setXmlFile(ch.ivyteam.ivy.scripting.objects.File _xmlFile)
  {
    xmlFile = _xmlFile;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.io.File> FileSet;

  /**
   * Gets the field FileSet.
   * @return the value of the field FileSet; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.io.File> getFileSet()
  {
    return FileSet;
  }

  /**
   * Sets the field FileSet.
   * @param _FileSet the new value of the field FileSet.
   */
  public void setFileSet(ch.ivyteam.ivy.scripting.objects.List<java.io.File> _FileSet)
  {
    FileSet = _FileSet;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.CorrespondenceContainer> recordingCorrespondenceContainers;

  /**
   * Gets the field recordingCorrespondenceContainers.
   * @return the value of the field recordingCorrespondenceContainers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.CorrespondenceContainer> getRecordingCorrespondenceContainers()
  {
    return recordingCorrespondenceContainers;
  }

  /**
   * Sets the field recordingCorrespondenceContainers.
   * @param _recordingCorrespondenceContainers the new value of the field recordingCorrespondenceContainers.
   */
  public void setRecordingCorrespondenceContainers(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.CorrespondenceContainer> _recordingCorrespondenceContainers)
  {
    recordingCorrespondenceContainers = _recordingCorrespondenceContainers;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orderBook;

  /**
   * Gets the field orderBook.
   * @return the value of the field orderBook; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrderBook()
  {
    return orderBook;
  }

  /**
   * Sets the field orderBook.
   * @param _orderBook the new value of the field orderBook.
   */
  public void setOrderBook(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orderBook)
  {
    orderBook = _orderBook;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Letter> orderContactsLetters;

  /**
   * Gets the field orderContactsLetters.
   * @return the value of the field orderContactsLetters; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Letter> getOrderContactsLetters()
  {
    return orderContactsLetters;
  }

  /**
   * Sets the field orderContactsLetters.
   * @param _orderContactsLetters the new value of the field orderContactsLetters.
   */
  public void setOrderContactsLetters(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Letter> _orderContactsLetters)
  {
    orderContactsLetters = _orderContactsLetters;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.PendencyTicket> orderPendencyTickets;

  /**
   * Gets the field orderPendencyTickets.
   * @return the value of the field orderPendencyTickets; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.PendencyTicket> getOrderPendencyTickets()
  {
    return orderPendencyTickets;
  }

  /**
   * Sets the field orderPendencyTickets.
   * @param _orderPendencyTickets the new value of the field orderPendencyTickets.
   */
  public void setOrderPendencyTickets(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.PendencyTicket> _orderPendencyTickets)
  {
    orderPendencyTickets = _orderPendencyTickets;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingMeta> recordingMetas;

  /**
   * Gets the field recordingMetas.
   * @return the value of the field recordingMetas; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingMeta> getRecordingMetas()
  {
    return recordingMetas;
  }

  /**
   * Sets the field recordingMetas.
   * @param _recordingMetas the new value of the field recordingMetas.
   */
  public void setRecordingMetas(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.RecordingMeta> _recordingMetas)
  {
    recordingMetas = _recordingMetas;
  }

  private ch.soreco.orderbook.bo.PendencyTicket currentPendencyTicket;

  /**
   * Gets the field currentPendencyTicket.
   * @return the value of the field currentPendencyTicket; may be null.
   */
  public ch.soreco.orderbook.bo.PendencyTicket getCurrentPendencyTicket()
  {
    return currentPendencyTicket;
  }

  /**
   * Sets the field currentPendencyTicket.
   * @param _currentPendencyTicket the new value of the field currentPendencyTicket.
   */
  public void setCurrentPendencyTicket(ch.soreco.orderbook.bo.PendencyTicket _currentPendencyTicket)
  {
    currentPendencyTicket = _currentPendencyTicket;
  }

  private java.lang.String genericView;

  /**
   * Gets the field genericView.
   * @return the value of the field genericView; may be null.
   */
  public java.lang.String getGenericView()
  {
    return genericView;
  }

  /**
   * Sets the field genericView.
   * @param _genericView the new value of the field genericView.
   */
  public void setGenericView(java.lang.String _genericView)
  {
    genericView = _genericView;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset relatedOrders;

  /**
   * Gets the field relatedOrders.
   * @return the value of the field relatedOrders; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRelatedOrders()
  {
    return relatedOrders;
  }

  /**
   * Sets the field relatedOrders.
   * @param _relatedOrders the new value of the field relatedOrders.
   */
  public void setRelatedOrders(ch.ivyteam.ivy.scripting.objects.Recordset _relatedOrders)
  {
    relatedOrders = _relatedOrders;
  }

  private ch.soreco.orderbook.bo.Order orderCopy;

  /**
   * Gets the field orderCopy.
   * @return the value of the field orderCopy; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrderCopy()
  {
    return orderCopy;
  }

  /**
   * Sets the field orderCopy.
   * @param _orderCopy the new value of the field orderCopy.
   */
  public void setOrderCopy(ch.soreco.orderbook.bo.Order _orderCopy)
  {
    orderCopy = _orderCopy;
  }

  private java.lang.String orderTypeLabel;

  /**
   * Gets the field orderTypeLabel.
   * @return the value of the field orderTypeLabel; may be null.
   */
  public java.lang.String getOrderTypeLabel()
  {
    return orderTypeLabel;
  }

  /**
   * Sets the field orderTypeLabel.
   * @param _orderTypeLabel the new value of the field orderTypeLabel.
   */
  public void setOrderTypeLabel(java.lang.String _orderTypeLabel)
  {
    orderTypeLabel = _orderTypeLabel;
  }

  private java.lang.Boolean newOrder;

  /**
   * Gets the field newOrder.
   * @return the value of the field newOrder; may be null.
   */
  public java.lang.Boolean getNewOrder()
  {
    return newOrder;
  }

  /**
   * Sets the field newOrder.
   * @param _newOrder the new value of the field newOrder.
   */
  public void setNewOrder(java.lang.Boolean _newOrder)
  {
    newOrder = _newOrder;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> orderTypeList;

  /**
   * Gets the field orderTypeList.
   * @return the value of the field orderTypeList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getOrderTypeList()
  {
    return orderTypeList;
  }

  /**
   * Sets the field orderTypeList.
   * @param _orderTypeList the new value of the field orderTypeList.
   */
  public void setOrderTypeList(ch.ivyteam.ivy.scripting.objects.List<Object> _orderTypeList)
  {
    orderTypeList = _orderTypeList;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> authorityGroups;

  /**
   * Gets the field authorityGroups.
   * @return the value of the field authorityGroups; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> getAuthorityGroups()
  {
    return authorityGroups;
  }

  /**
   * Sets the field authorityGroups.
   * @param _authorityGroups the new value of the field authorityGroups.
   */
  public void setAuthorityGroups(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> _authorityGroups)
  {
    authorityGroups = _authorityGroups;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> userList;

  /**
   * Gets the field userList.
   * @return the value of the field userList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getUserList()
  {
    return userList;
  }

  /**
   * Sets the field userList.
   * @param _userList the new value of the field userList.
   */
  public void setUserList(ch.ivyteam.ivy.scripting.objects.List<Object> _userList)
  {
    userList = _userList;
  }

  /**
   * Recordset of Order Quantity Types containing OrderQuantityType as Key and Label by "Generics"
   */
  private ch.ivyteam.ivy.scripting.objects.Recordset orderQuantityTypes;

  /**
   * Gets the field orderQuantityTypes.
   * @return the value of the field orderQuantityTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrderQuantityTypes()
  {
    return orderQuantityTypes;
  }

  /**
   * Sets the field orderQuantityTypes.
   * @param _orderQuantityTypes the new value of the field orderQuantityTypes.
   */
  public void setOrderQuantityTypes(ch.ivyteam.ivy.scripting.objects.Recordset _orderQuantityTypes)
  {
    orderQuantityTypes = _orderQuantityTypes;
  }

  /**
   * Recordset of Order Type Infos used in DependentOrder.mod (new Order) and Edit.mod (Redispach), containing Order Types and if is Requiring OrderQuantityType
   */
  private ch.ivyteam.ivy.scripting.objects.Recordset officialTeamOrderTypes;

  /**
   * Gets the field officialTeamOrderTypes.
   * @return the value of the field officialTeamOrderTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOfficialTeamOrderTypes()
  {
    return officialTeamOrderTypes;
  }

  /**
   * Sets the field officialTeamOrderTypes.
   * @param _officialTeamOrderTypes the new value of the field officialTeamOrderTypes.
   */
  public void setOfficialTeamOrderTypes(ch.ivyteam.ivy.scripting.objects.Recordset _officialTeamOrderTypes)
  {
    officialTeamOrderTypes = _officialTeamOrderTypes;
  }

}
