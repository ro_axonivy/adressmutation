package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderMassUpdate", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderMassUpdate extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 102610482978733628L;

  private ch.ivyteam.ivy.scripting.objects.List<Object> orderIdList;

  /**
   * Gets the field orderIdList.
   * @return the value of the field orderIdList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getOrderIdList()
  {
    return orderIdList;
  }

  /**
   * Sets the field orderIdList.
   * @param _orderIdList the new value of the field orderIdList.
   */
  public void setOrderIdList(ch.ivyteam.ivy.scripting.objects.List<Object> _orderIdList)
  {
    orderIdList = _orderIdList;
  }

  private ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> dispatchers;

  /**
   * Gets the field dispatchers.
   * @return the value of the field dispatchers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getDispatchers()
  {
    return dispatchers;
  }

  /**
   * Sets the field dispatchers.
   * @param _dispatchers the new value of the field dispatchers.
   */
  public void setDispatchers(ch.ivyteam.ivy.scripting.objects.List<Object> _dispatchers)
  {
    dispatchers = _dispatchers;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> editors;

  /**
   * Gets the field editors.
   * @return the value of the field editors; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getEditors()
  {
    return editors;
  }

  /**
   * Sets the field editors.
   * @param _editors the new value of the field editors.
   */
  public void setEditors(ch.ivyteam.ivy.scripting.objects.List<Object> _editors)
  {
    editors = _editors;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> checkers;

  /**
   * Gets the field checkers.
   * @return the value of the field checkers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getCheckers()
  {
    return checkers;
  }

  /**
   * Sets the field checkers.
   * @param _checkers the new value of the field checkers.
   */
  public void setCheckers(ch.ivyteam.ivy.scripting.objects.List<Object> _checkers)
  {
    checkers = _checkers;
  }

  private java.lang.Number i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Number getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Number _i)
  {
    i = _i;
  }

  private ch.soreco.orderbook.bo.Order orderCopy;

  /**
   * Gets the field orderCopy.
   * @return the value of the field orderCopy; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrderCopy()
  {
    return orderCopy;
  }

  /**
   * Sets the field orderCopy.
   * @param _orderCopy the new value of the field orderCopy.
   */
  public void setOrderCopy(ch.soreco.orderbook.bo.Order _orderCopy)
  {
    orderCopy = _orderCopy;
  }

  private ch.ivyteam.ivy.scripting.objects.List<Object> priorities;

  /**
   * Gets the field priorities.
   * @return the value of the field priorities; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<Object> getPriorities()
  {
    return priorities;
  }

  /**
   * Sets the field priorities.
   * @param _priorities the new value of the field priorities.
   */
  public void setPriorities(ch.ivyteam.ivy.scripting.objects.List<Object> _priorities)
  {
    priorities = _priorities;
  }

  private java.lang.String mode;

  /**
   * Gets the field mode.
   * @return the value of the field mode; may be null.
   */
  public java.lang.String getMode()
  {
    return mode;
  }

  /**
   * Sets the field mode.
   * @param _mode the new value of the field mode.
   */
  public void setMode(java.lang.String _mode)
  {
    mode = _mode;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset orderQuantityTypes;

  /**
   * Gets the field orderQuantityTypes.
   * @return the value of the field orderQuantityTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getOrderQuantityTypes()
  {
    return orderQuantityTypes;
  }

  /**
   * Sets the field orderQuantityTypes.
   * @param _orderQuantityTypes the new value of the field orderQuantityTypes.
   */
  public void setOrderQuantityTypes(ch.ivyteam.ivy.scripting.objects.Recordset _orderQuantityTypes)
  {
    orderQuantityTypes = _orderQuantityTypes;
  }

}
