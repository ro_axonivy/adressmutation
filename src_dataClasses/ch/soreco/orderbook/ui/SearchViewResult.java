package ch.soreco.orderbook.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class SearchViewResult", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class SearchViewResult extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6106842444691843812L;

  private transient java.lang.Integer countOfQueryResults;

  /**
   * Gets the field countOfQueryResults.
   * @return the value of the field countOfQueryResults; may be null.
   */
  public java.lang.Integer getCountOfQueryResults()
  {
    return countOfQueryResults;
  }

  /**
   * Sets the field countOfQueryResults.
   * @param _countOfQueryResults the new value of the field countOfQueryResults.
   */
  public void setCountOfQueryResults(java.lang.Integer _countOfQueryResults)
  {
    countOfQueryResults = _countOfQueryResults;
  }

  private transient java.lang.Boolean exceededSelectTop;

  /**
   * Gets the field exceededSelectTop.
   * @return the value of the field exceededSelectTop; may be null.
   */
  public java.lang.Boolean getExceededSelectTop()
  {
    return exceededSelectTop;
  }

  /**
   * Sets the field exceededSelectTop.
   * @param _exceededSelectTop the new value of the field exceededSelectTop.
   */
  public void setExceededSelectTop(java.lang.Boolean _exceededSelectTop)
  {
    exceededSelectTop = _exceededSelectTop;
  }

  private transient java.lang.Boolean exceededMessageShown;

  /**
   * Gets the field exceededMessageShown.
   * @return the value of the field exceededMessageShown; may be null.
   */
  public java.lang.Boolean getExceededMessageShown()
  {
    return exceededMessageShown;
  }

  /**
   * Sets the field exceededMessageShown.
   * @param _exceededMessageShown the new value of the field exceededMessageShown.
   */
  public void setExceededMessageShown(java.lang.Boolean _exceededMessageShown)
  {
    exceededMessageShown = _exceededMessageShown;
  }

  private transient java.lang.Boolean hasException;

  /**
   * Gets the field hasException.
   * @return the value of the field hasException; may be null.
   */
  public java.lang.Boolean getHasException()
  {
    return hasException;
  }

  /**
   * Sets the field hasException.
   * @param _hasException the new value of the field hasException.
   */
  public void setHasException(java.lang.Boolean _hasException)
  {
    hasException = _hasException;
  }

  private transient java.lang.String exceptionMessage;

  /**
   * Gets the field exceptionMessage.
   * @return the value of the field exceptionMessage; may be null.
   */
  public java.lang.String getExceptionMessage()
  {
    return exceptionMessage;
  }

  /**
   * Sets the field exceptionMessage.
   * @param _exceptionMessage the new value of the field exceptionMessage.
   */
  public void setExceptionMessage(java.lang.String _exceptionMessage)
  {
    exceptionMessage = _exceptionMessage;
  }

}
