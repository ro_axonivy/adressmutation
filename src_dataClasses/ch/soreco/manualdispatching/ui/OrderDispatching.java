package ch.soreco.manualdispatching.ui;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class OrderDispatching", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class OrderDispatching extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 4652237162227714970L;

  private ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> authUsers;

  /**
   * Gets the field authUsers.
   * @return the value of the field authUsers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> getAuthUsers()
  {
    return authUsers;
  }

  /**
   * Sets the field authUsers.
   * @param _authUsers the new value of the field authUsers.
   */
  public void setAuthUsers(ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> _authUsers)
  {
    authUsers = _authUsers;
  }

  private java.lang.String contactPanel;

  /**
   * Gets the field contactPanel.
   * @return the value of the field contactPanel; may be null.
   */
  public java.lang.String getContactPanel()
  {
    return contactPanel;
  }

  /**
   * Sets the field contactPanel.
   * @param _contactPanel the new value of the field contactPanel.
   */
  public void setContactPanel(java.lang.String _contactPanel)
  {
    contactPanel = _contactPanel;
  }

  private ch.soreco.orderbook.bo.Order currentOrder;

  /**
   * Gets the field currentOrder.
   * @return the value of the field currentOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getCurrentOrder()
  {
    return currentOrder;
  }

  /**
   * Sets the field currentOrder.
   * @param _currentOrder the new value of the field currentOrder.
   */
  public void setCurrentOrder(ch.soreco.orderbook.bo.Order _currentOrder)
  {
    currentOrder = _currentOrder;
  }

  private java.lang.String detailPanel;

  /**
   * Gets the field detailPanel.
   * @return the value of the field detailPanel; may be null.
   */
  public java.lang.String getDetailPanel()
  {
    return detailPanel;
  }

  /**
   * Sets the field detailPanel.
   * @param _detailPanel the new value of the field detailPanel.
   */
  public void setDetailPanel(java.lang.String _detailPanel)
  {
    detailPanel = _detailPanel;
  }

  private ch.soreco.orderbook.bo.Dispatching dispatchingData;

  /**
   * Gets the field dispatchingData.
   * @return the value of the field dispatchingData; may be null.
   */
  public ch.soreco.orderbook.bo.Dispatching getDispatchingData()
  {
    return dispatchingData;
  }

  /**
   * Sets the field dispatchingData.
   * @param _dispatchingData the new value of the field dispatchingData.
   */
  public void setDispatchingData(ch.soreco.orderbook.bo.Dispatching _dispatchingData)
  {
    dispatchingData = _dispatchingData;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset dispatchingRS;

  /**
   * Gets the field dispatchingRS.
   * @return the value of the field dispatchingRS; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getDispatchingRS()
  {
    return dispatchingRS;
  }

  /**
   * Sets the field dispatchingRS.
   * @param _dispatchingRS the new value of the field dispatchingRS.
   */
  public void setDispatchingRS(ch.ivyteam.ivy.scripting.objects.Recordset _dispatchingRS)
  {
    dispatchingRS = _dispatchingRS;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> dspPairs;

  /**
   * Gets the field dspPairs.
   * @return the value of the field dspPairs; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getDspPairs()
  {
    return dspPairs;
  }

  /**
   * Sets the field dspPairs.
   * @param _dspPairs the new value of the field dspPairs.
   */
  public void setDspPairs(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _dspPairs)
  {
    dspPairs = _dspPairs;
  }

  private java.lang.String editMode;

  /**
   * Gets the field editMode.
   * @return the value of the field editMode; may be null.
   */
  public java.lang.String getEditMode()
  {
    return editMode;
  }

  /**
   * Sets the field editMode.
   * @param _editMode the new value of the field editMode.
   */
  public void setEditMode(java.lang.String _editMode)
  {
    editMode = _editMode;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> editorUsers;

  /**
   * Gets the field editorUsers.
   * @return the value of the field editorUsers; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getEditorUsers()
  {
    return editorUsers;
  }

  /**
   * Sets the field editorUsers.
   * @param _editorUsers the new value of the field editorUsers.
   */
  public void setEditorUsers(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _editorUsers)
  {
    editorUsers = _editorUsers;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.ui.enums.ViewType eViewType;

  /**
   * Gets the field eViewType.
   * @return the value of the field eViewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getEViewType()
  {
    return eViewType;
  }

  /**
   * Sets the field eViewType.
   * @param _eViewType the new value of the field eViewType.
   */
  public void setEViewType(ch.soreco.orderbook.ui.enums.ViewType _eViewType)
  {
    eViewType = _eViewType;
  }

  private java.io.File ExcelFile;

  /**
   * Gets the field ExcelFile.
   * @return the value of the field ExcelFile; may be null.
   */
  public java.io.File getExcelFile()
  {
    return ExcelFile;
  }

  /**
   * Sets the field ExcelFile.
   * @param _ExcelFile the new value of the field ExcelFile.
   */
  public void setExcelFile(java.io.File _ExcelFile)
  {
    ExcelFile = _ExcelFile;
  }

  private ch.soreco.orderbook.bo.Order filter;

  /**
   * Gets the field filter.
   * @return the value of the field filter; may be null.
   */
  public ch.soreco.orderbook.bo.Order getFilter()
  {
    return filter;
  }

  /**
   * Sets the field filter.
   * @param _filter the new value of the field filter.
   */
  public void setFilter(ch.soreco.orderbook.bo.Order _filter)
  {
    filter = _filter;
  }

  private java.lang.Boolean furtherProcessing;

  /**
   * Gets the field furtherProcessing.
   * @return the value of the field furtherProcessing; may be null.
   */
  public java.lang.Boolean getFurtherProcessing()
  {
    return furtherProcessing;
  }

  /**
   * Sets the field furtherProcessing.
   * @param _furtherProcessing the new value of the field furtherProcessing.
   */
  public void setFurtherProcessing(java.lang.Boolean _furtherProcessing)
  {
    furtherProcessing = _furtherProcessing;
  }

  private java.lang.String html;

  /**
   * Gets the field html.
   * @return the value of the field html; may be null.
   */
  public java.lang.String getHtml()
  {
    return html;
  }

  /**
   * Sets the field html.
   * @param _html the new value of the field html.
   */
  public void setHtml(java.lang.String _html)
  {
    html = _html;
  }

  private java.lang.Number i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Number getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Number _i)
  {
    i = _i;
  }

  private java.lang.Number iDsp;

  /**
   * Gets the field iDsp.
   * @return the value of the field iDsp; may be null.
   */
  public java.lang.Number getIDsp()
  {
    return iDsp;
  }

  /**
   * Sets the field iDsp.
   * @param _iDsp the new value of the field iDsp.
   */
  public void setIDsp(java.lang.Number _iDsp)
  {
    iDsp = _iDsp;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> listOrder;

  /**
   * Gets the field listOrder.
   * @return the value of the field listOrder; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getListOrder()
  {
    return listOrder;
  }

  /**
   * Sets the field listOrder.
   * @param _listOrder the new value of the field listOrder.
   */
  public void setListOrder(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _listOrder)
  {
    listOrder = _listOrder;
  }

  private java.lang.Boolean modify;

  /**
   * Gets the field modify.
   * @return the value of the field modify; may be null.
   */
  public java.lang.Boolean getModify()
  {
    return modify;
  }

  /**
   * Sets the field modify.
   * @param _modify the new value of the field modify.
   */
  public void setModify(java.lang.Boolean _modify)
  {
    modify = _modify;
  }

  private java.lang.String nextAction;

  /**
   * Gets the field nextAction.
   * @return the value of the field nextAction; may be null.
   */
  public java.lang.String getNextAction()
  {
    return nextAction;
  }

  /**
   * Sets the field nextAction.
   * @param _nextAction the new value of the field nextAction.
   */
  public void setNextAction(java.lang.String _nextAction)
  {
    nextAction = _nextAction;
  }

  private ch.soreco.orderbook.bo.OrderBook orderBook;

  /**
   * Gets the field orderBook.
   * @return the value of the field orderBook; may be null.
   */
  public ch.soreco.orderbook.bo.OrderBook getOrderBook()
  {
    return orderBook;
  }

  /**
   * Sets the field orderBook.
   * @param _orderBook the new value of the field orderBook.
   */
  public void setOrderBook(ch.soreco.orderbook.bo.OrderBook _orderBook)
  {
    orderBook = _orderBook;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> orderBooks;

  /**
   * Gets the field orderBooks.
   * @return the value of the field orderBooks; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> getOrderBooks()
  {
    return orderBooks;
  }

  /**
   * Sets the field orderBooks.
   * @param _orderBooks the new value of the field orderBooks.
   */
  public void setOrderBooks(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> _orderBooks)
  {
    orderBooks = _orderBooks;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> orderBooksIndefinit;

  /**
   * Gets the field orderBooksIndefinit.
   * @return the value of the field orderBooksIndefinit; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> getOrderBooksIndefinit()
  {
    return orderBooksIndefinit;
  }

  /**
   * Sets the field orderBooksIndefinit.
   * @param _orderBooksIndefinit the new value of the field orderBooksIndefinit.
   */
  public void setOrderBooksIndefinit(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.OrderBook> _orderBooksIndefinit)
  {
    orderBooksIndefinit = _orderBooksIndefinit;
  }

  private java.lang.String orderBookType;

  /**
   * Gets the field orderBookType.
   * @return the value of the field orderBookType; may be null.
   */
  public java.lang.String getOrderBookType()
  {
    return orderBookType;
  }

  /**
   * Sets the field orderBookType.
   * @param _orderBookType the new value of the field orderBookType.
   */
  public void setOrderBookType(java.lang.String _orderBookType)
  {
    orderBookType = _orderBookType;
  }

  private ch.soreco.orderbook.bo.Contact orderContact;

  /**
   * Gets the field orderContact.
   * @return the value of the field orderContact; may be null.
   */
  public ch.soreco.orderbook.bo.Contact getOrderContact()
  {
    return orderContact;
  }

  /**
   * Sets the field orderContact.
   * @param _orderContact the new value of the field orderContact.
   */
  public void setOrderContact(ch.soreco.orderbook.bo.Contact _orderContact)
  {
    orderContact = _orderContact;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> orderContacts;

  /**
   * Gets the field orderContacts.
   * @return the value of the field orderContacts; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> getOrderContacts()
  {
    return orderContacts;
  }

  /**
   * Sets the field orderContacts.
   * @param _orderContacts the new value of the field orderContacts.
   */
  public void setOrderContacts(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Contact> _orderContacts)
  {
    orderContacts = _orderContacts;
  }

  private ch.soreco.orderbook.bo.Order orderToProcess;

  /**
   * Gets the field orderToProcess.
   * @return the value of the field orderToProcess; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrderToProcess()
  {
    return orderToProcess;
  }

  /**
   * Sets the field orderToProcess.
   * @param _orderToProcess the new value of the field orderToProcess.
   */
  public void setOrderToProcess(ch.soreco.orderbook.bo.Order _orderToProcess)
  {
    orderToProcess = _orderToProcess;
  }

  private java.lang.String orderType;

  /**
   * Gets the field orderType.
   * @return the value of the field orderType; may be null.
   */
  public java.lang.String getOrderType()
  {
    return orderType;
  }

  /**
   * Sets the field orderType.
   * @param _orderType the new value of the field orderType.
   */
  public void setOrderType(java.lang.String _orderType)
  {
    orderType = _orderType;
  }

  private ch.soreco.orderbook.enums.OrderType orderTypeEnum;

  /**
   * Gets the field orderTypeEnum.
   * @return the value of the field orderTypeEnum; may be null.
   */
  public ch.soreco.orderbook.enums.OrderType getOrderTypeEnum()
  {
    return orderTypeEnum;
  }

  /**
   * Sets the field orderTypeEnum.
   * @param _orderTypeEnum the new value of the field orderTypeEnum.
   */
  public void setOrderTypeEnum(ch.soreco.orderbook.enums.OrderType _orderTypeEnum)
  {
    orderTypeEnum = _orderTypeEnum;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> orderTypes;

  /**
   * Gets the field orderTypes.
   * @return the value of the field orderTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getOrderTypes()
  {
    return orderTypes;
  }

  /**
   * Sets the field orderTypes.
   * @param _orderTypes the new value of the field orderTypes.
   */
  public void setOrderTypes(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _orderTypes)
  {
    orderTypes = _orderTypes;
  }

  private java.lang.String orderTypesJSON;

  /**
   * Gets the field orderTypesJSON.
   * @return the value of the field orderTypesJSON; may be null.
   */
  public java.lang.String getOrderTypesJSON()
  {
    return orderTypesJSON;
  }

  /**
   * Sets the field orderTypesJSON.
   * @param _orderTypesJSON the new value of the field orderTypesJSON.
   */
  public void setOrderTypesJSON(java.lang.String _orderTypesJSON)
  {
    orderTypesJSON = _orderTypesJSON;
  }

  private java.lang.String panelCaption;

  /**
   * Gets the field panelCaption.
   * @return the value of the field panelCaption; may be null.
   */
  public java.lang.String getPanelCaption()
  {
    return panelCaption;
  }

  /**
   * Sets the field panelCaption.
   * @param _panelCaption the new value of the field panelCaption.
   */
  public void setPanelCaption(java.lang.String _panelCaption)
  {
    panelCaption = _panelCaption;
  }

  private java.lang.String pdfPanel;

  /**
   * Gets the field pdfPanel.
   * @return the value of the field pdfPanel; may be null.
   */
  public java.lang.String getPdfPanel()
  {
    return pdfPanel;
  }

  /**
   * Sets the field pdfPanel.
   * @param _pdfPanel the new value of the field pdfPanel.
   */
  public void setPdfPanel(java.lang.String _pdfPanel)
  {
    pdfPanel = _pdfPanel;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> priorityList;

  /**
   * Gets the field priorityList.
   * @return the value of the field priorityList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getPriorityList()
  {
    return priorityList;
  }

  /**
   * Sets the field priorityList.
   * @param _priorityList the new value of the field priorityList.
   */
  public void setPriorityList(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _priorityList)
  {
    priorityList = _priorityList;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset rsOrder;

  /**
   * Gets the field rsOrder.
   * @return the value of the field rsOrder; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRsOrder()
  {
    return rsOrder;
  }

  /**
   * Sets the field rsOrder.
   * @param _rsOrder the new value of the field rsOrder.
   */
  public void setRsOrder(ch.ivyteam.ivy.scripting.objects.Recordset _rsOrder)
  {
    rsOrder = _rsOrder;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> teams;

  /**
   * Gets the field teams.
   * @return the value of the field teams; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getTeams()
  {
    return teams;
  }

  /**
   * Sets the field teams.
   * @param _teams the new value of the field teams.
   */
  public void setTeams(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _teams)
  {
    teams = _teams;
  }

  private java.lang.String teamTabsHTML;

  /**
   * Gets the field teamTabsHTML.
   * @return the value of the field teamTabsHTML; may be null.
   */
  public java.lang.String getTeamTabsHTML()
  {
    return teamTabsHTML;
  }

  /**
   * Sets the field teamTabsHTML.
   * @param _teamTabsHTML the new value of the field teamTabsHTML.
   */
  public void setTeamTabsHTML(java.lang.String _teamTabsHTML)
  {
    teamTabsHTML = _teamTabsHTML;
  }

  private java.util.HashMap userHashMap;

  /**
   * Gets the field userHashMap.
   * @return the value of the field userHashMap; may be null.
   */
  public java.util.HashMap getUserHashMap()
  {
    return userHashMap;
  }

  /**
   * Sets the field userHashMap.
   * @param _userHashMap the new value of the field userHashMap.
   */
  public void setUserHashMap(java.util.HashMap _userHashMap)
  {
    userHashMap = _userHashMap;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> userList;

  /**
   * Gets the field userList.
   * @return the value of the field userList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> getUserList()
  {
    return userList;
  }

  /**
   * Sets the field userList.
   * @param _userList the new value of the field userList.
   */
  public void setUserList(ch.ivyteam.ivy.scripting.objects.List<ch.ivyteam.ivy.security.IUser> _userList)
  {
    userList = _userList;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> userStringList;

  /**
   * Gets the field userStringList.
   * @return the value of the field userStringList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getUserStringList()
  {
    return userStringList;
  }

  /**
   * Sets the field userStringList.
   * @param _userStringList the new value of the field userStringList.
   */
  public void setUserStringList(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _userStringList)
  {
    userStringList = _userStringList;
  }

  private ch.soreco.orderbook.ui.enums.ViewType viewType;

  /**
   * Gets the field viewType.
   * @return the value of the field viewType; may be null.
   */
  public ch.soreco.orderbook.ui.enums.ViewType getViewType()
  {
    return viewType;
  }

  /**
   * Sets the field viewType.
   * @param _viewType the new value of the field viewType.
   */
  public void setViewType(ch.soreco.orderbook.ui.enums.ViewType _viewType)
  {
    viewType = _viewType;
  }

  private ch.soreco.common.bo.AuthorityGroup authorityGroup;

  /**
   * Gets the field authorityGroup.
   * @return the value of the field authorityGroup; may be null.
   */
  public ch.soreco.common.bo.AuthorityGroup getAuthorityGroup()
  {
    return authorityGroup;
  }

  /**
   * Sets the field authorityGroup.
   * @param _authorityGroup the new value of the field authorityGroup.
   */
  public void setAuthorityGroup(ch.soreco.common.bo.AuthorityGroup _authorityGroup)
  {
    authorityGroup = _authorityGroup;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> authorityGroupList;

  /**
   * Gets the field authorityGroupList.
   * @return the value of the field authorityGroupList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> getAuthorityGroupList()
  {
    return authorityGroupList;
  }

  /**
   * Sets the field authorityGroupList.
   * @param _authorityGroupList the new value of the field authorityGroupList.
   */
  public void setAuthorityGroupList(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.common.bo.AuthorityGroup> _authorityGroupList)
  {
    authorityGroupList = _authorityGroupList;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset filterOrderTypes;

  /**
   * Gets the field filterOrderTypes.
   * @return the value of the field filterOrderTypes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getFilterOrderTypes()
  {
    return filterOrderTypes;
  }

  /**
   * Sets the field filterOrderTypes.
   * @param _filterOrderTypes the new value of the field filterOrderTypes.
   */
  public void setFilterOrderTypes(ch.ivyteam.ivy.scripting.objects.Recordset _filterOrderTypes)
  {
    filterOrderTypes = _filterOrderTypes;
  }

  private java.lang.Integer lotSize;

  /**
   * Gets the field lotSize.
   * @return the value of the field lotSize; may be null.
   */
  public java.lang.Integer getLotSize()
  {
    return lotSize;
  }

  /**
   * Sets the field lotSize.
   * @param _lotSize the new value of the field lotSize.
   */
  public void setLotSize(java.lang.Integer _lotSize)
  {
    lotSize = _lotSize;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> excludes;

  /**
   * Gets the field excludes.
   * @return the value of the field excludes; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getExcludes()
  {
    return excludes;
  }

  /**
   * Sets the field excludes.
   * @param _excludes the new value of the field excludes.
   */
  public void setExcludes(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _excludes)
  {
    excludes = _excludes;
  }

  private java.lang.String roleView;

  /**
   * Gets the field roleView.
   * @return the value of the field roleView; may be null.
   */
  public java.lang.String getRoleView()
  {
    return roleView;
  }

  /**
   * Sets the field roleView.
   * @param _roleView the new value of the field roleView.
   */
  public void setRoleView(java.lang.String _roleView)
  {
    roleView = _roleView;
  }

  /**
   * a
   */
  private ch.ivyteam.ivy.scripting.objects.Recordset filterOrderStates;

  /**
   * Gets the field filterOrderStates.
   * @return the value of the field filterOrderStates; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getFilterOrderStates()
  {
    return filterOrderStates;
  }

  /**
   * Sets the field filterOrderStates.
   * @param _filterOrderStates the new value of the field filterOrderStates.
   */
  public void setFilterOrderStates(ch.ivyteam.ivy.scripting.objects.Recordset _filterOrderStates)
  {
    filterOrderStates = _filterOrderStates;
  }

}
