<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title><%=ivy.content("Caption","String")%></title>
	<!--<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>-->	
</head>
	<body>
		<div id="dialog" title="<%=ivy.content("Caption","String")%>">
			<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>
		</div>
	</body>
</html>