<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache"); 
response.addHeader("Cache-Control","no-store"); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta http-equiv="expires" content="0">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta http-equiv="cache-control" content="no-cache, must-revalidate"> 

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%
	String jQueryVersion = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion");
	String jQueryUIVersion =ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUIVersion");
	String jQueryUITheme = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUITheme");
	%>
	<!--<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion")%>-->
	<!--<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>-->	
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-"+jQueryVersion+".min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-ui-"+jQueryUIVersion+".custom.min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.metadata.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.validate.js")%>"></script>

	<link type="text/css" href="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.css")%>" rel="stylesheet" >
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.js")%>"></script>

	<link title="<%=jQueryUITheme%>" type="text/css" href="<%=ivy.html.wr("css/"+jQueryUITheme+"/jquery-ui-"+jQueryUIVersion+".custom.css")%>" rel="stylesheet" >
	
	<title><%=ivy.content("Caption","String")%></title>
	
<style type="text/css">
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/Styles/Layout")%>
</style>

</head>

<body>	
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/inputTriggers")%>
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SmartTableLayout")%>
<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>		


</body>
</html>