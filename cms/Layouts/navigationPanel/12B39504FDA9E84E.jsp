<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP,  
						ch.soreco.webbies.common.html.error.ErrorDiv"%>
<%@ page import="ch.soreco.webbies.common.html.navigation.Menu"%>

<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<%
Object errorObject = ivy.html.getObject("in.error");
String error = "";
Boolean hasError = false;
error =ErrorDiv.ErrorToDiv(errorObject);
if(!error.equals("")) hasError = true;
%>
<% ivy.setRequest(request); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%
	String jQueryVersion = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion");
	String jQueryUIVersion =ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUIVersion");
	String jQueryUITheme = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUITheme");
	%>
	<!--<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion")%>-->
	<!--<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>-->	
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-"+jQueryVersion+".min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-ui-"+jQueryUIVersion+".custom.min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.bgiframe-2.1.1.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.cookie.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.metadata.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.validate.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("additional-methods.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("lib/jquery.form.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("localization/messages_de.js")%>"></script>	

	<link type="text/css" href="<%=ivy.html.wr("development-bundle/external/qunit.css")%>" rel="stylesheet" />
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/qunit.js")%>"></script>

	<link type="text/css" href="<%=ivy.html.wr("css/ui.jqgrid.css")%>" rel="stylesheet" />
	<%
	if(jQueryUITheme.equals("")) {
		out.println(ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryStyleSwitcher"));
		jQueryUITheme = "smoothness";
		}
	%>
	<link title="<%=jQueryUITheme%>" type="text/css" href="<%=ivy.html.wr("css/"+jQueryUITheme+"/jquery-ui-"+jQueryUIVersion+".custom.css")%>" rel="stylesheet" />

	<!--<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.tablesorter.min.js")%>"></script>-->
	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.tablesorter.pager.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter_filter.js")%>"></script>
	<link type="text/css" href="<%=ivy.html.wr("css/tablesorter/tablesorter.css")%>" rel="stylesheet" />
	<!-- jQuery ui Grid-->
	<script type="text/javascript" src="<%=ivy.html.wr("js/i18n/grid.locale-de.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.jqGrid.min.js")%>"></script>

	<title><%=ivy.content("Caption","String")%></title>
	
<style type="text/css">
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/Styles/jQueryHelper")%>
</style>

</head>

<body >
	<div><p>&nbsp;</p></div>
	<div id="CompletePage" class="ui-helper-reset ui-widget ui-widget-content ui-corner-all">
		<div id="PageHeader">
			<div id="banner"><%=ivy.cms.co("/Project/Banner")%></div>
			<div id="header"><%=ivy.cms.co("/Project/HeaderText")%></div>
		</div>
		<div id="PageMain" class="ui-corner-all">
			<div id="PageNavigation">
				<%=Menu.getMenuHtml(ivy.html.getCurrentContentPageUri(), ivy.html)%>
			</div>
			<div id="PageContainer" class="ui-corner-all">
				<div id="PageCaption" >
					<h2><%=ivy.content("Caption","String")%></h2>
				</div>
				<div id="PageContent" >
					<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>
				</div>
			</div>
		</div>
		<div id="PageFooter">
			<%=ivy.cms.co("/Project/FooterText")%>
		</div>
	</div>
	<%
	if(hasError) {
		%>
		<div id="errorMsg" style="display:none;"><%=error%></div>
		<%
		}
	%>
<!-- Initialize State Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SetState")%>
<!-- /Initialize State Helper -->
<!--Initialize DatePick Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryDatePicker")%>
<!--/Initialize DatePick Helper-->
<!--Initialize Button Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/ButtonHelper")%>
<!--/Initialize Button Helper-->
<!-- Initialize StateChangeBackground Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/setStateChangeClass")%>
<!--/Initialize StateChangeBackground Helper-->
<!-- /Initialize Menu -->
<script type="text/javascript">
$(document).ready(function() {
	var tabIndex = ($("#navigation h3").index($("#navigation h3.expanded")));
		
	$( "#navigation" ).accordion({
		fillSpace:true,
		clearStyle: true, 
		active:tabIndex
		});
	$( ".menu ul" ).menu();
	$(".ui-menu-item a").click(function() {
		document.location.href = $(this).attr("href");
		});
	$("#PageCaption").addClass("ui-widget-header").addClass("ui-helper-reset").addClass("ui-corner-top");
	$("#PageContent").addClass("ui-widget-content").addClass("ui-corner-bottom");
});
</script><!-- End Menu-->
</body>
</html>