<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>
	<title>Xpert.ivy</title>
	<link type="text/css" href="<%=ivy.html.wr("css/smoothness/jquery-ui-1.8.4.custom.css")%>" rel="stylesheet" />
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery-1.4.2.min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery-ui-1.8.4.custom.min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.bgiframe-2.1.1.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.cookie.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.metadata.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/qunit.js")%>"></script>
	<link type="text/css" href="<%=ivy.html.wr("development-bundle/external/qunit.css")%>" rel="stylesheet" />
</head>

<body>
	<div id="CompletePage">
		<div id="PageHeader">
			<div id="PageHeaderLogo"><%=ivy.cms.co("/Project/Banner")%></div>
			<div><%=ivy.cms.co("/Project/HeaderText")%></div>
		</div>
		
		<div id="PageCaption">
			<h2><%=ivy.content("Caption","String")%></h2>
		</div>
		
		<div id="PageContent">
			<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>
		</div>
		
		<div id="PageFooter">
			<%=ivy.cms.co("/Project/FooterText")%>
		</div>
	</div>
</body>
</html>