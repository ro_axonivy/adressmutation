<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<%@ page import="ch.soreco.webbies.common.html.navigation.Menu"%>
<%@ page import="ch.ivyteam.ivy.environment.Ivy"%>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache"); 
response.addHeader("Cache-Control","no-store"); 
%>
<%
String userName = "";
try { 
	userName = Ivy.session().getSessionUser().getDisplayName();
	}
catch (Exception e) {
	userName = "";
	}
%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<script type="text/javascript">
	//prevent loadiong main Page in an iframe
		if( top.location != document.location ) {
			top.location.href = location.href;
		} 
	</script>
	<meta http-equiv="expires" content="0">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta http-equiv="cache-control" content="no-cache, must-revalidate"> 

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%
	String jQueryVersion = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion");
	String jQueryUIVersion =ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUIVersion");
	String jQueryUITheme = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUITheme");
	%>
	<!--<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion")%>-->
	<!--<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>-->	
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-"+jQueryVersion+".min.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-ui-"+jQueryUIVersion+".custom.min.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.bgiframe-2.1.1.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.cookie.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.metadata.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.validate.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("additional-methods.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("lib/jquery.form.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("lib/jquery.iframe.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("localization/messages_de.js")%>"></script>	

	<script type="text/javascript" src="<%=ivy.html.wr("jquery.popupWindow.js")%>"></script> 
	<link type="text/css" href="<%=ivy.html.wr("development-bundle/external/qunit.css")%>" rel="stylesheet" > 
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/qunit.js")%>"></script> 
	
	<link type="text/css" href="<%=ivy.html.wr("css/ui.jqgrid.css")%>" rel="stylesheet" > 
	<%
	if(jQueryUITheme.equals("")) {
		out.println(ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryStyleSwitcher"));
		jQueryUITheme = "smoothness";
		}
	%> 
	<link title="<%=jQueryUITheme%>" type="text/css" href="<%=ivy.html.wr("css/"+jQueryUITheme+"/jquery-ui-"+jQueryUIVersion+".custom.css")%>" rel="stylesheet" >

	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.tablesorter.pager.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter_filter.js")%>"></script> 
	<link type="text/css" href="<%=ivy.html.wr("css/tablesorter/tablesorter.css")%>" rel="stylesheet" > 

	<!-- jQuery ui Grid-->
	<script type="text/javascript" src="<%=ivy.html.wr("js/i18n/grid.locale-de.js")%>"></script> 
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.jqGrid.min.js")%>"></script> 

	<!-- TimePicker -->
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.timepickr.min.js")%>"></script> 
	<link type="text/css" href="<%=ivy.html.wr("jquery.timepickr.css")%>" rel="stylesheet" >

	<!-- jQuery alert-->
	<link type="text/css" href="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.css")%>" rel="stylesheet" > 
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.js")%>"></script>	

	<!--Window-->
	<%=ivy.cms.co("/Content/Shared/Sources/Addressmutation/Window")%>
	
	<title><%=ivy.content("Caption","String")%></title>
	
<style type="text/css">
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/Styles/Layout")%>
</style> 
<script type="text/javascript">
function getMainPageId() {return "<%=ivy.html.getCurrentContentPageUri()%>"};
</script>

</head>

<body >
	<div id="Container" class="ui-helper-reset ui-widget ui-widget-content ui-corner-all">
		<table id="PageLayout" cellpadding=0 cellspacing=0 border=0 width="100%">
			<tbody>
				<tr>
					<td id="HeadContainer" height="10%">
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td valign="MIDDLE" align="left"><%=ivy.cms.co("/Project/HeaderText")%></td>
								<td valign="MIDDLE" align="right"><%=ivy.cms.co("/Project/Banner")%></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				<td>
					<table width="100%" border=0 cellpadding=0 cellspacing=0>
						<tbody>
							<tr>
					<td class="PageColumn1" valign="top" height="90%" >
						<div id="PageNavigation">
							<%=Menu.getMenuHtml(ivy.html.getCurrentContentPageUri(), ivy.html)%>
						</div>
						<!-- Initialize Menu -->
						<script type="text/javascript">
							var tabIndex = ($("#navigation h3").index($("#navigation h3.expanded")));
			
							$( "#navigation" ).accordion({
								active:2,//tabIndex, 
								//event: "mouseover",
								autoHeight: false
							});
							$( ".menu ul" ).menu();
							$(".noChildNodes.ui-accordion-header").find('span').remove();
							$(".ui-accordion-header[urltype='startref']").unbind('click').click(function(){
								var $anchor = $(this).find('a');
								var href = $anchor.attr('href');
								var ivpIndx = href.indexOf(".ivp");
								if(ivpIndx){
									href = href.substring(0,ivpIndx)+".ivp"
								}
								document.location.href = href;
								return false;
							});
							$(".ui-menu-item a").click(function() {
								var href = $(this).attr('href');
								var ivpIndx = href.indexOf(".ivp");
								if(ivpIndx){
									href = href.substring(0,ivpIndx)+".ivp"
								}
								document.location.href = href;
							});
						</script><!-- End Menu-->
					</td>
					<td class="PageColumn2" valign="top">
						<div id="PageMain" class="ui-corner-all">
								<div id="PageCaption" class="ui-widget-header ui-corner-top">
									<table border=0 cellpadding=2 cellspacing=2 width="100%">
										<tbody>
											<tr>
												<td><h2><%=ivy.content("Caption","String")%></h2></td>
												<td align="right"><%=userName%></td>
											</tr>
										</tbody>
									</table>
								</div>						
							<div id="PageContainer" class="ui-helper-reset">
								<!-- input Triggers -->
								<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/inputTriggers")%>
								<!--/input Triggers -->
								<!-- Smart Table Helper-->
								<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SmartTableLayout")%>
								<!--/Smart table helper-->								
								<div id="PageContent" class="ui-widget-content ui-corner-bottom">
									<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>
								</div>

							</div>							
						</div>
					</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
				
				<tr>
					<td id="FooterContainer" >
						<div id="PageFooter">
							<%=ivy.cms.co("/Project/FooterText")%>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- Initialize State Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SetState")%>
<!-- /Initialize State Helper -->
<!--Initialize DatePick Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryDatePicker")%>
<!--/Initialize DatePick Helper-->
<!--Initialize Button Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/ButtonHelper")%>
<!--/Initialize Button Helper-->
<!-- Initialize StateChangeBackground Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/setStateChangeClass")%>
<!--/Initialize StateChangeBackground Helper-->
<!--Initialize Label for disabling Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/disabledLabels")%>
<!--/Initialize Label for disabling Helper-->
<!--Initialize jQuery alert-->
<!-- Die Javascript alert funktion wird mit der jAlert funktion ersetzt -->
<script type="text/javascript">
(function() {
  var proxied = window.alert;
  window.alert = function() {
    // do something here
    //JAlert raises a modal div containing the alert message
    jAlert(arguments[0], "<%=ivy.content("Caption","String")%>");
    //raise normal window alert
    //return proxied.apply(this, arguments);
  };
})();
</script>
</body>
</html>