<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/> 
<% ivy.setRequest(request); %>

<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />

<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link type="text/css" href="<%=ivy.html.wr("css/smoothness/jquery-ui-1.8.4.custom.css")%>" rel="stylesheet" ></link>
<script type="text/javascript" src="<%=ivy.html.wr("js/jquery-1.4.2.min.js")%>"></script>
<script type="text/javascript" src="<%=ivy.html.wr("js/jquery-ui-1.8.4.custom.min.js")%>"></script>

<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>
<title>Xpert.ivy</title>
</head>

<body>
<table width="100%" >
<tr>
<td align="right">
<%=ivy.cms.co("/Project/Banner")%>
</td>
</tr>
</table>
<table width="100%">
<tr>
<td>
<table width="100%">
<tr>
<td>
<h1>
<%=ivy.content("Caption","String")%>&nbsp;
</h1>
<HR size="2">
</td>
</tr>
</table>
<tr height="80%">
<td>
<table width="100%" height="100%">
<tr>
<td>
<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="100%">
<tr>
<td>
<%=ivy.cms.co("/Project/Footer")%>
</td>
</tr>
</table>
</body>
</html>