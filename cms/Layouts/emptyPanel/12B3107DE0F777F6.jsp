<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/>
<% ivy.setRequest(request); %>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache"); 
response.addHeader("Cache-Control","no-store"); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />

	<meta http-equiv="expires" content="0">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta http-equiv="cache-control" content="no-cache, must-revalidate"> 

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%
	String jQueryVersion = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion");
	String jQueryUIVersion =ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUIVersion");
	String jQueryUITheme = ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryUITheme");
	%>
	<!--<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryVersion")%>-->
	<!--<link rel="stylesheet" type="text/css" href="<%=ivy.style()%>"/>-->	
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-"+jQueryVersion+".min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr( "js/jquery-ui-"+jQueryUIVersion+".custom.min.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.bgiframe-2.1.1.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.cookie.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/jquery.metadata.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.validate.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("additional-methods.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("lib/jquery.form.js")%>"></script>
	<!--<script type="text/javascript" src="<%=ivy.html.wr("lib/jquery.iframe.js")%>"></script>-->
	<script type="text/javascript" src="<%=ivy.html.wr("localization/messages_de.js")%>"></script>	

	<link type="text/css" href="<%=ivy.html.wr("development-bundle/external/qunit.css")%>" rel="stylesheet" >
	<script type="text/javascript" src="<%=ivy.html.wr("development-bundle/external/qunit.js")%>"></script>

	<link type="text/css" href="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.css")%>" rel="stylesheet" >
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.alerts-1.1/jquery.alerts.js")%>"></script>

	<link type="text/css" href="<%=ivy.html.wr("css/ui.jqgrid.css")%>" rel="stylesheet" >
	<%
	if(jQueryUITheme.equals("")) {
		out.println(ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryStyleSwitcher"));
		jQueryUITheme = "smoothness";
		}
	%>
	<link title="<%=jQueryUITheme%>" type="text/css" href="<%=ivy.html.wr("css/"+jQueryUITheme+"/jquery-ui-"+jQueryUIVersion+".custom.css")%>" rel="stylesheet" >

	<!--<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.tablesorter.min.js")%>"></script>-->
	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.tablesorter.pager.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/tablesorter_filter.js")%>"></script>
	<link type="text/css" href="<%=ivy.html.wr("css/tablesorter/tablesorter.css")%>" rel="stylesheet" >
	<!-- jQuery ui Grid-->
	<script type="text/javascript" src="<%=ivy.html.wr("js/i18n/grid.locale-de.js")%>"></script>
	<script type="text/javascript" src="<%=ivy.html.wr("js/jquery.jqGrid.min.js")%>"></script>
	<!-- TimePicker -->
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.timepickr.min.js")%>"></script>
	<link type="text/css" href="<%=ivy.html.wr("jquery.timepickr.css")%>" rel="stylesheet" >

	<!--Window-->
	<script type="text/javascript" src="<%=ivy.html.wr("jquery.popupWindow.js")%>"></script> 
	<%=ivy.cms.co("/Content/Shared/Sources/Addressmutation/Window")%>
	
	<title><%=ivy.content("Caption","String")%></title>
	
<style type="text/css">
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/Styles/Layout")%>
</style>

<script type="text/javascript">
function getTitle(){ return "<%=ivy.content("Caption","String")%>"; }
function getPageId() {return "<%=ivy.html.getCurrentContentPageUri()%>"}
$.ajaxSetup ({cache: false });
</script>
</head>

<body>	
	<!-- input Triggers -->
	<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/inputTriggers")%>
	<!--/input Triggers -->	
	<div id="CompletePage" class="ui-helper-reset ui-widget ui-widget-content ui-corner-all">
	<!--Loading Dialog-->
	<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/PageLoading")%>	
		<jsp:include page='<%=ivy.panel("Panel1")%>' flush="true"/>		
	</div>
<!-- Initialize State Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SetState")%>
<!-- /Initialize State Helper -->
<!--Initialize DatePick Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQueryDatePicker")%>
<!--/Initialize DatePick Helper-->
<!--Initialize Button Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/ButtonHelper")%>
<!--/Initialize Button Helper-->
<!-- Initialize StateChangeBackground Helper -->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/setStateChangeClass")%>
<!--/Initialize StateChangeBackground Helper-->
<!--Initialize Label for disabling Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/disabledLabels")%>
<!--/Initialize Label for disabling Helper-->
<!-- Smart Table Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/SmartTableLayout")%>
<!--/Smart table helper-->
<!-- Dialog Helper-->
<%=ivy.cms.co("/Content/Shared/Configuration/Layout/jQuerySources/jQueryChildDialog")%>
<!-- /Dialog Helper-->
</body>
</html>