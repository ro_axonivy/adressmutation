<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%>
<%@ page import="ch.soreco.webbies.common.html.smarttable.SmartTableFilter"%>

<jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/> 
<% ivy.setRequest(request); %>
<html>
<body>
<%
try {
SmartTableFilter smf = new SmartTableFilter(ivy);
out.println(smf.getTableOfParameters());
} catch (Exception e) {
out.println(e.getMessage());
}
%>

</body>
</html>