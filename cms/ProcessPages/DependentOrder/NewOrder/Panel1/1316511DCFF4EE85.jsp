<%@ page import="ch.ivyteam.ivy.page.engine.jsp.IvyJSP"%><jsp:useBean id="ivy" class="ch.ivyteam.ivy.page.engine.jsp.IvyJSP" scope="session"/><!--ivyjsp --><%@ page import="ch.ivyteam.ivy.scripting.objects.Recordset" %>
<div style=
	" 
		position: absolute;
		top:70%;
		left: 65%;
		margin-top: -25%;
		margin-left: -25%;
	"

>
<h3>Neuen Auftrag mit anderer Auftragsart erstellen</h3>
Es wird ein neuer Auftrag zur direkten Bearbeitung erstellt.<br>
BP-Id und Dokumente werden kopiert.<br>
<br>
<form id="orderForm" method="POST" action="<%=ivy.html.ref("create.ivp")%>">
	<label style="display: inline-block;width: 75px;"><%=ivy.cms.co("/ch/soreco/orderbook/bo/Order/orderType/0_name")%></label>
	<select id="orderType" name="orderCopy$orderType"
		class="{EDITED:{required:true, messages:{required:'Die <%=ivy.cms.co("/ch/soreco/orderbook/bo/Order/orderType/0_name")%> ist erforderlich'}}}"	
	>
	<option value='' selected='selected'></option>
	<%
	//as the present code is executed in "Source" content, we do not have full JSP support
	//so nested jsp code, repsonse.out, @page import does not work.
		Recordset orderTypes = (Recordset) ivy.html.getObject("in.officialTeamOrderTypes");
		String orderTypesOptions = "";
		for(int i=0;i<orderTypes.size();i++){
			String typeName = orderTypes.getField(i,"orderTypeName").toString();
			String typeLabel = orderTypes.getField(i,"orderTypeLabel").toString();
			String requires = "";
			if(orderTypes.getField(i, "requiresOrderQuantityType").toString().equals("true")){
				requires = "class='RequiresOrderQuantityType'";
			}
			orderTypesOptions += "<option value='"+typeName+"' "+requires+" >"+typeLabel+"</option>";
		}
	%>
	<%=orderTypesOptions%>
	</select>
	<br>
	<label style="display: inline-block;width: 75px;"><%=ivy.cms.co("/ch/soreco/orderbook/bo/Order/orderQuantityType/0_name")%></label>
	<select id="orderQuantityType" name="orderCopy$orderQuantityType"
		class="{EDITED:{required:isRequiringQuantity, messages:{required:'Bei dieser <%=ivy.cms.co("/ch/soreco/orderbook/bo/Order/orderType/0_name")%> ist die <%=ivy.cms.co("/ch/soreco/orderbook/bo/Order/orderQuantityType/0_name")%> erforderlich'}}}"
		>
		<option value=''></option>
	<%
	Recordset orderQuantityTypes = (Recordset) ivy.html.getObject("in.orderQuantityTypes");
	String orderQuantityType = ivy.html.get("in.order.orderQuantityType");
	for(int i=0;i<orderQuantityTypes.size();i++){
		String key = orderQuantityTypes.getField(i,"Key").toString();
		String label = orderQuantityTypes.getField(i,"Label").toString();
		String selected = orderQuantityType.equals(key)? "selected='selected'":"";
		%>
		<option value="<%=key%>" <%=selected%> ><%=label%></option>
		<%
	}
	%>
	</select>
	<br>
	<div id="errors" class=""></div>	
	<br>
	<input id="CreateOrder" type="button" class="stateChange asButton" value="erstellen">
	<input type="button" class="stateWhite asButton" onclick="cancel()" value="abbrechen">
</form><!--EOF:orderForm-->
</div>
<script type="text/javascript">
function isRequiringQuantity(){
	return $("#orderType :selected").hasClass('RequiresOrderQuantityType');
}
$('#CreateOrder').click(function(){
	if(formValidation()){
		this.form.submit();
	}
});
function formValidation(){
	var $form = $('#orderForm');
	jQuery.removeData($form[0], 'validator');
	$form.validate({meta:"EDITED", errorLabelContainer: '#errors', wrapper: "li"});
	$("#errors li").addClass("ui-state-error");
	return $form.valid();
}
$("#orderType").change(function(){formValidation();});
$("#orderQuantityType").change(function(){formValidation();});
function cancel(){
	document.location = '<%=ivy.html.ref("cancel.ivp")%>&ajaxcache=' + (new Date()).getTime();
}
</script><!--/ivyjsp -->