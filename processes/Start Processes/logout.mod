[Ivy]
12B8D381DF0473F6 3.20 #module
>Proto >Proto Collection #zClass
lt0 logout Big #zClass
lt0 B #cInfo
lt0 #process
lt0 @TextInP .resExport .resExport #zField
lt0 @TextInP .type .type #zField
lt0 @TextInP .processKind .processKind #zField
lt0 @AnnotationInP-0n ai ai #zField
lt0 @TextInP .xml .xml #zField
lt0 @TextInP .responsibility .responsibility #zField
lt0 @GridStep f38 '' #zField
lt0 @GridStep f35 '' #zField
lt0 @GridStep f37 '' #zField
lt0 @Alternative f39 '' #zField
lt0 @GridStep f34 '' #zField
lt0 @Page f36 '' #zField
lt0 @PushWFArc f40 '' #zField
lt0 @PushWFArc f42 '' #zField
lt0 @PushWFArc f43 '' #zField
lt0 @PushWFArc f45 '' #zField
lt0 @PushWFArc f5 '' #zField
lt0 @PushWFArc f2 '' #zField
lt0 @EndRequest f6 '' #zField
lt0 @Alternative f7 '' #zField
lt0 @PushWFArc f4 '' #zField
lt0 @GridStep f10 '' #zField
lt0 @PushWFArc f11 '' #zField
lt0 @EndRequest f9 '' #zField
lt0 @PushWFArc f12 '' #zField
lt0 @GridStep f13 '' #zField
lt0 @PushWFArc f14 '' #zField
lt0 @StartRequest f0 '' #zField
lt0 @PushWFArc f1 '' #zField
>Proto lt0 lt0 logout #zField
lt0 f38 actionDecl 'adressmutation.Data out;
' #txt
lt0 f38 actionTable 'out=in;
out.error="User or Password is not correct";
' #txt
lt0 f38 type adressmutation.Data #txt
lt0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>user/passwort not correct</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f38 734 332 36 24 20 -2 #rect
lt0 f38 @|StepIcon #fIcon
lt0 f35 actionDecl 'adressmutation.Data out;
' #txt
lt0 f35 actionTable 'out=in;
' #txt
lt0 f35 actionCode 'in.liEnvironments.clear();
in.currentEnvironment = ivy.session.activeEnvironment;
//in.liEnvironments.add("SWBNBBST");
in.liEnvironments.add("b2p.soreco.ch");
in.liEnvironments.add("b2p.ai.ch");

ivy.session.setActiveEnvironment("");

' #txt
lt0 f35 type adressmutation.Data #txt
lt0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f35 582 172 36 24 19 -2 #rect
lt0 f35 @|StepIcon #fIcon
lt0 f37 actionDecl 'adressmutation.Data out;
' #txt
lt0 f37 actionTable 'out=in;
' #txt
lt0 f37 actionCode 'import ch.ivyteam.ivy.security.AuthenticationException;
import ch.ivyteam.ivy.security.ISession;
import ch.ivyteam.security.Password;

ISession session = ivy.session;
	try
	{
		
		session.authenticateSessionUser(in.user, new Password(in.passwort));
		out.success = true;
	}
	catch(AuthenticationException ex)
	{
		out.success = false;
		out.error = ex.getMessage();
	}
	catch (Exception e){
		ivy.log.error("Error during Logout", e);
		}' #txt
lt0 f37 type adressmutation.Data #txt
lt0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check Login</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f37 582 284 36 24 20 -2 #rect
lt0 f37 @|StepIcon #fIcon
lt0 f39 type adressmutation.Data #txt
lt0 f39 586 402 28 28 14 0 #rect
lt0 f39 @|AlternativeIcon #fIcon
lt0 f34 actionDecl 'adressmutation.Data out;
' #txt
lt0 f34 actionTable 'out=in;
' #txt
lt0 f34 actionCode 'if(in.currentEnvironment!=ivy.session.activeEnvironment){
	ivy.session.setActiveEnvironment(in.currentEnvironment);
	}
' #txt
lt0 f34 type adressmutation.Data #txt
lt0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f34 582 332 36 24 20 -2 #rect
lt0 f34 @|StepIcon #fIcon
lt0 f36 outTypes "adressmutation.Data" #txt
lt0 f36 outLinks "submit.ivp" #txt
lt0 f36 template "/ProcessPages/logout/Login.ivc" #txt
lt0 f36 type adressmutation.Data #txt
lt0 f36 skipLink skip.ivp #txt
lt0 f36 sortLink sort.ivp #txt
lt0 f36 templateWizard '#
#Wed Nov 17 08:13:37 CET 2010
' #txt
lt0 f36 pageArchivingActivated false #txt
lt0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Login</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f36 @C|.responsibility Everybody #txt
lt0 f36 582 244 36 24 20 -2 #rect
lt0 f36 @|PageIcon #fIcon
lt0 f40 expr data #txt
lt0 f40 outCond ivp=="submit.ivp" #txt
lt0 f40 600 268 600 284 #arcP
lt0 f42 expr out #txt
lt0 f42 752 332 618 256 #arcP
lt0 f42 1 752 256 #addKink
lt0 f42 1 0.2406568234004777 0 0 #arcLabel
lt0 f43 expr out #txt
lt0 f43 600 196 600 244 #arcP
lt0 f45 expr out #txt
lt0 f45 600 356 600 402 #arcP
lt0 f5 expr in #txt
lt0 f5 3 #arcStyle
lt0 f5 614 416 752 356 #arcP
lt0 f5 1 752 416 #addKink
lt0 f5 0 0.7926730842537034 0 0 #arcLabel
lt0 f2 expr out #txt
lt0 f2 600 308 600 332 #arcP
lt0 f6 type adressmutation.Data #txt
lt0 f6 template "/ProcessPages/logout/logout.ivc" #txt
lt0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>logout and close page</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f6 59 203 26 26 14 0 #rect
lt0 f6 @|EndRequestIcon #fIcon
lt0 f7 type adressmutation.Data #txt
lt0 f7 58 114 28 28 14 0 #rect
lt0 f7 @|AlternativeIcon #fIcon
lt0 f4 expr in #txt
lt0 f4 outCond 'ivy.cms.co("/Content/Shared/Configuration/system/application") == "Production"' #txt
lt0 f4 72 142 72 203 #arcP
lt0 f10 actionDecl 'adressmutation.Data out;
' #txt
lt0 f10 actionTable 'out=in;
' #txt
lt0 f10 actionCode 'in.redirectUrl = ivy.html.startref("12B1A071447AFADE/index.ivp");
ivy.session.logoutSessionUser(ivy.task.getId());
ivy.log.debug("User Logged out redirecting to:"+in.redirectUrl);' #txt
lt0 f10 type adressmutation.Data #txt
lt0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Logout Session User and Redirect to Index</name>
        <nameStyle>41,7,9
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f10 294 172 36 24 20 -2 #rect
lt0 f10 @|StepIcon #fIcon
lt0 f11 expr in #txt
lt0 f11 86 128 312 172 #arcP
lt0 f11 1 312 128 #addKink
lt0 f11 0 0.6201365603649459 0 0 #arcLabel
lt0 f9 type adressmutation.Data #txt
lt0 f9 template "redirectToRedirectUrl.jsp" #txt
lt0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>redirect</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f9 299 243 26 26 14 0 #rect
lt0 f9 @|EndRequestIcon #fIcon
lt0 f12 expr out #txt
lt0 f12 312 196 312 243 #arcP
lt0 f13 actionDecl 'adressmutation.Data out;
' #txt
lt0 f13 actionTable 'out=in;
' #txt
lt0 f13 type adressmutation.Data #txt
lt0 f13 582 468 36 24 20 -2 #rect
lt0 f13 @|StepIcon #fIcon
lt0 f14 expr in #txt
lt0 f14 outCond in.success #txt
lt0 f14 6 #arcStyle
lt0 f14 600 430 600 468 #arcP
lt0 f14 0 0.5524741765800835 0 0 #arcLabel
lt0 f0 outLink start.ivp #txt
lt0 f0 type adressmutation.Data #txt
lt0 f0 inParamDecl '<> param;' #txt
lt0 f0 actionDecl 'adressmutation.Data out;
' #txt
lt0 f0 guid 161636BC66387573 #txt
lt0 f0 requestEnabled true #txt
lt0 f0 triggerEnabled false #txt
lt0 f0 callSignature start() #txt
lt0 f0 persist false #txt
lt0 f0 startName Logout #txt
lt0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.NAM=Logout
TaskTriggered.EXROL=Everybody' #txt
lt0 f0 caseData businessCase.attach=true #txt
lt0 f0 showInStartList 1 #txt
lt0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
lt0 f0 @C|.responsibility Everybody #txt
lt0 f0 163 51 26 26 14 0 #rect
lt0 f0 @|StartRequestIcon #fIcon
lt0 f1 expr out #txt
lt0 f1 164 70 81 123 #arcP
>Proto lt0 .type adressmutation.Data #txt
>Proto lt0 .processKind NORMAL #txt
>Proto lt0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>logout on Production</swimlaneLabel>
        <swimlaneLabel>logout on Integration</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>246</swimlaneSize>
    <swimlaneSize>616</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
</elementInfo>
' #txt
>Proto lt0 0 0 32 24 18 0 #rect
>Proto lt0 @|BIcon #fIcon
lt0 f36 out f40 tail #connect
lt0 f40 head f37 mainIn #connect
lt0 f38 mainOut f42 tail #connect
lt0 f42 head f36 mainIn #connect
lt0 f35 mainOut f43 tail #connect
lt0 f43 head f36 mainIn #connect
lt0 f34 mainOut f45 tail #connect
lt0 f45 head f39 in #connect
lt0 f5 head f38 mainIn #connect
lt0 f37 mainOut f2 tail #connect
lt0 f2 head f34 mainIn #connect
lt0 f7 out f4 tail #connect
lt0 f4 head f6 mainIn #connect
lt0 f7 out f11 tail #connect
lt0 f11 head f10 mainIn #connect
lt0 f10 mainOut f12 tail #connect
lt0 f12 head f9 mainIn #connect
lt0 f39 out f14 tail #connect
lt0 f14 head f13 mainIn #connect
lt0 f39 out f5 tail #connect
lt0 f0 mainOut f1 tail #connect
lt0 f1 head f7 in #connect
