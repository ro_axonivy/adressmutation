[Ivy]
[>Created: Fri Oct 04 16:08:12 CEST 2013]
12F4DCF66A8D77DE 3.17 #module
>Proto >Proto Collection #zClass
pg0 postScanning Big #zClass
pg0 B #cInfo
pg0 #process
pg0 @TextInP .resExport .resExport #zField
pg0 @TextInP .type .type #zField
pg0 @TextInP .processKind .processKind #zField
pg0 @AnnotationInP-0n ai ai #zField
pg0 @TextInP .xml .xml #zField
pg0 @TextInP .responsibility .responsibility #zField
pg0 @CallSub f74 '' #zField
pg0 @StartRequest f75 '' #zField
pg0 @PushWFArc f76 '' #zField
pg0 @Page f0 '' #zField
pg0 @EndTask f2 '' #zField
pg0 @PushWFArc f3 '' #zField
pg0 @CallSub f5 '' #zField
pg0 @PushWFArc f4 '' #zField
pg0 @PushWFArc f1 '' #zField
>Proto pg0 pg0 postScanning #zField
pg0 f74 type ch.soreco.postscanning.ui.PostScanning #txt
pg0 f74 processCall 'Functional Processes/Login:call()' #txt
pg0 f74 doCall true #txt
pg0 f74 requestActionDecl '<> param;
' #txt
pg0 f74 responseActionDecl 'ch.soreco.postscanning.ui.PostScanning out;
' #txt
pg0 f74 responseMappingAction 'out=in;
' #txt
pg0 f74 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login?</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f74 118 108 36 24 20 -2 #rect
pg0 f74 @|CallSubIcon #fIcon
pg0 f75 outLink PostScanning.ivp #txt
pg0 f75 type ch.soreco.postscanning.ui.PostScanning #txt
pg0 f75 inParamDecl '<> param;' #txt
pg0 f75 actionDecl 'ch.soreco.postscanning.ui.PostScanning out;
' #txt
pg0 f75 guid 12F4DF2C71A758E2 #txt
pg0 f75 requestEnabled true #txt
pg0 f75 triggerEnabled false #txt
pg0 f75 callSignature PostScanning() #txt
pg0 f75 persist false #txt
pg0 f75 startName 'PostScanning - Auftragsbuch' #txt
pg0 f75 taskData '#
#Wed Jul 20 16:52:28 CEST 2011
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
pg0 f75 caseData '#
#Wed Jul 20 16:52:28 CEST 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
pg0 f75 showInStartList 0 #txt
pg0 f75 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
pg0 f75 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>PostScanning.ivp</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f75 @C|.responsibility Everybody #txt
pg0 f75 123 43 26 26 14 0 #rect
pg0 f75 @|StartRequestIcon #fIcon
pg0 f76 expr out #txt
pg0 f76 136 69 136 108 #arcP
pg0 f0 outTypes "ch.soreco.postscanning.ui.PostScanning" #txt
pg0 f0 outLinks "LinkA.ivp" #txt
pg0 f0 template "/ProcessPages/postScanning/postScanningOverview.ivc" #txt
pg0 f0 type ch.soreco.postscanning.ui.PostScanning #txt
pg0 f0 skipLink skip.ivp #txt
pg0 f0 sortLink sort.ivp #txt
pg0 f0 templateWizard '#
#Fri Oct 04 16:08:09 CEST 2013
' #txt
pg0 f0 pageArchivingActivated false #txt
pg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>OrderBook</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f0 @C|.responsibility Everybody #txt
pg0 f0 118 220 36 24 20 -2 #rect
pg0 f0 @|PageIcon #fIcon
pg0 f2 type ch.soreco.postscanning.ui.PostScanning #txt
pg0 f2 123 291 26 26 14 0 #rect
pg0 f2 @|EndIcon #fIcon
pg0 f3 expr data #txt
pg0 f3 outCond ivp=="LinkA.ivp" #txt
pg0 f3 136 244 136 291 #arcP
pg0 f5 type ch.soreco.postscanning.ui.PostScanning #txt
pg0 f5 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
pg0 f5 doCall true #txt
pg0 f5 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
pg0 f5 requestMappingAction 'param.filter=in.currentOrder;
' #txt
pg0 f5 responseActionDecl 'ch.soreco.postscanning.ui.PostScanning out;
' #txt
pg0 f5 responseMappingAction 'out=in;
out.error=result.error;
out.orderBook=result.orderBook;
out.rsOrderBook=result.rsOrderBook;
out.success=result.success;
' #txt
pg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get OrderBook</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
pg0 f5 118 156 36 24 20 -2 #rect
pg0 f5 @|CallSubIcon #fIcon
pg0 f4 expr out #txt
pg0 f4 136 132 136 156 #arcP
pg0 f1 expr out #txt
pg0 f1 136 180 136 220 #arcP
>Proto pg0 .type ch.soreco.postscanning.ui.PostScanning #txt
>Proto pg0 .processKind NORMAL #txt
>Proto pg0 0 0 32 24 18 0 #rect
>Proto pg0 @|BIcon #fIcon
pg0 f75 mainOut f76 tail #connect
pg0 f76 head f74 mainIn #connect
pg0 f0 out f3 tail #connect
pg0 f3 head f2 mainIn #connect
pg0 f74 mainOut f4 tail #connect
pg0 f4 head f5 mainIn #connect
pg0 f5 mainOut f1 tail #connect
pg0 f1 head f0 mainIn #connect
