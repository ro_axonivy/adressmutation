[Ivy]
[>Created: Wed Feb 16 13:41:12 CET 2011]
12E2E5D7F2C3D171 3.15 #module
>Proto >Proto Collection #zClass
Sn0 ScanningInformation Big #zClass
Sn0 B #cInfo
Sn0 #process
Sn0 @TextInP .resExport .resExport #zField
Sn0 @TextInP .type .type #zField
Sn0 @TextInP .processKind .processKind #zField
Sn0 @AnnotationInP-0n ai ai #zField
Sn0 @TextInP .xml .xml #zField
Sn0 @TextInP .responsibility .responsibility #zField
Sn0 @StartSub f0 '' #zField
Sn0 @EndSub f1 '' #zField
Sn0 @CallSub f2 '' #zField
Sn0 @PushWFArc f3 '' #zField
Sn0 @Page f4 '' #zField
Sn0 @PushWFArc f5 '' #zField
Sn0 @PushWFArc f6 '' #zField
>Proto Sn0 Sn0 ScanningInformation #zField
Sn0 f0 inParamDecl '<java.lang.Integer scanningId> param;' #txt
Sn0 f0 inParamTable 'out.scanningId=param.scanningId;
' #txt
Sn0 f0 outParamDecl '<> result;
' #txt
Sn0 f0 actionDecl 'ch.soreco.orderbook.bo.Scanning out;
' #txt
Sn0 f0 callSignature call(Integer) #txt
Sn0 f0 type ch.soreco.orderbook.bo.Scanning #txt
Sn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Integer)</name>
    </language>
</elementInfo>
' #txt
Sn0 f0 87 37 26 26 14 0 #rect
Sn0 f0 @|StartSubIcon #fIcon
Sn0 f1 type ch.soreco.orderbook.bo.Scanning #txt
Sn0 f1 87 287 26 26 14 0 #rect
Sn0 f1 @|EndSubIcon #fIcon
Sn0 f2 type ch.soreco.orderbook.bo.Scanning #txt
Sn0 f2 processCall 'Functional Processes/scanning/Scanning:get(Boolean,ch.soreco.orderbook.bo.Scanning)' #txt
Sn0 f2 doCall true #txt
Sn0 f2 requestActionDecl '<java.lang.Boolean loadFiles,ch.soreco.orderbook.bo.Scanning filter> param;
' #txt
Sn0 f2 requestMappingAction 'param.loadFiles=false;
param.filter.scanningId=in.scanningId;
' #txt
Sn0 f2 responseActionDecl 'ch.soreco.orderbook.bo.Scanning out;
' #txt
Sn0 f2 responseMappingAction 'out=result.Scan;
' #txt
Sn0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get scan</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Sn0 f2 82 140 36 24 20 -2 #rect
Sn0 f2 @|CallSubIcon #fIcon
Sn0 f3 expr out #txt
Sn0 f3 100 63 100 140 #arcP
Sn0 f4 outTypes "ch.soreco.orderbook.bo.Scanning" #txt
Sn0 f4 outLinks "LinkA.ivp" #txt
Sn0 f4 template "scanDetail.ivc" #txt
Sn0 f4 type ch.soreco.orderbook.bo.Scanning #txt
Sn0 f4 skipLink skip.ivp #txt
Sn0 f4 sortLink sort.ivp #txt
Sn0 f4 templateWizard '#
#Wed Feb 16 13:41:10 CET 2011
' #txt
Sn0 f4 pageArchivingActivated false #txt
Sn0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show scanning information</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
Sn0 f4 @C|.responsibility Everybody #txt
Sn0 f4 82 204 36 24 20 -2 #rect
Sn0 f4 @|PageIcon #fIcon
Sn0 f5 expr out #txt
Sn0 f5 100 164 100 204 #arcP
Sn0 f6 expr data #txt
Sn0 f6 outCond ivp=="LinkA.ivp" #txt
Sn0 f6 100 228 100 287 #arcP
>Proto Sn0 .type ch.soreco.orderbook.bo.Scanning #txt
>Proto Sn0 .processKind CALLABLE_SUB #txt
>Proto Sn0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Sn0 0 0 32 24 18 0 #rect
>Proto Sn0 @|BIcon #fIcon
Sn0 f0 mainOut f3 tail #connect
Sn0 f3 head f2 mainIn #connect
Sn0 f2 mainOut f5 tail #connect
Sn0 f5 head f4 mainIn #connect
Sn0 f4 out f6 tail #connect
Sn0 f6 head f1 mainIn #connect
