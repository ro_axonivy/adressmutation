[Ivy]
[>Created: Thu Aug 13 11:03:27 CEST 2015]
12DE760AA82FA9F0 3.17 #module
>Proto >Proto Collection #zClass
Vs0 CheckDocuments Big #zClass
Vs0 B #cInfo
Vs0 #process
Vs0 @TextInP .resExport .resExport #zField
Vs0 @TextInP .type .type #zField
Vs0 @TextInP .processKind .processKind #zField
Vs0 @AnnotationInP-0n ai ai #zField
Vs0 @TextInP .xml .xml #zField
Vs0 @TextInP .responsibility .responsibility #zField
Vs0 @StartSub f0 '' #zField
Vs0 @EndSub f1 '' #zField
Vs0 @CallSub f11 '' #zField
Vs0 @GridStep f13 '' #zField
Vs0 @CallSub f15 '' #zField
Vs0 @CallSub f17 '' #zField
Vs0 @PushWFArc f12 '' #zField
Vs0 @CallSub f26 '' #zField
Vs0 @GridStep f27 '' #zField
Vs0 @PushWFArc f10 '' #zField
Vs0 @Page f2 '' #zField
Vs0 @PushWFArc f3 '' #zField
Vs0 @PushWFArc f4 '' #zField
Vs0 @PushWFArc f5 '' #zField
Vs0 @PushWFArc f6 '' #zField
Vs0 @PushWFArc f7 '' #zField
Vs0 @CallSub f8 '' #zField
Vs0 @PushWFArc f16 '' #zField
Vs0 @PushWFArc f18 '' #zField
Vs0 @StartSub f19 '' #zField
Vs0 @InfoButton f21 '' #zField
Vs0 @PushWFArc f29 '' #zField
Vs0 @GridStep f9 '' #zField
Vs0 @PushWFArc f22 '' #zField
Vs0 @PushWFArc f23 '' #zField
Vs0 @GridStep f24 '' #zField
Vs0 @CallSub f28 '' #zField
Vs0 @CallSub f25 '' #zField
Vs0 @PushWFArc f31 '' #zField
Vs0 @Alternative f33 '' #zField
Vs0 @PushWFArc f34 '' #zField
Vs0 @PushWFArc f30 '' #zField
Vs0 @PushWFArc f35 '' #zField
Vs0 @Page f20 '' #zField
Vs0 @PushWFArc f36 '' #zField
Vs0 @PushWFArc f37 '' #zField
Vs0 @Page f38 '' #zField
Vs0 @Alternative f32 '' #zField
Vs0 @PushWFArc f40 '' #zField
Vs0 @PushWFArc f41 '' #zField
Vs0 @PushWFArc f42 '' #zField
Vs0 @GridStep f43 '' #zField
Vs0 @CallSub f14 '' #zField
Vs0 @PushWFArc f45 '' #zField
Vs0 @PushWFArc f46 '' #zField
Vs0 @PushWFArc f50 '' #zField
Vs0 @PushWFArc f47 '' #zField
>Proto Vs0 Vs0 CheckDocuments #zField
Vs0 f0 inParamDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId> param;' #txt
Vs0 f0 inParamTable 'out.currentOrder.orderId=param.orderId;
out.viewType=param.viewType;
' #txt
Vs0 f0 outParamDecl '<> result;
' #txt
Vs0 f0 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f0 callSignature call(ch.soreco.orderbook.ui.enums.ViewType,Integer) #txt
Vs0 f0 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(ViewType,orderId)</name>
        <nameStyle>22,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f0 19 59 26 26 14 0 #rect
Vs0 f0 @|StartSubIcon #fIcon
Vs0 f1 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f1 1243 691 26 26 14 0 #rect
Vs0 f1 @|EndSubIcon #fIcon
Vs0 f11 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f11 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Vs0 f11 doCall true #txt
Vs0 f11 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Vs0 f11 requestMappingAction 'param.filter.orderId=in.currentOrder.orderId;
' #txt
Vs0 f11 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f11 responseMappingAction 'out=in;
out.currentOrder.dispatchingId=result.order.dispatchingId;
out.currentOrder.orderId=result.order.dispatchingId > 0  ? null : result.order.orderId;
' #txt
Vs0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f11 278 108 36 24 20 -2 #rect
Vs0 f11 @|CallSubIcon #fIcon
Vs0 f11 -5972572|-1|-16777216 #nodeStyle
Vs0 f13 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f13 actionTable 'out=in;
' #txt
Vs0 f13 actionCode 'import ch.soreco.orderbook.enums.ArchivingState;
import ch.soreco.orderbook.bo.Scanning;

String htmlTabs = "";
int i = 0;

StringBuilder tabList = new StringBuilder("");
StringBuilder tabContent = new StringBuilder("");

tabList.append("<ul>");
for(Scanning cScan : in.scanningList){
	String link = "<%=ivy.html.ref(\"showFile.ivp\")%>&tabIndex="+i+"&Document="+cScan.docNo;	
	tabList.append("<li><a class=''tab'' href=\"#tab"+i+"\">"+cScan.docType+"-"+cScan.docNo+"</a></li>");

	//Prepare IFrame 
	String iFrame = "<iframe id=\"scanFileFrame"+i+"\" src=\""+link+"\" width=\"100%\" height=\"800\"></iframe>";		
	iFrame = "<div class=''iframeAppender'' frameName=''frame"+i+"'' frameSource=\""+link+"\"></div>";
	//init Tab Content
	tabContent.append("<div id=\"tab"+i+"\">");	
	//May we need to Append Additional Scanning Infos to Tab Content	
	StringBuilder scanInfo = new StringBuilder("");
	if(!in.uiType.equalsIgnoreCase("SCANPLAIN")){
		String checked = "checked=checked";
		Boolean isArchived =cScan.archivingState.equalsIgnoreCase(ArchivingState.IMTF_ARCHIVED.toString()); 
		if(isArchived){
			checked = "";	
		}
		tabContent.append("<table width=\"100%\" border=\"0\">");
		tabContent.append("<tr><td  width=\"40%\"  valign=\"top\">");
		tabContent.append("<INPUT TYPE=\"CHECKBOX\" NAME=\"currentOrder$$scanning$$archivingState_"+i+"\" VALUE=\""+isArchived+"\" "+checked+" >Dokument bereits archiviert<br><br>");
		tabContent.append("<div id=\"scanInfo"+i+"\" ></div>");
		tabContent.append("</td>");
		tabContent.append("<td width=\"60%\">");
		//append iframe
		tabContent.append(iFrame);
		tabContent.append("</td></tr>");
		tabContent.append("</table>");
	}
	else {
		tabContent.append(iFrame);		
	}
	//close tab content
	tabContent.append("</div><!--TabContent #tab"+i+"-->");		
	i++;//iterator value
}
tabList.append("</ul>");
out.html = tabList.toString()+tabContent.toString();
out.tabCount = i;

' #txt
Vs0 f13 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Tabs</name>
        <nameStyle>10
</nameStyle>
        <desc>&lt;script type=&quot;text/javascript&quot;&gt;
$(document).ready(function(){
$(&quot;.iframeAppender&quot;).each(function(){
	var href = $(this).attr(&quot;frameSource&quot;);
	var name = $(this).attr(&quot;frameName&quot;);
	$(this).height($(window).height()-100);
	$(this).append(&quot;&lt;iframe name=''&quot;+name+&quot;'' src=''redirect.jsp?uri=&quot;+href+&quot;'' frameborder=''1'' width=''100%'' height=''100%''&gt;&lt;/iframe&gt;&quot;);

	$(this).bind(&quot;reload&quot;, function(){
		//prepend BrowserCaching
		href = href+&quot;&amp;ajaxcache=&quot;+(new Date()).getTime();
		//$(this).prepend(&quot;&lt;p&gt;Document loaded at &quot;+(new Date()).getTime()+&quot; href: redirect.jsp?uri=&quot;+href+&quot;&lt;/p&gt;&quot;);
		$(this).find(&quot;iframe&quot;).attr(&quot;src&quot;, href);
		});
	});
	//as the browser does not load always iframes properly
	//check if content is loaded
	$(&quot;.tab&quot;).click(function(){
		var target = $(this).attr(&quot;href&quot;);
		var frame = $(target).find(&quot;.iframeAppender&quot;);
		frame.trigger(&quot;reload&quot;);
	});	
});
$(window).resize(function() {
  $(''.iframeAppender'').height($(window).height()-100);
});
&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
$(document).ready(function(){
	$( &quot;#docTabs&quot; ).tabs();

	if(&quot;&lt;%=ivy.html.get(&quot;in.viewType&quot;)%&gt;&quot;== ''READ''){
		//$(''#scanComment'').attr(''disabled'',''disabled'');
		$('':input'').each(function(i){
			$(this).attr(''disabled'',''disabled'');
		});
		$(''#close'').attr(''disabled'','''');
	}
	
	$(''#save'').click(function(){
		//$(''#formDocCheck'').submit()
		$.post(''&lt;%=ivy.html.ref(&quot;save.ivp&quot;)%&gt;'',$(''#formDocCheck'').serialize());
	});
	
	$(''#close'').click(function(){
		$(''#iFrameDialog'', panel.document.body).dialog(''close'');
	});
	
	if(''&lt;%=ivy.html.get(&quot;in.uiType&quot;)%&gt;'' == &quot;SCANPLAIN&quot;){
		$(''#scanningCommentDiv'').css(''display'',''none'');
		$(''#scanningCommentDiv'').css(''height'',''0px'');
	} else {
		$(''#scanningCommentDiv'').css(''display'',''block'');
	}	
});
&lt;/script&gt;

&lt;!--
&lt;script type=&quot;text/javascript&quot;&gt;
$(document).ready(function(){	
	//as the browser does not load always iframes properly
	//check if content is loaded
	$(&quot;.tab&quot;).click(function(){
		var target = $(this).attr(&quot;href&quot;);
		var frame = $(target).find(&quot;iframe&quot;);
		var href = frame.attr(&quot;src&quot;);
		href = href+&quot;&amp;ajaxcache=&quot;+(new Date()).getTime();
		frame.attr(&quot;src&quot;, href);		
		//frame.load(href);		
		$(target).append(&quot;&lt;p&gt;Document loaded at &quot;+(new Date()).getTime()+&quot; href: &quot;+href+&quot;&lt;/p&gt;&quot;);	
	});

	//do load the first tab, which also does not work properly!
	//$(&quot;.tab&quot;).first().click();
	
	var frame = $(''#docTabs'').find(&quot;iframe&quot;);
	frame.load(href);		
});
&lt;/script&gt;
--&gt;</desc>
    </language>
</elementInfo>
' #txt
Vs0 f13 558 364 36 24 20 -2 #rect
Vs0 f13 @|StepIcon #fIcon
Vs0 f15 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f15 processCall 'Functional Processes/orderbook/Order:set(ch.soreco.orderbook.bo.Order)' #txt
Vs0 f15 doCall true #txt
Vs0 f15 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Vs0 f15 requestMappingAction 'param.order=in.currentOrder;
param.order.scanning.archivingState=ch.soreco.orderbook.enums.ArchivingState.IN_REPAIR.toString();
' #txt
Vs0 f15 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f15 responseMappingAction 'out=in;
' #txt
Vs0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f15 958 516 36 24 20 -2 #rect
Vs0 f15 @|CallSubIcon #fIcon
Vs0 f15 -985168|-1|-16777216 #nodeStyle
Vs0 f17 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f17 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Vs0 f17 doCall true #txt
Vs0 f17 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Vs0 f17 requestMappingAction 'param.filter.fileId=in.scanningList.get(in.tabIndex).scanFileId;
' #txt
Vs0 f17 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f17 responseMappingAction 'out=in;
' #txt
Vs0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show(File)</name>
        <nameStyle>10,5,7
</nameStyle>
        <desc>in.currentOrder.scanFileId
in.scanningList.get(0).scanFileId

in.currentOrder.scanFileId</desc>
    </language>
</elementInfo>
' #txt
Vs0 f17 718 580 36 24 20 -2 #rect
Vs0 f17 @|CallSubIcon #fIcon
Vs0 f12 expr out #txt
Vs0 f12 45 72 296 108 #arcP
Vs0 f12 1 296 72 #addKink
Vs0 f12 1 0.27696080106203075 0 0 #arcLabel
Vs0 f26 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f26 processCall 'Start Processes/Archive/ScanningInformation:call(Integer)' #txt
Vs0 f26 doCall true #txt
Vs0 f26 requestActionDecl '<java.lang.Integer scanningId> param;
' #txt
Vs0 f26 requestMappingAction 'param.scanningId=in.scanningList.get(in.tabIndex).scanningId;
' #txt
Vs0 f26 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f26 responseMappingAction 'out=in;
out.index=null;
' #txt
Vs0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ScanInfo page</name>
        <nameStyle>13,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f26 718 516 36 24 -4 -36 #rect
Vs0 f26 @|CallSubIcon #fIcon
Vs0 f27 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f27 actionTable 'out=in;
' #txt
Vs0 f27 actionCode 'import ch.soreco.orderbook.enums.ArchivingState;
import ch.soreco.orderbook.bo.Scanning;
import java.util.Iterator;

in.currentOrder.comment = ivy.request.getFirstParameter("currentOrder$$comment");

Iterator it = ivy.request.getParameterNames();
while(it.hasNext()){
	String cElem = it.next().toString();
	
	if(cElem.equalsIgnoreCase("currentOrder$$comment")){
		continue;
	}
	
	String scanIndex = cElem.split("_").get(1);
	
	Scanning cScan = in.currentOrder.scannings.get(Integer.parseInt(scanIndex)) as Scanning;
	
	//in.panelCaption += ivy.request.getFirstParameter(cElem) + " - ";
	
	if(ivy.request.getFirstParameter(cElem).equalsIgnoreCase("true")){
			cScan.archivingState = ArchivingState.IMTF_ARCHIVED.toString();	
	} else {
			cScan.archivingState = ArchivingState.IN_REPAIR.toString();	
	}

	in.currentOrder.scannings.set(Integer.parseInt(scanIndex),cScan);
	//in.panelCaption += it.next().toString() + " - ";
}' #txt
Vs0 f27 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map request data</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f27 958 452 36 24 20 -2 #rect
Vs0 f27 @|StepIcon #fIcon
Vs0 f10 expr out #txt
Vs0 f10 976 476 976 516 #arcP
Vs0 f2 outTypes "ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair" #txt
Vs0 f2 outLinks "end.ivp","loadScanningInfo.ivp","showFile.ivp","save.ivp","LinkA.ivp","showAttachment.ivp" #txt
Vs0 f2 template "/ProcessPages/CheckDocuments/overview.ivc" #txt
Vs0 f2 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f2 skipLink skip.ivp #txt
Vs0 f2 sortLink sort.ivp #txt
Vs0 f2 templateWizard '#
#Wed Jul 29 02:39:01 CEST 2015
' #txt
Vs0 f2 pageArchivingActivated false #txt
Vs0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>DocumentsOverview</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f2 @C|.responsibility Everybody #txt
Vs0 f2 478 452 36 24 20 -2 #rect
Vs0 f2 @|PageIcon #fIcon
Vs0 f3 expr out #txt
Vs0 f3 576 388 496 452 #arcP
Vs0 f4 expr data #txt
Vs0 f4 outCond ivp=="end.ivp" #txt
Vs0 f4 496 476 1243 704 #arcP
Vs0 f4 1 496 704 #addKink
Vs0 f4 1 0.28176630638390776 0 0 #arcLabel
Vs0 f5 expr data #txt
Vs0 f5 outCond ivp=="loadScanningInfo.ivp" #txt
Vs0 f5 496 476 718 528 #arcP
Vs0 f5 1 544 528 #addKink
Vs0 f5 0 0.9210779226720268 0 0 #arcLabel
Vs0 f6 expr data #txt
Vs0 f6 outCond ivp=="showFile.ivp" #txt
Vs0 f6 496 476 718 592 #arcP
Vs0 f6 1 544 592 #addKink
Vs0 f6 1 0.012984547202252736 0 0 #arcLabel
Vs0 f7 expr data #txt
Vs0 f7 outCond ivp=="save.ivp" #txt
Vs0 f7 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>save</name>
        <nameStyle>4
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f7 514 464 958 464 #arcP
Vs0 f7 0 0.9211711711711712 0 -10 #arcLabel
Vs0 f8 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f8 processCall 'Functional Processes/scanning/Scanning:setList(Integer,List<ch.soreco.orderbook.bo.Scanning>)' #txt
Vs0 f8 doCall true #txt
Vs0 f8 requestActionDecl '<java.lang.Integer orderId,List<ch.soreco.orderbook.bo.Scanning> scannings> param;
' #txt
Vs0 f8 requestMappingAction 'param.orderId=in.currentOrder.orderId;
param.scannings=in.currentOrder.scannings;
' #txt
Vs0 f8 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f8 responseMappingAction 'out=in;
' #txt
Vs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setList(Integer,List&lt;Scanning&gt;)</name>
    </language>
</elementInfo>
' #txt
Vs0 f8 958 580 36 24 20 -2 #rect
Vs0 f8 @|CallSubIcon #fIcon
Vs0 f8 -985168|-1|-16777216 #nodeStyle
Vs0 f16 expr out #txt
Vs0 f16 976 540 976 580 #arcP
Vs0 f16 0 0.7980077442940796 0 0 #arcLabel
Vs0 f18 expr out #txt
Vs0 f18 976 604 1243 704 #arcP
Vs0 f18 1 976 704 #addKink
Vs0 f18 0 0.9362301120094716 0 0 #arcLabel
Vs0 f19 inParamDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId,java.lang.String uiType> param;' #txt
Vs0 f19 inParamTable 'out.currentOrder.orderId=param.orderId;
out.uiType=param.uiType;
out.viewType=param.viewType;
' #txt
Vs0 f19 outParamDecl '<> result;
' #txt
Vs0 f19 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f19 callSignature call(ch.soreco.orderbook.ui.enums.ViewType,Integer,String) #txt
Vs0 f19 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(ViewType,orderId,uiType)
</name>
        <nameStyle>30,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f19 19 107 26 26 14 0 #rect
Vs0 f19 @|StartSubIcon #fIcon
Vs0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>process to show scannings of order.
if order was manual dispatched, 
show scanning of original order

UI type:
SCANINFO - scanning info
   full info screen
SCANPLAIN - scannings plain
   only documents</name>
        <nameStyle>201
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f21 4 168 25 129 16 -68 #rect
Vs0 f21 @|IBIcon #fIcon
Vs0 f21 -1|-1|-16777216 #nodeStyle
Vs0 f29 expr out #txt
Vs0 f29 45 120 278 120 #arcP
Vs0 f29 0 0.3558372707902325 0 0 #arcLabel
Vs0 f9 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f9 actionTable 'out=in;
' #txt
Vs0 f9 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f9 558 420 36 24 20 -2 #rect
Vs0 f9 @|StepIcon #fIcon
Vs0 f22 expr data #txt
Vs0 f22 outCond ivp=="LinkA.ivp" #txt
Vs0 f22 514 464 558 432 #arcP
Vs0 f23 expr out #txt
Vs0 f23 576 420 576 388 #arcP
Vs0 f24 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f24 actionTable 'out=in;
' #txt
Vs0 f24 actionCode 'import ch.soreco.orderbook.enums.ArchivingState;
import ch.soreco.orderbook.bo.Scanning;

String htmlTabs = "";
int i = 0;
int o = 0;

htmlTabs += "<ul>";
for(Scanning cScan : in.scanningList){

	htmlTabs += "<li><a href=\"#tab"+i+"\">"+cScan.docType+"-"+cScan.pageNoStart+"</a></li>";
	i++;
}
htmlTabs += "</ul>";

while(o < i){
		String checked = "true\" checked";
		if(!in.scanningList.get(o).archivingState.equalsIgnoreCase(ArchivingState.IMTF_ARCHIVED.toString())){
			checked = "false\"";
		}
	if(in.uiType.equalsIgnoreCase("SCANINFO")){	
			htmlTabs += "<div id=\"tab"+o+"\">";
			htmlTabs += "<table width=\"100%\" border=\"0\">";
			htmlTabs += "<tr><td  width=\"40%\"  valign=\"top\">";
			htmlTabs += "<INPUT TYPE=\"CHECKBOX\" NAME=\"currentOrder$$scanning$$archivingState_"+o+"\" VALUE=\""+checked+"\" >Dokument bereits archiviert<br><br>";
			htmlTabs += "<div id=\"scanInfo"+o+"\" ></div>";
			htmlTabs += "</td>";
			htmlTabs += "<td width=\"60%\">";
			htmlTabs += "<div id=\"scanFile\"><iframe id=\"scanFileFrame"+o+"\" src=\"\" style=\"width:100%; height:100%;\"></iframe></div>";
			htmlTabs += "</td>";
			htmlTabs += "</tr>";
			htmlTabs += "</table>";
			htmlTabs += "</div>";
		} else {
			htmlTabs += "<div id=\"tab"+o+"\" style=\"display: none; height: auto\">";
			//htmlTabs += "<table width=\"100%\" height=\"100%\" border=\"0\">";
			//htmlTabs += "<tr height=\"100%\">";
			//htmlTabs += "<td width=\"100%\" height=\"100%\">";
			//htmlTabs += "<div id=\"scanFile"+o+"\" height=\"100%\"><iframe id=\"scanFileFrame"+o+"\" src=\"\" width=\"100%\" height=\"800\"></iframe></div>";
			//htmlTabs += "</td>";
			//htmlTabs += "</tr>";
			//htmlTabs += "</table>";
			htmlTabs += "</div>";	
		}
		o++;
}
htmlTabs += "<div id=\"iframeDiv\" height=\"100%\">";
htmlTabs += "<div id=\"scanFile\" height=\"100%\"><iframe id=\"tFrame\" name=\"tFrameName\" src=\"\" width=\"100%\" height=\"100%\"></iframe></div>";
//htmlTabs += "<div id=\"scanFile\" height=\"100%\"><embed  id=\"tFrame\" src=\"\" width=\"100%\" height=\"800\"/> </div>";
htmlTabs += "</div>";
out.html = htmlTabs;
out.tabCount = i;
' #txt
Vs0 f24 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build tabs OLD</name>
        <nameStyle>14
</nameStyle>
        <desc>reverted to old version because new don''t run on bzp server</desc>
    </language>
</elementInfo>
' #txt
Vs0 f24 574 268 36 24 20 -2 #rect
Vs0 f24 @|StepIcon #fIcon
Vs0 f28 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f28 processCall 'Functional Processes/orderbook/Dispatching:get(ch.soreco.orderbook.bo.Dispatching)' #txt
Vs0 f28 doCall true #txt
Vs0 f28 requestActionDecl '<ch.soreco.orderbook.bo.Dispatching filter> param;
' #txt
Vs0 f28 requestMappingAction 'param.filter.id=in.currentOrder.dispatchingId;
' #txt
Vs0 f28 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f28 responseMappingAction 'out=in;
out.currentOrder.orderId=result.dispatchingData.orderId;
' #txt
Vs0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Dispatching)</name>
    </language>
</elementInfo>
' #txt
Vs0 f28 278 204 36 24 20 -2 #rect
Vs0 f28 @|CallSubIcon #fIcon
Vs0 f25 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f25 processCall 'Functional Processes/scanning/Scanning:getScanningList(ch.soreco.orderbook.bo.Scanning)' #txt
Vs0 f25 doCall true #txt
Vs0 f25 requestActionDecl '<ch.soreco.orderbook.bo.Scanning Scanning> param;
' #txt
Vs0 f25 requestMappingAction 'param.Scanning.orderId=in.currentOrder.orderId;
' #txt
Vs0 f25 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f25 responseMappingAction 'out=in;
out.scanningList=result.Scannings;
' #txt
Vs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getScanningList(Scanning)</name>
        <nameStyle>25,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f25 278 276 36 24 20 -2 #rect
Vs0 f25 @|CallSubIcon #fIcon
Vs0 f31 expr out #txt
Vs0 f31 296 228 296 276 #arcP
Vs0 f33 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual dispatched 
order?</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f33 282 146 28 28 14 0 #rect
Vs0 f33 @|AlternativeIcon #fIcon
Vs0 f34 expr out #txt
Vs0 f34 296 132 296 146 #arcP
Vs0 f34 0 0.9622348327121627 0 0 #arcLabel
Vs0 f30 expr in #txt
Vs0 f30 outCond 'in.currentOrder.dispatchingId > 0' #txt
Vs0 f30 296 174 296 204 #arcP
Vs0 f35 expr in #txt
Vs0 f35 282 160 278 288 #arcP
Vs0 f35 1 248 160 #addKink
Vs0 f35 2 248 288 #addKink
Vs0 f35 1 0.3840820426198463 0 0 #arcLabel
Vs0 f20 template "dataPersisted.jsp" #txt
Vs0 f20 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f20 skipLink skip.ivp #txt
Vs0 f20 sortLink sort.ivp #txt
Vs0 f20 templateWizard '#
#Mon Jun 20 16:44:41 CEST 2011
' #txt
Vs0 f20 pageArchivingActivated false #txt
Vs0 f20 @C|.responsibility Everybody #txt
Vs0 f20 846 580 36 24 20 -2 #rect
Vs0 f20 @|PageIcon #fIcon
Vs0 f36 expr out #txt
Vs0 f36 754 592 846 592 #arcP
Vs0 f37 expr out #txt
Vs0 f37 754 528 864 580 #arcP
Vs0 f37 1 824 528 #addKink
Vs0 f37 1 0.14246072110105726 0 0 #arcLabel
Vs0 f38 outTypes "ch.soreco.archive.ui.OrderRepair" #txt
Vs0 f38 outLinks "LinkA.ivp" #txt
Vs0 f38 template "/ProcessPages/CheckDocuments/noDocument.ivc" #txt
Vs0 f38 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f38 skipLink skip.ivp #txt
Vs0 f38 sortLink sort.ivp #txt
Vs0 f38 templateWizard '#
#Wed Apr 10 09:32:28 CEST 2013
' #txt
Vs0 f38 pageArchivingActivated false #txt
Vs0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show error</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f38 @C|.responsibility Everybody #txt
Vs0 f38 342 436 36 24 20 -2 #rect
Vs0 f38 @|PageIcon #fIcon
Vs0 f32 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has documents?</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f32 346 322 28 28 14 0 #rect
Vs0 f32 @|AlternativeIcon #fIcon
Vs0 f40 expr out #txt
Vs0 f40 296 300 346 336 #arcP
Vs0 f40 1 296 336 #addKink
Vs0 f40 1 0.25132607916808825 0 0 #arcLabel
Vs0 f41 expr in #txt
Vs0 f41 360 350 360 436 #arcP
Vs0 f42 expr data #txt
Vs0 f42 outCond ivp=="LinkA.ivp" #txt
Vs0 f42 360 460 1256 717 #arcP
Vs0 f42 1 360 760 #addKink
Vs0 f42 2 1256 760 #addKink
Vs0 f42 1 0.3528063380844211 0 0 #arcLabel
Vs0 f43 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f43 actionTable 'out=in;
' #txt
Vs0 f43 actionCode 'import ch.soreco.orderbook.service.ScanningUtils;
out.html = ScanningUtils.buildScanningTabs(in.scanningList,in.uiType);
ivy.log.trace("Scanning Tabs HTML Markup is:\n"+out.html);
out.tabCount = in.scanningList.size();' #txt
Vs0 f43 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Tabs</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f43 478 324 36 24 20 -2 #rect
Vs0 f43 @|StepIcon #fIcon
Vs0 f14 type ch.soreco.archive.ui.OrderRepair #txt
Vs0 f14 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Vs0 f14 doCall true #txt
Vs0 f14 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Vs0 f14 requestMappingAction 'param.filter.fileId=in.attachmentFileId;
' #txt
Vs0 f14 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Vs0 f14 responseMappingAction 'out=in;
' #txt
Vs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show(File)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Vs0 f14 718 644 36 24 20 -2 #rect
Vs0 f14 @|CallSubIcon #fIcon
Vs0 f45 expr out #txt
Vs0 f45 754 656 864 604 #arcP
Vs0 f45 1 832 656 #addKink
Vs0 f45 1 0.1407045797780838 0 0 #arcLabel
Vs0 f46 expr data #txt
Vs0 f46 outCond ivp=="showAttachment.ivp" #txt
Vs0 f46 496 476 718 656 #arcP
Vs0 f46 1 544 656 #addKink
Vs0 f46 1 0.040996992729066414 0 0 #arcLabel
Vs0 f50 expr in #txt
Vs0 f50 outCond 'in.scanningList.size() > 0' #txt
Vs0 f50 374 336 478 336 #arcP
Vs0 f50 0 0.5000000000000002 0 0 #arcLabel
Vs0 f47 expr out #txt
Vs0 f47 496 348 496 452 #arcP
>Proto Vs0 .type ch.soreco.archive.ui.OrderRepair #txt
>Proto Vs0 .processKind CALLABLE_SUB #txt
>Proto Vs0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>start</swimlaneLabel>
        <swimlaneLabel>get data</swimlaneLabel>
        <swimlaneLabel>panel</swimlaneLabel>
        <swimlaneLabel>subPanels</swimlaneLabel>
        <swimlaneLabel>set data</swimlaneLabel>
        <swimlaneLabel>end</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>234</swimlaneSize>
    <swimlaneSize>230</swimlaneSize>
    <swimlaneSize>198</swimlaneSize>
    <swimlaneSize>245</swimlaneSize>
    <swimlaneSize>294</swimlaneSize>
    <swimlaneSize>150</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
</elementInfo>
' #txt
>Proto Vs0 0 0 32 24 18 0 #rect
>Proto Vs0 @|BIcon #fIcon
Vs0 f0 mainOut f12 tail #connect
Vs0 f12 head f11 mainIn #connect
Vs0 f27 mainOut f10 tail #connect
Vs0 f10 head f15 mainIn #connect
Vs0 f13 mainOut f3 tail #connect
Vs0 f3 head f2 mainIn #connect
Vs0 f2 out f4 tail #connect
Vs0 f4 head f1 mainIn #connect
Vs0 f2 out f5 tail #connect
Vs0 f5 head f26 mainIn #connect
Vs0 f2 out f6 tail #connect
Vs0 f6 head f17 mainIn #connect
Vs0 f2 out f7 tail #connect
Vs0 f7 head f27 mainIn #connect
Vs0 f15 mainOut f16 tail #connect
Vs0 f16 head f8 mainIn #connect
Vs0 f8 mainOut f18 tail #connect
Vs0 f18 head f1 mainIn #connect
Vs0 f19 mainOut f29 tail #connect
Vs0 f29 head f11 mainIn #connect
Vs0 f2 out f22 tail #connect
Vs0 f22 head f9 mainIn #connect
Vs0 f9 mainOut f23 tail #connect
Vs0 f23 head f13 mainIn #connect
Vs0 f28 mainOut f31 tail #connect
Vs0 f31 head f25 mainIn #connect
Vs0 f11 mainOut f34 tail #connect
Vs0 f34 head f33 in #connect
Vs0 f33 out f30 tail #connect
Vs0 f30 head f28 mainIn #connect
Vs0 f33 out f35 tail #connect
Vs0 f35 head f25 mainIn #connect
Vs0 f17 mainOut f36 tail #connect
Vs0 f36 head f20 mainIn #connect
Vs0 f26 mainOut f37 tail #connect
Vs0 f37 head f20 mainIn #connect
Vs0 f25 mainOut f40 tail #connect
Vs0 f40 head f32 in #connect
Vs0 f41 head f38 mainIn #connect
Vs0 f38 out f42 tail #connect
Vs0 f42 head f1 mainIn #connect
Vs0 f14 mainOut f45 tail #connect
Vs0 f45 head f20 mainIn #connect
Vs0 f2 out f46 tail #connect
Vs0 f46 head f14 mainIn #connect
Vs0 f32 out f50 tail #connect
Vs0 f50 head f43 mainIn #connect
Vs0 f32 out f41 tail #connect
Vs0 f43 mainOut f47 tail #connect
Vs0 f47 head f2 mainIn #connect
