[Ivy]
[>Created: Wed Jul 20 16:55:29 CEST 2011]
12DDC6BF5BC9880F 3.15 #module
>Proto >Proto Collection #zClass
Rr0 Repair Big #zClass
Rr0 B #cInfo
Rr0 #process
Rr0 @TextInP .resExport .resExport #zField
Rr0 @TextInP .type .type #zField
Rr0 @TextInP .processKind .processKind #zField
Rr0 @AnnotationInP-0n ai ai #zField
Rr0 @TextInP .xml .xml #zField
Rr0 @TextInP .responsibility .responsibility #zField
Rr0 @StartRequest f0 '' #zField
Rr0 @CallSub f36 '' #zField
Rr0 @PushWFArc f1 '' #zField
Rr0 @CallSub f3 '' #zField
Rr0 @CallSub f4 '' #zField
Rr0 @PushWFArc f5 '' #zField
Rr0 @PushWFArc f6 '' #zField
Rr0 @Page f7 '' #zField
Rr0 @PushWFArc f2 '' #zField
Rr0 @PushWFArc f8 '' #zField
Rr0 @CallSub f16 '' #zField
Rr0 @CallSub f14 '' #zField
Rr0 @PushWFArc f23 '' #zField
Rr0 @GridStep f10 '' #zField
Rr0 @PushWFArc f26 '' #zField
Rr0 @PushWFArc f24 '' #zField
Rr0 @PushWFArc f22 '' #zField
Rr0 @GridStep f18 '' #zField
Rr0 @GridStep f21 '' #zField
Rr0 @PushWFArc f27 '' #zField
Rr0 @PushWFArc f20 '' #zField
Rr0 @GridStep f29 '' #zField
Rr0 @PushWFArc f30 '' #zField
Rr0 @Page f31 '' #zField
Rr0 @PushWFArc f32 '' #zField
Rr0 @CallSub f33 '' #zField
Rr0 @PushWFArc f15 '' #zField
Rr0 @PushWFArc f34 '' #zField
Rr0 @PushWFArc f28 '' #zField
Rr0 @PushWFArc f19 '' #zField
Rr0 @PushWFArc f9 '' #zField
Rr0 @GridStep f11 '' #zField
Rr0 @PushWFArc f12 '' #zField
Rr0 @PushWFArc f13 '' #zField
>Proto Rr0 Rr0 Repair #zField
Rr0 f0 outLink start.ivp #txt
Rr0 f0 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f0 inParamDecl '<> param;' #txt
Rr0 f0 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f0 guid 12DDC6C0EE291B77 #txt
Rr0 f0 requestEnabled true #txt
Rr0 f0 triggerEnabled false #txt
Rr0 f0 callSignature start() #txt
Rr0 f0 persist false #txt
Rr0 f0 startName 'Auftragsbuch - Repair' #txt
Rr0 f0 taskData '#
#Wed Jul 20 16:54:44 CEST 2011
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Rr0 f0 caseData '#
#Wed Jul 20 16:54:44 CEST 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Rr0 f0 showInStartList 0 #txt
Rr0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Rr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>repair.ivp</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f0 @C|.responsibility Everybody #txt
Rr0 f0 51 75 26 26 14 0 #rect
Rr0 f0 @|StartRequestIcon #fIcon
Rr0 f36 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f36 processCall 'Functional Processes/Login:call()' #txt
Rr0 f36 doCall true #txt
Rr0 f36 requestActionDecl '<> param;
' #txt
Rr0 f36 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f36 responseMappingAction 'out=in;
' #txt
Rr0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login?</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f36 46 132 36 24 20 -2 #rect
Rr0 f36 @|CallSubIcon #fIcon
Rr0 f1 expr out #txt
Rr0 f1 64 101 64 132 #arcP
Rr0 f3 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f3 processCall 'Functional Processes/Archiving/getOrdersViaSubstateOfScanning:call(String)' #txt
Rr0 f3 doCall true #txt
Rr0 f3 requestActionDecl '<java.lang.String archiveState> param;
' #txt
Rr0 f3 requestMappingAction 'param.archiveState=ch.soreco.orderbook.enums.ArchivingState.PREPARED.toString();
' #txt
Rr0 f3 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f3 responseMappingAction 'out=in;
out.preparedOrders=result.orderRecordset;
' #txt
Rr0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get orders
PREPARED</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f3 222 132 36 24 20 -2 #rect
Rr0 f3 @|CallSubIcon #fIcon
Rr0 f4 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f4 processCall 'Functional Processes/Archiving/getOrdersViaSubstateOfScanning:call(String)' #txt
Rr0 f4 doCall true #txt
Rr0 f4 requestActionDecl '<java.lang.String archiveState> param;
' #txt
Rr0 f4 requestMappingAction 'param.archiveState=ch.soreco.orderbook.enums.ArchivingState.IN_REPAIR.toString();
' #txt
Rr0 f4 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f4 responseMappingAction 'out=in;
out.inRepairOrders=result.orderRecordset;
' #txt
Rr0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get orders
IN_REPAIR</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f4 222 204 36 24 20 -2 #rect
Rr0 f4 @|CallSubIcon #fIcon
Rr0 f5 expr out #txt
Rr0 f5 82 144 222 144 #arcP
Rr0 f5 0 0.3008405107078394 0 0 #arcLabel
Rr0 f6 expr out #txt
Rr0 f6 240 156 240 204 #arcP
Rr0 f7 outTypes "ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair" #txt
Rr0 f7 outLinks "repairOrder.ivp","readOrderPrepared.ivp","readOrderInRepair.ivp","refresh.ivp","setScanningStateRepair.ivp","filter.ivp" #txt
Rr0 f7 template "/ProcessPages/Repair/OrderRepairOverview.ivc" #txt
Rr0 f7 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f7 skipLink skip.ivp #txt
Rr0 f7 sortLink sort.ivp #txt
Rr0 f7 templateWizard '#
#Mon Feb 28 16:18:42 CET 2011
' #txt
Rr0 f7 pageArchivingActivated false #txt
Rr0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>RepairOrder
Overview</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f7 @C|.responsibility Everybody #txt
Rr0 f7 390 260 36 24 20 -2 #rect
Rr0 f7 @|PageIcon #fIcon
Rr0 f2 expr data #txt
Rr0 f2 outCond ivp=="refresh.ivp" #txt
Rr0 f2 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>refresh</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f2 408 260 258 144 #arcP
Rr0 f2 1 408 144 #addKink
Rr0 f2 0 1.0 -19 -11 #arcLabel
Rr0 f8 expr out #txt
Rr0 f8 240 228 390 272 #arcP
Rr0 f8 1 240 272 #addKink
Rr0 f8 1 0.2304025474650096 0 0 #arcLabel
Rr0 f16 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f16 processCall 'Start Processes/Archive/CheckDocuments:call(ch.soreco.orderbook.ui.enums.ViewType,Integer)' #txt
Rr0 f16 doCall true #txt
Rr0 f16 requestActionDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId> param;
' #txt
Rr0 f16 requestMappingAction 'param.viewType=in.viewType;
param.orderId=in.currentOrder.orderId;
' #txt
Rr0 f16 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f16 responseMappingAction 'out=in;
' #txt
Rr0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check documents</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f16 822 332 36 24 20 -2 #rect
Rr0 f16 @|CallSubIcon #fIcon
Rr0 f14 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f14 processCall 'Functional Processes/scanning/Scanning:set(ch.soreco.orderbook.bo.Scanning)' #txt
Rr0 f14 doCall true #txt
Rr0 f14 requestActionDecl '<ch.soreco.orderbook.bo.Scanning scan> param;
' #txt
Rr0 f14 requestMappingAction 'param.scan=(in.preparedOrders.getField(in.index,"scannings") as List<ch.soreco.orderbook.bo.Scanning>).get(0);
param.scan.archivingState=ch.soreco.orderbook.enums.ArchivingState.IN_REPAIR.toString();
' #txt
Rr0 f14 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f14 responseMappingAction 'out=in;
' #txt
Rr0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Scanning)</name>
        <nameStyle>13,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f14 822 260 36 24 20 -2 #rect
Rr0 f14 @|CallSubIcon #fIcon
Rr0 f23 expr data #txt
Rr0 f23 outCond ivp=="setScanningStateRepair.ivp" #txt
Rr0 f23 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set scanningState
IN_REPAIR</name>
        <nameStyle>27
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f23 426 272 822 272 #arcP
Rr0 f23 0 0.8257575757575758 0 15 #arcLabel
Rr0 f10 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f10 actionTable 'out=in;
' #txt
Rr0 f10 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>loop back</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f10 950 84 36 24 20 -2 #rect
Rr0 f10 @|StepIcon #fIcon
Rr0 f26 expr out #txt
Rr0 f26 858 272 968 108 #arcP
Rr0 f26 1 968 272 #addKink
Rr0 f26 0 0.40259713141910686 0 0 #arcLabel
Rr0 f24 expr out #txt
Rr0 f24 858 344 968 108 #arcP
Rr0 f24 1 968 344 #addKink
Rr0 f24 0 0.44498036129268426 0 0 #arcLabel
Rr0 f22 expr out #txt
Rr0 f22 950 96 252 132 #arcP
Rr0 f22 1 288 96 #addKink
Rr0 f22 0 0.5436934642824407 0 0 #arcLabel
Rr0 f18 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f18 actionTable 'out=in;
out.currentOrder.orderId=in.inRepairOrders.getField(in.index,"orderId").toString().toNumber();
out.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
' #txt
Rr0 f18 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>READ Order
IN_REPAIR</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f18 542 428 36 24 20 -2 #rect
Rr0 f18 @|StepIcon #fIcon
Rr0 f21 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f21 actionTable 'out=in;
out.currentOrder.orderId=in.preparedOrders.getField(in.index,"orderId").toString().toNumber();
out.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
' #txt
Rr0 f21 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>READ Order
PREPARED</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f21 542 372 36 24 20 -2 #rect
Rr0 f21 @|StepIcon #fIcon
Rr0 f27 expr data #txt
Rr0 f27 outCond ivp=="readOrderPrepared.ivp" #txt
Rr0 f27 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name></name>
    </language>
</elementInfo>
' #txt
Rr0 f27 408 284 542 384 #arcP
Rr0 f27 1 408 384 #addKink
Rr0 f27 1 0.6268656716417911 0 -14 #arcLabel
Rr0 f20 expr data #txt
Rr0 f20 outCond ivp=="readOrderInRepair.ivp" #txt
Rr0 f20 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name></name>
    </language>
</elementInfo>
' #txt
Rr0 f20 408 284 542 440 #arcP
Rr0 f20 1 408 440 #addKink
Rr0 f20 1 0.7014925373134329 0 6 #arcLabel
Rr0 f29 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f29 actionTable 'out=in;
out.currentOrder.orderId=in.inRepairOrders.getField(in.index,"orderId").toString().toNumber();
out.viewType=ch.soreco.orderbook.ui.enums.ViewType.EDIT;
' #txt
Rr0 f29 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>EDIT Order
IN_REPAIR</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f29 542 324 36 24 20 -2 #rect
Rr0 f29 @|StepIcon #fIcon
Rr0 f30 expr data #txt
Rr0 f30 outCond ivp=="repairOrder.ivp" #txt
Rr0 f30 408 284 542 336 #arcP
Rr0 f30 1 408 336 #addKink
Rr0 f30 1 0.3705350746437208 0 0 #arcLabel
Rr0 f31 outTypes "ch.soreco.archive.ui.OrderRepair","ch.soreco.archive.ui.OrderRepair" #txt
Rr0 f31 outLinks "documents.ivp","orderInformations.ivp" #txt
Rr0 f31 template "RepairOrder.ivc" #txt
Rr0 f31 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f31 skipLink skip.ivp #txt
Rr0 f31 sortLink sort.ivp #txt
Rr0 f31 templateWizard '#
#Fri Feb 18 12:40:42 CET 2011
' #txt
Rr0 f31 pageArchivingActivated false #txt
Rr0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>RepairOrder</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f31 @C|.responsibility Everybody #txt
Rr0 f31 702 372 36 24 20 -2 #rect
Rr0 f31 @|PageIcon #fIcon
Rr0 f32 expr data #txt
Rr0 f32 outCond ivp=="documents.ivp" #txt
Rr0 f32 734 372 822 344 #arcP
Rr0 f32 1 768 344 #addKink
Rr0 f32 1 0.011279646788551687 0 0 #arcLabel
Rr0 f33 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f33 processCall 'Business Processes/chooseProcess:call(Integer,ch.soreco.orderbook.ui.enums.ViewType)' #txt
Rr0 f33 doCall true #txt
Rr0 f33 requestActionDecl '<java.lang.Integer orderId,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
Rr0 f33 requestMappingAction 'param.orderId=in.currentOrder.orderId;
param.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
' #txt
Rr0 f33 responseActionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f33 responseMappingAction 'out=in;
' #txt
Rr0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>read order</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f33 822 412 36 24 20 -2 #rect
Rr0 f33 @|CallSubIcon #fIcon
Rr0 f15 expr data #txt
Rr0 f15 outCond ivp=="orderInformations.ivp" #txt
Rr0 f15 734 396 822 424 #arcP
Rr0 f15 1 768 424 #addKink
Rr0 f15 1 0.011279646788551687 0 0 #arcLabel
Rr0 f34 expr out #txt
Rr0 f34 578 440 711 396 #arcP
Rr0 f34 1 680 440 #addKink
Rr0 f34 0 0.8848035958665158 0 0 #arcLabel
Rr0 f28 expr out #txt
Rr0 f28 578 384 702 384 #arcP
Rr0 f28 0 0.5409641362158006 0 0 #arcLabel
Rr0 f19 expr out #txt
Rr0 f19 578 336 708 372 #arcP
Rr0 f19 1 672 336 #addKink
Rr0 f19 0 0.7931149366640697 0 0 #arcLabel
Rr0 f9 expr out #txt
Rr0 f9 858 424 968 108 #arcP
Rr0 f9 1 968 424 #addKink
Rr0 f9 1 0.2918797373824036 0 0 #arcLabel
Rr0 f11 actionDecl 'ch.soreco.archive.ui.OrderRepair out;
' #txt
Rr0 f11 actionTable 'out=in;
' #txt
Rr0 f11 type ch.soreco.archive.ui.OrderRepair #txt
Rr0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>filter</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
Rr0 f11 542 492 36 24 20 -2 #rect
Rr0 f11 @|StepIcon #fIcon
Rr0 f12 expr data #txt
Rr0 f12 outCond ivp=="filter.ivp" #txt
Rr0 f12 408 284 542 504 #arcP
Rr0 f12 1 408 504 #addKink
Rr0 f12 0 0.8341648609879412 0 0 #arcLabel
Rr0 f13 expr out #txt
Rr0 f13 578 504 968 108 #arcP
Rr0 f13 1 968 504 #addKink
Rr0 f13 1 0.019784165404473465 0 0 #arcLabel
>Proto Rr0 .type ch.soreco.archive.ui.OrderRepair #txt
>Proto Rr0 .processKind NORMAL #txt
>Proto Rr0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>start</swimlaneLabel>
        <swimlaneLabel>get data</swimlaneLabel>
        <swimlaneLabel>panel</swimlaneLabel>
        <swimlaneLabel>methods</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>162</swimlaneSize>
    <swimlaneSize>171</swimlaneSize>
    <swimlaneSize>176</swimlaneSize>
    <swimlaneSize>537</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
</elementInfo>
' #txt
>Proto Rr0 0 0 32 24 18 0 #rect
>Proto Rr0 @|BIcon #fIcon
Rr0 f0 mainOut f1 tail #connect
Rr0 f1 head f36 mainIn #connect
Rr0 f36 mainOut f5 tail #connect
Rr0 f5 head f3 mainIn #connect
Rr0 f3 mainOut f6 tail #connect
Rr0 f6 head f4 mainIn #connect
Rr0 f2 head f3 mainIn #connect
Rr0 f4 mainOut f8 tail #connect
Rr0 f8 head f7 mainIn #connect
Rr0 f23 head f14 mainIn #connect
Rr0 f14 mainOut f26 tail #connect
Rr0 f26 head f10 mainIn #connect
Rr0 f16 mainOut f24 tail #connect
Rr0 f24 head f10 mainIn #connect
Rr0 f10 mainOut f22 tail #connect
Rr0 f22 head f3 mainIn #connect
Rr0 f27 head f21 mainIn #connect
Rr0 f20 head f18 mainIn #connect
Rr0 f7 out f30 tail #connect
Rr0 f30 head f29 mainIn #connect
Rr0 f7 out f27 tail #connect
Rr0 f7 out f20 tail #connect
Rr0 f7 out f2 tail #connect
Rr0 f7 out f23 tail #connect
Rr0 f31 out f32 tail #connect
Rr0 f32 head f16 mainIn #connect
Rr0 f31 out f15 tail #connect
Rr0 f15 head f33 mainIn #connect
Rr0 f18 mainOut f34 tail #connect
Rr0 f34 head f31 mainIn #connect
Rr0 f21 mainOut f28 tail #connect
Rr0 f28 head f31 mainIn #connect
Rr0 f29 mainOut f19 tail #connect
Rr0 f19 head f31 mainIn #connect
Rr0 f33 mainOut f9 tail #connect
Rr0 f9 head f10 mainIn #connect
Rr0 f7 out f12 tail #connect
Rr0 f12 head f11 mainIn #connect
Rr0 f11 mainOut f13 tail #connect
Rr0 f13 head f10 mainIn #connect
