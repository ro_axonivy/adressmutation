[Ivy]
[>Created: Mon Nov 14 16:37:32 CET 2011]
132008536D682933 3.15 #module
>Proto >Proto Collection #zClass
Ut0 UserImport Big #zClass
Ut0 B #cInfo
Ut0 #process
Ut0 @TextInP .resExport .resExport #zField
Ut0 @TextInP .type .type #zField
Ut0 @TextInP .processKind .processKind #zField
Ut0 @AnnotationInP-0n ai ai #zField
Ut0 @TextInP .xml .xml #zField
Ut0 @TextInP .responsibility .responsibility #zField
Ut0 @StartRequest f0 '' #zField
Ut0 @EndTask f1 '' #zField
Ut0 @DBStep f2 '' #zField
Ut0 @PushWFArc f3 '' #zField
Ut0 @GridStep f4 '' #zField
Ut0 @PushWFArc f5 '' #zField
Ut0 @PushWFArc f6 '' #zField
>Proto Ut0 Ut0 UserImport #zField
Ut0 f0 outLink start.ivp #txt
Ut0 f0 type ch.soreco.common.functional.UserImport #txt
Ut0 f0 inParamDecl '<> param;' #txt
Ut0 f0 actionDecl 'ch.soreco.common.functional.UserImport out;
' #txt
Ut0 f0 guid 13200853F011487D #txt
Ut0 f0 requestEnabled false #txt
Ut0 f0 triggerEnabled false #txt
Ut0 f0 callSignature start() #txt
Ut0 f0 persist false #txt
Ut0 f0 startName UserImport #txt
Ut0 f0 taskData '#
#Mon Nov 14 16:37:29 CET 2011
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Ut0 f0 caseData '#
#Mon Nov 14 16:37:29 CET 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Ut0 f0 showInStartList 1 #txt
Ut0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Ut0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name></name>
    </language>
</elementInfo>
' #txt
Ut0 f0 @C|.responsibility Administrator #txt
Ut0 f0 163 115 26 26 14 0 #rect
Ut0 f0 @|StartRequestIcon #fIcon
Ut0 f1 type ch.soreco.common.functional.UserImport #txt
Ut0 f1 163 243 26 26 14 0 #rect
Ut0 f1 @|EndIcon #fIcon
Ut0 f2 actionDecl 'ch.soreco.common.functional.UserImport out;
' #txt
Ut0 f2 actionTable 'out=in;
out.userRS=recordset;
' #txt
Ut0 f2 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE SELECT SYSTEM  ""sqlStatements.dtd"">
<SELECT><Table name=''IWA_User''/><Condition><Relation kind=''=''><Column name=''ApplicationId''/><Number>801</Number></Relation></Condition></SELECT>' #txt
Ut0 f2 dbUrl bzp_ivy_system #txt
Ut0 f2 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Ut0 f2 dbWizard 'ApplicationId = 801' #txt
Ut0 f2 lotSize 2147483647 #txt
Ut0 f2 startIdx 0 #txt
Ut0 f2 type ch.soreco.common.functional.UserImport #txt
Ut0 f2 158 164 36 24 20 -2 #rect
Ut0 f2 @|DBStepIcon #fIcon
Ut0 f3 expr out #txt
Ut0 f3 176 141 176 164 #arcP
Ut0 f4 actionDecl 'ch.soreco.common.functional.UserImport out;
' #txt
Ut0 f4 actionTable 'out=in;
' #txt
Ut0 f4 actionCode 'import ch.ivyteam.ivy.security.IUser;
for(int i = 0; i < in.userRS.size(); i++){

	IUser cUser = ivy.wf.getSecurityContext().findUser(in.userRS.getField(i,"Name").toString());
	if(cUser == null){
		
		ivy.wf.getSecurityContext().createUser(
			in.userRS.getField(i,"Name").toString(),
			in.userRS.getField(i,"FullName").toString(),
			in.userRS.getField(i,"Name").toString(),
			java.util.Locale.GERMAN,
			in.userRS.getField(i,"EMailAddress").toString(),
			in.userRS.getField(i,"ExternalSecurityName").toString());
		
	//ivy.log.debug(in.userRS.getField(i,"Name").toString());
	}

	
}	
' #txt
Ut0 f4 type ch.soreco.common.functional.UserImport #txt
Ut0 f4 158 204 36 24 20 -2 #rect
Ut0 f4 @|StepIcon #fIcon
Ut0 f5 expr out #txt
Ut0 f5 176 188 176 204 #arcP
Ut0 f6 expr out #txt
Ut0 f6 176 228 176 243 #arcP
>Proto Ut0 .type ch.soreco.common.functional.UserImport #txt
>Proto Ut0 .processKind NORMAL #txt
>Proto Ut0 0 0 32 24 18 0 #rect
>Proto Ut0 @|BIcon #fIcon
Ut0 f0 mainOut f3 tail #connect
Ut0 f3 head f2 mainIn #connect
Ut0 f2 mainOut f5 tail #connect
Ut0 f5 head f4 mainIn #connect
Ut0 f4 mainOut f6 tail #connect
Ut0 f6 head f1 mainIn #connect
