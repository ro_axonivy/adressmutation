[Ivy]
12B77D26AE05437D 3.20 #module
>Proto >Proto Collection #zClass
sa0 setTestData Big #zClass
sa0 B #cInfo
sa0 #process
sa0 @TextInP .resExport .resExport #zField
sa0 @TextInP .type .type #zField
sa0 @TextInP .processKind .processKind #zField
sa0 @AnnotationInP-0n ai ai #zField
sa0 @TextInP .xml .xml #zField
sa0 @TextInP .responsibility .responsibility #zField
sa0 @StartRequest f0 '' #zField
sa0 @GridStep f9 '' #zField
sa0 @EndRequest f14 '' #zField
sa0 @CallSub f36 '' #zField
sa0 @PushWFArc f16 '' #zField
sa0 @GridStep f17 '' #zField
sa0 @Page f21 '' #zField
sa0 @PushWFArc f23 '' #zField
sa0 @PushWFArc f19 '' #zField
sa0 @GridStep f29 '' #zField
sa0 @CallSub f32 '' #zField
sa0 @Alternative f37 '' #zField
sa0 @PushWFArc f38 '' #zField
sa0 @PushWFArc f35 '' #zField
sa0 @PushWFArc f39 '' #zField
sa0 @PushWFArc f40 '' #zField
sa0 @GridStep f24 '' #zField
sa0 @PushWFArc f41 '' #zField
sa0 @PushWFArc f34 '' #zField
sa0 @PushWFArc f42 '' #zField
sa0 @PushWFArc f1 '' #zField
sa0 @PushWFArc f4 '' #zField
sa0 @CallSub f6 '' #zField
sa0 @PushWFArc f7 '' #zField
sa0 @PushWFArc f5 '' #zField
sa0 @CallSub f2 '' #zField
sa0 @PushWFArc f3 '' #zField
sa0 @PushWFArc f8 '' #zField
sa0 @EndRequest f10 '' #zField
sa0 @ProcessException f11 '' #zField
sa0 @PushWFArc f12 '' #zField
>Proto sa0 sa0 setTestData #zField
sa0 f0 outLink startSetTestData.ivp #txt
sa0 f0 type ch.soreco.orderbook.ui.Order #txt
sa0 f0 inParamDecl '<> param;' #txt
sa0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f0 guid 12B77D29A4C71563 #txt
sa0 f0 requestEnabled true #txt
sa0 f0 triggerEnabled false #txt
sa0 f0 callSignature startSetTestData() #txt
sa0 f0 persist false #txt
sa0 f0 startName 'Administration - Auftragsbuch - Test Data' #txt
sa0 f0 taskData '#
#Wed Jul 20 16:54:20 CEST 2011
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
sa0 f0 caseData '#
#Wed Jul 20 16:54:20 CEST 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
sa0 f0 showInStartList 0 #txt
sa0 f0 roleExceptionId 12B77D26AE05437D-f11-buffer #txt
sa0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>startSetTestData.ivp</name>
        <nameStyle>20,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f0 @C|.responsibility Administrator #txt
sa0 f0 307 59 26 26 14 0 #rect
sa0 f0 @|StartRequestIcon #fIcon
sa0 f9 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f9 actionTable 'out=in;
out.i=0;
' #txt
sa0 f9 actionCode 'import ch.soreco.filehandling.SmbHelper;
String user = "Administrator";
String password = "Soreco$$08";

SmbHelper smb = new SmbHelper(user, password);
String netPath = "smb://SRVB2P01/bzp-ivyPRE/";
//"smb://server/share/path/document.pdf"
for(String path : smb.listFiles(netPath)){
	ivy.log.debug("Found File "+path);
	}



' #txt
sa0 f9 type ch.soreco.orderbook.ui.Order #txt
sa0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set i=0</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f9 342 276 36 24 20 -2 #rect
sa0 f9 @|StepIcon #fIcon
sa0 f14 type ch.soreco.orderbook.ui.Order #txt
sa0 f14 template "/ProcessPages/setTestData/RedirectOrderBook.ivc" #txt
sa0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Start Addressmutation</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f14 307 507 26 26 14 0 #rect
sa0 f14 @|EndRequestIcon #fIcon
sa0 f36 type ch.soreco.orderbook.ui.Order #txt
sa0 f36 processCall 'Functional Processes/Login:call()' #txt
sa0 f36 doCall true #txt
sa0 f36 requestActionDecl '<> param;
' #txt
sa0 f36 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f36 responseMappingAction 'out=in;
' #txt
sa0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login?</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f36 302 116 36 24 20 -2 #rect
sa0 f36 @|CallSubIcon #fIcon
sa0 f16 expr out #txt
sa0 f16 320 85 320 116 #arcP
sa0 f17 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f17 actionTable 'out=in;
out.error="Deleting All Data in "+ivy.session.activeEnvironment;
' #txt
sa0 f17 actionCode 'import ch.ivyteam.ivy.workflow.IProcessStart;
import ch.soreco.webbies.common.html.navigation.StartRef;
import ch.soreco.common.bo.ErrorLog;
import ch.soreco.orderbook.bo.ScanLog;
import ch.soreco.orderbook.bo.Correspondence;
import ch.soreco.orderbook.bo.CorrespondenceContainer;
import ch.soreco.orderbook.bo.BusinessPartner;
import ch.soreco.webbies.common.db.EntityFilter;
import ch.soreco.orderbook.bo.Affirmation;
import ch.soreco.orderbook.bo.RecordingLSV;
import ch.soreco.orderbook.bo.DomicileOrOwner;
import ch.soreco.orderbook.bo.Recording;
import ch.soreco.orderbook.bo.ContactAddress;
import ch.soreco.orderbook.bo.Scanning;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Paper;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.Order; 


out.error = StartRef.getProcessStartRefByName("12C9CF32100C49C3/Test_Fileimport.ivp");
List<Order> orders = ivy.persistence.Adressmutation.findAll(Order.class);
for(Order order:orders) {
	//first unlock ForeignKey constraints
	order.scanning = null;
	order.scanningId = null;
	order.recording = null;
	ivy.persistence.Adressmutation.merge(order);	
	}
List<Scanning> scannings = ivy.persistence.Adressmutation.findAll(Scanning.class);
for(Scanning scanning:scannings){
	scanning.orderId = null;
	scanning.orderMatching = null;
	scanning.scanShare = null;
	ivy.persistence.Adressmutation.remove(scanning);
	}
List<Recording> recordings = ivy.persistence.Adressmutation.findAll(Recording.class);
for(Recording recording:recordings){
	recording.affirmation = null;
	ivy.persistence.Adressmutation.merge(recording);
	}
EntityFilter.removeAll(Contact.class,ivy.persistence.Adressmutation);	
EntityFilter.removeAll(ContactAddress.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(Paper.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(SignatureRequest.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(DomicileOrOwner.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(RecordingLSV.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(Affirmation.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(CorrespondenceContainer.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(Correspondence.class,ivy.persistence.Adressmutation);	
EntityFilter.removeAll(BusinessPartner.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(Recording.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(Order.class,ivy.persistence.Adressmutation);

EntityFilter.removeAll(ScanLog.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(ErrorLog.class,ivy.persistence.Adressmutation);
EntityFilter.removeAll(ch.soreco.common.bo.File.class,ivy.persistence.Adressmutation);
' #txt
sa0 f17 type ch.soreco.orderbook.ui.Order #txt
sa0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete all</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f17 182 292 36 24 20 -2 #rect
sa0 f17 @|StepIcon #fIcon
sa0 f21 outTypes "ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order" #txt
sa0 f21 outLinks "reset.ivp","append.ivp","appendOrder.ivp","exit.ivp","fileUpload.ivp" #txt
sa0 f21 template "/ProcessPages/setTestData/SetTestData.ivc" #txt
sa0 f21 type ch.soreco.orderbook.ui.Order #txt
sa0 f21 skipLink skip.ivp #txt
sa0 f21 sortLink sort.ivp #txt
sa0 f21 templateWizard '#
#Tue Mar 08 20:47:55 CET 2011
' #txt
sa0 f21 pageArchivingActivated false #txt
sa0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set TestData</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f21 @C|.responsibility Everybody #txt
sa0 f21 302 180 36 24 20 -2 #rect
sa0 f21 @|PageIcon #fIcon
sa0 f23 expr out #txt
sa0 f23 320 140 320 180 #arcP
sa0 f19 expr data #txt
sa0 f19 outCond ivp=="reset.ivp" #txt
sa0 f19 307 204 213 292 #arcP
sa0 f29 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f29 actionTable 'out=in;
' #txt
sa0 f29 actionCode 'import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;

//Target directory exists?
File directory = new File("/prescanning");
if(!directory.isDirectory()){
		directory.mkdir();
		}
//set fileUri''s
String xmlFileUrl = in.xmlFile.getJavaFile().getAbsolutePath();
String pdfFileUrl = in.order.scanning.scanFile; //in.order.scanning.scanFile.getJavaFile().getAbsolutePath();

String xmlTargetUrl = "/prescanning/"+ in.xmlFile.getName();
String pdfTargetUrl = "/prescanning/"+ in.order.scanning.scanFile; //in.order.scanning.scanFile.getName();

//now copy to target
java.io.File xmlSource = new java.io.File(xmlFileUrl);
java.io.File pdfSource = new java.io.File(pdfFileUrl);

//copy XML File
if(xmlSource.exists()) {
	File target = new File(xmlTargetUrl);
	FileChannel inChannel  = new FileInputStream(xmlSource).getChannel();
	FileChannel outChannel = new FileOutputStream(target.getJavaFile(), true ).getChannel();
	try {
	    inChannel.transferTo( 0, inChannel.size(), outChannel );
	} 
	finally {
			if( inChannel  != null ) {
					try {	inChannel.close();	} catch( IOException ex ){}
					}
	   	if( outChannel != null ){
					try {	outChannel.close();	} catch( IOException ex ) {}
					}
	}
	out.xmlFile = new File(xmlTargetUrl);
	}

//copy PDF File
if(pdfSource.exists()) {
	File target = new File(pdfTargetUrl);
	FileChannel inChannel  = new FileInputStream(pdfSource).getChannel();
	FileChannel outChannel = new FileOutputStream(target.getJavaFile(), true ).getChannel();
	try {
	    inChannel.transferTo( 0, inChannel.size(), outChannel );
	} 
	finally {
			if( inChannel  != null ) {
					try {	inChannel.close();	} catch( IOException ex ){}
					}
	   	if( outChannel != null ){
					try {	outChannel.close();	} catch( IOException ex ) {}
					}
	}
	File f = new File(pdfTargetUrl);
	out.order.scanning.scanFile = f.getPath();
	}

out.order.contacts.clear();
' #txt
sa0 f29 type ch.soreco.orderbook.ui.Order #txt
sa0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get CopyFile</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f29 630 244 36 24 20 -2 #rect
sa0 f29 @|StepIcon #fIcon
sa0 f32 type ch.soreco.orderbook.ui.Order #txt
sa0 f32 processCall 'Functional Processes/Scanning/FileImport:ManualImport(List<java.io.File>)' #txt
sa0 f32 doCall true #txt
sa0 f32 requestActionDecl '<List<java.io.File> FileSet> param;
' #txt
sa0 f32 requestMappingAction 'param.FileSet=in.FileSet;
' #txt
sa0 f32 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f32 responseMappingAction 'out=in;
out.error=result.error;
out.success=result.success;
' #txt
sa0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ImportFiles</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f32 630 364 36 24 20 -2 #rect
sa0 f32 @|CallSubIcon #fIcon
sa0 f37 type ch.soreco.orderbook.ui.Order #txt
sa0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Successful?</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f37 634 506 28 28 14 0 #rect
sa0 f37 @|AlternativeIcon #fIcon
sa0 f38 expr out #txt
sa0 f38 648 388 648 506 #arcP
sa0 f38 0 0.8569941012322395 0 0 #arcLabel
sa0 f35 expr in #txt
sa0 f35 outCond in.success #txt
sa0 f35 634 520 333 520 #arcP
sa0 f35 0 0.34954653738434494 0 0 #arcLabel
sa0 f39 expr in #txt
sa0 f39 662 520 332 180 #arcP
sa0 f39 1 1064 520 #addKink
sa0 f39 2 1064 168 #addKink
sa0 f39 3 344 168 #addKink
sa0 f39 2 0.21290746165460397 0 0 #arcLabel
sa0 f40 expr data #txt
sa0 f40 outCond ivp=="appendOrder.ivp" #txt
sa0 f40 338 192 648 244 #arcP
sa0 f40 1 648 192 #addKink
sa0 f40 0 0.9119187993180968 0 0 #arcLabel
sa0 f24 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f24 actionTable 'out=in;
' #txt
sa0 f24 actionCode 'in.FileSet.add(0,new java.io.File(in.xmlFile.getPath()));
in.FileSet.add(1,new java.io.File(in.order.scanning.scanFile));
in.FileSet.add(2,new java.io.File(in.order.scanning.scanFile));' #txt
sa0 f24 type ch.soreco.orderbook.ui.Order #txt
sa0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get FileSet</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f24 630 300 36 24 20 -2 #rect
sa0 f24 @|StepIcon #fIcon
sa0 f41 expr out #txt
sa0 f41 648 268 648 300 #arcP
sa0 f34 expr out #txt
sa0 f34 648 324 648 364 #arcP
sa0 f42 expr out #txt
sa0 f42 218 304 320 204 #arcP
sa0 f42 1 272 280 #addKink
sa0 f42 1 0.14021423950067533 0 0 #arcLabel
sa0 f1 expr data #txt
sa0 f1 outCond ivp=="exit.ivp" #txt
sa0 f1 320 204 320 507 #arcP
sa0 f4 expr out #txt
sa0 f4 358 300 322 507 #arcP
sa0 f6 type ch.soreco.orderbook.ui.Order #txt
sa0 f6 processCall 'Functional Processes/common/File:set(Boolean,Boolean,File)' #txt
sa0 f6 doCall true #txt
sa0 f6 requestActionDecl '<java.lang.Boolean keepFile,java.lang.Boolean deleteFromFileSystem,ch.ivyteam.ivy.scripting.objects.File file> param;
' #txt
sa0 f6 requestMappingAction 'param.keepFile=false;
param.deleteFromFileSystem=true;
param.file=in.xmlFile;
' #txt
sa0 f6 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f6 responseMappingAction 'out=in;
out.error=result.error;
out.i=result.file.fileId;
out.success=result.success;
' #txt
sa0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Upload a File</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f6 182 172 36 24 20 -2 #rect
sa0 f6 @|CallSubIcon #fIcon
sa0 f7 expr data #txt
sa0 f7 outCond ivp=="fileUpload.ivp" #txt
sa0 f7 302 191 218 185 #arcP
sa0 f5 expr data #txt
sa0 f5 outCond ivp=="append.ivp" #txt
sa0 f5 325 204 355 276 #arcP
sa0 f2 type ch.soreco.orderbook.ui.Order #txt
sa0 f2 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
sa0 f2 doCall true #txt
sa0 f2 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
sa0 f2 requestMappingAction 'param.filter.fileId=in.i-1;
' #txt
sa0 f2 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f2 responseMappingAction 'out=in;
' #txt
sa0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show Uploaded file</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f2 182 220 36 24 20 -2 #rect
sa0 f2 @|CallSubIcon #fIcon
sa0 f3 expr out #txt
sa0 f3 200 196 200 220 #arcP
sa0 f8 expr out #txt
sa0 f8 218 226 302 198 #arcP
sa0 f10 type ch.soreco.orderbook.ui.Order #txt
sa0 f10 template "/ProcessPages/Configurations/RoleViolation.ivc" #txt
sa0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Role Exception</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f10 27 243 26 26 14 0 #rect
sa0 f10 @|EndRequestIcon #fIcon
sa0 f11 .resExport export #txt
sa0 f11 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
sa0 f11 actionTable 'out=in;
out.error=exception;
' #txt
sa0 f11 type ch.soreco.orderbook.ui.Order #txt
sa0 f11 errorCode unspecified #txt
sa0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>RoleViolation</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
sa0 f11 27 51 26 26 14 0 #rect
sa0 f11 @|ExceptionIcon #fIcon
sa0 f12 expr out #txt
sa0 f12 40 77 40 243 #arcP
>Proto sa0 .type ch.soreco.orderbook.ui.Order #txt
>Proto sa0 .processKind NORMAL #txt
>Proto sa0 0 0 32 24 18 0 #rect
>Proto sa0 @|BIcon #fIcon
sa0 f0 mainOut f16 tail #connect
sa0 f16 head f36 mainIn #connect
sa0 f36 mainOut f23 tail #connect
sa0 f23 head f21 mainIn #connect
sa0 f21 out f19 tail #connect
sa0 f19 head f17 mainIn #connect
sa0 f32 mainOut f38 tail #connect
sa0 f38 head f37 in #connect
sa0 f37 out f35 tail #connect
sa0 f35 head f14 mainIn #connect
sa0 f37 out f39 tail #connect
sa0 f39 head f21 mainIn #connect
sa0 f40 head f29 mainIn #connect
sa0 f29 mainOut f41 tail #connect
sa0 f41 head f24 mainIn #connect
sa0 f24 mainOut f34 tail #connect
sa0 f34 head f32 mainIn #connect
sa0 f17 mainOut f42 tail #connect
sa0 f42 head f21 mainIn #connect
sa0 f1 head f14 mainIn #connect
sa0 f9 mainOut f4 tail #connect
sa0 f4 head f14 mainIn #connect
sa0 f7 head f6 mainIn #connect
sa0 f21 out f5 tail #connect
sa0 f5 head f9 mainIn #connect
sa0 f21 out f40 tail #connect
sa0 f21 out f1 tail #connect
sa0 f21 out f7 tail #connect
sa0 f6 mainOut f3 tail #connect
sa0 f3 head f2 mainIn #connect
sa0 f2 mainOut f8 tail #connect
sa0 f8 head f21 mainIn #connect
sa0 f11 mainOut f12 tail #connect
sa0 f12 head f10 mainIn #connect
