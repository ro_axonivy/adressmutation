[Ivy]
[>Created: Fri Oct 04 16:08:42 CEST 2013]
1314CB0B012B83FE 3.17 #module
>Proto >Proto Collection #zClass
os0 orderStatistics Big #zClass
os0 B #cInfo
os0 #process
os0 @TextInP .resExport .resExport #zField
os0 @TextInP .type .type #zField
os0 @TextInP .processKind .processKind #zField
os0 @AnnotationInP-0n ai ai #zField
os0 @TextInP .xml .xml #zField
os0 @TextInP .responsibility .responsibility #zField
os0 @StartRequest f0 '' #zField
os0 @EndTask f1 '' #zField
os0 @CallSub f3 '' #zField
os0 @GridStep f5 '' #zField
os0 @GridStep f7 '' #zField
os0 @Page f9 '' #zField
os0 @GridStep f13 '' #zField
os0 @PushWFArc f10 '' #zField
os0 @PushWFArc f2 '' #zField
os0 @GridStep f11 '' #zField
os0 @CallSub f45 '' #zField
os0 @PushWFArc f16 '' #zField
os0 @PushWFArc f6 '' #zField
os0 @GridStep f17 '' #zField
os0 @PushWFArc f18 '' #zField
os0 @PushWFArc f15 '' #zField
os0 @GridStep f19 '' #zField
os0 @PushWFArc f8 '' #zField
os0 @GridStep f21 '' #zField
os0 @GridStep f23 '' #zField
os0 @PushWFArc f24 '' #zField
os0 @PushWFArc f22 '' #zField
os0 @CallSub f66 '' #zField
os0 @PushWFArc f25 '' #zField
os0 @CallSub f26 '' #zField
os0 @CallSub f28 '' #zField
os0 @PushWFArc f29 '' #zField
os0 @PushWFArc f30 '' #zField
os0 @PushWFArc f12 '' #zField
os0 @GridStep f31 '' #zField
os0 @PushWFArc f32 '' #zField
os0 @GridStep f35 '' #zField
os0 @PushWFArc f36 '' #zField
os0 @PushWFArc f4 '' #zField
os0 @GridStep f27 '' #zField
os0 @PushWFArc f33 '' #zField
os0 @PushWFArc f20 '' #zField
os0 @GridStep f34 '' #zField
os0 @GridStep f38 '' #zField
os0 @PushWFArc f39 '' #zField
os0 @PushWFArc f40 '' #zField
os0 @PushWFArc f41 '' #zField
os0 @Alternative f42 '' #zField
os0 @PushWFArc f43 '' #zField
os0 @PushWFArc f44 '' #zField
os0 @PushWFArc f14 '' #zField
os0 @CallSub f46 '' #zField
os0 @Alternative f47 '' #zField
os0 @PushWFArc f48 '' #zField
os0 @PushWFArc f37 '' #zField
os0 @PushWFArc f49 '' #zField
os0 @PushWFArc f50 '' #zField
os0 @GridStep f51 '' #zField
os0 @PushWFArc f52 '' #zField
os0 @PushWFArc f53 '' #zField
os0 @CallSub f54 '' #zField
os0 @PushWFArc f56 '' #zField
os0 @GridStep f57 '' #zField
os0 @PushWFArc f58 '' #zField
os0 @PushWFArc f55 '' #zField
>Proto os0 os0 orderStatistics #zField
os0 f0 outLink start.ivp #txt
os0 f0 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f0 inParamDecl '<> param;' #txt
os0 f0 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f0 guid 1314CB0C7D437DE1 #txt
os0 f0 requestEnabled true #txt
os0 f0 triggerEnabled false #txt
os0 f0 callSignature start() #txt
os0 f0 persist false #txt
os0 f0 startName 'Auftragsbuch - Statistiken' #txt
os0 f0 taskData '#
#Sun Aug 21 17:59:02 CEST 2011
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
os0 f0 caseData '#
#Sun Aug 21 17:59:02 CEST 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
os0 f0 showInStartList 0 #txt
os0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
os0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f0 @C|.responsibility Everybody #txt
os0 f0 243 51 26 26 14 0 #rect
os0 f0 @|StartRequestIcon #fIcon
os0 f1 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f1 99 483 26 26 14 0 #rect
os0 f1 @|EndIcon #fIcon
os0 f3 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f3 processCall 'Functional Processes/orderbook/OrderStatistics:byDay(Integer,Integer,Integer,Integer)' #txt
os0 f3 doCall true #txt
os0 f3 requestActionDecl '<java.lang.Integer forOrderBookId,java.lang.Integer forDay,java.lang.Integer forMonth,java.lang.Integer forYear> param;
' #txt
os0 f3 requestMappingAction 'param.forOrderBookId=in.orderBookId;
param.forDay=in.forDay;
param.forMonth=in.forMonth;
param.forYear=in.forYear;
' #txt
os0 f3 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f3 responseMappingAction 'out=in;
out.rsOrderStatistics=result.rsOrderStatistics;
' #txt
os0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Statistics of Choosen</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f3 454 796 36 24 20 -5 #rect
os0 f3 @|CallSubIcon #fIcon
os0 f5 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f5 actionTable 'out=in;
out.forDate=new Date();
out.statisticType="Offene";
out.statistics=["Offene", "Kumuliert", "Offene nach Feld"];
' #txt
os0 f5 actionCode 'import ch.soreco.orderbook.enums.PendencyTicketType;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.webbies.common.html.generics.AttributeClass;
import java.lang.reflect.Field;
import ch.soreco.orderbook.bo.Order;

in.additionalFieldsMap.clear();
in.orderStatesKeyLabelMap.clear();
AttributeClass acOrderStates = new AttributeClass(OrderState.class);
in.orderStatesKeyLabelMap = acOrderStates.getKeyLabelMap();

AttributeClass acOrder = new AttributeClass(Order.class,"Statistics");
in.additionalFieldsMap = acOrder.getKeyContentLabelMap();
in.pendencyTicketTypeKeyLabelMap = new AttributeClass(PendencyTicketType.class).getKeyContentLabelMap();
in.rsAdditionalFields = new Recordset(["value", "label"]);
in.rsAdditionalFields.clear();
for(String key:acOrder.getKeys()){
	in.rsAdditionalFields.add([key,in.additionalFieldsMap.get(key).toString()]);
	}' #txt
os0 f5 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initialize</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f5 238 164 36 24 20 -2 #rect
os0 f5 @|StepIcon #fIcon
os0 f7 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f7 actionTable 'out=in;
' #txt
os0 f7 actionCode 'import java.util.Collections;
import org.apache.commons.lang.StringUtils;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.bo.OrderBook;
import ch.soreco.orderbook.util.Authority;


List<OrderBook> books = Authority.getInstance().getBooks();

in.rsOrderBooks = new Recordset();
in.rsOrderBooks.addColumn("orderBookId",[]);
in.rsOrderBooks.addColumn("name",[]);
in.rsOrderBooks.addColumn("orderTypes",[]);
in.rsOrderBooks.addColumn("sortSequence",[]);

for(OrderBook book:books){
	if(book.analysable){
		List<OrderType> types = book.getOrderTypeList();
		List<String> labels = new List<String>();
		types.remove(OrderType.Default);
		for(OrderType type:types){
			if(in.orderTypeLabelMap.containsKey(type.name())){
				String label = in.orderTypeLabelMap.get(type.name()).toString();
				if(!label.trim().equals("")){
					labels.add(label);
					}
				}
			else {
				labels.add(type.name());
				}
			}
		Collections.sort(labels);
		String typesHtml = "";
		if(types.size()>0){
			typesHtml = "<ul><li>"+StringUtils.join(labels,"</li><li>")+"</li></ul>";
			}
		Record rec = new Record();
		rec.putField("orderBookId",book.orderBookId);
		rec.putField("name",book.name);
		rec.putField("orderTypes",typesHtml);
		rec.putField("sortSequence",book.sortSequence);
		//may we need to check if present orderbook is 
		//accessible by present user: ivy.session.getSessionUserName()
		if(types.size()>0){
			in.rsOrderBooks.add(rec);
			}		
		}
	}
	
in.rsOrderBooks.sort("sortSequence");' #txt
os0 f7 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get OrderBooks</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f7 238 308 36 24 20 -2 #rect
os0 f7 @|StepIcon #fIcon
os0 f9 outTypes "ch.soreco.orderbook.ui.OrderStatistics","ch.soreco.orderbook.ui.OrderStatistics","ch.soreco.orderbook.ui.OrderStatistics","ch.soreco.orderbook.ui.OrderStatistics","ch.soreco.orderbook.ui.OrderStatistics","ch.soreco.orderbook.ui.OrderStatistics" #txt
os0 f9 outLinks "getStatistics.ivp","exit.ivp","excelExport.ivp","getDay.ivp","getMonthOrYear.ivp","setStatisticType.ivp" #txt
os0 f9 template "/ProcessPages/orderStatistics/OrderStatistics.ivc" #txt
os0 f9 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f9 skipLink skip.ivp #txt
os0 f9 sortLink sort.ivp #txt
os0 f9 templateWizard '#
#Fri Oct 04 15:59:20 CEST 2013
' #txt
os0 f9 pageArchivingActivated false #txt
os0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Statistics</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f9 @C|.responsibility Everybody #txt
os0 f9 238 484 36 24 20 -2 #rect
os0 f9 @|PageIcon #fIcon
os0 f13 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f13 actionTable 'out=in;
' #txt
os0 f13 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f13 238 436 36 24 20 -2 #rect
os0 f13 @|StepIcon #fIcon
os0 f10 expr out #txt
os0 f10 256 460 256 484 #arcP
os0 f2 expr data #txt
os0 f2 outCond ivp=="exit.ivp" #txt
os0 f2 238 496 125 496 #arcP
os0 f11 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f11 actionTable 'out=in;
out.orderBookId=in.rsOrderBooks.getAt(in.i).getField("orderBookId").toString().toNumber();
' #txt
os0 f11 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get OrderBookId</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f11 238 540 36 24 20 -2 #rect
os0 f11 @|StepIcon #fIcon
os0 f45 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f45 processCall 'Functional Processes/Login:call()' #txt
os0 f45 doCall true #txt
os0 f45 requestActionDecl '<> param;
' #txt
os0 f45 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f45 responseMappingAction 'out=in;
' #txt
os0 f45 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login?</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f45 238 100 36 24 20 -2 #rect
os0 f45 @|CallSubIcon #fIcon
os0 f16 expr out #txt
os0 f16 256 77 256 100 #arcP
os0 f6 expr out #txt
os0 f6 256 124 256 164 #arcP
os0 f17 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f17 actionTable 'out=in;
' #txt
os0 f17 actionCode 'import org.apache.commons.lang.StringUtils;
if(in.rsOrderStatistics.getKeys().contains("Wiedervorlagegrund")){
	for(int i=0;i<in.rsOrderStatistics.size();i++){
		String type = in.rsOrderStatistics.getField(i,"Wiedervorlagegrund").toString().trim();
		if(in.pendencyTicketTypeKeyLabelMap.containsKey(type)){
			type = in.pendencyTicketTypeKeyLabelMap.get(type).toString();
			}
		in.rsOrderStatistics.putField(i,"Wiedervorlagegrund",type);
		}
	in.rsOrderStatistics.sort("Wiedervorlagegrund");
	}
	
if(in.rsOrderStatistics.getKeys().contains("Status")){
	for(int i=0;i<in.rsOrderStatistics.size();i++){
		String type = in.rsOrderStatistics.getField(i,"Status").toString().trim();
		if(in.orderStatesKeyLabelMap.containsKey(type)){
			type = in.orderStatesKeyLabelMap.get(type).toString();
			}
		in.rsOrderStatistics.putField(i,"Status",type);
		}
	in.rsOrderStatistics.sort("Status");
	}
		
//Replace orderType enum names with its corresponding label
if(in.rsOrderStatistics.getKeys().contains("Auftragstyp")){
	for(int i=0;i<in.rsOrderStatistics.size();i++){
		String type = in.rsOrderStatistics.getField(i,"Auftragstyp").toString().trim();
		if(in.orderTypeLabelMap.containsKey(type)){
			type = in.orderTypeLabelMap.get(type).toString();
			}
		in.rsOrderStatistics.putField(i,"Auftragstyp",type);
		}
	in.rsOrderStatistics.sort("Auftragstyp");
	}
	

//calculate totals
if(in.rsOrderStatistics.columnCount()>1){
		in.totals.clear();
		for(int col=1;col<in.rsOrderStatistics.columnCount();col++){
			int total = 0;
			Boolean doCount = false;
			for(int row=0;row<in.rsOrderStatistics.size();row++){
					if(StringUtils.isNumeric(in.rsOrderStatistics.getField(row,col).toString())){
						total = total+in.rsOrderStatistics.getField(row,col).toString().toNumber();
					}
				}
			in.totals.add(total);
		}
	}
if(in.rsOrderStatistics.getKeys().contains("Datum")){
	//cast String Datum to Date 
	for(int i=0;i<in.rsOrderStatistics.size();i++){
		Date aDate = in.rsOrderStatistics.getAt(i).getField("Datum").toString().toDate();
		in.rsOrderStatistics.putField(i,"Datum",aDate);
		}
	//sort it
	in.rsOrderStatistics.sort("Datum");
	Recordset rs = new Recordset();
	for(int i=0;i<in.rsOrderStatistics.columnCount();i++){
		String name = in.rsOrderStatistics.getKeys().get(i);
		if(name.equals("Datum")){
			rs.addColumn(name,in.rsOrderStatistics.getColumn(name));
			}
		else {
			String label = name;
			if(in.orderTypeLabelMap.containsKey(name)){
				label = in.orderTypeLabelMap.get(name).toString();
				}
			rs.addColumn(label,in.rsOrderStatistics.getColumn(name));
			}
		}
	in.rsOrderStatistics = rs;
	}' #txt
os0 f17 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>put Labels for Order Types</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f17 238 852 36 24 20 -2 #rect
os0 f17 @|StepIcon #fIcon
os0 f18 expr out #txt
os0 f18 454 813 274 859 #arcP
os0 f18 0 0.5522201894997458 0 0 #arcLabel
os0 f15 expr out #txt
os0 f15 238 864 238 448 #arcP
os0 f15 1 56 864 #addKink
os0 f15 2 56 448 #addKink
os0 f15 1 0.5 0 0 #arcLabel
os0 f19 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f19 actionTable 'out=in;
' #txt
os0 f19 actionCode 'import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.webbies.common.html.generics.AttributeClass;
AttributeClass orderAc = new AttributeClass(OrderType.class);
in.orderTypeLabelMap = orderAc.getKeyLabelMap();' #txt
os0 f19 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Key Label Map for orderTypes</name>
        <nameStyle>32
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f19 238 252 36 24 20 -2 #rect
os0 f19 @|StepIcon #fIcon
os0 f8 expr out #txt
os0 f8 256 276 256 308 #arcP
os0 f21 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f21 actionTable 'out=in;
' #txt
os0 f21 actionCode 'import ch.soreco.webbies.common.excel.ColumnMeta;
import ch.soreco.webbies.common.excel.SheetMeta;
import ch.soreco.webbies.common.excel.ExcelWriter;
SheetMeta meta = new SheetMeta(in.rsOrderStatistics);
String orderBookName = in.rsOrderBooks.getAt(in.i).getField("name").toString().trim();
meta.caption = "Auftragsbuch "+orderBookName;
meta.sheetName = orderBookName;
ExcelWriter excel = new ExcelWriter("Statistics-"+orderBookName.replace(" ","-")+".xls");


try {
excel.export(meta);
File excelFile = excel.getExcelFile();
out.excel = new java.io.File(excelFile.getJavaFile().getAbsolutePath());//new java.io.File(excelFile.getPath());
} catch (Exception e){
	ivy.log.error("Error during exporting to excel.", e);
	}' #txt
os0 f21 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Excel</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f21 102 564 36 24 20 -2 #rect
os0 f21 @|StepIcon #fIcon
os0 f23 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f23 actionTable 'out=in;
' #txt
os0 f23 actionCode 'List<Object> totals = new List<Object>();
totals.add(0,"Gesamt");
totals.addAll(in.totals);
in.rsOrderStatistics.add(totals);' #txt
os0 f23 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>prepare export</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f23 102 524 36 24 20 -2 #rect
os0 f23 @|StepIcon #fIcon
os0 f24 expr data #txt
os0 f24 outCond ivp=="excelExport.ivp" #txt
os0 f24 238 501 138 531 #arcP
os0 f22 expr out #txt
os0 f22 120 548 120 564 #arcP
os0 f66 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f66 processCall 'Functional Processes/common/File:show(java.io.File)' #txt
os0 f66 doCall true #txt
os0 f66 requestActionDecl '<java.io.File file> param;
' #txt
os0 f66 requestMappingAction 'param.file=in.excel;
' #txt
os0 f66 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f66 responseMappingAction 'out=in;
' #txt
os0 f66 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Excel</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f66 102 612 36 24 20 -2 #rect
os0 f66 @|CallSubIcon #fIcon
os0 f25 expr out #txt
os0 f25 120 588 120 612 #arcP
os0 f26 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f26 processCall 'Functional Processes/orderbook/OrderStatistics:getGroupBy(Integer,List<String>,List<String>)' #txt
os0 f26 doCall true #txt
os0 f26 requestActionDecl '<java.lang.Integer forOrderBookId,List<java.lang.String> columns,List<java.lang.String> aliases> param;
' #txt
os0 f26 requestMappingAction 'param.forOrderBookId=in.orderBookId;
param.columns=["Month(orderStart)", "DateName( month , orderStart )"];
param.aliases=["Month", "[MonthName]"];
' #txt
os0 f26 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f26 responseMappingAction 'out=in;
out.months=result.rsGroupBy
.insertAt(0,[]);
' #txt
os0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Months</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f26 238 620 36 24 20 -2 #rect
os0 f26 @|CallSubIcon #fIcon
os0 f28 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f28 processCall 'Functional Processes/orderbook/OrderStatistics:getGroupBy(Integer,List<String>,List<String>)' #txt
os0 f28 doCall true #txt
os0 f28 requestActionDecl '<java.lang.Integer forOrderBookId,List<java.lang.String> columns,List<java.lang.String> aliases> param;
' #txt
os0 f28 requestMappingAction 'param.forOrderBookId=in.orderBookId;
param.columns=["Year(orderStart)", "CAST(Year(orderStart) as varchar(4))"];
param.aliases=["[Year]", "YearName"];
' #txt
os0 f28 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f28 responseMappingAction 'out=in;
out.years=result.rsGroupBy
.insertAt(0,[]);
' #txt
os0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Years</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f28 238 660 36 24 20 -2 #rect
os0 f28 @|CallSubIcon #fIcon
os0 f29 expr out #txt
os0 f29 256 644 256 660 #arcP
os0 f30 expr data #txt
os0 f30 outCond ivp=="getStatistics.ivp" #txt
os0 f30 256 508 256 540 #arcP
os0 f12 expr out #txt
os0 f12 256 564 256 620 #arcP
os0 f31 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f31 actionTable 'out=in;
' #txt
os0 f31 actionCode 'if(in.orderBookId==0){
	in.orderBookId= in.rsOrderBooks.getAt(0).getField("orderBookId").toString().toNumber();
	}' #txt
os0 f31 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>If none selected</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f31 414 516 36 24 20 -2 #rect
os0 f31 @|StepIcon #fIcon
os0 f32 expr data #txt
os0 f32 outCond ivp=="getDay.ivp" #txt
os0 f32 274 496 432 516 #arcP
os0 f32 1 432 496 #addKink
os0 f32 1 0.05594004802031852 0 0 #arcLabel
os0 f35 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f35 actionTable 'out=in;
' #txt
os0 f35 actionCode 'in.forDay = in.forDate.getDay();
in.forMonth = in.forDate.getMonth();
in.forYear = in.forDate.getYear();' #txt
os0 f35 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set date</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f35 414 564 36 24 20 -2 #rect
os0 f35 @|StepIcon #fIcon
os0 f36 expr out #txt
os0 f36 432 540 432 564 #arcP
os0 f36 0 0.4890816199312572 0 0 #arcLabel
os0 f4 expr out #txt
os0 f4 432 588 274 632 #arcP
os0 f4 1 432 632 #addKink
os0 f4 1 0.41005359565141747 0 0 #arcLabel
os0 f27 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f27 actionTable 'out=in;
' #txt
os0 f27 actionCode 'in.forDay = in.forDate.getDay();
in.forMonth = in.forDate.getMonth();
in.forYear = in.forDate.getYear();' #txt
os0 f27 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set date</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f27 238 204 36 24 20 -2 #rect
os0 f27 @|StepIcon #fIcon
os0 f33 expr out #txt
os0 f33 256 188 256 204 #arcP
os0 f20 expr out #txt
os0 f20 256 228 256 252 #arcP
os0 f34 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f34 actionTable 'out=in;
' #txt
os0 f34 actionCode 'in.forDate = null;
in.forDay = 0;
if(in.forYear==0){
	Date currentDate = new Date();
	in.forYear= currentDate.getYear();
	}' #txt
os0 f34 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set month</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f34 542 564 36 24 20 -2 #rect
os0 f34 @|StepIcon #fIcon
os0 f38 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f38 actionTable 'out=in;
' #txt
os0 f38 actionCode 'if(in.orderBookId==0){
	in.orderBookId= in.rsOrderBooks.getAt(0).getField("orderBookId").toString().toNumber();
	}' #txt
os0 f38 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>If none selected</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f38 542 516 36 24 20 -2 #rect
os0 f38 @|StepIcon #fIcon
os0 f39 expr out #txt
os0 f39 560 540 560 564 #arcP
os0 f40 expr data #txt
os0 f40 outCond ivp=="getMonthOrYear.ivp" #txt
os0 f40 274 496 560 516 #arcP
os0 f40 1 560 496 #addKink
os0 f40 0 0.5611623355983363 0 0 #arcLabel
os0 f41 expr out #txt
os0 f41 560 588 274 632 #arcP
os0 f41 1 560 632 #addKink
os0 f41 1 0.4489405853074403 0 0 #arcLabel
os0 f42 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f42 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has OrderBooks</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f42 242 362 28 28 14 0 #rect
os0 f42 @|AlternativeIcon #fIcon
os0 f43 expr out #txt
os0 f43 256 332 256 362 #arcP
os0 f44 expr in #txt
os0 f44 outCond in.rsOrderBooks.size()>0 #txt
os0 f44 264 382 418 516 #arcP
os0 f14 expr in #txt
os0 f14 256 390 256 436 #arcP
os0 f46 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f46 processCall 'Functional Processes/orderbook/OrderStatistics:getPivot(Integer,Integer,Integer,Integer)' #txt
os0 f46 doCall true #txt
os0 f46 requestActionDecl '<java.lang.Integer forMonth,java.lang.Integer forDay,java.lang.Integer forYear,java.lang.Integer forOrderBookId> param;
' #txt
os0 f46 requestMappingAction 'param.forMonth=in.forMonth;
param.forDay=in.forDay;
param.forYear=in.forYear;
param.forOrderBookId=in.orderBookId;
' #txt
os0 f46 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f46 responseMappingAction 'out=in;
out.rsOrderStatistics=result.rsStatistics;
' #txt
os0 f46 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Pivot</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f46 638 796 36 24 20 -2 #rect
os0 f46 @|CallSubIcon #fIcon
os0 f47 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f47 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is &quot;Offene&quot;? </name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f47 242 706 28 28 14 0 #rect
os0 f47 @|AlternativeIcon #fIcon
os0 f48 expr out #txt
os0 f48 256 684 256 706 #arcP
os0 f37 expr in #txt
os0 f37 outCond in.statisticType=="Offene" #txt
os0 f37 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Offene</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f37 266 724 454 801 #arcP
os0 f49 expr in #txt
os0 f49 outCond in.statisticType=="Kumuliert" #txt
os0 f49 267 723 638 804 #arcP
os0 f50 expr out #txt
os0 f50 638 811 274 861 #arcP
os0 f51 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f51 actionTable 'out=in;
' #txt
os0 f51 actionCode 'if(in.orderBookId==0){
	in.orderBookId= in.rsOrderBooks.getAt(0).getField("orderBookId").toString().toNumber();
	}' #txt
os0 f51 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f51 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>If none selected</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f51 694 516 36 24 20 -2 #rect
os0 f51 @|StepIcon #fIcon
os0 f52 expr data #txt
os0 f52 outCond ivp=="setStatisticType.ivp" #txt
os0 f52 274 496 712 516 #arcP
os0 f52 1 712 496 #addKink
os0 f52 0 0.5409210082546029 0 0 #arcLabel
os0 f53 expr out #txt
os0 f53 712 540 274 632 #arcP
os0 f53 1 712 632 #addKink
os0 f53 1 0.3840150119168502 0 0 #arcLabel
os0 f54 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f54 processCall 'Functional Processes/orderbook/OrderStatistics:byDayAndField(String,Integer,Integer,Integer,Integer)' #txt
os0 f54 doCall true #txt
os0 f54 requestActionDecl '<java.lang.String additionalField,java.lang.Integer forOrderBookId,java.lang.Integer forDay,java.lang.Integer forMonth,java.lang.Integer forYear> param;
' #txt
os0 f54 requestMappingAction 'param.additionalField=in.selectAdditionalField;
param.forOrderBookId=in.orderBookId;
param.forDay=in.forDay;
param.forMonth=in.forMonth;
param.forYear=in.forYear;
' #txt
os0 f54 responseActionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f54 responseMappingAction 'out=in;
out.rsOrderStatistics=result.rsOrderStatistics;
' #txt
os0 f54 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Statistics of Choosen</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f54 238 796 36 24 20 -5 #rect
os0 f54 @|CallSubIcon #fIcon
os0 f56 expr out #txt
os0 f56 256 820 256 852 #arcP
os0 f57 actionDecl 'ch.soreco.orderbook.ui.OrderStatistics out;
' #txt
os0 f57 actionTable 'out=in;
' #txt
os0 f57 actionCode 'if(in.additionalFieldName.trim().equals("")){
	in.additionalFieldName = "orderState";
	in.selectAdditionalField = "ISNULL(orderState, '''') as "+in.additionalFieldsMap.get("orderState") as String;
	}
else {
	if(in.additionalFieldName.equals("priority")){
		in.selectAdditionalField = "ISNULL("+in.additionalFieldName+", 0) as "+in.additionalFieldsMap.get(in.additionalFieldName) as String;
		}
	else {
		in.selectAdditionalField = "ISNULL("+in.additionalFieldName+", '''') as "+in.additionalFieldsMap.get(in.additionalFieldName) as String;
		}
	}
' #txt
os0 f57 type ch.soreco.orderbook.ui.OrderStatistics #txt
os0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set additional field to orderState as Status</name>
        <nameStyle>44
</nameStyle>
    </language>
</elementInfo>
' #txt
os0 f57 238 748 36 24 20 -2 #rect
os0 f57 @|StepIcon #fIcon
os0 f58 expr in #txt
os0 f58 outCond 'in.statisticType=="Offene nach Feld"' #txt
os0 f58 256 734 256 748 #arcP
os0 f55 expr out #txt
os0 f55 256 772 256 796 #arcP
>Proto os0 .type ch.soreco.orderbook.ui.OrderStatistics #txt
>Proto os0 .processKind NORMAL #txt
>Proto os0 0 0 32 24 18 0 #rect
>Proto os0 @|BIcon #fIcon
os0 f13 mainOut f10 tail #connect
os0 f10 head f9 mainIn #connect
os0 f2 head f1 mainIn #connect
os0 f0 mainOut f16 tail #connect
os0 f16 head f45 mainIn #connect
os0 f45 mainOut f6 tail #connect
os0 f6 head f5 mainIn #connect
os0 f3 mainOut f18 tail #connect
os0 f18 head f17 mainIn #connect
os0 f17 mainOut f15 tail #connect
os0 f15 head f13 mainIn #connect
os0 f19 mainOut f8 tail #connect
os0 f8 head f7 mainIn #connect
os0 f24 head f23 mainIn #connect
os0 f23 mainOut f22 tail #connect
os0 f22 head f21 mainIn #connect
os0 f21 mainOut f25 tail #connect
os0 f25 head f66 mainIn #connect
os0 f26 mainOut f29 tail #connect
os0 f29 head f28 mainIn #connect
os0 f9 out f30 tail #connect
os0 f30 head f11 mainIn #connect
os0 f9 out f2 tail #connect
os0 f9 out f24 tail #connect
os0 f11 mainOut f12 tail #connect
os0 f12 head f26 mainIn #connect
os0 f9 out f32 tail #connect
os0 f32 head f31 mainIn #connect
os0 f31 mainOut f36 tail #connect
os0 f36 head f35 mainIn #connect
os0 f35 mainOut f4 tail #connect
os0 f4 head f26 mainIn #connect
os0 f5 mainOut f33 tail #connect
os0 f33 head f27 mainIn #connect
os0 f27 mainOut f20 tail #connect
os0 f20 head f19 mainIn #connect
os0 f38 mainOut f39 tail #connect
os0 f39 head f34 mainIn #connect
os0 f9 out f40 tail #connect
os0 f40 head f38 mainIn #connect
os0 f34 mainOut f41 tail #connect
os0 f41 head f26 mainIn #connect
os0 f7 mainOut f43 tail #connect
os0 f43 head f42 in #connect
os0 f42 out f44 tail #connect
os0 f44 head f31 mainIn #connect
os0 f42 out f14 tail #connect
os0 f14 head f13 mainIn #connect
os0 f28 mainOut f48 tail #connect
os0 f48 head f47 in #connect
os0 f47 out f37 tail #connect
os0 f37 head f3 mainIn #connect
os0 f47 out f49 tail #connect
os0 f49 head f46 mainIn #connect
os0 f46 mainOut f50 tail #connect
os0 f50 head f17 mainIn #connect
os0 f9 out f52 tail #connect
os0 f52 head f51 mainIn #connect
os0 f51 mainOut f53 tail #connect
os0 f53 head f26 mainIn #connect
os0 f54 mainOut f56 tail #connect
os0 f56 head f17 mainIn #connect
os0 f47 out f58 tail #connect
os0 f58 head f57 mainIn #connect
os0 f57 mainOut f55 tail #connect
os0 f55 head f54 mainIn #connect
