[Ivy]
[>Created: Wed Sep 04 15:21:59 CEST 2013]
1313CD386D98EB94 3.15 #module
>Proto >Proto Collection #zClass
Oe0 OrderMassUpdate Big #zClass
Oe0 B #cInfo
Oe0 #process
Oe0 @TextInP .resExport .resExport #zField
Oe0 @TextInP .type .type #zField
Oe0 @TextInP .processKind .processKind #zField
Oe0 @AnnotationInP-0n ai ai #zField
Oe0 @TextInP .xml .xml #zField
Oe0 @TextInP .responsibility .responsibility #zField
Oe0 @StartSub f0 '' #zField
Oe0 @EndSub f1 '' #zField
Oe0 @Page f2 '' #zField
Oe0 @Alternative f11 '' #zField
Oe0 @PushWFArc f12 '' #zField
Oe0 @CallSub f13 '' #zField
Oe0 @GridStep f14 '' #zField
Oe0 @PushWFArc f16 '' #zField
Oe0 @PushWFArc f17 '' #zField
Oe0 @PushWFArc f4 '' #zField
Oe0 @CallSub f18 '' #zField
Oe0 @PushWFArc f19 '' #zField
Oe0 @PushWFArc f15 '' #zField
Oe0 @GridStep f20 '' #zField
Oe0 @PushWFArc f21 '' #zField
Oe0 @Alternative f8 '' #zField
Oe0 @CallSub f9 '' #zField
Oe0 @GridStep f10 '' #zField
Oe0 @CallSub f22 '' #zField
Oe0 @PushWFArc f23 '' #zField
Oe0 @PushWFArc f24 '' #zField
Oe0 @PushWFArc f25 '' #zField
Oe0 @PushWFArc f7 '' #zField
Oe0 @PushWFArc f28 '' #zField
Oe0 @GridStep f3 '' #zField
Oe0 @PushWFArc f5 '' #zField
Oe0 @PushWFArc f26 '' #zField
Oe0 @GridStep f27 '' #zField
Oe0 @PushWFArc f29 '' #zField
Oe0 @PushWFArc f6 '' #zField
>Proto Oe0 Oe0 OrderMassUpdate #zField
Oe0 f0 inParamDecl '<List<java.lang.String> orderIdList> param;' #txt
Oe0 f0 inParamTable 'out.orderIdList=param.orderIdList;
' #txt
Oe0 f0 outParamDecl '<> result;
' #txt
Oe0 f0 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f0 callSignature updateOrders(List<String>) #txt
Oe0 f0 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>updateOrders</name>
        <nameStyle>12,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f0 83 35 26 26 14 0 #rect
Oe0 f0 @|StartSubIcon #fIcon
Oe0 f1 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f1 811 403 26 26 14 0 #rect
Oe0 f1 @|EndSubIcon #fIcon
Oe0 f2 outTypes "ch.soreco.orderbook.ui.OrderMassUpdate","ch.soreco.orderbook.ui.OrderMassUpdate" #txt
Oe0 f2 outLinks "update.ivp","updateLight.ivp" #txt
Oe0 f2 template "updateOrders.ivc" #txt
Oe0 f2 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f2 skipLink skip.ivp #txt
Oe0 f2 sortLink sort.ivp #txt
Oe0 f2 templateWizard '#
#Wed Sep 04 15:18:44 CEST 2013
' #txt
Oe0 f2 pageArchivingActivated false #txt
Oe0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Update Data Dialog</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f2 @C|.responsibility Everybody #txt
Oe0 f2 78 260 36 24 20 -2 #rect
Oe0 f2 @|PageIcon #fIcon
Oe0 f11 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f11 258 442 28 28 14 0 #rect
Oe0 f11 @|AlternativeIcon #fIcon
Oe0 f12 expr data #txt
Oe0 f12 outCond ivp=="update.ivp" #txt
Oe0 f12 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>full</name>
        <nameStyle>4
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f12 96 284 258 456 #arcP
Oe0 f12 1 96 456 #addKink
Oe0 f12 1 0.30246913580246915 0 -6 #arcLabel
Oe0 f13 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f13 processCall 'Functional Processes/orderbook/Order:setPlainOrder(ch.soreco.orderbook.bo.Order)' #txt
Oe0 f13 doCall true #txt
Oe0 f13 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Oe0 f13 requestMappingAction 'param.order=in.orderCopy;
param.order.checkerUserId=in.order.checkerUserId.equalsIgnoreCase("") ? in.orderCopy.checkerUserId : in.order.checkerUserId;
param.order.dispatcherUserId=in.order.dispatcherUserId.equalsIgnoreCase("") ? in.orderCopy.dispatcherUserId : in.order.dispatcherUserId;
param.order.editorUserId=in.order.editorUserId.equalsIgnoreCase("") ? in.orderCopy.editorUserId : in.order.editorUserId;
param.order.orderId=in.orderIdList.get(in.i).toString().toNumber();
param.order.orderQuantityType=in.order.orderQuantityType.equalsIgnoreCase("") ? in.orderCopy.orderQuantityType : in.order.orderQuantityType;
param.order.priority=in.order.priority == 0 || in.order.priority == null ? in.orderCopy.#priority: in.order.priority;
' #txt
Oe0 f13 responseActionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f13 responseMappingAction 'out=in;
' #txt
Oe0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setPlainOrder(Order)</name>
        <nameStyle>20,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f13 518 492 36 24 20 -2 #rect
Oe0 f13 @|CallSubIcon #fIcon
Oe0 f14 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f14 actionTable 'out=in;
out.i=in.i + 1;
' #txt
Oe0 f14 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f14 518 444 36 24 20 -2 #rect
Oe0 f14 @|StepIcon #fIcon
Oe0 f16 expr out #txt
Oe0 f16 536 492 536 468 #arcP
Oe0 f17 expr out #txt
Oe0 f17 518 456 286 456 #arcP
Oe0 f4 expr in #txt
Oe0 f4 272 442 811 416 #arcP
Oe0 f4 1 272 416 #addKink
Oe0 f4 1 0.20906619108023114 0 0 #arcLabel
Oe0 f18 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f18 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Oe0 f18 doCall true #txt
Oe0 f18 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Oe0 f18 requestMappingAction 'param.filter.orderId=in.orderIdList.get(in.i).toString().toNumber();
' #txt
Oe0 f18 responseActionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f18 responseMappingAction 'out=in;
out.orderCopy=result.order;
' #txt
Oe0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f18 358 492 36 24 20 -2 #rect
Oe0 f18 @|CallSubIcon #fIcon
Oe0 f19 expr in #txt
Oe0 f19 outCond 'in.i < in.orderIdList.size()' #txt
Oe0 f19 279 463 358 504 #arcP
Oe0 f19 1 320 504 #addKink
Oe0 f19 1 0.2827160146454308 0 0 #arcLabel
Oe0 f15 expr out #txt
Oe0 f15 394 504 518 504 #arcP
Oe0 f20 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f20 actionTable 'out=in;
' #txt
Oe0 f20 actionCode 'import ch.soreco.orderbook.util.Authority;
import java.util.Collections;

// build user list
List userStringList;
userStringList.clear();
Authority auth = Authority.getInstance();
userStringList.add("");
userStringList.addAll(auth.getEditorUsers());
Collections.sort(userStringList, String.CASE_INSENSITIVE_ORDER);

out.checkers = userStringList;
out.dispatchers = userStringList;
out.editors = userStringList;

// set priorities

in.priorities.clear();
in.priorities.add("");
in.priorities.add("1");
in.priorities.add("2");
in.priorities.add("3");
in.priorities.add("4");
in.priorities.add("5");


// check role
List roleList = ivy.session.getSessionUser().getAllRoles();
if(roleList.contains(ivy.wf.getSecurityContext().findRole("TeamLeader"))){
	out.mode = "full";
}' #txt
Oe0 f20 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get all users
get priorities
check role</name>
        <nameStyle>39,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f20 78 92 36 24 20 -2 #rect
Oe0 f20 @|StepIcon #fIcon
Oe0 f21 expr out #txt
Oe0 f21 96 61 96 92 #arcP
Oe0 f8 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f8 258 322 28 28 14 0 #rect
Oe0 f8 @|AlternativeIcon #fIcon
Oe0 f9 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f9 processCall 'Functional Processes/orderbook/Order:setPlainOrder(ch.soreco.orderbook.bo.Order)' #txt
Oe0 f9 doCall true #txt
Oe0 f9 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Oe0 f9 requestMappingAction 'param.order=in.orderCopy;
param.order.orderId=in.orderIdList.get(in.i).toString().toNumber();
param.order.priority=in.order.priority == 0 ? in.orderCopy.priority : in.order.priority;
' #txt
Oe0 f9 responseActionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f9 responseMappingAction 'out=in;
' #txt
Oe0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setPlainOrder(Order)</name>
        <nameStyle>20,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f9 574 372 36 24 20 -2 #rect
Oe0 f9 @|CallSubIcon #fIcon
Oe0 f10 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f10 actionTable 'out=in;
out.i=in.i + 1;
' #txt
Oe0 f10 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f10 574 324 36 24 20 -2 #rect
Oe0 f10 @|StepIcon #fIcon
Oe0 f22 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f22 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Oe0 f22 doCall true #txt
Oe0 f22 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Oe0 f22 requestMappingAction 'param.filter.orderId=in.orderIdList.get(in.i).toString().toNumber();
' #txt
Oe0 f22 responseActionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f22 responseMappingAction 'out=in;
out.orderCopy=result.order;
' #txt
Oe0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f22 318 372 36 24 20 -2 #rect
Oe0 f22 @|CallSubIcon #fIcon
Oe0 f23 expr out #txt
Oe0 f23 592 372 592 348 #arcP
Oe0 f24 expr out #txt
Oe0 f24 574 336 286 336 #arcP
Oe0 f25 expr in #txt
Oe0 f25 outCond 'in.i < in.orderIdList.size()' #txt
Oe0 f25 278 344 318 384 #arcP
Oe0 f25 1 304 384 #addKink
Oe0 f25 1 0.10654016109504169 0 0 #arcLabel
Oe0 f7 expr in #txt
Oe0 f7 272 350 811 416 #arcP
Oe0 f7 1 272 416 #addKink
Oe0 f7 1 0.721761662103899 0 0 #arcLabel
Oe0 f28 expr data #txt
Oe0 f28 outCond ivp=="updateLight.ivp" #txt
Oe0 f28 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>light</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f28 96 284 258 336 #arcP
Oe0 f28 1 96 336 #addKink
Oe0 f28 1 0.30864197530864196 0 -7 #arcLabel
Oe0 f3 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f3 actionTable 'out=in;
' #txt
Oe0 f3 actionCode 'import ch.soreco.orderbook.enums.OrderState;
/*
// no dispatcher => set dispatcher
if(in.orderCopy.dispatcherUserId.length() == 0){
		out.orderCopy.dispatcherUserId = in.order.editorUserId;
} else

// dispatcher but no editor and checker => set editor
if(in.orderCopy.dispatcherUserId.length() > 0 
	&& in.orderCopy.editorUserId.length() == 0 
	&& in.orderCopy.checkerUserId.length() == 0){
		out.orderCopy.editorUserId = in.order.editorUserId;
} else

// editor but no checker => set checker
if(in.orderCopy.editorUserId.length() > 0 && in.orderCopy.checkerUserId.length() == 0){
		in.orderCopy.checkerUserId = in.order.editorUserId;
} else

// editor and checker => set editor
if(in.orderCopy.editorUserId.length() > 0 && in.orderCopy.checkerUserId.length() > 0){
		in.orderCopy.editorUserId = in.order.editorUserId;
}
*/


if(in.orderCopy.orderState.equalsIgnoreCase(OrderState.InDispatchingNew.name())){
		out.orderCopy.editorUserId = in.order.editorUserId;
} else
if(in.orderCopy.orderState.equalsIgnoreCase(OrderState.Dispatched.name())){
	out.orderCopy.editorUserId = in.order.editorUserId;	
} else
if(in.orderCopy.orderState.equalsIgnoreCase(OrderState.InDispatchingOpen.name())){
		out.orderCopy.dispatcherUserId = in.order.editorUserId;
} else
if(in.orderCopy.orderState.equalsIgnoreCase(OrderState.Recording.name())){
	out.orderCopy.editorUserId = in.order.editorUserId;	
} else
if(in.orderCopy.orderState.equalsIgnoreCase(OrderState.In4eCheck.name())){
	out.orderCopy.checkerUserId = in.order.editorUserId;	
} else
if(in.orderCopy.orderState.contains("Check")){
	out.orderCopy.checkerUserId = in.order.editorUserId;	
} 
else {
	out.orderCopy.editorUserId = in.order.editorUserId;	
}' #txt
Oe0 f3 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set next user</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f3 446 372 36 24 20 -2 #rect
Oe0 f3 @|StepIcon #fIcon
Oe0 f5 expr out #txt
Oe0 f5 354 384 446 384 #arcP
Oe0 f26 expr out #txt
Oe0 f26 482 384 574 384 #arcP
Oe0 f27 actionDecl 'ch.soreco.orderbook.ui.OrderMassUpdate out;
' #txt
Oe0 f27 actionTable 'out=in;
' #txt
Oe0 f27 actionCode 'import ch.soreco.webbies.common.html.form.AttributeClass;
import ch.soreco.orderbook.enums.OrderQuantityType;
import java.util.HashMap;

AttributeClass acOfOrderQuantityType = new AttributeClass(OrderQuantityType.class);
HashMap mapOfOrderQuantityType = acOfOrderQuantityType.getLabelsAndIcons();
Recordset rs = new Recordset(["Key", "Label"]);
rs.add([null, ""]);
for(String key:mapOfOrderQuantityType.keySet()){
	rs.add([key, mapOfOrderQuantityType.get(key).toString()]);
	}
in.orderQuantityTypes = rs;' #txt
Oe0 f27 type ch.soreco.orderbook.ui.OrderMassUpdate #txt
Oe0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get OrderQuantityTypes</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
Oe0 f27 78 164 36 24 20 -2 #rect
Oe0 f27 @|StepIcon #fIcon
Oe0 f29 expr out #txt
Oe0 f29 96 116 96 164 #arcP
Oe0 f6 expr out #txt
Oe0 f6 96 188 96 260 #arcP
>Proto Oe0 .type ch.soreco.orderbook.ui.OrderMassUpdate #txt
>Proto Oe0 .processKind CALLABLE_SUB #txt
>Proto Oe0 0 0 32 24 18 0 #rect
>Proto Oe0 @|BIcon #fIcon
Oe0 f2 out f12 tail #connect
Oe0 f12 head f11 in #connect
Oe0 f13 mainOut f16 tail #connect
Oe0 f16 head f14 mainIn #connect
Oe0 f14 mainOut f17 tail #connect
Oe0 f17 head f11 in #connect
Oe0 f4 head f1 mainIn #connect
Oe0 f11 out f19 tail #connect
Oe0 f19 head f18 mainIn #connect
Oe0 f11 out f4 tail #connect
Oe0 f18 mainOut f15 tail #connect
Oe0 f15 head f13 mainIn #connect
Oe0 f0 mainOut f21 tail #connect
Oe0 f21 head f20 mainIn #connect
Oe0 f9 mainOut f23 tail #connect
Oe0 f23 head f10 mainIn #connect
Oe0 f10 mainOut f24 tail #connect
Oe0 f24 head f8 in #connect
Oe0 f8 out f25 tail #connect
Oe0 f25 head f22 mainIn #connect
Oe0 f8 out f7 tail #connect
Oe0 f7 head f1 mainIn #connect
Oe0 f2 out f28 tail #connect
Oe0 f28 head f8 in #connect
Oe0 f22 mainOut f5 tail #connect
Oe0 f5 head f3 mainIn #connect
Oe0 f3 mainOut f26 tail #connect
Oe0 f26 head f9 mainIn #connect
Oe0 f20 mainOut f29 tail #connect
Oe0 f29 head f27 mainIn #connect
Oe0 f27 mainOut f6 tail #connect
Oe0 f6 head f2 mainIn #connect
