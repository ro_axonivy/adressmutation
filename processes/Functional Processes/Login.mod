[Ivy]
[>Created: Fri Oct 17 16:19:47 CEST 2014]
12B8CFCFB35D2820 3.17 #module
>Proto >Proto Collection #zClass
Ln0 Login Big #zClass
Ln0 B #cInfo
Ln0 #process
Ln0 @TextInP .resExport .resExport #zField
Ln0 @TextInP .type .type #zField
Ln0 @TextInP .processKind .processKind #zField
Ln0 @AnnotationInP-0n ai ai #zField
Ln0 @TextInP .xml .xml #zField
Ln0 @TextInP .responsibility .responsibility #zField
Ln0 @StartSub f0 '' #zField
Ln0 @EndSub f1 '' #zField
Ln0 @Page f2 '' #zField
Ln0 @GridStep f5 '' #zField
Ln0 @PushWFArc f6 '' #zField
Ln0 @Alternative f7 '' #zField
Ln0 @PushWFArc f4 '' #zField
Ln0 @GridStep f10 '' #zField
Ln0 @PushWFArc f11 '' #zField
Ln0 @PushWFArc f9 '' #zField
Ln0 @Alternative f14 '' #zField
Ln0 @PushWFArc f16 '' #zField
Ln0 @PushWFArc f17 '' #zField
Ln0 @StartSub f12 '' #zField
Ln0 @EndSub f15 '' #zField
Ln0 @GridStep f29 '' #zField
Ln0 @PushWFArc f30 '' #zField
Ln0 @GridStep f22 '' #zField
Ln0 @PushWFArc f28 '' #zField
Ln0 @PushWFArc f8 '' #zField
Ln0 @GridStep f34 '' #zField
Ln0 @GridStep f35 '' #zField
Ln0 @Page f36 '' #zField
Ln0 @GridStep f37 '' #zField
Ln0 @GridStep f38 '' #zField
Ln0 @Alternative f39 '' #zField
Ln0 @PushWFArc f40 '' #zField
Ln0 @PushWFArc f42 '' #zField
Ln0 @PushWFArc f43 '' #zField
Ln0 @PushWFArc f44 '' #zField
Ln0 @PushWFArc f45 '' #zField
Ln0 @PushWFArc f46 '' #zField
Ln0 @PushWFArc f19 '' #zField
Ln0 @GridStep f18 '' #zField
Ln0 @PushWFArc f20 '' #zField
Ln0 @PushWFArc f13 '' #zField
Ln0 @PushWFArc f24 '' #zField
>Proto Ln0 Ln0 Login #zField
Ln0 f0 outParamDecl '<> result;
' #txt
Ln0 f0 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f0 callSignature call() #txt
Ln0 f0 type adressmutation.Data #txt
Ln0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f0 211 39 26 26 14 0 #rect
Ln0 f0 @|StartSubIcon #fIcon
Ln0 f1 type adressmutation.Data #txt
Ln0 f1 211 547 26 26 14 0 #rect
Ln0 f1 @|EndSubIcon #fIcon
Ln0 f2 outTypes "adressmutation.Data" #txt
Ln0 f2 outLinks "submit.ivp" #txt
Ln0 f2 template "/ProcessPages/Login/Login.ivc" #txt
Ln0 f2 type adressmutation.Data #txt
Ln0 f2 skipLink skip.ivp #txt
Ln0 f2 sortLink sort.ivp #txt
Ln0 f2 templateWizard '#
#Sun Oct 31 14:31:43 CET 2010
' #txt
Ln0 f2 pageArchivingActivated false #txt
Ln0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Login</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f2 @C|.responsibility Everybody #txt
Ln0 f2 206 300 36 24 20 -2 #rect
Ln0 f2 @|PageIcon #fIcon
Ln0 f5 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f5 actionTable 'out=in;
' #txt
Ln0 f5 actionCode '/*
import ch.ivyteam.ivy.security.AuthenticationException;
import ch.ivyteam.ivy.security.ISession;
import ch.ivyteam.security.Password;

ISession session = ivy.session;
	try
	{
		session.authenticateSessionUser(in.user, new Password(in.passwort));
		out.success = true;
	}
	catch(AuthenticationException ex)
	{
		out.success = false;
		out.error = ex.getMessage();
	}
*/
ivy.session.loginSessionUser(in.user, in.passwort);
ch.ivyteam.ivy.request.impl.HttpProcessRequest httpRequest = ivy.request as ch.ivyteam.ivy.request.impl.HttpProcessRequest;
httpRequest.getHttpServletRequest()
	.getSession()
	.setAttribute("ch.ivy.wfui.returnUrl", ivy.html.startref("12B1A071447AFADE/index.ivp"));' #txt
Ln0 f5 type adressmutation.Data #txt
Ln0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check Login</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f5 206 340 36 24 20 -2 #rect
Ln0 f5 @|StepIcon #fIcon
Ln0 f6 expr data #txt
Ln0 f6 outCond ivp=="submit.ivp" #txt
Ln0 f6 224 324 224 340 #arcP
Ln0 f7 type adressmutation.Data #txt
Ln0 f7 210 458 28 28 14 0 #rect
Ln0 f7 @|AlternativeIcon #fIcon
Ln0 f4 expr in #txt
Ln0 f4 outCond !ivy.session.isSessionUserUnknown() #txt
Ln0 f4 6 #arcStyle
Ln0 f4 224 486 224 547 #arcP
Ln0 f10 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f10 actionTable 'out=in;
out.error="User or Password is not correct";
' #txt
Ln0 f10 type adressmutation.Data #txt
Ln0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>user/passwort not correct</name>
        <nameStyle>25,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f10 326 372 36 24 20 -2 #rect
Ln0 f10 @|StepIcon #fIcon
Ln0 f11 expr in #txt
Ln0 f11 3 #arcStyle
Ln0 f11 238 472 344 396 #arcP
Ln0 f11 1 344 472 #addKink
Ln0 f11 1 0.3116883116883117 0 0 #arcLabel
Ln0 f9 expr out #txt
Ln0 f9 344 372 242 312 #arcP
Ln0 f9 1 344 312 #addKink
Ln0 f9 1 0.115496077547579 0 0 #arcLabel
Ln0 f14 type adressmutation.Data #txt
Ln0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isUnknown?</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f14 210 114 28 28 14 0 #rect
Ln0 f14 @|AlternativeIcon #fIcon
Ln0 f16 expr in #txt
Ln0 f16 210 128 211 560 #arcP
Ln0 f16 1 128 128 #addKink
Ln0 f16 2 128 560 #addKink
Ln0 f16 1 0.39252910908689376 0 0 #arcLabel
Ln0 f17 expr out #txt
Ln0 f17 224 65 224 114 #arcP
Ln0 f12 outParamDecl '<> result;
' #txt
Ln0 f12 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f12 callSignature login() #txt
Ln0 f12 type adressmutation.Data #txt
Ln0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>login</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f12 691 43 26 26 14 0 #rect
Ln0 f12 @|StartSubIcon #fIcon
Ln0 f15 type adressmutation.Data #txt
Ln0 f15 691 491 26 26 14 0 #rect
Ln0 f15 @|EndSubIcon #fIcon
Ln0 f29 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f29 actionTable 'out=in;
' #txt
Ln0 f29 actionCode 'import java.util.Enumeration;
import javax.servlet.http.HttpSession;

in.liEnvironments.clear();
in.liEnvironments.add("b2p.soreco.ch");

ch.ivyteam.ivy.request.impl.HttpProcessRequest request = ivy.request as ch.ivyteam.ivy.request.impl.HttpProcessRequest;
HttpSession httpSession = request.getHttpServletRequest().getSession();

ivy.log.debug("ivy.request.requestPath:"+ivy.request.getRequestPath());
Enumeration attributeNames = httpSession.getAttributeNames();
while(attributeNames.hasMoreElements()){
	String name = attributeNames.nextElement().toString();
	ivy.log.debug("session.attribute."+name+":"+httpSession.getAttribute(name));
	}




' #txt
Ln0 f29 type adressmutation.Data #txt
Ln0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f29 206 172 36 24 19 -2 #rect
Ln0 f29 @|StepIcon #fIcon
Ln0 f30 expr in #txt
Ln0 f30 outCond 'ivy.session.isSessionUserUnknown()
||ivy.session.getSessionUser().getName().equals("Developer")' #txt
Ln0 f30 224 142 224 172 #arcP
Ln0 f22 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f22 actionTable 'out=in;
' #txt
Ln0 f22 actionCode 'if(in.currentEnvironment!=ivy.session.activeEnvironment){
	ivy.session.setActiveEnvironment(in.currentEnvironment);
	}
' #txt
Ln0 f22 type adressmutation.Data #txt
Ln0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f22 206 388 36 24 20 -2 #rect
Ln0 f22 @|StepIcon #fIcon
Ln0 f28 expr out #txt
Ln0 f28 224 364 224 388 #arcP
Ln0 f8 expr out #txt
Ln0 f8 224 412 224 458 #arcP
Ln0 f34 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f34 actionTable 'out=in;
' #txt
Ln0 f34 actionCode 'if(in.currentEnvironment!=ivy.session.activeEnvironment){
	ivy.session.setActiveEnvironment(in.currentEnvironment);
	}
' #txt
Ln0 f34 type adressmutation.Data #txt
Ln0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f34 686 332 36 24 20 -2 #rect
Ln0 f34 @|StepIcon #fIcon
Ln0 f35 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f35 actionTable 'out=in;
' #txt
Ln0 f35 actionCode 'in.liEnvironments.clear();
//in.liEnvironments.add("SWBNBBST");
in.liEnvironments.add("b2p.soreco.ch");
in.liEnvironments.add("b2p.ai.ch");

' #txt
Ln0 f35 type adressmutation.Data #txt
Ln0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Environment</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f35 686 172 36 24 19 -2 #rect
Ln0 f35 @|StepIcon #fIcon
Ln0 f36 outTypes "adressmutation.Data" #txt
Ln0 f36 outLinks "submit.ivp" #txt
Ln0 f36 template "/ProcessPages/Login/Login.ivc" #txt
Ln0 f36 type adressmutation.Data #txt
Ln0 f36 skipLink skip.ivp #txt
Ln0 f36 sortLink sort.ivp #txt
Ln0 f36 templateWizard '#
#Fri Oct 04 13:26:48 CEST 2013
' #txt
Ln0 f36 pageArchivingActivated false #txt
Ln0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Login</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f36 @C|.responsibility Everybody #txt
Ln0 f36 686 244 36 24 20 -2 #rect
Ln0 f36 @|PageIcon #fIcon
Ln0 f37 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f37 actionTable 'out=in;
' #txt
Ln0 f37 actionCode 'import ch.ivyteam.ivy.security.AuthenticationException;
import ch.ivyteam.ivy.security.ISession;
import ch.ivyteam.security.Password;

ISession session = ivy.session;
	try
	{
		session.authenticateSessionUser(in.user, new Password(in.passwort));
		out.success = true;
	}
	catch(AuthenticationException ex)
	{
		out.success = false;
		out.error = ex.getMessage();
	}
' #txt
Ln0 f37 type adressmutation.Data #txt
Ln0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check Login</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f37 686 284 36 24 20 -2 #rect
Ln0 f37 @|StepIcon #fIcon
Ln0 f38 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f38 actionTable 'out=in;
out.error="User or Password is not correct";
' #txt
Ln0 f38 type adressmutation.Data #txt
Ln0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>user/passwort not correct</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f38 822 316 36 24 20 -2 #rect
Ln0 f38 @|StepIcon #fIcon
Ln0 f39 type adressmutation.Data #txt
Ln0 f39 690 402 28 28 14 0 #rect
Ln0 f39 @|AlternativeIcon #fIcon
Ln0 f40 expr data #txt
Ln0 f40 outCond ivp=="submit.ivp" #txt
Ln0 f40 704 268 704 284 #arcP
Ln0 f42 expr out #txt
Ln0 f42 840 316 722 256 #arcP
Ln0 f42 1 840 256 #addKink
Ln0 f42 1 0.2274175391805671 0 0 #arcLabel
Ln0 f43 expr out #txt
Ln0 f43 704 196 704 244 #arcP
Ln0 f44 expr out #txt
Ln0 f44 704 308 704 332 #arcP
Ln0 f45 expr out #txt
Ln0 f45 704 356 704 402 #arcP
Ln0 f46 expr out #txt
Ln0 f46 704 69 704 172 #arcP
Ln0 f46 0 0.5053109320290192 0 0 #arcLabel
Ln0 f19 expr in #txt
Ln0 f19 3 #arcStyle
Ln0 f19 718 416 840 340 #arcP
Ln0 f19 1 840 416 #addKink
Ln0 f19 0 0.5519268020426328 0 0 #arcLabel
Ln0 f18 actionDecl 'adressmutation.Data out;
' #txt
Ln0 f18 actionTable 'out=in;
out.error="Login for "+ivy.session.getSessionUser().fullName+" Successful";
' #txt
Ln0 f18 type adressmutation.Data #txt
Ln0 f18 598 316 36 24 20 -2 #rect
Ln0 f18 @|StepIcon #fIcon
Ln0 f20 expr in #txt
Ln0 f20 outCond in.success #txt
Ln0 f20 6 #arcStyle
Ln0 f20 690 416 616 340 #arcP
Ln0 f20 1 616 416 #addKink
Ln0 f20 1 0.6552918744414226 0 0 #arcLabel
Ln0 f13 expr out #txt
Ln0 f13 616 316 686 256 #arcP
Ln0 f13 1 616 256 #addKink
Ln0 f13 1 0.06624392811522836 0 0 #arcLabel
Ln0 f24 expr out #txt
Ln0 f24 224 196 224 300 #arcP
>Proto Ln0 .type adressmutation.Data #txt
>Proto Ln0 .processKind CALLABLE_SUB #txt
>Proto Ln0 0 0 32 24 18 0 #rect
>Proto Ln0 @|BIcon #fIcon
Ln0 f2 out f6 tail #connect
Ln0 f6 head f5 mainIn #connect
Ln0 f7 out f4 tail #connect
Ln0 f4 head f1 mainIn #connect
Ln0 f7 out f11 tail #connect
Ln0 f11 head f10 mainIn #connect
Ln0 f10 mainOut f9 tail #connect
Ln0 f9 head f2 mainIn #connect
Ln0 f16 head f1 mainIn #connect
Ln0 f0 mainOut f17 tail #connect
Ln0 f17 head f14 in #connect
Ln0 f14 out f30 tail #connect
Ln0 f30 head f29 mainIn #connect
Ln0 f14 out f16 tail #connect
Ln0 f5 mainOut f28 tail #connect
Ln0 f28 head f22 mainIn #connect
Ln0 f22 mainOut f8 tail #connect
Ln0 f8 head f7 in #connect
Ln0 f36 out f40 tail #connect
Ln0 f40 head f37 mainIn #connect
Ln0 f38 mainOut f42 tail #connect
Ln0 f42 head f36 mainIn #connect
Ln0 f35 mainOut f43 tail #connect
Ln0 f43 head f36 mainIn #connect
Ln0 f37 mainOut f44 tail #connect
Ln0 f44 head f34 mainIn #connect
Ln0 f34 mainOut f45 tail #connect
Ln0 f45 head f39 in #connect
Ln0 f12 mainOut f46 tail #connect
Ln0 f46 head f35 mainIn #connect
Ln0 f19 head f38 mainIn #connect
Ln0 f39 out f20 tail #connect
Ln0 f20 head f18 mainIn #connect
Ln0 f39 out f19 tail #connect
Ln0 f18 mainOut f13 tail #connect
Ln0 f13 head f36 mainIn #connect
Ln0 f29 mainOut f24 tail #connect
Ln0 f24 head f2 mainIn #connect
