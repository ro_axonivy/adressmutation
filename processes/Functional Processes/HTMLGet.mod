[Ivy]
[>Created: Wed Oct 22 16:02:57 CEST 2014]
12B81D3D9C96DD14 3.17 #module
>Proto >Proto Collection #zClass
Ht0 HTMLGet Big #zClass
Ht0 B #cInfo
Ht0 #process
Ht0 @TextInP .resExport .resExport #zField
Ht0 @TextInP .type .type #zField
Ht0 @TextInP .processKind .processKind #zField
Ht0 @AnnotationInP-0n ai ai #zField
Ht0 @TextInP .xml .xml #zField
Ht0 @TextInP .responsibility .responsibility #zField
Ht0 @StartSub f0 '' #zField
Ht0 @EndSub f1 '' #zField
Ht0 @GridStep f19 '' #zField
Ht0 @GridStep f27 '' #zField
Ht0 @Alternative f11 '' #zField
Ht0 @PushWFArc f28 '' #zField
Ht0 @GridStep f12 '' #zField
Ht0 @PushWFArc f34 '' #zField
Ht0 @PushWFArc f35 '' #zField
Ht0 @GridStep f7 '' #zField
Ht0 @PushWFArc f31 '' #zField
Ht0 @GridStep f5 '' #zField
Ht0 @CallSub f33 '' #zField
Ht0 @PushWFArc f36 '' #zField
Ht0 @CallSub f40 '' #zField
Ht0 @PushWFArc f41 '' #zField
Ht0 @PushWFArc f18 '' #zField
Ht0 @GridStep f21 '' #zField
Ht0 @GridStep f25 '' #zField
Ht0 @PushWFArc f26 '' #zField
Ht0 @PushWFArc f24 '' #zField
Ht0 @PushWFArc f30 '' #zField
Ht0 @GridStep f38 '' #zField
Ht0 @PushWFArc f3 '' #zField
Ht0 @GridStep f47 '' #zField
Ht0 @GridStep f17 '' #zField
Ht0 @PushWFArc f50 '' #zField
Ht0 @GridStep f51 '' #zField
Ht0 @PushWFArc f52 '' #zField
Ht0 @PushWFArc f48 '' #zField
Ht0 @CallSub f57 '' #zField
Ht0 @PushWFArc f39 '' #zField
Ht0 @InfoButton f49 '' #zField
Ht0 @PushWFArc f60 '' #zField
Ht0 @PushWFArc f29 '' #zField
>Proto Ht0 Ht0 HTMLGet #zField
Ht0 f0 inParamDecl '<java.lang.Integer currentChild,ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;' #txt
Ht0 f0 inParamTable 'out.currentChild=param.currentChild;
out.eViewType=param.viewType;
out.order=param.order;
' #txt
Ht0 f0 outParamDecl '<ch.soreco.orderbook.ui.Order orderUI> result;
' #txt
Ht0 f0 outParamTable 'result.orderUI=in;
' #txt
Ht0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f0 callSignature call(Integer,ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType) #txt
Ht0 f0 type ch.soreco.orderbook.ui.Order #txt
Ht0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order,ViewType)</name>
        <nameStyle>20,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f0 187 27 26 26 14 0 #rect
Ht0 f0 @|StartSubIcon #fIcon
Ht0 f1 type ch.soreco.orderbook.ui.Order #txt
Ht0 f1 187 915 26 26 14 0 #rect
Ht0 f1 @|EndSubIcon #fIcon
Ht0 f19 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f19 actionTable 'out=in;
' #txt
Ht0 f19 actionCode 'import ch.soreco.orderbook.ui.enums.ViewType;
Boolean canModify = false;
if(in.eViewType.equals(ViewType.EDIT)
||in.eViewType.equals(ViewType.DISPATCH)){
	canModify = true;
	}
out.modify = canModify;
' #txt
Ht0 f19 type ch.soreco.orderbook.ui.Order #txt
Ht0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Modify</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f19 182 788 36 24 20 -2 #rect
Ht0 f19 @|StepIcon #fIcon
Ht0 f27 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f27 actionTable 'out=in;
' #txt
Ht0 f27 actionCode 'import ch.soreco.orderbook.bo.ContactAddress;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Paper;

if(in.order.contacts.size()==0){
	Contact contact = new Contact();
	contact.contactOrderId = in.order.orderId;
	contact.scanFileId = in.order.scanFileId;
	contact.signatures.add(new SignatureRequest());
	contact.papers.add(new Paper());
	contact.address.addressContactId = contact.contactId;
	contact.addressCheck.addressCheckContactId = contact.contactId;
 in.order.contacts.add(contact);
}' #txt
Ht0 f27 type ch.soreco.orderbook.ui.Order #txt
Ht0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initialize new contact tab</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f27 294 380 36 24 20 -2 #rect
Ht0 f27 @|StepIcon #fIcon
Ht0 f11 type ch.soreco.orderbook.ui.Order #txt
Ht0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no contact?</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f11 186 354 28 28 13 -24 #rect
Ht0 f11 @|AlternativeIcon #fIcon
Ht0 f28 expr in #txt
Ht0 f28 outCond in.order.contacts.size()==0&&in.order.matchedToOrder>0 #txt
Ht0 f28 212 370 294 388 #arcP
Ht0 f12 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f12 actionTable 'out=in;
' #txt
Ht0 f12 actionCode 'import ch.soreco.orderbook.bo.Letter;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.webbies.common.util.IvyListSort;
import java.util.Collections;
import ch.soreco.orderbook.enums.OrderIncomeType;
import ch.soreco.orderbook.enums.StandardPaperType;
import ch.soreco.orderbook.enums.PaperType;
import ch.soreco.orderbook.bo.ContactAddress;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Paper;

in.orderContacts.clear();
in.orderContactsSignatureRequests.clear();
in.orderContactPapers.clear();
in.orderContactsLetters.clear();

for(Contact contact:in.order.contacts){
	if (contact.orderIncomeType.trim().equals("")) {
			//reset all as incomeType is not set
			contact.contactCheckedBy = "";
			contact.contactIsChecked = null;
			contact.faxOrMailReversType="";
			contact.faxReversType = "";
			contact.faxSignatureType = "";
			contact.postalSignatureType = "";
			contact.postalOriginalType = "";
		}
	if(contact.orderIncomeType.equals(OrderIncomeType.a1Postal.toString())){
			contact.faxOrMailReversType="";
			contact.faxReversType = "";
			contact.faxSignatureType = "";		
		}
	if(contact.orderIncomeType.equals(OrderIncomeType.a2FaxOrMail.toString())){
			contact.faxReversType = "";
			contact.faxSignatureType = "";		
			contact.postalSignatureType = "";
			contact.postalOriginalType = "";
		}
	if(contact.orderIncomeType.equals(OrderIncomeType.a3Fax.toString())){
			contact.faxOrMailReversType="";	
			contact.postalSignatureType = "";
			contact.postalOriginalType = "";
		}
			
	if(contact.signatures.size()==0){
		contact.signatures.add(new SignatureRequest());
	}
	for (SignatureRequest signatureRequest: contact.signatures){
		signatureRequest.contactId = contact.contactId;		
		in.orderContactsSignatureRequests.add(signatureRequest);
	}
	if(contact.papers.size()==0){
		contact.papers.add(new Paper());
	}
	//Is Not generic
	if(!in.order.orderType.equals(OrderType.Addressmutation.toString())){
		if(contact.letters.size()==0){
			contact.letters.add(new Letter());
			}
		for(Letter letter:contact.letters){
			letter.contactId = contact.contactId;
			in.orderContactsLetters.add(letter);
			}
		}
	for(Paper paper: contact.papers){
		paper.contactId = contact.contactId;		
		if(paper.paperType.equals(PaperType.isStandard.toString())){
			paper.individualPaperType = "";
			}
		else {
			paper.standardPaperType = StandardPaperType.Default.toString();
			}
		in.orderContactPapers.add(paper);
		}
	if(contact.address==null) {
		contact.address.addressContactId = contact.contactId;
		}
	contact.addressCheck.addressCheckContactId = contact.contactId;
	in.orderContacts.add(contact);
}
try {
IvyListSort sortIncomeDate = new IvyListSort("incomeDate", true);
Collections.sort(in.orderContacts,sortIncomeDate);
} catch (Exception e){
	ivy.log.debug("Error on sorting Contacts by income dates", e);
	}' #txt
Ht0 f12 type ch.soreco.orderbook.ui.Order #txt
Ht0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map contacts</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f12 182 412 36 24 20 -2 #rect
Ht0 f12 @|StepIcon #fIcon
Ht0 f34 expr in #txt
Ht0 f34 200 382 200 412 #arcP
Ht0 f35 expr out #txt
Ht0 f35 294 397 218 419 #arcP
Ht0 f35 0 0.38713087309895694 0 0 #arcLabel
Ht0 f7 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f7 actionTable 'out=in;
out.currentContact=null;
out.error=null;
out.exception=null;
out.html=null;
out.htmlBuild=null;
out.i=null;
out.modify=null;
out.orderContactPapers=null;
out.orderContacts=null;
out.orderContactsSignatureRequests=null;
out.recordingLSV=null;
out.size=null;
out.success=null;
' #txt
Ht0 f7 type ch.soreco.orderbook.ui.Order #txt
Ht0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset Objects</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f7 182 92 36 24 20 -2 #rect
Ht0 f7 @|StepIcon #fIcon
Ht0 f31 expr out #txt
Ht0 f31 200 53 200 92 #arcP
Ht0 f31 0 0.17723116381707585 0 0 #arcLabel
Ht0 f5 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f5 actionTable 'out=in;
' #txt
Ht0 f5 actionCode 'import ch.soreco.orderbook.enums.CorrespondenceType;
import ch.soreco.orderbook.bo.Correspondence;
import ch.soreco.orderbook.bo.BusinessPartner;
import ch.soreco.orderbook.bo.DomicileOrOwner;
import ch.soreco.orderbook.bo.CorrespondenceContainer;
import ch.soreco.orderbook.bo.RecordingLSV;

if(in.order.recording.businessPartner==null){
	in.order.recording.businessPartner = new BusinessPartner();
	}
if(in.order.recording.domicileOrOwner.size()==0){
	in.order.recording.domicileOrOwner.add(new DomicileOrOwner());
	}
if(in.order.recording.correspondence==null) {
	in.order.recording.correspondence = new Correspondence();
	}
if(in.order.recording.correspondence.correspondenceContainer.size()==0){
	in.order.recording.correspondence.correspondenceContainer.add(new CorrespondenceContainer());
	}
if(in.order.recording.lsv.size()==0){
	in.order.recording.lsv.add(new RecordingLSV());
	}
if(in.order.recording.affirmation==null){
	in.order.recording.affirmation;	
	}
if(in.order.recording.correspondence.correspondenceType.equals(CorrespondenceType.changeAddress.toString())){
	in.order.recording.correspondence.correspondenceContainer = new List<CorrespondenceContainer>();
	}
else {
	List<CorrespondenceContainer> containers = new List<CorrespondenceContainer>();
	for(CorrespondenceContainer container: in.order.recording.correspondence.correspondenceContainer) {
		containers.add(container);
		}
	if(in.order.recording.correspondence.correspondenceContainer.size()==0){
		CorrespondenceContainer cont = new CorrespondenceContainer();
		cont.correspondence = in.order.recording.correspondence;
		containers.add(cont);
		}
	in.order.recording.correspondence.correspondenceContainer = containers;	
	}
if(!in.order.recording.correspondence.correspondenceType.equals(CorrespondenceType.changeAddress.toString())){
	in.order.recording.correspondence.changeAddressCheckedBy = "";
	in.order.recording.correspondence.changeAddressOrderNo = "";
	in.order.recording.correspondence.changeAddressIsChecked = null;
	}
if(!in.order.recording.correspondence.correspondenceType.equals(CorrespondenceType.newAddress.toString())){
	in.order.recording.correspondence.newAddressIsChecked = null;
	in.order.recording.correspondence.newAddressOrderNo = "";
	in.order.recording.correspondence.newAdressCheckedBy = "";
	}
' #txt
Ht0 f5 type ch.soreco.orderbook.ui.Order #txt
Ht0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initialize Erfassung</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f5 182 468 36 24 20 -2 #rect
Ht0 f5 @|StepIcon #fIcon
Ht0 f33 type ch.soreco.orderbook.ui.Order #txt
Ht0 f33 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Ht0 f33 doCall true #txt
Ht0 f33 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Ht0 f33 requestMappingAction 'param.filter.orderId=in.order.orderId;
' #txt
Ht0 f33 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f33 responseMappingAction 'out=in;
out.order=result.order;
' #txt
Ht0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Order</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f33 182 140 36 24 20 -2 #rect
Ht0 f33 @|CallSubIcon #fIcon
Ht0 f36 expr out #txt
Ht0 f36 200 116 200 140 #arcP
Ht0 f40 type ch.soreco.orderbook.ui.Order #txt
Ht0 f40 processCall 'Functional Processes/HTMLBuild:call(ch.soreco.orderbook.ui.Order)' #txt
Ht0 f40 doCall true #txt
Ht0 f40 requestActionDecl '<ch.soreco.orderbook.ui.Order orderUi> param;
' #txt
Ht0 f40 requestMappingAction 'param.orderUi=in;
' #txt
Ht0 f40 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f40 responseMappingAction 'out=result.orderUi;
' #txt
Ht0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build Html</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f40 182 844 36 24 20 -2 #rect
Ht0 f40 @|CallSubIcon #fIcon
Ht0 f41 expr out #txt
Ht0 f41 200 812 200 844 #arcP
Ht0 f18 expr out #txt
Ht0 f18 200 868 200 915 #arcP
Ht0 f21 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f21 actionTable 'out=in;
' #txt
Ht0 f21 actionCode 'import ch.soreco.orderbook.bo.DomicileOrOwner;
in.recordingDomicileOrOwners.clear();
for(DomicileOrOwner doo:in.order.recording.domicileOrOwner){
	in.recordingDomicileOrOwners.add(doo);
	}' #txt
Ht0 f21 type ch.soreco.orderbook.ui.Order #txt
Ht0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map domicile or owners</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f21 182 516 36 24 20 -2 #rect
Ht0 f21 @|StepIcon #fIcon
Ht0 f25 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f25 actionTable 'out=in;
' #txt
Ht0 f25 actionCode 'import ch.soreco.orderbook.bo.RecordingLSV;
in.recordingLSV.clear();
for(RecordingLSV lsv:in.order.recording.lsv){
	in.recordingLSV.add(lsv);
	}' #txt
Ht0 f25 type ch.soreco.orderbook.ui.Order #txt
Ht0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map lsv</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f25 182 564 36 24 20 -2 #rect
Ht0 f25 @|StepIcon #fIcon
Ht0 f26 expr out #txt
Ht0 f26 200 540 200 564 #arcP
Ht0 f24 expr out #txt
Ht0 f24 200 492 200 516 #arcP
Ht0 f30 expr out #txt
Ht0 f30 200 436 200 468 #arcP
Ht0 f30 0 0.2310942685512103 0 0 #arcLabel
Ht0 f38 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f38 actionTable 'out=in;
' #txt
Ht0 f38 actionCode 'in.orderType = ivy.cms.co("/ch/soreco/orderbook/enums/OrderType/"+in.order.orderType+"/0_name");
in.orderState = ivy.cms.co("/ch/soreco/orderbook/enums/OrderState/"+in.order.orderState+"/0_name");' #txt
Ht0 f38 type ch.soreco.orderbook.ui.Order #txt
Ht0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order type and State</name>
        <nameStyle>24
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f38 182 300 36 24 20 -2 #rect
Ht0 f38 @|StepIcon #fIcon
Ht0 f3 expr out #txt
Ht0 f3 200 324 200 354 #arcP
Ht0 f47 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f47 actionTable 'out=in;
' #txt
Ht0 f47 actionCode 'import ch.soreco.orderbook.bo.CorrespondenceContainer;
in.recordingCorrespondenceContainers.clear();
for(CorrespondenceContainer cont:in.order.recording.correspondence.correspondenceContainer){
	in.recordingCorrespondenceContainers.add(cont);
	}' #txt
Ht0 f47 type ch.soreco.orderbook.ui.Order #txt
Ht0 f47 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map Correspondence Containers</name>
        <nameStyle>29
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f47 182 724 36 24 20 -2 #rect
Ht0 f47 @|StepIcon #fIcon
Ht0 f17 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f17 actionTable 'out=in;
' #txt
Ht0 f17 actionCode 'import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.bo.RecordingMeta;
in.recordingMetas.clear();
if(!in.order.orderType.equals(OrderType.Addressmutation.toString())){
	if(in.order.recording.metaTypes.size()==0){
		RecordingMeta meta = new RecordingMeta();
		meta.recording = in.order.recording;
		in.order.recording.metaTypes.add(meta);
		}
	}
for(RecordingMeta meta:in.order.recording.metaTypes){
	in.recordingMetas.add(meta);
	}' #txt
Ht0 f17 type ch.soreco.orderbook.ui.Order #txt
Ht0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map RecordingMeta</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f17 182 612 36 24 20 -2 #rect
Ht0 f17 @|StepIcon #fIcon
Ht0 f50 expr out #txt
Ht0 f50 200 588 200 612 #arcP
Ht0 f51 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f51 actionTable 'out=in;
' #txt
Ht0 f51 actionCode 'import ch.soreco.orderbook.bo.PendencyTicket;
in.orderPendencyTickets.clear();
for(PendencyTicket ticket:in.order.pendencyTickets){
	in.orderPendencyTickets.add(ticket);
	}' #txt
Ht0 f51 type ch.soreco.orderbook.ui.Order #txt
Ht0 f51 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map Pendency Tickets</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f51 182 660 36 24 20 -2 #rect
Ht0 f51 @|StepIcon #fIcon
Ht0 f52 expr out #txt
Ht0 f52 200 636 200 660 #arcP
Ht0 f48 expr out #txt
Ht0 f48 200 684 200 724 #arcP
Ht0 f57 type ch.soreco.orderbook.ui.Order #txt
Ht0 f57 processCall 'Functional Processes/orderbook/OrderBook:getRelatedOrders(Number,Number)' #txt
Ht0 f57 doCall true #txt
Ht0 f57 requestActionDecl '<java.lang.Number matchedToOrderId,java.lang.Number dispatchingId> param;
' #txt
Ht0 f57 requestMappingAction 'param.matchedToOrderId=in.order.orderId;
param.dispatchingId=in.order.dispatchingId;
' #txt
Ht0 f57 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Ht0 f57 responseMappingAction 'out=in;
out.relatedOrders=result.relatedOrders;
' #txt
Ht0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getRelatedOrders(Number,Number)</name>
        <nameStyle>31,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f57 182 212 36 24 20 -2 #rect
Ht0 f57 @|CallSubIcon #fIcon
Ht0 f39 expr out #txt
Ht0 f39 200 236 200 300 #arcP
Ht0 f49 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Issue:SoreConnect-Nr. 25180
Sometimes Layout of editing dialog is not loaded Properly. This is now moved to ClassConverterUtil.setUI() method which loads ui information.
Once the ClassConverter.getClassHtml() method is called ClassConverterUtil.setUI()  is called again.</name>
        <nameStyle>270
</nameStyle>
    </language>
</elementInfo>
' #txt
Ht0 f49 426 691 492 91 -232 -24 #rect
Ht0 f49 @|IBIcon #fIcon
Ht0 f49 -1|-1|-16777216 #nodeStyle
Ht0 f60 expr out #txt
Ht0 f60 200 748 200 788 #arcP
Ht0 f29 expr out #txt
Ht0 f29 200 164 200 212 #arcP
>Proto Ht0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Ht0 .processKind CALLABLE_SUB #txt
>Proto Ht0 0 0 32 24 18 0 #rect
>Proto Ht0 @|BIcon #fIcon
Ht0 f11 out f28 tail #connect
Ht0 f28 head f27 mainIn #connect
Ht0 f34 head f12 mainIn #connect
Ht0 f27 mainOut f35 tail #connect
Ht0 f35 head f12 mainIn #connect
Ht0 f11 out f34 tail #connect
Ht0 f0 mainOut f31 tail #connect
Ht0 f31 head f7 mainIn #connect
Ht0 f7 mainOut f36 tail #connect
Ht0 f36 head f33 mainIn #connect
Ht0 f19 mainOut f41 tail #connect
Ht0 f41 head f40 mainIn #connect
Ht0 f40 mainOut f18 tail #connect
Ht0 f18 head f1 mainIn #connect
Ht0 f21 mainOut f26 tail #connect
Ht0 f26 head f25 mainIn #connect
Ht0 f5 mainOut f24 tail #connect
Ht0 f24 head f21 mainIn #connect
Ht0 f12 mainOut f30 tail #connect
Ht0 f30 head f5 mainIn #connect
Ht0 f38 mainOut f3 tail #connect
Ht0 f3 head f11 in #connect
Ht0 f25 mainOut f50 tail #connect
Ht0 f50 head f17 mainIn #connect
Ht0 f17 mainOut f52 tail #connect
Ht0 f52 head f51 mainIn #connect
Ht0 f51 mainOut f48 tail #connect
Ht0 f48 head f47 mainIn #connect
Ht0 f57 mainOut f39 tail #connect
Ht0 f39 head f38 mainIn #connect
Ht0 f47 mainOut f60 tail #connect
Ht0 f60 head f19 mainIn #connect
Ht0 f33 mainOut f29 tail #connect
Ht0 f29 head f57 mainIn #connect
