[Ivy]
[>Created: Mon Apr 14 10:26:15 CEST 2014]
12BF196D74EDF053 3.17 #module
>Proto >Proto Collection #zClass
cs0 cms Big #zClass
cs0 B #cInfo
cs0 #process
cs0 @TextInP .resExport .resExport #zField
cs0 @TextInP .type .type #zField
cs0 @TextInP .processKind .processKind #zField
cs0 @AnnotationInP-0n ai ai #zField
cs0 @TextInP .xml .xml #zField
cs0 @TextInP .responsibility .responsibility #zField
cs0 @StartSub f0 '' #zField
cs0 @EndSub f1 '' #zField
cs0 @Page f2 '' #zField
cs0 @PushWFArc f4 '' #zField
cs0 @GridStep f5 '' #zField
cs0 @Page f7 '' #zField
cs0 @GridStep f10 '' #zField
cs0 @PushWFArc f11 '' #zField
cs0 @PushWFArc f8 '' #zField
cs0 @GridStep f12 '' #zField
cs0 @PushWFArc f13 '' #zField
cs0 @GridStep f15 '' #zField
cs0 @PushWFArc f16 '' #zField
cs0 @Page f17 '' #zField
cs0 @PushWFArc f18 '' #zField
cs0 @GridStep f19 '' #zField
cs0 @PushWFArc f20 '' #zField
cs0 @PushWFArc f22 '' #zField
cs0 @GridStep f21 '' #zField
cs0 @PushWFArc f23 '' #zField
cs0 @PushWFArc f24 '' #zField
cs0 @GridStep f25 '' #zField
cs0 @PushWFArc f26 '' #zField
cs0 @PushWFArc f27 '' #zField
cs0 @PushWFArc f28 '' #zField
cs0 @PushWFArc f9 '' #zField
cs0 @Alternative f14 '' #zField
cs0 @PushWFArc f29 '' #zField
cs0 @PushWFArc f30 '' #zField
cs0 @PushWFArc f3 '' #zField
cs0 @PushWFArc f31 '' #zField
cs0 @GridStep f32 '' #zField
cs0 @PushWFArc f33 '' #zField
cs0 @PushWFArc f6 '' #zField
>Proto cs0 cs0 cms #zField
cs0 f0 outParamDecl '<> result;
' #txt
cs0 f0 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f0 callSignature call() #txt
cs0 f0 type ch.soreco.common.functional.CMS #txt
cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
cs0 f0 259 35 26 26 14 0 #rect
cs0 f0 @|StartSubIcon #fIcon
cs0 f1 type ch.soreco.common.functional.CMS #txt
cs0 f1 259 427 26 26 14 0 #rect
cs0 f1 @|EndSubIcon #fIcon
cs0 f2 outTypes "ch.soreco.common.functional.CMS","ch.soreco.common.functional.CMS","ch.soreco.common.functional.CMS","ch.soreco.common.functional.CMS","ch.soreco.common.functional.CMS" #txt
cs0 f2 outLinks "exit.ivp","edit.ivp","editCMS.ivp","filterAttributes.ivp","filterCMS.ivp" #txt
cs0 f2 template "/ProcessPages/cms/CMS.ivc" #txt
cs0 f2 type ch.soreco.common.functional.CMS #txt
cs0 f2 skipLink skip.ivp #txt
cs0 f2 sortLink sort.ivp #txt
cs0 f2 templateWizard '#
#Sat Jan 22 13:50:29 CET 2011
' #txt
cs0 f2 pageArchivingActivated false #txt
cs0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>CMS Management</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f2 @C|.responsibility Everybody #txt
cs0 f2 254 292 36 24 20 -2 #rect
cs0 f2 @|PageIcon #fIcon
cs0 f4 expr data #txt
cs0 f4 outCond ivp=="exit.ivp" #txt
cs0 f4 272 316 272 427 #arcP
cs0 f5 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f5 actionTable 'out=in;
' #txt
cs0 f5 actionCode 'import ch.soreco.webbies.common.html.form.AttributeCMS;
import ch.ivyteam.ivy.cm.IContentObject;

in.cmsPath = "/ch/soreco";
in.rsCMS = AttributeCMS.getCMSRecordset(["/Content/Shared", "/Navigation", in.cmsPath, "/ProcessPages"]);
in.rsAttributes = AttributeCMS.getAttributeRecordset(in.cmsPath);
in.rsCMSClone = in.rsCMS.clone();
in.rsAttributesClone = in.rsAttributes.clone();

in.filterClass = "";
in.filterContentType = "";
in.filterUriContains ="";

in.liClasses.clear();
in.liClasses.add("");
for(String clazz :in.rsAttributes.getColumn("className")){
	if(in.liClasses.indexOf(clazz)==-1){
		in.liClasses.add(clazz);
		}
	}

in.liContentTypes.clear();
in.liContentTypes.add("");
for(String type :in.rsCMS.getColumn("coType")){
	if(in.liContentTypes.indexOf(type)==-1){
		in.liContentTypes.add(type);
		}
	}

in.liFolders.clear();
in.liFolders.add("");
for(String uri :in.rsCMS.getColumn("coUri")){
	String folder = uri.substring(0,uri.lastIndexOf("/"));
	if(in.liFolders.indexOf(folder)==-1){
		in.liFolders.add(folder);
		}
	}



' #txt
cs0 f5 type ch.soreco.common.functional.CMS #txt
cs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Content rs</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f5 254 140 36 24 20 -2 #rect
cs0 f5 @|StepIcon #fIcon
cs0 f7 outTypes "ch.soreco.common.functional.CMS","ch.soreco.common.functional.CMS" #txt
cs0 f7 outLinks "back.ivp","submit.ivp" #txt
cs0 f7 template "/ProcessPages/cms/AttributeContentEdit.ivc" #txt
cs0 f7 type ch.soreco.common.functional.CMS #txt
cs0 f7 skipLink skip.ivp #txt
cs0 f7 sortLink sort.ivp #txt
cs0 f7 templateWizard '#
#Mon Apr 14 10:25:53 CEST 2014
' #txt
cs0 f7 pageArchivingActivated false #txt
cs0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Edit Attribute</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f7 @C|.responsibility Everybody #txt
cs0 f7 438 404 36 24 20 -2 #rect
cs0 f7 @|PageIcon #fIcon
cs0 f10 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f10 actionTable 'out=in;
' #txt
cs0 f10 actionCode 'import ch.soreco.webbies.common.html.form.InputProperties;
import ch.soreco.webbies.common.html.form.AttributeContent;
import ch.soreco.webbies.common.html.form.AttributeContentType;

in.attributeUri = in.rsAttributes.getAt(in.i).getField("attributeUri").toString();
in.attribute = new AttributeContent(in.attributeUri);
in.attributeLabel = in.attribute.getAttributeContentString(AttributeContentType.NAME);
in.attributeSource = in.attribute.getAttributeContentString(AttributeContentType.SOURCE);
in.attributeVisualOrder = in.attribute.ordinal;

in.attributeLabelAttributes="";
in.attributeLabelCellAttributes = "";
in.attributeInputMeta = "";
in.attributeInputCellAttributes = "";
in.attributeInputType = "";
in.attributeSelectEnum = "";

if(in.attribute.description.trim().length()>0){
	if(in.attribute.properties.size()>0){
		if(in.attribute.properties.containsKey(InputProperties.LabelAttributes.toString())){
			in.attributeLabelAttributes = in.attribute.properties.get(InputProperties.LabelAttributes.toString()).toString();
			}
		if(in.attribute.properties.containsKey(InputProperties.SelectEnum.toString())){
			in.attributeSelectEnum = in.attribute.properties.get(InputProperties.SelectEnum.toString()).toString();
			}			
		if(in.attribute.properties.containsKey(InputProperties.LabelCellAttributes.toString())){
			in.attributeLabelCellAttributes = in.attribute.properties.get(InputProperties.LabelCellAttributes.toString()).toString();
			}
		if(in.attribute.properties.containsKey(InputProperties.InputMeta.toString())){
			in.attributeInputMeta = in.attribute.properties.get(InputProperties.InputMeta.toString()).toString();
			}		
		if(in.attribute.properties.containsKey(InputProperties.InputCellAttributes.toString())){
			in.attributeInputCellAttributes = in.attribute.properties.get(InputProperties.InputCellAttributes.toString()).toString();
			}			
		if(in.attribute.properties.containsKey(InputProperties.InputType.toString())){
			in.attributeInputType = in.attribute.properties.get(InputProperties.InputType.toString()).toString();
			}	
		}
	}

' #txt
cs0 f10 type ch.soreco.common.functional.CMS #txt
cs0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Attribute</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f10 438 332 36 24 20 -2 #rect
cs0 f10 @|StepIcon #fIcon
cs0 f11 expr data #txt
cs0 f11 outCond ivp=="edit.ivp" #txt
cs0 f11 290 304 456 332 #arcP
cs0 f11 1 456 304 #addKink
cs0 f11 0 0.6869013697319222 0 0 #arcLabel
cs0 f8 expr out #txt
cs0 f8 456 356 456 404 #arcP
cs0 f12 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f12 actionTable 'out=in;
' #txt
cs0 f12 actionCode 'import ch.soreco.webbies.common.html.form.InputProperties;
import ch.soreco.webbies.common.html.form.AttributeContentType;

//get old values
String oldAttributeLabel = in.attribute.getAttributeContentString(AttributeContentType.NAME);
String oldAttributeSource = in.attribute.getAttributeContentString(AttributeContentType.SOURCE);
String oldLabelAttributes = "";
String oldLabelCellAttributes = "";
String oldInputMeta = "";
String oldInputCellAttributes = "";
String oldInputType ="";
String oldSelectEnum = "";

in.attributeInputCellAttributes = in.attributeInputCellAttributes.replace("<_%","<%").replace("%_>","%>");
in.attributeInputMeta = in.attributeInputMeta.replace("<_%","<%").replace("%_>","%>");
in.attributeLabel = in.attributeLabel.replace("<_%","<%").replace("%_>","%>");
in.attributeLabelAttributes = in.attributeLabelAttributes.replace("<_%","<%").replace("%_>","%>");
in.attributeLabelCellAttributes = in.attributeLabelCellAttributes.replace("<_%","<%").replace("%_>","%>");
in.attributeSource = in.attributeSource.replace("<_%","<%").replace("%_>","%>");
in.attributeSelectEnum = in.attributeSelectEnum.replace("<_%","<%").replace("%_>","%>");

if(in.attribute.description.trim().length()>0){
		if(in.attribute.properties.containsKey(InputProperties.LabelAttributes.toString())){
			oldLabelAttributes = in.attribute.properties.get(InputProperties.LabelAttributes.toString()).toString();
			}
		if(in.attribute.properties.containsKey(InputProperties.LabelCellAttributes.toString())){
			oldLabelCellAttributes = in.attribute.properties.get(InputProperties.LabelCellAttributes.toString()).toString();
			}
		if(in.attribute.properties.containsKey(InputProperties.InputMeta.toString())){
			oldInputMeta = in.attribute.properties.get(InputProperties.InputMeta.toString()).toString();
			}		
		if(in.attribute.properties.containsKey(InputProperties.InputCellAttributes.toString())){
			oldInputCellAttributes = in.attribute.properties.get(InputProperties.InputCellAttributes.toString()).toString();
			}	
		if(in.attribute.properties.containsKey(InputProperties.InputType.toString())){
			oldInputType = in.attribute.properties.get(InputProperties.InputType.toString()).toString();
			}			
		if(in.attribute.properties.containsKey(InputProperties.SelectEnum.toString())){
			oldSelectEnum = in.attribute.properties.get(InputProperties.SelectEnum.toString()).toString();
			}		
	}
	
	
//Has Changed Properties?
Boolean propertiesChanged = false;
if(!oldInputType.equals(in.attributeInputType)){
	in.attribute.setProperty(InputProperties.InputType.toString(),in.attributeInputType);
	propertiesChanged = true;
	}
if(!oldLabelAttributes.equals(in.attributeLabelAttributes)){
	in.attribute.setProperty(InputProperties.LabelAttributes.toString(),in.attributeLabelAttributes);
	propertiesChanged = true;
	}
if(!oldLabelCellAttributes.equals(in.attributeLabelCellAttributes)){
	in.attribute.setProperty(InputProperties.LabelCellAttributes.toString(),in.attributeLabelCellAttributes);
	propertiesChanged = true;
	}
if(!oldInputMeta.equals(in.attributeInputMeta)){
	in.attribute.setProperty(InputProperties.InputMeta.toString(),in.attributeInputMeta);
	propertiesChanged = true;
	}
if(!oldInputCellAttributes.equals(in.attributeInputCellAttributes)){
	in.attribute.setProperty(InputProperties.InputCellAttributes.toString(),in.attributeInputCellAttributes);
	propertiesChanged = true;
	}
if(!oldSelectEnum.equals(in.attributeSelectEnum)){
	in.attribute.setProperty(InputProperties.SelectEnum.toString(),in.attributeSelectEnum);
	propertiesChanged = true;
	}
if(propertiesChanged){
	for(InputProperties property:InputProperties.values()){
		if(in.attribute.properties.containsKey(property.toString())){
			//ivy.log.debug(property.toString()+" Property:"+in.attribute.properties.get(property.toString()));			
			}
		}
	in.attribute.storeProperties();
	}
in.attribute.setAttributeVisualOrder(in.attributeVisualOrder as int);
if(!oldAttributeLabel.equals(in.attributeLabel)){
	in.attribute.setAttributeContent(AttributeContentType.NAME,in.attributeLabel);
	}
if(!oldAttributeSource.equals(in.attributeSource)){
	in.attribute.setAttributeContent(AttributeContentType.SOURCE,in.attributeSource);
	}' #txt
cs0 f12 type ch.soreco.common.functional.CMS #txt
cs0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Update Attribute</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f12 622 404 36 24 20 -2 #rect
cs0 f12 @|StepIcon #fIcon
cs0 f13 expr data #txt
cs0 f13 outCond ivp=="submit.ivp" #txt
cs0 f13 474 416 622 416 #arcP
cs0 f15 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f15 actionTable 'out=in;
' #txt
cs0 f15 actionCode 'in.attributeUri = in.rsCMS.getAt(in.i).getField("coUri").toString();
in.contentType =  in.rsCMS.getAt(in.i).getField("coType").toString();;
in.attributeSource = ivy.cms.co(in.attributeUri);
' #txt
cs0 f15 type ch.soreco.common.functional.CMS #txt
cs0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Content uri</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f15 94 340 36 24 20 -2 #rect
cs0 f15 @|StepIcon #fIcon
cs0 f16 expr data #txt
cs0 f16 outCond ivp=="editCMS.ivp" #txt
cs0 f16 254 304 112 340 #arcP
cs0 f16 1 112 304 #addKink
cs0 f16 0 0.6640372437773798 0 0 #arcLabel
cs0 f17 outTypes "ch.soreco.common.functional.CMS" #txt
cs0 f17 outLinks "submit.ivp" #txt
cs0 f17 template "/ProcessPages/cms/EditContent.ivc" #txt
cs0 f17 type ch.soreco.common.functional.CMS #txt
cs0 f17 skipLink skip.ivp #txt
cs0 f17 sortLink sort.ivp #txt
cs0 f17 templateWizard '#
#Mon Apr 14 10:26:13 CEST 2014
' #txt
cs0 f17 pageArchivingActivated false #txt
cs0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Content</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f17 @C|.responsibility Everybody #txt
cs0 f17 94 404 36 24 20 -2 #rect
cs0 f17 @|PageIcon #fIcon
cs0 f18 expr out #txt
cs0 f18 112 364 112 404 #arcP
cs0 f19 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f19 actionTable 'out=in;
' #txt
cs0 f19 actionCode 'import ch.ivyteam.ivy.cm.IContentObjectValue;

import ch.ivyteam.ivy.cm.IContentObject;
IContentObject co = ivy.cms.getContentObject(in.attributeUri);

in.attributeSource = in.attributeSource.replace("<_%","<%").replace("%_>","%>");
try {
	IContentObjectValue value = ivy.cms.getContentObjectValue(in.attributeUri,ivy.session.getContentLocale());
	value.setContent(in.attributeSource, ivy.session.getSessionUserName());
	co.touch(ivy.session.getSessionUserName());
} catch(Exception e){
	ivy.log.error(e);
	out.error = e.getMessage();
}
' #txt
cs0 f19 type ch.soreco.common.functional.CMS #txt
cs0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>update</name>
        <nameStyle>6,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f19 94 468 36 24 20 -2 #rect
cs0 f19 @|StepIcon #fIcon
cs0 f20 expr data #txt
cs0 f20 outCond ivp=="submit.ivp" #txt
cs0 f20 112 428 112 468 #arcP
cs0 f22 expr out #txt
cs0 f22 94 480 254 152 #arcP
cs0 f22 1 40 480 #addKink
cs0 f22 2 40 152 #addKink
cs0 f22 2 0.19751018455390712 0 0 #arcLabel
cs0 f21 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f21 actionTable 'out=in;
' #txt
cs0 f21 actionCode 'in.rsAttributes.clear();
in.tabIndex = 0;
if(in.filterClass.trim().equalsIgnoreCase("")
	&&in.filterValue.trim().equalsIgnoreCase("")
	&&in.filterUriContains.trim().equalsIgnoreCase("")){
		in.rsAttributes = in.rsAttributesClone.clone();
		}
else {
	for(Record rc:in.rsAttributesClone.getRecords()){
		Boolean doAdd = false;
		if(rc.getField("attributeUri").toString().contains(in.filterUriContains)
		&&!in.filterUriContains.trim().equalsIgnoreCase("")){
			doAdd = true;
			}
		if(!in.filterValue.trim().equalsIgnoreCase("")){
			for(Object value : rc.getValues()){
				if(value.toString().contains(in.filterValue)){
					doAdd = true;
					break;
					}
				}
			}
		if(rc.getField("className").toString().equalsIgnoreCase(in.filterClass)
		&&!in.filterClass.trim().equalsIgnoreCase("")){
			doAdd = true;
			}
	
		if(doAdd){
			in.rsAttributes.add(rc);
			}
		}

	}
	' #txt
cs0 f21 type ch.soreco.common.functional.CMS #txt
cs0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>filter Attributes</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f21 366 236 36 24 20 -2 #rect
cs0 f21 @|StepIcon #fIcon
cs0 f23 expr data #txt
cs0 f23 outCond ivp=="filterAttributes.ivp" #txt
cs0 f23 290 295 366 257 #arcP
cs0 f24 expr out #txt
cs0 f24 366 257 290 295 #arcP
cs0 f25 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f25 actionTable 'out=in;
' #txt
cs0 f25 actionCode 'in.rsCMS.clear();
in.tabIndex = 1;
for(Record rc:in.rsCMSClone.getRecords()){
	Boolean doAdd = false;
	if(rc.getField("coUri").toString().contains(in.filterUriContains)
	&&!in.filterUriContains.trim().equalsIgnoreCase("")){
		doAdd = true;
		}
	if(rc.getField("coUri").toString().startsWith(in.filterFolder)
	&&!in.filterFolder.trim().equalsIgnoreCase("")){
		doAdd = true;
		}
	if(rc.getField("coType").toString().equalsIgnoreCase(in.filterContentType)){
		doAdd = true;
		}
	if(!in.filterValue.trim().equalsIgnoreCase("")){
		for(Object value : rc.getValues()){
			if(value.toString().contains(in.filterValue)){
				doAdd = true;
				break;
				}
			}
		}

	if(doAdd){
		in.rsCMS.add(rc);
		}
	}
	
' #txt
cs0 f25 type ch.soreco.common.functional.CMS #txt
cs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>filter CMS</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f25 158 236 36 24 20 -2 #rect
cs0 f25 @|StepIcon #fIcon
cs0 f26 expr data #txt
cs0 f26 outCond ivp=="filterCMS.ivp" #txt
cs0 f26 254 294 194 259 #arcP
cs0 f27 expr out #txt
cs0 f27 194 259 254 294 #arcP
cs0 f28 expr out #txt
cs0 f28 640 404 474 344 #arcP
cs0 f28 1 640 344 #addKink
cs0 f28 1 0.20804737801798845 0 0 #arcLabel
cs0 f9 expr data #txt
cs0 f9 outCond ivp=="back.ivp" #txt
cs0 f9 456 428 290 152 #arcP
cs0 f9 1 456 480 #addKink
cs0 f9 2 752 480 #addKink
cs0 f9 3 752 152 #addKink
cs0 f9 2 0.4698734795811046 0 0 #arcLabel
cs0 f14 type ch.soreco.common.functional.CMS #txt
cs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>choose tab</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f14 258 186 28 28 14 0 #rect
cs0 f14 @|AlternativeIcon #fIcon
cs0 f29 expr out #txt
cs0 f29 272 164 272 186 #arcP
cs0 f30 expr in #txt
cs0 f30 outCond in.tabIndex==0 #txt
cs0 f30 282 204 366 240 #arcP
cs0 f3 expr in #txt
cs0 f3 outCond in.tabIndex==1 #txt
cs0 f3 263 205 194 239 #arcP
cs0 f31 expr in #txt
cs0 f31 272 214 272 292 #arcP
cs0 f32 actionDecl 'ch.soreco.common.functional.CMS out;
' #txt
cs0 f32 actionTable 'out=in;
out.tabIndex=0;
' #txt
cs0 f32 actionCode '
' #txt
cs0 f32 type ch.soreco.common.functional.CMS #txt
cs0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set tabIndex</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f32 254 92 36 24 20 -2 #rect
cs0 f32 @|StepIcon #fIcon
cs0 f33 expr out #txt
cs0 f33 272 61 272 92 #arcP
cs0 f6 expr out #txt
cs0 f6 272 116 272 140 #arcP
>Proto cs0 .type ch.soreco.common.functional.CMS #txt
>Proto cs0 .processKind CALLABLE_SUB #txt
>Proto cs0 0 0 32 24 18 0 #rect
>Proto cs0 @|BIcon #fIcon
cs0 f2 out f4 tail #connect
cs0 f4 head f1 mainIn #connect
cs0 f2 out f11 tail #connect
cs0 f11 head f10 mainIn #connect
cs0 f10 mainOut f8 tail #connect
cs0 f8 head f7 mainIn #connect
cs0 f13 head f12 mainIn #connect
cs0 f2 out f16 tail #connect
cs0 f16 head f15 mainIn #connect
cs0 f15 mainOut f18 tail #connect
cs0 f18 head f17 mainIn #connect
cs0 f17 out f20 tail #connect
cs0 f20 head f19 mainIn #connect
cs0 f19 mainOut f22 tail #connect
cs0 f22 head f5 mainIn #connect
cs0 f2 out f23 tail #connect
cs0 f23 head f21 mainIn #connect
cs0 f21 mainOut f24 tail #connect
cs0 f24 head f2 mainIn #connect
cs0 f2 out f26 tail #connect
cs0 f26 head f25 mainIn #connect
cs0 f25 mainOut f27 tail #connect
cs0 f27 head f2 mainIn #connect
cs0 f12 mainOut f28 tail #connect
cs0 f28 head f10 mainIn #connect
cs0 f7 out f9 tail #connect
cs0 f9 head f5 mainIn #connect
cs0 f7 out f13 tail #connect
cs0 f5 mainOut f29 tail #connect
cs0 f29 head f14 in #connect
cs0 f14 out f30 tail #connect
cs0 f30 head f21 mainIn #connect
cs0 f14 out f3 tail #connect
cs0 f3 head f25 mainIn #connect
cs0 f14 out f31 tail #connect
cs0 f31 head f2 mainIn #connect
cs0 f0 mainOut f33 tail #connect
cs0 f33 head f32 mainIn #connect
cs0 f32 mainOut f6 tail #connect
cs0 f6 head f5 mainIn #connect
