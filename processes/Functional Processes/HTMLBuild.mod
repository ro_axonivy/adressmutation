[Ivy]
[>Created: Fri Oct 24 17:22:07 CEST 2014]
12BBFA2BF2BE33BC 3.17 #module
>Proto >Proto Collection #zClass
Hd0 HTMLBuild Big #zClass
Hd0 B #cInfo
Hd0 #process
Hd0 @TextInP .resExport .resExport #zField
Hd0 @TextInP .type .type #zField
Hd0 @TextInP .processKind .processKind #zField
Hd0 @AnnotationInP-0n ai ai #zField
Hd0 @TextInP .xml .xml #zField
Hd0 @TextInP .responsibility .responsibility #zField
Hd0 @StartSub f0 '' #zField
Hd0 @EndSub f1 '' #zField
Hd0 @GridStep f6 '' #zField
Hd0 @GridStep f17 '' #zField
Hd0 @GridStep f23 '' #zField
Hd0 @GridStep f25 '' #zField
Hd0 @GridStep f4 '' #zField
Hd0 @PushWFArc f5 '' #zField
Hd0 @GridStep f7 '' #zField
Hd0 @PushWFArc f8 '' #zField
Hd0 @Alternative f3 '' #zField
Hd0 @PushWFArc f11 '' #zField
Hd0 @PushWFArc f12 '' #zField
Hd0 @InfoButton f13 '' #zField
Hd0 @Alternative f14 '' #zField
Hd0 @PushWFArc f16 '' #zField
Hd0 @PushWFArc f18 '' #zField
Hd0 @GridStep f19 '' #zField
Hd0 @PushWFArc f20 '' #zField
Hd0 @GridStep f22 '' #zField
Hd0 @PushWFArc f24 '' #zField
Hd0 @PushWFArc f2 '' #zField
Hd0 @PushWFArc f30 '' #zField
Hd0 @Alternative f10 '' #zField
Hd0 @PushWFArc f26 '' #zField
Hd0 @PushWFArc f9 '' #zField
Hd0 @Alternative f27 '' #zField
Hd0 @PushWFArc f28 '' #zField
Hd0 @PushWFArc f21 '' #zField
Hd0 @PushWFArc f29 '' #zField
>Proto Hd0 Hd0 HTMLBuild #zField
Hd0 f0 inParamDecl '<ch.soreco.orderbook.ui.Order orderUi> param;' #txt
Hd0 f0 inParamTable 'out=param.orderUi;
' #txt
Hd0 f0 outParamDecl '<ch.soreco.orderbook.ui.Order orderUi> result;
' #txt
Hd0 f0 outParamTable 'result.orderUi=in;
' #txt
Hd0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f0 callSignature call(ch.soreco.orderbook.ui.Order) #txt
Hd0 f0 type ch.soreco.orderbook.ui.Order #txt
Hd0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(OrderUI)</name>
        <nameStyle>13,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f0 323 35 26 26 14 0 #rect
Hd0 f0 @|StartSubIcon #fIcon
Hd0 f1 type ch.soreco.orderbook.ui.Order #txt
Hd0 f1 323 755 26 26 14 0 #rect
Hd0 f1 @|EndSubIcon #fIcon
Hd0 f6 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f6 actionTable 'out=in;
' #txt
Hd0 f6 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClassHtml;
import ch.soreco.orderbook.bo.Letter;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.ui.enums.ViewType;
import ch.soreco.webbies.common.html.form.ClassConverter;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Paper;

StringBuffer html = new StringBuffer("");
html.append("<h3><%=ivy.cms.co(\"/Content/Shared/Labels/Contact\")%></h3>");
html.append("<div id=\"Contacts\">");
html.append("<div id=\"ContactsTab\">");
StringBuffer tab = new StringBuffer("");
tab.append("<ul>");
for(Contact contact :in.orderContacts){
	tab.append("<li><a title=''"+contact.incomeDate.format("dd.MM.yyyy HH:mm:ss")+"'' href=\"#contact"+contact.contactId+" \">"+contact.incomeDate.format("dd.MM.yyyy")+"</a></li>");
}
tab.append("</ul>");
for(int iContact=0;iContact<in.orderContacts.size();iContact++){
	Contact contact = in.orderContacts.get(iContact);
	tab.append("<div id=\"contact"+contact.contactId+"\">");
	if(contact.incomeDate!=null){
		tab.append("<p>Schreiben vom "+contact.incomeDate.format("dd.MM.yyyy")+"&nbsp;&nbsp;");
		//PopUpOpener removed
		tab.append("<a class='''' title=''Kontakt File öffnen'' target=''_blank'' href=''<%=ivy.html.ref(""showContactFile.ivp"")%>&i="+iContact+"''><%=ivy.cms.co(""/Content/Shared/Images/DocumentIcons/pdf"")%></a></p>");	
	}
	tab.append("<fieldset>");
	tab.append("<legend>Unterschrift / Revers prüfen</legend>");

	String htmlPart = ClassConverter.getClassHTML(in,contact,"orderContacts$$"+iContact,in.modify,false);
	htmlPart = htmlPart.replace("$$Contact","");
	tab.append(htmlPart);
	tab.append("</fieldset>");
	if(in.order.orderType.equals(OrderType.Addressmutation.toString())){
		//Append Untierschrift Anfordern Fieldset
		StringBuffer fieldsetSignatures = new StringBuffer("");
		fieldsetSignatures.append("<fieldset>");
		fieldsetSignatures.append("<legend>Unterschrift anfordern</legend>");
		fieldsetSignatures.append(ClassConverter.getClassHTML(in, contact.address, "orderContacts$$"+iContact+"$$address", in.modify,true));
		int iSign = 0;
		for(SignatureRequest signatureRequest:in.orderContactsSignatureRequests){
			if(signatureRequest.contactId==contact.contactId){
				String sign = ClassConverter.getClassHTML(in,signatureRequest,"orderContactsSignatureRequests$$"+iSign,in.modify,true);
				fieldsetSignatures.append(sign);
			}
			iSign++;
		}

		if(in.eViewType.equals(ViewType.EDIT)){
			String appender ="<a class=''asButton objectAppender''  forObject=''SignatureRequest'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=SignatureRequest&currentContact$$contactId="+contact.contactId+"&i=''>weitere</a>";
			fieldsetSignatures.append(appender);
		}
		fieldsetSignatures.append("</fieldset>");
		tab.append(fieldsetSignatures);

		// AdressCheck
		AttributeClassHtml addressCheckClassHtml = new AttributeClassHtml(in.genericView, contact.addressCheck,"orderContacts$$"+iContact+"$$addressCheck");
		tab.append(addressCheckClassHtml.toHtml());

		StringBuffer fieldset = new StringBuffer("");
		fieldset.append("<fieldset id=''PapersFieldset''>");
		fieldset.append("<legend>Zusätzliche Unterlagen anfordern</legend>");
		int iPaper = 0;
		for(Paper paper :in.orderContactPapers){
			if(paper.contactId == contact.contactId){
				String papers = ClassConverter.getClassHTML(in,paper,"orderContactPapers$$"+iPaper,in.modify,false);
				fieldset.append(papers);
			}
			iPaper++;
		}
		if(in.eViewType.equals(ViewType.EDIT)){
			String appender ="<a class=''asButton objectAppender'' forObject=''Paper'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=Paper&currentContact$$contactId="+contact.contactId+"&i=''>weitere</a>";
			fieldset.append(appender);
		}
		fieldset.append("</fieldset>");	
		tab.append(fieldset);
	} else {
		//Append Untierschrift Anfordern Fieldset
		StringBuffer fieldsetSignatures = new StringBuffer("");
		fieldsetSignatures.append("<fieldset>");
		fieldsetSignatures.append("<legend>Unterschrift anfordern</legend>");
		int iSign = 0;
		for(SignatureRequest signatureRequest:in.orderContactsSignatureRequests){
			if(signatureRequest.contactId==contact.contactId){
				String sign = ClassConverter.getClassHTML(in,signatureRequest,"orderContactsSignatureRequests$$"+iSign,in.modify,true);
				fieldsetSignatures.append(sign);
			}
			iSign++;
		}

		if(in.eViewType.equals(ViewType.EDIT)){
			String appender ="<a class=''asButton objectAppender''  forObject=''SignatureRequest'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=SignatureRequest&currentContact$$contactId="+contact.contactId+"&i=''>weitere</a>";
			fieldsetSignatures.append(appender);
		}
		fieldsetSignatures.append("</fieldset>");
		tab.append(fieldsetSignatures);	
		// AdressCheck
		AttributeClassHtml addressCheckClassHtml = new AttributeClassHtml(in.genericView, contact.addressCheck,"orderContacts$$"+iContact+"$$addressCheck");
		tab.append(addressCheckClassHtml.toHtml());

		//Metatypen
		StringBuffer fieldset = new StringBuffer("");
		fieldset.append("<fieldset id=''LettersFieldset''>");
		fieldset.append("<legend>Weitere Briefarten</legend>");
		int iLetter = 0;
		for(Letter letter :in.orderContactsLetters){
			if(letter.contactId == contact.contactId){
				AttributeClassHtml classHtml = new AttributeClassHtml(in.genericView, letter,"orderContactsLetters", iLetter);
				fieldset.append(classHtml.toHtml());
			}
			iLetter++;
		}
		if(in.eViewType.equals(ViewType.EDIT)){
			String appender ="<a class=''asButton objectAppender'' forObject=''Letter'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=Letter&currentContact$$contactId="+contact.contactId+"&i=''>weitere</a>";
			fieldset.append(appender);
		}
		fieldset.append("</fieldset>");	
		tab.append(fieldset);
	}
	tab.append("</div><!-- /Contact Tab-->");
}
html.append(tab);
html.append("</div><!--/Contact Tabs-->");	
html.append("</div><!--/Contact-->");

out.htmlBuild.append(html);' #txt
Hd0 f6 type ch.soreco.orderbook.ui.Order #txt
Hd0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Contact</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f6 318 300 36 24 20 -2 #rect
Hd0 f6 @|StepIcon #fIcon
Hd0 f17 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f17 actionTable 'out=in;
' #txt
Hd0 f17 actionCode 'import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.ui.enums.ViewType;
import java.util.LinkedHashMap;
import ch.soreco.webbies.common.html.generics.AttributeConstants;
import ch.soreco.orderbook.enums.PendencyTicketType;
import ch.soreco.orderbook.bo.PendencyTicket;

import ch.soreco.webbies.common.html.generics.AttributeClass;
import ch.soreco.webbies.common.html.form.ClassConverter;
StringBuffer html = new StringBuffer("");

html.append("<iframe frameBorder=\"0\" id=\"ObjectDialogHistoryM\" style=\"display:none\" title=''History'' ></iframe>");
html.append("<a class=''asButton objectModalDialog'' forObject=''HistoryM''"
						+" href=\"<%=ivy.html.ref(\"showHistory.ivp\")%>&order$$orderId="+in.order.orderId+"\">Auftrags History</a>");

html.append("<h3><%=ivy.cms.co(\"/Content/Shared/Labels/General\")%></h3>");
html.append("<div id=''OrderGeneral''>");
html.append(ClassConverter.getClassHTML(in, in.order,"order", in.modify, false));

// build related orders
//&& in.relatedOrders.getColumn("orderId").contains(in.order.orderId)
for(int i = 0; i < in.relatedOrders.size(); i++){
	if(in.relatedOrders.getField(i,"orderId").toString().toNumber()==in.order.orderId){
		in.relatedOrders.removeAt(i);
	}
}
//in.relatedOrders.removeAt(in.relatedOrders.indexOf(in.relatedOrders.get))
if(in.relatedOrders.size() > 0){
	html.append("<fieldset>");
	html.append("<legend>Zugehörige Aufträge</legend>");
	for(int i = 0; i < in.relatedOrders.size(); i++){
		if(!in.relatedOrders.getField(i,"orderState").toString().equals(OrderState.Deleted.toString())
		&& !in.relatedOrders.getField(i,"orderId").toString().equals(in.order.orderId.toString())
		){
			String relatedOrderName = in.relatedOrders.getField(i,"orderId").toString()+" - "+ivy.cms.co("/ch/soreco/orderbook/enums/OrderType/"+in.relatedOrders.getField(i,"orderType").toString()+"/0_name");
			String relatedOrderState = ivy.cms.co("/ch/soreco/orderbook/enums/OrderState/"+in.relatedOrders.getField(i,"orderState").toString()+"/0_name");
			String relatedOrderEditor = in.relatedOrders.getField(i,"editorUserId").toString();
			String relatedOrderChecker = in.relatedOrders.getField(i,"checkerUserId").toString();
			try{
				if(relatedOrderEditor.length() > 0){
					relatedOrderEditor = ivy.session.getSecurityContext().findUser(relatedOrderEditor).getFullName();
				}
			}catch(Exception e){
				ivy.log.error("editor not found "+e.getMessage());
				if(relatedOrderEditor.equalsIgnoreCase("null")){
					relatedOrderEditor = "";
				}
			}
			try{
				if(relatedOrderChecker.length() > 0){
					relatedOrderChecker = ivy.session.getSecurityContext().findUser(relatedOrderChecker).getFullName();
				}
			}catch(Exception e){
				ivy.log.error("checked not found "+e.getMessage());
					if(relatedOrderChecker.equalsIgnoreCase("null")){
						relatedOrderChecker = "";
					}
			}

			String relatedOrderLink = "<a href=\"<%=ivy.html.ref(\"showRelatedOrder.ivp\")%>&order$$orderId="+in.relatedOrders.getField(i,"orderId").toString()+"\" target=\"_blank\">"+relatedOrderName+" / "+relatedOrderState+" / Bearbeiter: "+relatedOrderEditor+" / Prüfer: "+relatedOrderChecker+"</a>";
			//String relatedOrderScript = "window.open(\"<%=ivy.html.ref(\"showRelatedOrder.ivp\")%>&order$$orderId="+in.relatedOrders.getField(i,"orderId").toString()+"\", \"relatedOrdersWindow\", \"location=1,status=0,menubar=0,directories=0,scrollbars=1,  width=100,height=100\")";
			//String relatedOrderLink = "<a href=\"#\" onclick=\"javascript:"+relatedOrderScript+";return false;\">"+relatedOrderName+"</a>";
			
			html.append(relatedOrderLink+"<br>");
		}
		
	}
	html.append("</fieldset>");
}

html.append("<fieldset id=''PendencyTickets''>");
String hasNoOpenPendencies ="";
for(PendencyTicket pendency:in.orderPendencyTickets){
	if(!pendency.solved){
		if(in.eViewType.name().equalsIgnoreCase(ViewType.EDIT.name())){
			hasNoOpenPendencies = "<input id=''hasOpenPendencies'' type=''hidden'' value='''' class=""{EDITED:{required:true, number: true, messages:{required:''Bitte die Offenen Pendenzen abschliessen''}}}"">";
		}
		if(in.order.orderState.equalsIgnoreCase(OrderState.In4eCheck.name())){
			hasNoOpenPendencies = "<input id=''hasOpenPendencies'' type=''hidden'' value='''' class=""{CHECK:{required:true, number: true, messages:{required:''Bitte die Offenen Pendenzen abschliessen''}}}"">";
		}
		break;
	}
}
html.append(hasNoOpenPendencies);
String add = "";
if(in.eViewType.equals(ViewType.EDIT)){
	add ="<input value=''Add'' type=''Button'' class=''asButton objectDialog'' forObject=''PendencyTicket''"
						+" onclick=\"this.form.action=''<%=ivy.html.ref(\"addOrEditObjectDialog.ivp\")%>&newObject=PendencyTicket&i=-1'';this.form.submit()\";>";
//	add ="<a class=''asButton objectDialog'' forObject=''PendencyTicket'' href=''<%=ivy.html.ref(\"addOrEditObjectDialog.ivp\")%>&newObject=PendencyTicket&i=-1''>add</a>";
}
html.append("<legend>Pendency Tickets&nbsp;&nbsp;"+add+"</legend>");	
if(in.orderPendencyTickets.size()==0){
	html.append("<p id=''noTickets''>Keine Tickets zu diesem Auftrag</p>");
	}
html.append("<table border=''0''>");
html.append("<tbody>");
String view = AttributeConstants.CONTENTPREFIX;
AttributeClass pendencyTicketType = new AttributeClass(PendencyTicketType.class, view );
LinkedHashMap pendencyTicketLabelMap = pendencyTicketType.getKeyLabelMap();
int i = 0;
for(PendencyTicket ticket:in.orderPendencyTickets){
	html.append("<tr>");
	String label = ticket.pendencyTicketType;
	if(pendencyTicketLabelMap.containsKey(ticket.pendencyTicketType.trim())){
		label = pendencyTicketLabelMap.get(ticket.pendencyTicketType.trim()).toString();
		}
	html.append("<td>"+label+"</td>");
	String remindAt = ticket.remindAt.toString().length() > 0 ? ticket.remindAt.format("dd.MM.yyyy") : "";
	html.append("<td>"+remindAt+"</td>");
	if(ticket.remindAt.toString().length() > 0){
		html.append("<td> - </td>");
	} else {
		html.append("<td></td>");
	}
	
	html.append("<td>"+ticket.createDateTime.format("dd.MM.yyyy")+"</td>");
	html.append("<td>"+ticket.createUser+"</td>");
	
	if(in.eViewType.equals(ViewType.EDIT)){
		String edit ="<input value=''Edit'' type=''Button'' class=''asButton objectDialog'' forObject=''PendencyTicket''"
						+" onclick=\"this.form.action=''<%=ivy.html.ref(\"addOrEditObjectDialog.ivp\")%>&newObject=PendencyTicket&i="+i+"'';this.form.submit();\">";
//	String edit ="<a class=''asButton objectDialog'' forObject=''PendencyTicket'' href=''<%=ivy.html.ref(\"addOrEditObjectDialog.ivp\")%>&newObject=PendencyTicket&i="+i+"''>add</a>";
		html.append("<td>"+edit+"</td>");	
		}	

	String solved = ticket.solved == true ? "<img src=''"+ivy.html.coref("/ch/soreco/common/enums/BooleanOption/TRUE/0_icon")+"''/>": "";
	html.append("<td>"+solved+"</td>");	
	html.append("</tr>");
	
	html.append("<tr>");
	html.append("<td colpan=''7''>");
//		html.append("Fällig: "+ticket.dueDate.format("dd.MM.yyyy")+" / ");
//		html.append("Erinn.: "+ticket.remindAt+" Tagen / ");
		html.append("An: "+ticket.delegateTo+" / ");		
		html.append("Abt.: "+ticket.department+" / ");
		html.append("CRM: "+ticket.CRMIssueOrderNo);						
	html.append("</td>");	
	html.append("</tr>");
	
	i++;
	}
html.append("</tbody></table>");
html.append("</fieldset>");

html.append("</div><!--in Order-->");

out.htmlBuild.append(html);' #txt
Hd0 f17 type ch.soreco.orderbook.ui.Order #txt
Hd0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build General</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f17 318 196 36 24 20 -2 #rect
Hd0 f17 @|StepIcon #fIcon
Hd0 f23 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f23 actionTable 'out=in;
' #txt
Hd0 f23 actionCode 'import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.webbies.common.html.form.ClassConverter;
import ch.soreco.orderbook.bo.DomicileOrOwner;
import ch.soreco.orderbook.bo.CorrespondenceContainer;
import ch.soreco.orderbook.bo.RecordingLSV;
import ch.soreco.orderbook.ui.enums.ViewType;
Boolean canModify = in.modify;
if(canModify&&in.order.orderState.equals(OrderState.InPendencyRecording.toString())){
	canModify = false;
	}
StringBuffer html = new StringBuffer("");
html.append("<h3><%=ivy.cms.co(\"/Content/Shared/Labels/Recording\")%></h3>");
html.append("<div id=\"Recording\" width=\"100%\">");
html.append("<fieldset>");
html.append("<legend>Business Partner</legend>");
html.append(ClassConverter.getClassHTML(in,in.order.recording.businessPartner,"order$$recording$$businessPartner",canModify,true));
html.append("</fieldset><!--Business Partner-->");
		
html.append("<fieldset id=\"orderRecordingDomicileOrOwners\">");
html.append("<legend>Domicile/Owner</legend>");	
int i = 0;
for(DomicileOrOwner domicileOrOwner : in.recordingDomicileOrOwners){
	html.append(ClassConverter.getClassHTML(in, domicileOrOwner,"recordingDomicileOrOwners$$"+i,canModify,true));
	i=i+1;				
}
if(in.eViewType.equals(ViewType.EDIT)&&!in.order.orderState.equals(OrderState.InPendencyRecording.toString())){
	String appender ="<a class=''asButton objectAppender'' forObject=''DomicileOrOwner'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=DomicileOrOwner&i=''>weitere</a>";
	html.append(appender);
}	
html.append("</fieldset><!--Domicile / Owner-->");		
	
html.append("<fieldset>");
html.append("<legend>Korrespondenz</legend>");	
html.append(ClassConverter.getClassHTML(in,in.order.recording.correspondence,"order$$recording$$correspondence",canModify,false));				
html.append("<fieldset id=\"CorrespondenceContainerFields\">");
html.append("<legend>Container</legend>");	
int j=0;
for(CorrespondenceContainer container:in.recordingCorrespondenceContainers){
	html.append(ClassConverter.getClassHTML(in, container,"recordingCorrespondenceContainers$$"+j,canModify,true));				
	j=j+1;
}
if(in.eViewType.equals(ViewType.EDIT)&&!in.order.orderState.equals(OrderState.InPendencyRecording.toString())){
	String appender ="<a class=''asButton objectAppender'' forObject=''CorrespondenceContainer'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=CorrespondenceContainer&i=''>weitere</a>";
	html.append(appender);
}
html.append("</fieldset><!--Korrespondez Container-->");
html.append("</fieldset><!--Korrespondez-->");
		
html.append("<fieldset id=\"orderRecordingLSV\">");
html.append("<legend>LSV</legend>");	
int k=0;
for(RecordingLSV lsv : in.recordingLSV){
	html.append(ClassConverter.getClassHTML(in, lsv,"recordingLSV$$"+k,canModify,true));			
	k=k+1;	
}
if(in.eViewType.equals(ViewType.EDIT)&&!in.order.orderState.equals(OrderState.InPendencyRecording.toString())){
	String appender ="<a class=''asButton objectAppender'' forObject=''RecordingLSV'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=RecordingLSV&i=''>weitere</a>";
	html.append(appender);
}
html.append("</fieldset><!--LSV-->");
		
html.append("<fieldset>");
html.append("<legend>Bestätigung</legend>");	
html.append(ClassConverter.getClassHTML(in,in.order.recording.affirmation,"order$$recording$$affirmation",canModify,true));				
html.append("</fieldset><!--Adressmutation bestätigt-->");
						
html.append("</div><!--/Recording-->");
	
out.htmlBuild.append(html);' #txt
Hd0 f23 type ch.soreco.orderbook.ui.Order #txt
Hd0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Erfassung</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f23 382 428 36 24 20 -2 #rect
Hd0 f23 @|StepIcon #fIcon
Hd0 f25 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f25 actionTable 'out=in;
' #txt
Hd0 f25 actionCode 'out.htmlBuild.insert(0,"<div id=\"AdressmutationAccordion\" class=\"ui-widget ui-widget-content\">\n");
out.htmlBuild.append("<input type=\"hidden\" value=\""+in.currentChild+"\" name=\"currentChild\" id=\"currentChild\">");
out.htmlBuild.append("</div>");
out.html = out.htmlBuild.toString();


' #txt
Hd0 f25 type ch.soreco.orderbook.ui.Order #txt
Hd0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Wrap build and append currentChild hidden field</name>
        <nameStyle>47
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f25 318 684 36 24 20 -2 #rect
Hd0 f25 @|StepIcon #fIcon
Hd0 f4 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f4 actionTable 'out=in;
out.htmlBuild=new StringBuilder("");
' #txt
Hd0 f4 type ch.soreco.orderbook.ui.Order #txt
Hd0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset StringBuilder</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f4 318 92 36 24 20 -2 #rect
Hd0 f4 @|StepIcon #fIcon
Hd0 f5 expr out #txt
Hd0 f5 336 61 336 92 #arcP
Hd0 f7 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f7 actionTable 'out=in;
' #txt
Hd0 f7 actionCode 'import ch.soreco.orderbook.enums.OrderState;

if(in.currentChild>0){
	in.showChild = in.currentChild;
	}
else {
	if(in.order.orderState.equals(OrderState.InPendencyRecording.toString())){
		in.showChild = 4;
		}
	if(in.order.orderState.equals(OrderState.Recording.toString())){
		in.showChild = 4;
		}
	in.currentChild = in.showChild;
}
' #txt
Hd0 f7 type ch.soreco.orderbook.ui.Order #txt
Hd0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set open Accordion</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f7 318 628 36 24 20 -2 #rect
Hd0 f7 @|StepIcon #fIcon
Hd0 f8 expr out #txt
Hd0 f8 336 652 336 684 #arcP
Hd0 f3 type ch.soreco.orderbook.ui.Order #txt
Hd0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>matched?</name>
        <nameStyle>8
</nameStyle>
        <desc>no match =&gt; no contact</desc>
    </language>
</elementInfo>
' #txt
Hd0 f3 322 242 28 28 14 0 #rect
Hd0 f3 @|AlternativeIcon #fIcon
Hd0 f11 expr out #txt
Hd0 f11 336 220 336 242 #arcP
Hd0 f12 expr in #txt
Hd0 f12 outCond 'in.order.matchedToOrder > 0 && in.order.contacts.size()>0' #txt
Hd0 f12 6 #arcStyle
Hd0 f12 336 270 336 300 #arcP
Hd0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>23.03.2011 / PB
don''t display contact and recording 
when 1st contact didn''t happen yet (1st contact after BPMatch)

</name>
        <nameStyle>117
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f13 514 258 60 60 -22 -22 #rect
Hd0 f13 @|IBIcon #fIcon
Hd0 f13 -1|-1|-16777216 #nodeStyle
Hd0 f14 type ch.soreco.orderbook.ui.Order #txt
Hd0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isAdressmuation?</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f14 322 354 28 28 14 -20 #rect
Hd0 f14 @|AlternativeIcon #fIcon
Hd0 f16 expr out #txt
Hd0 f16 336 324 336 354 #arcP
Hd0 f18 expr in #txt
Hd0 f18 outCond in.order.orderType.equals(ch.soreco.orderbook.enums.OrderType.Addressmutation.toString()) #txt
Hd0 f18 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f18 6 #arcStyle
Hd0 f18 350 368 400 428 #arcP
Hd0 f18 1 400 368 #addKink
Hd0 f18 0 1.0 12 8 #arcLabel
Hd0 f19 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f19 actionTable 'out=in;
' #txt
Hd0 f19 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClassHtml;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.webbies.common.html.form.ClassConverter;
import ch.soreco.orderbook.bo.RecordingMeta;
import ch.soreco.orderbook.ui.enums.ViewType;

Boolean canModify = in.modify;
StringBuffer html = new StringBuffer("");
html.append("<h3><%=ivy.cms.co(\"/Content/Shared/Labels/Recording\")%></h3>");
html.append("<div id=\"Recording\" width=\"100%\">");				
		
html.append("<fieldset id=\"recordingMetas\">");
html.append("<legend>Order-Typ</legend>");	

int k=0;
for(RecordingMeta meta : in.recordingMetas){
	AttributeClassHtml ach = new AttributeClassHtml(in.genericView, meta, "recordingMetas", k);
	html.append(ach.toHtml());
	k=k+1;	
}
if(in.eViewType.equals(ViewType.CHECK) || in.eViewType.equals(ViewType.EDIT)){
	String appender ="<a class=''asButton objectAppender'' forObject=''RecordingMeta'' href=''<%=ivy.html.ref(\"addNew.ivp\")%>&newObject=RecordingMeta&i=''>weitere</a>";
	html.append(appender);
}
html.append("</fieldset><!--RecordingMeta-->");						
html.append("</div><!--/Recording-->");
	
out.htmlBuild.append(html);' #txt
Hd0 f19 type ch.soreco.orderbook.ui.Order #txt
Hd0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build Erfassung</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f19 254 428 36 24 20 -2 #rect
Hd0 f19 @|StepIcon #fIcon
Hd0 f20 expr in #txt
Hd0 f20 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f20 3 #arcStyle
Hd0 f20 322 368 272 428 #arcP
Hd0 f20 1 272 368 #addKink
Hd0 f20 0 1.0 -8 5 #arcLabel
Hd0 f22 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Hd0 f22 actionTable 'out=in;
out.genericView=in.order.orderType+"_"+in.eViewType.toString();
' #txt
Hd0 f22 type ch.soreco.orderbook.ui.Order #txt
Hd0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set generic View: &lt;OrderType&gt;_&lt;ViewType&gt;</name>
        <nameStyle>40
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f22 318 140 36 24 20 -2 #rect
Hd0 f22 @|StepIcon #fIcon
Hd0 f24 expr out #txt
Hd0 f24 336 116 336 140 #arcP
Hd0 f2 expr out #txt
Hd0 f2 336 164 336 196 #arcP
Hd0 f30 expr out #txt
Hd0 f30 336 708 336 755 #arcP
Hd0 f10 type ch.soreco.orderbook.ui.Order #txt
Hd0 f10 322 498 28 28 14 0 #rect
Hd0 f10 @|AlternativeIcon #fIcon
Hd0 f26 expr out #txt
Hd0 f26 400 452 350 512 #arcP
Hd0 f26 1 400 512 #addKink
Hd0 f26 0 0.9785684671787076 0 0 #arcLabel
Hd0 f9 expr out #txt
Hd0 f9 272 452 322 512 #arcP
Hd0 f9 1 272 512 #addKink
Hd0 f9 0 0.9785684671787076 0 0 #arcLabel
Hd0 f27 type ch.soreco.orderbook.ui.Order #txt
Hd0 f27 322 562 28 28 14 0 #rect
Hd0 f27 @|AlternativeIcon #fIcon
Hd0 f28 expr in #txt
Hd0 f28 336 526 336 562 #arcP
Hd0 f21 expr in #txt
Hd0 f21 336 590 336 628 #arcP
Hd0 f29 expr in #txt
Hd0 f29 3 #arcStyle
Hd0 f29 350 256 350 576 #arcP
Hd0 f29 1 512 256 #addKink
Hd0 f29 2 512 576 #addKink
Hd0 f29 1 0.1960607296248131 0 0 #arcLabel
>Proto Hd0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Hd0 .processKind CALLABLE_SUB #txt
>Proto Hd0 0 0 32 24 18 0 #rect
>Proto Hd0 @|BIcon #fIcon
Hd0 f0 mainOut f5 tail #connect
Hd0 f5 head f4 mainIn #connect
Hd0 f7 mainOut f8 tail #connect
Hd0 f8 head f25 mainIn #connect
Hd0 f17 mainOut f11 tail #connect
Hd0 f11 head f3 in #connect
Hd0 f3 out f12 tail #connect
Hd0 f12 head f6 mainIn #connect
Hd0 f6 mainOut f16 tail #connect
Hd0 f16 head f14 in #connect
Hd0 f14 out f18 tail #connect
Hd0 f18 head f23 mainIn #connect
Hd0 f14 out f20 tail #connect
Hd0 f20 head f19 mainIn #connect
Hd0 f4 mainOut f24 tail #connect
Hd0 f24 head f22 mainIn #connect
Hd0 f22 mainOut f2 tail #connect
Hd0 f2 head f17 mainIn #connect
Hd0 f25 mainOut f30 tail #connect
Hd0 f30 head f1 mainIn #connect
Hd0 f23 mainOut f26 tail #connect
Hd0 f26 head f10 in #connect
Hd0 f19 mainOut f9 tail #connect
Hd0 f9 head f10 in #connect
Hd0 f10 out f28 tail #connect
Hd0 f28 head f27 in #connect
Hd0 f27 out f21 tail #connect
Hd0 f21 head f7 mainIn #connect
Hd0 f3 out f29 tail #connect
Hd0 f29 head f27 in #connect
