[Ivy]
[>Created: Wed Jul 20 16:26:25 CEST 2011]
130947B4C2F4A24C 3.15 #module
>Proto >Proto Collection #zClass
Ot0 OrderScanningsGet Big #zClass
Ot0 B #cInfo
Ot0 #process
Ot0 @TextInP .resExport .resExport #zField
Ot0 @TextInP .type .type #zField
Ot0 @TextInP .processKind .processKind #zField
Ot0 @AnnotationInP-0n ai ai #zField
Ot0 @TextInP .xml .xml #zField
Ot0 @TextInP .responsibility .responsibility #zField
Ot0 @StartSub f0 '' #zField
Ot0 @EndSub f1 '' #zField
Ot0 @Page f2 '' #zField
Ot0 @CallSub f3 '' #zField
Ot0 @PushWFArc f4 '' #zField
Ot0 @GridStep f5 '' #zField
Ot0 @PushWFArc f6 '' #zField
Ot0 @PushWFArc f7 '' #zField
Ot0 @CallSub f8 '' #zField
Ot0 @PushWFArc f9 '' #zField
>Proto Ot0 Ot0 OrderScanningsGet #zField
Ot0 f0 inParamDecl '<java.lang.Integer orderId> param;' #txt
Ot0 f0 inParamTable 'out.orderId=param.orderId;
' #txt
Ot0 f0 outParamDecl '<> result;
' #txt
Ot0 f0 actionDecl 'ch.soreco.orderbook.ui.OrderScannings out;
' #txt
Ot0 f0 callSignature call(Integer) #txt
Ot0 f0 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Integer)</name>
    </language>
</elementInfo>
' #txt
Ot0 f0 87 37 26 26 14 0 #rect
Ot0 f0 @|StartSubIcon #fIcon
Ot0 f1 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f1 87 287 26 26 14 0 #rect
Ot0 f1 @|EndSubIcon #fIcon
Ot0 f2 outTypes "ch.soreco.orderbook.ui.OrderScannings" #txt
Ot0 f2 outLinks "showFile.ivp" #txt
Ot0 f2 template "ScanningTabs.jsp" #txt
Ot0 f2 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f2 skipLink skip.ivp #txt
Ot0 f2 sortLink sort.ivp #txt
Ot0 f2 templateWizard '#
#Wed Jun 15 21:28:24 CEST 2011
' #txt
Ot0 f2 pageArchivingActivated false #txt
Ot0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Scanning Tabs</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Ot0 f2 @C|.responsibility Everybody #txt
Ot0 f2 86 220 36 24 20 -2 #rect
Ot0 f2 @|PageIcon #fIcon
Ot0 f3 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f3 processCall 'Functional Processes/scanning/Scanning:getScanningList(Boolean,Integer)' #txt
Ot0 f3 doCall true #txt
Ot0 f3 requestActionDecl '<java.lang.Boolean loadFiles,java.lang.Integer OrderId> param;
' #txt
Ot0 f3 requestMappingAction 'param.loadFiles=false;
param.OrderId=in.orderId;
' #txt
Ot0 f3 responseActionDecl 'ch.soreco.orderbook.ui.OrderScannings out;
' #txt
Ot0 f3 responseMappingAction 'out=in;
out.scannings=result.Scannings;
' #txt
Ot0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Scannings</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Ot0 f3 86 92 36 24 20 -2 #rect
Ot0 f3 @|CallSubIcon #fIcon
Ot0 f4 expr out #txt
Ot0 f4 100 62 103 92 #arcP
Ot0 f5 actionDecl 'ch.soreco.orderbook.ui.OrderScannings out;
' #txt
Ot0 f5 actionTable 'out=in;
out.tabCount=in.scannings.size();
out.tabIndex=0;
' #txt
Ot0 f5 actionCode 'import ch.soreco.orderbook.bo.Scanning;

StringBuilder tabs = new StringBuilder("");
StringBuilder divs = new StringBuilder("");
StringBuilder iframe = new StringBuilder("");
tabs.append("<ul>");
int i = 0;
for(Scanning scan:in.scannings){
	tabs.append("<li><a href=\"#tab"+i+"\">"+scan.docType+"-"+scan.docNo+"</a></li>");
	divs.append("<div id=\"tab"+i+"\" style=\"display: none; height: auto\"></div>");
	i++;
	}
tabs.append("</ul>");

iframe.append("<div id=\"iframeDiv\" height=\"100%\">");
iframe.append("<div id=\"scanFile\" height=\"100%\">");
iframe.append("<iframe id=\"tFrame\" name=\"tFrameName\" src=\"\" width=\"100%\" height=\"100%\"></iframe>");
iframe.append("</div>");
iframe.append("</div>");

in.html = tabs.toString()+divs.toString()+iframe.toString();

	
		
	


	
	
' #txt
Ot0 f5 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Build Tabs</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Ot0 f5 86 140 36 24 20 -2 #rect
Ot0 f5 @|StepIcon #fIcon
Ot0 f6 expr out #txt
Ot0 f6 104 116 104 140 #arcP
Ot0 f7 expr out #txt
Ot0 f7 104 164 104 220 #arcP
Ot0 f8 type ch.soreco.orderbook.ui.OrderScannings #txt
Ot0 f8 processCall 'Functional Processes/common/File:get(ch.soreco.common.bo.File)' #txt
Ot0 f8 doCall true #txt
Ot0 f8 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Ot0 f8 requestMappingAction 'param.filter.fileId=in.scannings.get(in.tabIndex).scanFileId;
' #txt
Ot0 f8 responseActionDecl 'ch.soreco.orderbook.ui.OrderScannings out;
' #txt
Ot0 f8 responseMappingAction 'out=in;
' #txt
Ot0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show File</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Ot0 f8 230 236 36 24 20 -2 #rect
Ot0 f8 @|CallSubIcon #fIcon
Ot0 f9 expr data #txt
Ot0 f9 outCond ivp=="showFile.ivp" #txt
Ot0 f9 122 234 230 246 #arcP
>Proto Ot0 .type ch.soreco.orderbook.ui.OrderScannings #txt
>Proto Ot0 .processKind CALLABLE_SUB #txt
>Proto Ot0 0 0 32 24 18 0 #rect
>Proto Ot0 @|BIcon #fIcon
Ot0 f0 mainOut f4 tail #connect
Ot0 f4 head f3 mainIn #connect
Ot0 f3 mainOut f6 tail #connect
Ot0 f6 head f5 mainIn #connect
Ot0 f5 mainOut f7 tail #connect
Ot0 f7 head f2 mainIn #connect
Ot0 f2 out f9 tail #connect
Ot0 f9 head f8 mainIn #connect
