[Ivy]
[>Created: Thu Jul 21 08:52:09 CEST 2011]
130922C052A1A722 3.15 #module
>Proto >Proto Collection #zClass
Se0 SplitPane Big #zClass
Se0 B #cInfo
Se0 #process
Se0 @TextInP .resExport .resExport #zField
Se0 @TextInP .type .type #zField
Se0 @TextInP .processKind .processKind #zField
Se0 @AnnotationInP-0n ai ai #zField
Se0 @TextInP .xml .xml #zField
Se0 @TextInP .responsibility .responsibility #zField
Se0 @StartSub f0 '' #zField
Se0 @EndSub f1 '' #zField
Se0 @GridStep f2 '' #zField
Se0 @PushWFArc f3 '' #zField
Se0 @PushWFArc f4 '' #zField
>Proto Se0 Se0 SplitPane #zField
Se0 f0 inParamDecl '<ch.soreco.common.template.functional.SplitPane splitPane> param;' #txt
Se0 f0 inParamTable 'out=param.splitPane;
' #txt
Se0 f0 outParamDecl '<java.lang.String generatedHTML> result;
' #txt
Se0 f0 outParamTable 'result.generatedHTML=in.generatedHTML;
' #txt
Se0 f0 actionDecl 'ch.soreco.common.template.functional.SplitPane out;
' #txt
Se0 f0 callSignature build(ch.soreco.common.template.functional.SplitPane) #txt
Se0 f0 type ch.soreco.common.template.functional.SplitPane #txt
Se0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build(SplitPane)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f0 139 59 26 26 14 0 #rect
Se0 f0 @|StartSubIcon #fIcon
Se0 f1 type ch.soreco.common.template.functional.SplitPane #txt
Se0 f1 139 315 26 26 14 0 #rect
Se0 f1 @|EndSubIcon #fIcon
Se0 f2 actionDecl 'ch.soreco.common.template.functional.SplitPane out;
' #txt
Se0 f2 actionTable 'out=in;
' #txt
Se0 f2 actionCode 'String html = "";

html += "<table border=\"0\" width=\"100%\" height=\"100%\">";
html += "<tr height=\"100%\">";
html += "<!-- Panel1 START -->";
html += "	<td valign=\"top\" height=\"100%\" width=\""+in.balance.split("/").get(0)+"%\">"+in.panel1+"</td>";
html += "<!-- Panel1 END -->";
html += "<!-- Panel2 START -->";
html += "	<td valign=\"top\" width=\""+in.balance.split("/").get(1)+"%\">"+in.panel2+"</td>";
html += "<!-- Panel2 END -->";
html += "</tr>";
html += "</table>";

out.generatedHTML = html;' #txt
Se0 f2 type ch.soreco.common.template.functional.SplitPane #txt
Se0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>vertical splitpane</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Se0 f2 134 164 36 24 20 -2 #rect
Se0 f2 @|StepIcon #fIcon
Se0 f3 expr out #txt
Se0 f3 152 85 152 164 #arcP
Se0 f4 expr out #txt
Se0 f4 152 188 152 315 #arcP
>Proto Se0 .type ch.soreco.common.template.functional.SplitPane #txt
>Proto Se0 .processKind CALLABLE_SUB #txt
>Proto Se0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Se0 0 0 32 24 18 0 #rect
>Proto Se0 @|BIcon #fIcon
Se0 f0 mainOut f3 tail #connect
Se0 f3 head f2 mainIn #connect
Se0 f2 mainOut f4 tail #connect
Se0 f4 head f1 mainIn #connect
