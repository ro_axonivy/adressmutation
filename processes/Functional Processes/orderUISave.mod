[Ivy]
[>Created: Mon Sep 12 09:24:58 CEST 2011]
12C023378E85EA85 3.15 #module
>Proto >Proto Collection #zClass
oe0 orderUISave Big #zClass
oe0 B #cInfo
oe0 #process
oe0 @TextInP .resExport .resExport #zField
oe0 @TextInP .type .type #zField
oe0 @TextInP .processKind .processKind #zField
oe0 @AnnotationInP-0n ai ai #zField
oe0 @TextInP .xml .xml #zField
oe0 @TextInP .responsibility .responsibility #zField
oe0 @StartSub f0 '' #zField
oe0 @EndSub f1 '' #zField
oe0 @GridStep f24 '' #zField
oe0 @CallSub f16 '' #zField
oe0 @GridStep f21 '' #zField
oe0 @PushWFArc f3 '' #zField
oe0 @GridStep f4 '' #zField
oe0 @GridStep f7 '' #zField
oe0 @PushWFArc f8 '' #zField
oe0 @PushWFArc f5 '' #zField
oe0 @GridStep f9 '' #zField
oe0 @PushWFArc f10 '' #zField
oe0 @GridStep f12 '' #zField
oe0 @PushWFArc f13 '' #zField
oe0 @GridStep f36 '' #zField
oe0 @PushWFArc f2 '' #zField
oe0 @Alternative f15 '' #zField
oe0 @PushWFArc f17 '' #zField
oe0 @PushWFArc f14 '' #zField
oe0 @GridStep f19 '' #zField
oe0 @PushWFArc f20 '' #zField
oe0 @PushWFArc f18 '' #zField
oe0 @GridStep f22 '' #zField
oe0 @PushWFArc f23 '' #zField
oe0 @PushWFArc f6 '' #zField
oe0 @GridStep f25 '' #zField
oe0 @PushWFArc f26 '' #zField
oe0 @GridStep f27 '' #zField
oe0 @PushWFArc f28 '' #zField
oe0 @GridStep f29 '' #zField
oe0 @PushWFArc f30 '' #zField
oe0 @PushWFArc f11 '' #zField
>Proto oe0 oe0 orderUISave #zField
oe0 f0 inParamDecl '<ch.soreco.orderbook.ui.Order orderUI> param;' #txt
oe0 f0 inParamTable 'out=param.orderUI;
' #txt
oe0 f0 outParamDecl '<ch.soreco.orderbook.ui.Order orderUI> result;
' #txt
oe0 f0 outParamTable 'result.orderUI=in;
' #txt
oe0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f0 callSignature call(ch.soreco.orderbook.ui.Order) #txt
oe0 f0 type ch.soreco.orderbook.ui.Order #txt
oe0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
    </language>
</elementInfo>
' #txt
oe0 f0 211 11 26 26 14 0 #rect
oe0 f0 @|StartSubIcon #fIcon
oe0 f1 type ch.soreco.orderbook.ui.Order #txt
oe0 f1 211 827 26 26 14 0 #rect
oe0 f1 @|EndSubIcon #fIcon
oe0 f24 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f24 actionTable 'out=in;
' #txt
oe0 f24 actionCode 'import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.Paper;

//clear papers
for(Contact contact:in.order.contacts){
	contact.papers.clear();
	for(Paper paper:in.orderContactPapers){
		if(contact.contactId ==paper.contactId){
			if(paper.isPaper) {
				contact.papers.add(paper);
				}
			else {
				if(paper.paperId>0){
					ivy.persistence.Adressmutation.remove(paper);
					}
				}
			}
		}
	}
' #txt
oe0 f24 type ch.soreco.orderbook.ui.Order #txt
oe0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Papers back</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f24 206 500 36 24 20 -2 #rect
oe0 f24 @|StepIcon #fIcon
oe0 f16 type ch.soreco.orderbook.ui.Order #txt
oe0 f16 processCall 'Functional Processes/orderbook/Order:set(ch.soreco.orderbook.bo.Order)' #txt
oe0 f16 doCall true #txt
oe0 f16 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
oe0 f16 requestMappingAction 'param.order=in.order;
' #txt
oe0 f16 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f16 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
oe0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save</name>
        <nameStyle>4
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f16 206 756 36 24 20 -2 #rect
oe0 f16 @|CallSubIcon #fIcon
oe0 f21 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f21 actionTable 'out=in;
out.order.contacts=in.orderContacts;
' #txt
oe0 f21 type ch.soreco.orderbook.ui.Order #txt
oe0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map contacts back</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f21 206 220 36 24 20 -2 #rect
oe0 f21 @|StepIcon #fIcon
oe0 f3 expr out #txt
oe0 f3 224 780 224 827 #arcP
oe0 f4 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f4 actionTable 'out=in;
' #txt
oe0 f4 actionCode 'import ch.soreco.orderbook.bo.RecordingLSV;
in.order.recording.lsv.clear();
for(RecordingLSV lsv :in.recordingLSV){
	if(lsv.isLSV) {
			lsv.recording = in.order.recording;
			in.order.recording.lsv.add(lsv);
		}
			else {
				if(lsv.lsvId>0){
					ivy.persistence.Adressmutation.remove(lsv);
					}
				}
	}' #txt
oe0 f4 type ch.soreco.orderbook.ui.Order #txt
oe0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map LSV Back</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f4 206 596 36 24 20 -2 #rect
oe0 f4 @|StepIcon #fIcon
oe0 f7 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f7 actionTable 'out=in;
' #txt
oe0 f7 actionCode 'import ch.soreco.orderbook.bo.DomicileOrOwner;

in.order.recording.domicileOrOwner.clear();
for(DomicileOrOwner doo :in.recordingDomicileOrOwners){
	if(doo.isAddressmutation){
			doo.recording = in.order.recording;
			in.order.recording.domicileOrOwner.add(doo);
		}
	else {
				if(doo.domicileOrOwnerId>0){
					ivy.persistence.Adressmutation.remove(doo);
					}
				}
	}' #txt
oe0 f7 type ch.soreco.orderbook.ui.Order #txt
oe0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map DomicileOrOwners back</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f7 206 548 36 24 20 -2 #rect
oe0 f7 @|StepIcon #fIcon
oe0 f8 expr out #txt
oe0 f8 224 524 224 548 #arcP
oe0 f5 expr out #txt
oe0 f5 224 572 224 596 #arcP
oe0 f9 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f9 actionTable 'out=in;
' #txt
oe0 f9 actionCode 'import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Contact;


//clear papers
for(Contact contact:in.order.contacts){
	contact.signatures.clear();
	for(SignatureRequest sign:in.orderContactsSignatureRequests){
		if(contact.contactId ==sign.contactId){
			if(sign.isRequested) {
				contact.signatures.add(sign);
				}
			else {
				if(sign.signatureRequestId>0){
					ivy.persistence.Adressmutation.remove(sign);
					}
				}
			}
		}
	}
' #txt
oe0 f9 type ch.soreco.orderbook.ui.Order #txt
oe0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Signature Requests back</name>
        <nameStyle>27
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f9 206 268 36 24 20 -2 #rect
oe0 f9 @|StepIcon #fIcon
oe0 f10 expr out #txt
oe0 f10 224 244 224 268 #arcP
oe0 f12 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f12 actionTable 'out=in;
' #txt
oe0 f12 actionCode 'import ch.soreco.orderbook.bo.CorrespondenceContainer;
List<CorrespondenceContainer> containers= new List<CorrespondenceContainer>();
for(CorrespondenceContainer cont:in.recordingCorrespondenceContainers) {
	if(cont.change||cont.spedit) {
		containers.add(cont);
		}
	else {
		if(cont.correspondenceContainerId>0){
			ivy.persistence.Adressmutation.remove(cont);
			}
		}
	}
in.order.recording.correspondence.correspondenceContainer = containers;
' #txt
oe0 f12 type ch.soreco.orderbook.ui.Order #txt
oe0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map correspondeceContainer Back</name>
        <nameStyle>31
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f12 206 644 36 24 20 -2 #rect
oe0 f12 @|StepIcon #fIcon
oe0 f13 expr out #txt
oe0 f13 224 620 224 644 #arcP
oe0 f36 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f36 actionTable 'out=in;
' #txt
oe0 f36 actionCode 'import ch.soreco.orderbook.bo.CorrespondenceContainer;
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Paper;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.RecordingLSV;
import ch.soreco.orderbook.bo.DomicileOrOwner;

String user = ivy.session.getSessionUserName();

//contacts
for(Contact contact : in.orderContacts){
	if(contact.#contactIsChecked || contact.#contactIsChecked == false){
		contact.contactCheckedBy = user;
		}
	if(contact.address.#addressIsChecked || contact.address.#addressIsChecked  == false){
		contact.address.addressCheckedBy = user;
		}
	}
for(Paper paper :in.orderContactPapers){
	Paper paperClone = paper.clone();
	if(paperClone.#paperIsChecked || paperClone.#paperIsChecked == false){
		paper.paperCheckedBy = user;
		}
	}
for(SignatureRequest sign:in.orderContactsSignatureRequests){
	SignatureRequest signClone = sign.clone();
	if(signClone.#signatureIsChecked || signClone.#signatureIsChecked == false){
		sign.signatureCheckedBy = user;
		}
	}
//recording
if(in.order.recording.affirmation.#affirmationIsChecked || in.order.recording.affirmation.#affirmationIsChecked == false){
	in.order.recording.affirmation.affirmationCheckedBy = user;
	}
if(in.order.recording.businessPartner.#businessPartnerIsChecked || in.order.recording.businessPartner.#businessPartnerIsChecked == false){
	in.order.recording.businessPartner.businessPartnerCheckedBy = user;
	}
if(in.order.recording.correspondence.#changeAddressIsChecked || in.order.recording.correspondence.#changeAddressIsChecked == false){
	in.order.recording.correspondence.changeAddressCheckedBy = user;
	}
if(in.order.recording.correspondence.#newAddressIsChecked || in.order.recording.correspondence.#newAddressIsChecked == false){
	in.order.recording.correspondence.newAdressCheckedBy = user;
	}
for(DomicileOrOwner domi:in.recordingDomicileOrOwners){
	if(domi.#domicileOrOwnerIsChecked || domi.#domicileOrOwnerIsChecked == false){
		domi.domicileOrOwnerCheckedBy = user;
		}
	}
for(RecordingLSV lsv :in.order.recording.lsv){
	if(lsv.#LSVIsChecked || lsv.#LSVIsChecked == false){
		lsv.LSVCheckedBy = user;
		}
	}
for(CorrespondenceContainer cont:in.recordingCorrespondenceContainers){
	if(cont.#containerIsChecked || cont.#containerIsChecked == false){
		cont.containerCheckedBy = user;
		}
	}' #txt
oe0 f36 type ch.soreco.orderbook.ui.Order #txt
oe0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set checked by</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f36 310 148 36 24 20 -2 #rect
oe0 f36 @|StepIcon #fIcon
oe0 f2 expr out #txt
oe0 f2 311 172 241 220 #arcP
oe0 f15 type ch.soreco.orderbook.ui.Order #txt
oe0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isCheck?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f15 210 74 28 28 14 0 #rect
oe0 f15 @|AlternativeIcon #fIcon
oe0 f17 expr out #txt
oe0 f17 224 37 224 74 #arcP
oe0 f14 expr in #txt
oe0 f14 outCond in.eViewType.equals(ch.soreco.orderbook.ui.enums.ViewType.CHECK) #txt
oe0 f14 232 94 311 148 #arcP
oe0 f19 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f19 actionTable 'out=in;
' #txt
oe0 f19 actionCode '
' #txt
oe0 f19 type ch.soreco.orderbook.ui.Order #txt
oe0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set checked by BPId</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f19 102 148 36 24 20 -2 #rect
oe0 f19 @|StepIcon #fIcon
oe0 f20 expr in #txt
oe0 f20 216 94 137 148 #arcP
oe0 f18 expr out #txt
oe0 f18 137 172 207 220 #arcP
oe0 f22 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f22 actionTable 'out=in;
' #txt
oe0 f22 actionCode 'import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.Paper;

// Contact checks
int i = 0;
for(Contact contact : in.orderContacts){
	
	if(contact.address.addressCheckedBy.equals("")){
		contact.address.addressIsChecked = null;

	}
	if(contact.contactCheckedBy.equals("")){
		contact.contactIsChecked = null;
	}
	
	in.orderContacts.set(i,contact);
	i++;
}
	



// Papers
int p = 0;
for(Paper paper : in.orderContactPapers){
		if(paper.paperCheckedBy.equals("")){
			paper.paperIsChecked = null;	
		}
		in.orderContactPapers.set(p,paper);
		p++;
	}
	


// Signatures
int o = 0;
for(SignatureRequest sigReq : in.orderContactsSignatureRequests){
	
	if(sigReq.signatureCheckedBy.equals("")){
		sigReq.signatureIsChecked = null;	
	}
	in.orderContactsSignatureRequests.set(o,sigReq);
	o++;
}

// Business Partner checks
if(in.order.recording.businessPartner.businessPartnerCheckedBy.equals("")){
	in.order.recording.businessPartner.businessPartnerIsChecked = null;
}

// Correspondence checks
if(in.order.recording.correspondence.changeAddressCheckedBy.equals("")){
	in.order.recording.correspondence.changeAddressIsChecked = null;
}
if(in.order.recording.correspondence.newAdressCheckedBy.equals("")){
	in.order.recording.correspondence.newAddressIsChecked = null;
}

if(in.order.recording.affirmation.affirmationCheckedBy.equalsIgnoreCase("")){
	in.order.recording.affirmation.affirmationIsChecked = null;
}

' #txt
oe0 f22 type ch.soreco.orderbook.ui.Order #txt
oe0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>[Workaround]
reset checks</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f22 206 700 36 24 20 -2 #rect
oe0 f22 @|StepIcon #fIcon
oe0 f22 -4942081|-1|-16777216 #nodeStyle
oe0 f23 expr out #txt
oe0 f23 224 668 224 700 #arcP
oe0 f6 expr out #txt
oe0 f6 224 724 224 756 #arcP
oe0 f25 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f25 actionTable 'out=in;
' #txt
oe0 f25 actionCode 'import ch.soreco.orderbook.enums.LetterType;
import ch.soreco.orderbook.bo.Letter;
import ch.soreco.orderbook.bo.Contact;



for(Contact contact:in.order.contacts){
	//clear letters
	contact.letters.clear();
	for(Letter letter:in.orderContactsLetters){
		if(contact.contactId ==letter.contactId){
			if(!letter.letterType.trim().equals(LetterType.UNDEFINED.toString())) {
				contact.letters.add(letter);
				}
			else {
				if(letter.id>0){
					ivy.persistence.Adressmutation.remove(letter);
					}
				}
			}
		}
	}
' #txt
oe0 f25 type ch.soreco.orderbook.ui.Order #txt
oe0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Letters back</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f25 206 324 36 24 20 -2 #rect
oe0 f25 @|StepIcon #fIcon
oe0 f26 expr out #txt
oe0 f26 224 292 224 324 #arcP
oe0 f27 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f27 actionTable 'out=in;
' #txt
oe0 f27 actionCode 'import ch.soreco.orderbook.enums.RecordingMetaType;
import ch.soreco.orderbook.bo.RecordingMeta;

in.order.recording.metaTypes.clear();
for(RecordingMeta meta :in.recordingMetas){
	if(!meta.recordingMetaType.trim().equals(RecordingMetaType.DEFAULT.toString())){
			meta.recording = in.order.recording;
			in.order.recording.metaTypes.add(meta);
		}
	else {
		ivy.log.debug("Deleting Recording Meta Type "+meta.id);
				if(meta.id>0){
					ivy.persistence.Adressmutation.remove(meta);
					}
			}
	}
in.recordingMetas.clear();' #txt
oe0 f27 type ch.soreco.orderbook.ui.Order #txt
oe0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Recording Meta back</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
oe0 f27 206 372 36 24 20 -2 #rect
oe0 f27 @|StepIcon #fIcon
oe0 f28 expr out #txt
oe0 f28 224 348 224 372 #arcP
oe0 f29 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
oe0 f29 actionTable 'out=in;
' #txt
oe0 f29 actionCode 'import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.Iterator;
import ch.soreco.orderbook.bo.PendencyTicket;

in.order.pendencyTickets.clear();
List<Date> dates = new List<Date>();

HashMap pendencyReminder = new HashMap();

for(PendencyTicket ticket :in.orderPendencyTickets){
        if(!ticket.pendencyTicketType.equals("")){
                        ticket.order = in.order;
                        if(ticket.id==0){
                                ticket.createDateTime = new DateTime();
                                ticket.createUser = ivy.session.getSessionUser().getName();
                                }
                        //dates.add(ticket.dueDate);
                        if(!ticket.solved){
                                //dates.add(ticket.remindAt);
																pendencyReminder.put(ticket.remindAt,ticket.pendencyTicketType);
                        }
                        in.order.pendencyTickets.add(ticket);
                }
        else {
                                if(ticket.id>0){
                                        ivy.persistence.Adressmutation.remove(ticket);
                                        }
                                }
        }

if(pendencyReminder.size() > 0){
	SortedSet sortedset= new TreeSet(pendencyReminder.keySet());
	Iterator it = sortedset.iterator();
	Date nextDate = it.next() as Date;
	String ticketType = pendencyReminder.get(nextDate) as String;
	in.order.nextDueDate = nextDate;
	in.order.pendencyTicketType = ticketType;
} else {
	in.order.nextDueDate = null;
	in.order.pendencyTicketType = null;
}

/*
Collections.sort(dates);
if(dates.size()>0){
	in.order.nextDueDate = dates.get(0);
}
else {
	in.order.nextDueDate = null;
}*/' #txt
oe0 f29 type ch.soreco.orderbook.ui.Order #txt
oe0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Pendency Tickets back</name>
        <nameStyle>25
</nameStyle>
        <desc>02.09.2011 PB: added pendencyTicketType to Order, sort by date

-------------------------------------------------------------------------------------------------------------------------------------------------
OLD SOURCE:
import java.util.Collections;
import ch.soreco.orderbook.bo.PendencyTicket;

in.order.pendencyTickets.clear();
List&lt;Date&gt; dates = new List&lt;Date&gt;();

for(PendencyTicket ticket :in.orderPendencyTickets){
        if(!ticket.pendencyTicketType.equals(&quot;&quot;)){
                        ticket.order = in.order;
                        if(ticket.id==0){
                                ticket.createDateTime = new DateTime();
                                ticket.createUser = ivy.session.getSessionUser().getName();
                                }
                        //dates.add(ticket.dueDate);
                        if(!ticket.solved){
                                dates.add(ticket.remindAt);
                        }
                        in.order.pendencyTickets.add(ticket);
                }
        else {
                                if(ticket.id&gt;0){
                                        ivy.persistence.Adressmutation.remove(ticket);
                                        }
                                }
        }

Collections.sort(dates);
if(dates.size()&gt;0){
	in.order.nextDueDate = dates.get(0);
}
else {
	in.order.nextDueDate = null;
}</desc>
    </language>
</elementInfo>
' #txt
oe0 f29 206 420 36 24 20 -2 #rect
oe0 f29 @|StepIcon #fIcon
oe0 f30 expr out #txt
oe0 f30 224 396 224 420 #arcP
oe0 f11 expr out #txt
oe0 f11 224 444 224 500 #arcP
>Proto oe0 .type ch.soreco.orderbook.ui.Order #txt
>Proto oe0 .processKind CALLABLE_SUB #txt
>Proto oe0 0 0 32 24 18 0 #rect
>Proto oe0 @|BIcon #fIcon
oe0 f16 mainOut f3 tail #connect
oe0 f3 head f1 mainIn #connect
oe0 f24 mainOut f8 tail #connect
oe0 f8 head f7 mainIn #connect
oe0 f7 mainOut f5 tail #connect
oe0 f5 head f4 mainIn #connect
oe0 f21 mainOut f10 tail #connect
oe0 f10 head f9 mainIn #connect
oe0 f4 mainOut f13 tail #connect
oe0 f13 head f12 mainIn #connect
oe0 f36 mainOut f2 tail #connect
oe0 f2 head f21 mainIn #connect
oe0 f0 mainOut f17 tail #connect
oe0 f17 head f15 in #connect
oe0 f15 out f14 tail #connect
oe0 f14 head f36 mainIn #connect
oe0 f15 out f20 tail #connect
oe0 f20 head f19 mainIn #connect
oe0 f19 mainOut f18 tail #connect
oe0 f18 head f21 mainIn #connect
oe0 f12 mainOut f23 tail #connect
oe0 f23 head f22 mainIn #connect
oe0 f22 mainOut f6 tail #connect
oe0 f6 head f16 mainIn #connect
oe0 f9 mainOut f26 tail #connect
oe0 f26 head f25 mainIn #connect
oe0 f25 mainOut f28 tail #connect
oe0 f28 head f27 mainIn #connect
oe0 f27 mainOut f30 tail #connect
oe0 f30 head f29 mainIn #connect
oe0 f29 mainOut f11 tail #connect
oe0 f11 head f24 mainIn #connect
