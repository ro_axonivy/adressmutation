[Ivy]
[>Created: Mon Jun 06 16:32:16 CEST 2011]
12F966A731AAF10B 3.15 #module
>Proto >Proto Collection #zClass
Et0 Edit Big #zClass
Et0 B #cInfo
Et0 #process
Et0 @TextInP .resExport .resExport #zField
Et0 @TextInP .type .type #zField
Et0 @TextInP .processKind .processKind #zField
Et0 @AnnotationInP-0n ai ai #zField
Et0 @TextInP .xml .xml #zField
Et0 @TextInP .responsibility .responsibility #zField
Et0 @StartSub f0 '' #zField
Et0 @EndSub f1 '' #zField
Et0 @GridStep f2 '' #zField
Et0 @PushWFArc f3 '' #zField
Et0 @Page f4 '' #zField
Et0 @PushWFArc f5 '' #zField
Et0 @GridStep f6 '' #zField
Et0 @PushWFArc f7 '' #zField
Et0 @PushWFArc f8 '' #zField
>Proto Et0 Et0 Edit #zField
Et0 f0 inParamDecl '<java.lang.String uri> param;' #txt
Et0 f0 inParamTable 'out.uri=param.uri;
' #txt
Et0 f0 outParamDecl '<> result;
' #txt
Et0 f0 actionDecl 'ch.soreco.common.webbies.cms.ui.functional.ContentEdit out;
' #txt
Et0 f0 callSignature call(String) #txt
Et0 f0 type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
Et0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(String)</name>
    </language>
</elementInfo>
' #txt
Et0 f0 87 37 26 26 14 0 #rect
Et0 f0 @|StartSubIcon #fIcon
Et0 f1 type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
Et0 f1 87 287 26 26 14 0 #rect
Et0 f1 @|EndSubIcon #fIcon
Et0 f2 actionDecl 'ch.soreco.common.webbies.cms.ui.functional.ContentEdit out;
' #txt
Et0 f2 actionTable 'out=in;
out.type=ivy.cms.getContentObject(in.uri)
.getContentObjectType()
.getCoType().toString();
out.value=ivy.cms.co(in.uri);
' #txt
Et0 f2 type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
Et0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Value and Type</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f2 78 108 36 24 20 -2 #rect
Et0 f2 @|StepIcon #fIcon
Et0 f3 expr out #txt
Et0 f3 99 62 97 108 #arcP
Et0 f4 outTypes "ch.soreco.common.webbies.cms.ui.functional.ContentEdit" #txt
Et0 f4 outLinks "LinkA.ivp" #txt
Et0 f4 template "" #txt
Et0 f4 type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
Et0 f4 skipLink skip.ivp #txt
Et0 f4 sortLink sort.ivp #txt
Et0 f4 86 164 36 24 20 -2 #rect
Et0 f4 @|PageIcon #fIcon
Et0 f5 expr out #txt
Et0 f5 98 132 102 164 #arcP
Et0 f6 actionDecl 'ch.soreco.common.webbies.cms.ui.functional.ContentEdit out;
' #txt
Et0 f6 actionTable 'out=in;
' #txt
Et0 f6 actionCode 'import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.cm.IContentObject;
//get content object
IContentObject co = ivy.cms.getContentObject(in.uri);
IContentObjectValue value = co.getValue(new Date(),ivy.cms.getDefaultLanguage());
//as html page escapes
in.value = in.value.replace("<_%","<%").replace("%_>","%>");
value.setContent(in.value, ivy.session.getSessionUserName());
co.touch(ivy.session.getSessionUserName());
' #txt
Et0 f6 type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
Et0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>update value</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f6 86 236 36 24 20 -2 #rect
Et0 f6 @|StepIcon #fIcon
Et0 f7 expr data #txt
Et0 f7 outCond ivp=="LinkA.ivp" #txt
Et0 f7 104 188 104 236 #arcP
Et0 f8 expr out #txt
Et0 f8 103 260 100 287 #arcP
>Proto Et0 .type ch.soreco.common.webbies.cms.ui.functional.ContentEdit #txt
>Proto Et0 .processKind CALLABLE_SUB #txt
>Proto Et0 0 0 32 24 18 0 #rect
>Proto Et0 @|BIcon #fIcon
Et0 f0 mainOut f3 tail #connect
Et0 f3 head f2 mainIn #connect
Et0 f2 mainOut f5 tail #connect
Et0 f5 head f4 mainIn #connect
Et0 f4 out f7 tail #connect
Et0 f7 head f6 mainIn #connect
Et0 f6 mainOut f8 tail #connect
Et0 f8 head f1 mainIn #connect
