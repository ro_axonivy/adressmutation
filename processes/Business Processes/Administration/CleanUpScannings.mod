[Ivy]
[>Created: Thu Oct 16 11:05:32 CEST 2014]
13A9D300FBA1EA31 3.17 #module
>Proto >Proto Collection #zClass
Cs0 CleanUpScannings Big #zClass
Cs0 B #cInfo
Cs0 #process
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @StartRequest f0 '' #zField
Cs0 @GridStep f1 '' #zField
Cs0 @Page f3 '' #zField
Cs0 @EndTask f4 '' #zField
Cs0 @GridStep f7 '' #zField
Cs0 @DBStep f9 '' #zField
Cs0 @GridStep f10 '' #zField
Cs0 @PushWFArc f5 '' #zField
Cs0 @DBStep f14 '' #zField
Cs0 @Alternative f15 '' #zField
Cs0 @PushWFArc f8 '' #zField
Cs0 @GridStep f18 '' #zField
Cs0 @GridStep f21 '' #zField
Cs0 @PushWFArc f22 '' #zField
Cs0 @GridStep f23 '' #zField
Cs0 @Alternative f25 '' #zField
Cs0 @PushWFArc f19 '' #zField
Cs0 @GridStep f28 '' #zField
Cs0 @GridStep f29 '' #zField
Cs0 @PushWFArc f30 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @Alternative f31 '' #zField
Cs0 @PushWFArc f13 '' #zField
Cs0 @StartEvent f35 '' #zField
Cs0 @PushWFArc f36 '' #zField
Cs0 @PushWFArc f11 '' #zField
Cs0 @PushWFArc f17 '' #zField
Cs0 @PushWFArc f20 '' #zField
Cs0 @InfoButton f24 '' #zField
Cs0 @Alternative f42 '' #zField
Cs0 @PushWFArc f12 '' #zField
Cs0 @GridStep f44 '' #zField
Cs0 @PushWFArc f45 '' #zField
Cs0 @PushWFArc f43 '' #zField
Cs0 @PushWFArc f46 '' #zField
Cs0 @PushWFArc f47 '' #zField
Cs0 @ProcessException f6 '' #zField
Cs0 @PushWFArc f33 '' #zField
Cs0 @GridStep f37 '' #zField
Cs0 @PushWFArc f40 '' #zField
Cs0 @PushWFArc f39 '' #zField
Cs0 @PushWFArc f16 '' #zField
Cs0 @PushWFArc f32 '' #zField
Cs0 @GridStep f38 '' #zField
Cs0 @PushWFArc f41 '' #zField
Cs0 @PushWFArc f48 '' #zField
Cs0 @Alternative f49 '' #zField
Cs0 @GridStep f51 '' #zField
Cs0 @PushWFArc f52 '' #zField
Cs0 @PushWFArc f54 '' #zField
Cs0 @PushWFArc f53 '' #zField
Cs0 @PushWFArc f26 '' #zField
Cs0 @PushWFArc f50 '' #zField
Cs0 @PushWFArc f34 '' #zField
>Proto Cs0 Cs0 CleanUpScannings #zField
Cs0 f0 outLink start.ivp #txt
Cs0 f0 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f0 inParamDecl '<> param;' #txt
Cs0 f0 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f0 guid 13A9D301B2E668D9 #txt
Cs0 f0 requestEnabled true #txt
Cs0 f0 triggerEnabled false #txt
Cs0 f0 callSignature start() #txt
Cs0 f0 persist false #txt
Cs0 f0 taskData '#
#Wed Oct 31 15:13:38 CET 2012
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Cs0 f0 caseData '#
#Wed Oct 31 15:13:38 CET 2012
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Cs0 f0 showInStartList 0 #txt
Cs0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f0 @C|.responsibility Everybody #txt
Cs0 f0 131 35 26 26 14 0 #rect
Cs0 f0 @|StartRequestIcon #fIcon
Cs0 f1 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f1 actionTable 'out=in;
out.i=0;
' #txt
Cs0 f1 actionCode '/* set period */
String tmpPeriod = ivy.cms.co("/Content/Shared/Configuration/system/ScanningCleanUp/period");
out.cleanUpPeriod = tmpPeriod.split("/").get(0).toNumber();
out.periodType = tmpPeriod.split("/").get(1);

/* set time */
out.cleanUpTime = ivy.cms.co("/Content/Shared/Configuration/system/ScanningCleanUp/cleanUpTime").toDateTime();
/* set backup folder */
out.backupFolder = ivy.cms.co("/Content/Shared/Configuration/system/ScanningCleanUp/backupFolder");

/* set max Clean up Files */
out.maxFilesToCleanUp = ivy.cms.co("/Content/Shared/Configuration/system/ScanningCleanUp/MaxFilesToCleanUp").toNumber();' #txt
Cs0 f1 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f1 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get properties
from CMS</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f1 334 164 36 24 20 -2 #rect
Cs0 f1 @|StepIcon #fIcon
Cs0 f3 outTypes "ch.soreco.common.functional.ScanningCleanUp","ch.soreco.common.functional.ScanningCleanUp" #txt
Cs0 f3 outLinks "LinkProcessCleanup.ivp","ChangeConfiguration.ivp" #txt
Cs0 f3 template "/ProcessPages/CleanUpScannings/admin.ivc" #txt
Cs0 f3 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f3 skipLink skip.ivp #txt
Cs0 f3 sortLink sort.ivp #txt
Cs0 f3 templateWizard '#
#Tue Oct 22 11:36:02 CEST 2013
' #txt
Cs0 f3 pageArchivingActivated false #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show affected orders</name>
        <nameStyle>20,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f3 @C|.responsibility Everybody #txt
Cs0 f3 118 588 36 24 20 -2 #rect
Cs0 f3 @|PageIcon #fIcon
Cs0 f4 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>end</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f4 1091 419 26 26 14 0 #rect
Cs0 f4 @|EndIcon #fIcon
Cs0 f7 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f7 actionTable 'out=in;
out.i=0;
' #txt
Cs0 f7 actionCode 'out.qry = "
SELECT o.orderId, o.BPId, o.orderState,s.archivingState, o.orderType, o.orderEnd, s.archivingPreparedDate, s.scanFileId,  s.orderId FROM tbl_Orders o 
INNER JOIN tbl_Scanning s on o.scanningId = s.scanningId
INNER JOIN tbl_Files f on s.scanFileId = f.fileId
WHERE 
o.orderState in (''Archived'',''Merged'',''Aborted'',''Deleted'')
AND o.orderEnd <= DATEADD("+in.periodType+",-"+in.cleanUpPeriod+",GETDATE())
AND f.fileBlob IS NOT NULL
AND (
		select COUNT(o2.orderId) 
		from tbl_Orders o2 
		where o2.scanningId = o.scanningId 
		AND o2.orderState not in (''Archived'',''Merged'',''Aborted'',''Deleted'') 
) = 0
AND s.archivingState = ''IMTF_ARCHIVED''
order by o.orderEnd desc
";
' #txt
Cs0 f7 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build selection query</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f7 334 356 36 24 20 -2 #rect
Cs0 f7 @|StepIcon #fIcon
Cs0 f9 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f9 actionTable 'out=in;
out.ordersToCleanUpRS=recordset;
' #txt
Cs0 f9 dbExceptionId 13A9D300FBA1EA31-f6-buffer #txt
Cs0 f9 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''false''>in.qry</Verbatim></ANY_SQL>' #txt
Cs0 f9 dbUrl bzp_ivy_daten #txt
Cs0 f9 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Cs0 f9 dbWizard in.qry #txt
Cs0 f9 lotSize 2147483647 #txt
Cs0 f9 startIdx 0 #txt
Cs0 f9 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>execute query</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f9 334 412 36 24 20 -2 #rect
Cs0 f9 @|DBStepIcon #fIcon
Cs0 f10 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f10 actionTable 'out=in;
' #txt
Cs0 f10 actionCode 'out.currentFileId = in.ordersToCleanUpRS.getField(in.i,"scanFileId").toString().toNumber();
' #txt
Cs0 f10 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set current fileId</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f10 774 676 36 24 20 -2 #rect
Cs0 f10 @|StepIcon #fIcon
Cs0 f5 expr out #txt
Cs0 f5 352 380 352 412 #arcP
Cs0 f14 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f14 actionTable 'out=in;
' #txt
Cs0 f14 dbExceptionId 13A9D300FBA1EA31-f6-buffer #txt
Cs0 f14 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''true''>update tbl_Files set fileBlob = null
where fileId = in.currentFileId</Verbatim></ANY_SQL>' #txt
Cs0 f14 dbUrl bzp_ivy_daten #txt
Cs0 f14 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Cs0 f14 dbWizard 'update tbl_Files set fileBlob = null
where fileId = in.currentFileId' #txt
Cs0 f14 lotSize 2147483647 #txt
Cs0 f14 startIdx 0 #txt
Cs0 f14 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete fileBlob</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f14 774 772 36 24 20 -2 #rect
Cs0 f14 @|DBStepIcon #fIcon
Cs0 f15 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>foreach scanning</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f15 778 618 28 28 14 0 #rect
Cs0 f15 @|AlternativeIcon #fIcon
Cs0 f8 expr in #txt
Cs0 f8 outCond 'in.i <in.ordersToCleanUpSize' #txt
Cs0 f8 792 646 792 676 #arcP
Cs0 f18 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f18 actionTable 'out=in;
out.i=in.i + 1;
' #txt
Cs0 f18 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f18 934 772 36 24 20 -2 #rect
Cs0 f18 @|StepIcon #fIcon
Cs0 f21 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f21 actionTable 'out=in;
' #txt
Cs0 f21 actionCode 'import org.apache.commons.io.FileUtils;
import ch.soreco.webbies.common.db.BlobHelper;

// try to download the file to the backup folder and delete temporary ivy file
try{
	BlobHelper uploader = new BlobHelper("bzp_ivy_daten");
	File ivyFile = uploader.fileDownload(in.currentFileId);
	java.io.File javaFile = ivyFile.getJavaFile();
	java.io.File backupFile = new java.io.File(in.backupFolder+"/"+javaFile.getName());
	FileUtils.copyFile(javaFile,backupFile);
	ivyFile.delete();
} catch(Exception e){
	ivy.log.error("CleanUpScannings failed:" +e.getMessage());
}
' #txt
Cs0 f21 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>backup file</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f21 774 724 36 24 20 -2 #rect
Cs0 f21 @|StepIcon #fIcon
Cs0 f22 expr out #txt
Cs0 f22 792 700 792 724 #arcP
Cs0 f23 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f23 actionTable 'out=in;
' #txt
Cs0 f23 actionCode 'import org.apache.commons.io.FileUtils;
if(in.mode.equalsIgnoreCase("auto")){
	FileUtils.cleanDirectory(new java.io.File(in.backupFolder));
}
' #txt
Cs0 f23 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>cleanup backup folder</name>
        <nameStyle>21,7
</nameStyle>
        <desc>clean the folder if mode is &quot;auto&quot;</desc>
    </language>
</elementInfo>
' #txt
Cs0 f23 582 580 36 24 20 -2 #rect
Cs0 f23 @|StepIcon #fIcon
Cs0 f25 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual?</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f25 778 418 28 28 14 0 #rect
Cs0 f25 @|AlternativeIcon #fIcon
Cs0 f19 expr in #txt
Cs0 f19 outCond in.mode.equalsIgnoreCase("manual") #txt
Cs0 f19 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual</name>
        <nameStyle>6,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f19 2 #arcStyle
Cs0 f19 792 418 370 368 #arcP
Cs0 f19 1 792 368 #addKink
Cs0 f19 0 0.398876404494382 28 0 #arcLabel
Cs0 f28 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f28 actionTable 'out=in;
out.mode="auto";
' #txt
Cs0 f28 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set mode
&quot;auto&quot;</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f28 22 108 36 24 20 -2 #rect
Cs0 f28 @|StepIcon #fIcon
Cs0 f29 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f29 actionTable 'out=in;
out.mode="manual";
' #txt
Cs0 f29 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set mode
&quot;manual&quot;</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f29 126 108 36 24 20 -2 #rect
Cs0 f29 @|StepIcon #fIcon
Cs0 f30 expr out #txt
Cs0 f30 144 61 144 108 #arcP
Cs0 f2 expr out #txt
Cs0 f2 162 120 334 176 #arcP
Cs0 f2 0 0.40531260889424475 0 0 #arcLabel
Cs0 f31 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual?</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f31 338 514 28 28 -20 17 #rect
Cs0 f31 @|AlternativeIcon #fIcon
Cs0 f13 expr in #txt
Cs0 f13 outCond in.mode.equalsIgnoreCase("manual") #txt
Cs0 f13 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual</name>
        <nameStyle>6,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f13 2 #arcStyle
Cs0 f13 338 528 136 588 #arcP
Cs0 f13 1 136 528 #addKink
Cs0 f13 0 0.5209281222399197 0 -13 #arcLabel
Cs0 f35 outerBean "ch.ivyteam.ivy.process.eventstart.beans.AutoProcessStarterEventBean" #txt
Cs0 f35 beanConfig "60" #txt
Cs0 f35 outLink eventLink.ivp #txt
Cs0 f35 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>auto</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f35 @C|.responsibility Everybody #txt
Cs0 f35 27 35 26 26 14 0 #rect
Cs0 f35 @|StartEventIcon #fIcon
Cs0 f36 expr out #txt
Cs0 f36 40 61 40 108 #arcP
Cs0 f11 expr out #txt
Cs0 f11 792 748 792 772 #arcP
Cs0 f17 expr out #txt
Cs0 f17 810 784 934 784 #arcP
Cs0 f20 expr out #txt
Cs0 f20 952 772 806 632 #arcP
Cs0 f20 1 952 632 #addKink
Cs0 f20 0 0.7947856842990988 0 0 #arcLabel
Cs0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Der Prozess wird täglich zum konfigurierten Zeitpunkt einmal aufgerufen.
Es werden alle Scannings, der Aufträge im Status Archived, Merged, Aborted oder Deleted mit Enddatum kleiner gleich des konfigurierten Wertes (Default: kleiner als 4 Monate) gelesen.
Die Scannings werden jedoch nur selektiert, wenn alle abhängigen Aufträge ebenfalls einen der eben erwähnten Statis erreicht haben.
Anschliessend wird das konfigurierte Backup Verzeichnis geleert und die eben gelesenen Dokumente werden darin gespeichert.
Das Backup Verzeichnis wird nur beim automatischen Durchlauf geleert. Wird der Prozess manuell gestartet bleiben alle bereits bestehenden Dokumente im Verzeichnis bestehen.
Nach erfolgreichem Backup werden alle gelesenen Dokumente aus der Datenbank gelöscht.
</name>
        <nameStyle>770,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f24 42 842 1036 156 -487 -54 #rect
Cs0 f24 @|IBIcon #fIcon
Cs0 f24 -1|-1|-16777216 #nodeStyle
Cs0 f42 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f42 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>manual or time to cleanup?</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f42 338 274 28 28 0 17 #rect
Cs0 f42 @|AlternativeIcon #fIcon
Cs0 f12 expr in #txt
Cs0 f12 outCond 'in.mode.equalsIgnoreCase("manual") 
|| in.canCleanUp' #txt
Cs0 f12 352 302 352 356 #arcP
Cs0 f44 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f44 actionTable 'out=in;
' #txt
Cs0 f44 actionCode 'import java.text.SimpleDateFormat;
import java.util.Calendar;
import ch.soreco.webbies.common.cms.CMSUtils;

DateTime now = new DateTime();
Calendar today = Calendar.getInstance();
today.setTime(now.getDate());
today.set(Calendar.HOUR_OF_DAY, now.getHours());
today.set(Calendar.MINUTE, now.getMinutes());

int hour = in.cleanUpTime.getHours();
int minutes = in.cleanUpTime.getMinutes();
Calendar nextInvokation  = Calendar.getInstance();
nextInvokation.setTime(in.cleanUpTime.getDate());
nextInvokation.set(Calendar.HOUR_OF_DAY, hour);
nextInvokation.set(Calendar.MINUTE, minutes);

ivy.log.debug(">>> CleanUp Job: "+in.mode);
ivy.log.debug("--- CleanUp Job: Invocation Time:"+nextInvokation.getTime().format("dd.MM.yyyy HH:mm"));
ivy.log.debug("--- CleanUp Job: DAY_OF_YEAR: "+today.get(Calendar.DAY_OF_YEAR)+":"+nextInvokation.get(Calendar.DAY_OF_YEAR));
ivy.log.debug("--- CleanUp Job: HOUR_OF_DAY: "+today.get(Calendar.HOUR_OF_DAY)+":"+nextInvokation.get(Calendar.HOUR_OF_DAY));
ivy.log.debug("--- CleanUp Job: MINUTE: "+today.get(Calendar.MINUTE)+":"+nextInvokation.get(Calendar.MINUTE));
	
if(today.get(Calendar.YEAR) == nextInvokation.get(Calendar.YEAR)
	&&today.get(Calendar.DAY_OF_YEAR) == nextInvokation.get(Calendar.DAY_OF_YEAR) 
		&& today.get(Calendar.HOUR_OF_DAY) == nextInvokation.get(Calendar.HOUR_OF_DAY)
		&& today.get(Calendar.MINUTE) == nextInvokation.get(Calendar.MINUTE)){
	out.canCleanUp = true;
	nextInvokation.add(Calendar.DATE,1);
	ivy.log.info(">>> CleanUp Job: New Invocation Time at:"+nextInvokation.getTime().format("dd.MM.yyyy HH:mm"));
	CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","cleanUpTime",nextInvokation.getTime().format("dd.MM.yyyy HH:mm"));
} else {
	ivy.log.debug(">>> CleanUp Job: Not to process as is not at invocation time.");
	out.canCleanUp = false;
}' #txt
Cs0 f44 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f44 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check and update time
for next invocation</name>
        <nameStyle>41,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f44 334 220 36 24 20 -2 #rect
Cs0 f44 @|StepIcon #fIcon
Cs0 f45 expr out #txt
Cs0 f45 352 188 352 220 #arcP
Cs0 f43 expr out #txt
Cs0 f43 352 244 352 274 #arcP
Cs0 f46 expr in #txt
Cs0 f46 366 288 1104 419 #arcP
Cs0 f46 1 1104 288 #addKink
Cs0 f46 0 0.69171506203594 0 0 #arcLabel
Cs0 f47 expr in #txt
Cs0 f47 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>auto</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f47 6 #arcStyle
Cs0 f47 366 528 600 580 #arcP
Cs0 f47 1 600 528 #addKink
Cs0 f47 0 0.7755102040816326 0 -10 #arcLabel
Cs0 f6 .resExport export #txt
Cs0 f6 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f6 actionTable 'out=in;
' #txt
Cs0 f6 actionCode 'ivy.log.error("CleanUpScannings failed: "+exception);' #txt
Cs0 f6 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f6 1211 27 26 26 14 0 #rect
Cs0 f6 @|ExceptionIcon #fIcon
Cs0 f33 expr out #txt
Cs0 f33 1224 53 1117 432 #arcP
Cs0 f33 1 1224 432 #addKink
Cs0 f33 0 0.6209427795524923 0 0 #arcLabel
Cs0 f37 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f37 actionTable 'out=in;
' #txt
Cs0 f37 actionCode 'if(in.maxFilesToCleanUp>0&&in.ordersToCleanUpRS.size()>in.maxFilesToCleanUp){
	out.ordersToCleanUpSize = in.maxFilesToCleanUp;
	}
else {
	out.ordersToCleanUpSize = in.ordersToCleanUpRS.size();
	}
out.foundOrdersToCleanUpSize = in.ordersToCleanUpRS.size();
ivy.log.info("--- CleanUp Job: Batch Size: "+out.ordersToCleanUpSize+" of "+in.ordersToCleanUpRS.size());
' #txt
Cs0 f37 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set ordersToCleanUpSize</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f37 334 460 36 24 20 -2 #rect
Cs0 f37 @|StepIcon #fIcon
Cs0 f40 expr data #txt
Cs0 f40 outCond ivp=="LinkProcessCleanup.ivp" #txt
Cs0 f40 2 #arcStyle
Cs0 f40 136 612 784 638 #arcP
Cs0 f40 1 136 672 #addKink
Cs0 f40 2 744 672 #addKink
Cs0 f40 1 0.5038817040796386 0 0 #arcLabel
Cs0 f39 expr out #txt
Cs0 f39 600 604 778 632 #arcP
Cs0 f39 1 600 632 #addKink
Cs0 f39 1 0.11262547454443267 0 0 #arcLabel
Cs0 f16 expr out #txt
Cs0 f16 352 436 352 460 #arcP
Cs0 f32 expr out #txt
Cs0 f32 352 484 352 514 #arcP
Cs0 f38 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f38 actionTable 'out=in;
' #txt
Cs0 f38 actionCode 'import ch.soreco.webbies.common.cms.CMSUtils;
/*
CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","backupFolder",in.backupFolder);
*/
CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","cleanUpTime",in.cleanUpTime.format("dd.MM.yyyy HH:mm"));
CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","period",""+in.cleanUpPeriod+"/"+in.periodType);
CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","MaxFilesToCleanUp",""+in.maxFilesToCleanUp);' #txt
Cs0 f38 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Configuration Properties</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f38 46 372 36 24 20 -2 #rect
Cs0 f38 @|StepIcon #fIcon
Cs0 f41 expr data #txt
Cs0 f41 outCond ivp=="ChangeConfiguration.ivp" #txt
Cs0 f41 118 588 64 396 #arcP
Cs0 f41 1 64 552 #addKink
Cs0 f41 1 0.2534735396278921 0 0 #arcLabel
Cs0 f48 expr out #txt
Cs0 f48 64 372 334 181 #arcP
Cs0 f48 1 64 248 #addKink
Cs0 f48 1 0.028506608216378886 0 0 #arcLabel
Cs0 f49 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f49 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is maxFilesToCleanUp 
greater than Files to be CleanedUp?
</name>
        <nameStyle>58
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f49 866 418 28 28 -18 21 #rect
Cs0 f49 @|AlternativeIcon #fIcon
Cs0 f51 actionDecl 'ch.soreco.common.functional.ScanningCleanUp out;
' #txt
Cs0 f51 actionTable 'out=in;
' #txt
Cs0 f51 actionCode 'import ch.soreco.webbies.common.cms.CMSUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;

Calendar nextInvokation  = Calendar.getInstance();
//set next batch execution time 2 Minutes in the future
//as the "timer" polls in 60 seconds interval
nextInvokation.add(Calendar.MINUTE, 2);
CMSUtils.setContent(ch.ivyteam.ivy.cm.CoType.STRING,"/Content/Shared/Configuration/system/ScanningCleanUp","cleanUpTime",nextInvokation.getTime().format("dd.MM.yyyy HH:mm"));
' #txt
Cs0 f51 type ch.soreco.common.functional.ScanningCleanUp #txt
Cs0 f51 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set next CleanupTime to now+2minutes</name>
        <nameStyle>36
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f51 942 372 36 24 -19 -33 #rect
Cs0 f51 @|StepIcon #fIcon
Cs0 f52 expr in #txt
Cs0 f52 3 #arcStyle
Cs0 f52 889 427 942 395 #arcP
Cs0 f54 expr out #txt
Cs0 f54 978 390 1091 427 #arcP
Cs0 f53 expr in #txt
Cs0 f53 outCond 'in.maxFilesToCleanUp>in.ordersToCleanUpRS.size()
||in.maxFilesToCleanUp==0' #txt
Cs0 f53 6 #arcStyle
Cs0 f53 894 432 1091 432 #arcP
Cs0 f26 expr in #txt
Cs0 f26 792 618 792 446 #arcP
Cs0 f26 0 0.2217369044932667 0 0 #arcLabel
Cs0 f50 expr in #txt
Cs0 f50 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>auto</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f50 6 #arcStyle
Cs0 f50 806 432 866 432 #arcP
Cs0 f50 0 0.5151515151515151 0 -10 #arcLabel
Cs0 f34 expr out #txt
Cs0 f34 40 132 334 176 #arcP
Cs0 f34 1 40 176 #addKink
Cs0 f34 1 0.35147562835000323 0 0 #arcLabel
>Proto Cs0 .type ch.soreco.common.functional.ScanningCleanUp #txt
>Proto Cs0 .processKind NORMAL #txt
>Proto Cs0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>start</swimlaneLabel>
        <swimlaneLabel>get data</swimlaneLabel>
        <swimlaneLabel>process documents / orders</swimlaneLabel>
        <swimlaneLabel>finished</swimlaneLabel>
        <swimlaneLabel>exception</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>284</swimlaneSize>
    <swimlaneSize>211</swimlaneSize>
    <swimlaneSize>510</swimlaneSize>
    <swimlaneSize>150</swimlaneSize>
    <swimlaneSize>150</swimlaneSize>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-1</swimlaneColor>
    <swimlaneColor>-65485</swimlaneColor>
</elementInfo>
' #txt
>Proto Cs0 0 0 32 24 18 0 #rect
>Proto Cs0 @|BIcon #fIcon
Cs0 f7 mainOut f5 tail #connect
Cs0 f5 head f9 mainIn #connect
Cs0 f15 out f8 tail #connect
Cs0 f8 head f10 mainIn #connect
Cs0 f10 mainOut f22 tail #connect
Cs0 f22 head f21 mainIn #connect
Cs0 f25 out f19 tail #connect
Cs0 f19 head f7 mainIn #connect
Cs0 f0 mainOut f30 tail #connect
Cs0 f30 head f29 mainIn #connect
Cs0 f29 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f31 out f13 tail #connect
Cs0 f13 head f3 mainIn #connect
Cs0 f35 mainOut f36 tail #connect
Cs0 f36 head f28 mainIn #connect
Cs0 f21 mainOut f11 tail #connect
Cs0 f11 head f14 mainIn #connect
Cs0 f14 mainOut f17 tail #connect
Cs0 f17 head f18 mainIn #connect
Cs0 f18 mainOut f20 tail #connect
Cs0 f20 head f15 in #connect
Cs0 f42 out f12 tail #connect
Cs0 f12 head f7 mainIn #connect
Cs0 f1 mainOut f45 tail #connect
Cs0 f45 head f44 mainIn #connect
Cs0 f44 mainOut f43 tail #connect
Cs0 f43 head f42 in #connect
Cs0 f42 out f46 tail #connect
Cs0 f46 head f4 mainIn #connect
Cs0 f31 out f47 tail #connect
Cs0 f47 head f23 mainIn #connect
Cs0 f6 mainOut f33 tail #connect
Cs0 f33 head f4 mainIn #connect
Cs0 f3 out f40 tail #connect
Cs0 f40 head f15 in #connect
Cs0 f23 mainOut f39 tail #connect
Cs0 f39 head f15 in #connect
Cs0 f9 mainOut f16 tail #connect
Cs0 f16 head f37 mainIn #connect
Cs0 f37 mainOut f32 tail #connect
Cs0 f32 head f31 in #connect
Cs0 f3 out f41 tail #connect
Cs0 f41 head f38 mainIn #connect
Cs0 f38 mainOut f48 tail #connect
Cs0 f48 head f1 mainIn #connect
Cs0 f52 head f51 mainIn #connect
Cs0 f51 mainOut f54 tail #connect
Cs0 f54 head f4 mainIn #connect
Cs0 f49 out f53 tail #connect
Cs0 f53 head f4 mainIn #connect
Cs0 f49 out f52 tail #connect
Cs0 f15 out f26 tail #connect
Cs0 f26 head f25 in #connect
Cs0 f25 out f50 tail #connect
Cs0 f50 head f49 in #connect
Cs0 f28 mainOut f34 tail #connect
Cs0 f34 head f1 mainIn #connect
