[Ivy]
[>Created: Thu Oct 16 10:39:41 CEST 2014]
133A2919FEF4A911 3.17 #module
>Proto >Proto Collection #zClass
Ur0 UnlockOrder Big #zClass
Ur0 B #cInfo
Ur0 #process
Ur0 @TextInP .resExport .resExport #zField
Ur0 @TextInP .type .type #zField
Ur0 @TextInP .processKind .processKind #zField
Ur0 @AnnotationInP-0n ai ai #zField
Ur0 @TextInP .xml .xml #zField
Ur0 @TextInP .responsibility .responsibility #zField
Ur0 @GridStep f2 '' #zField
Ur0 @Page f3 '' #zField
Ur0 @PushWFArc f5 '' #zField
Ur0 @StartRequest f0 '' #zField
Ur0 @PushWFArc f4 '' #zField
Ur0 @PushWFArc f8 '' #zField
>Proto Ur0 Ur0 UnlockOrder #zField
Ur0 f2 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
Ur0 f2 actionTable 'out=in;
' #txt
Ur0 f2 actionCode 'import ch.ivyteam.ivy.ext.SessionLockManager;
SessionLockManager lockManager = SessionLockManager.getInstance();
if(lockManager.isIdLocked(in.order.orderId.toString())){
	lockManager.releaseLockId(in.order.orderId.toString());
	out.error = "Auftrag "+in.order.orderId+" wurde entsperrt";
} else {
	out.error = "Auftrag "+in.order.orderId+" ist nicht gesperrt";
}
' #txt
Ur0 f2 type ch.soreco.orderbook.functional.Order #txt
Ur0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Release Lock of Session
and set message</name>
        <nameStyle>24,7
15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ur0 f2 86 180 36 24 20 -2 #rect
Ur0 f2 @|StepIcon #fIcon
Ur0 f3 outTypes "ch.soreco.orderbook.functional.Order" #txt
Ur0 f3 outLinks "unlock.ivp" #txt
Ur0 f3 template "/ProcessPages/UnlockOrder/unlockOrder.ivc" #txt
Ur0 f3 type ch.soreco.orderbook.functional.Order #txt
Ur0 f3 skipLink skip.ivp #txt
Ur0 f3 sortLink sort.ivp #txt
Ur0 f3 templateWizard '#
#Mon Nov 14 16:34:13 CET 2011
' #txt
Ur0 f3 pageArchivingActivated false #txt
Ur0 f3 @C|.responsibility Everybody #txt
Ur0 f3 86 132 36 24 20 -2 #rect
Ur0 f3 @|PageIcon #fIcon
Ur0 f5 expr data #txt
Ur0 f5 outCond ivp=="unlock.ivp" #txt
Ur0 f5 104 156 104 180 #arcP
Ur0 f0 outLink start.ivp #txt
Ur0 f0 type ch.soreco.orderbook.functional.Order #txt
Ur0 f0 inParamDecl '<> param;' #txt
Ur0 f0 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
Ur0 f0 guid 133A299B2BDBE5B8 #txt
Ur0 f0 requestEnabled true #txt
Ur0 f0 triggerEnabled false #txt
Ur0 f0 callSignature start() #txt
Ur0 f0 persist false #txt
Ur0 f0 taskData '#
#Fri Feb 24 13:07:21 CET 2012
TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody
' #txt
Ur0 f0 caseData '#
#Fri Feb 24 13:07:21 CET 2012
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Ur0 f0 showInStartList 0 #txt
Ur0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Ur0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ur0 f0 @C|.responsibility Everybody #txt
Ur0 f0 91 43 26 26 14 0 #rect
Ur0 f0 @|StartRequestIcon #fIcon
Ur0 f4 expr out #txt
Ur0 f4 122 192 122 144 #arcP
Ur0 f4 1 164 192 #addKink
Ur0 f4 2 164 144 #addKink
Ur0 f4 1 0.5 0 0 #arcLabel
Ur0 f8 expr out #txt
Ur0 f8 104 69 104 132 #arcP
>Proto Ur0 .type ch.soreco.orderbook.functional.Order #txt
>Proto Ur0 .processKind CALLABLE_SUB #txt
>Proto Ur0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Ur0 0 0 32 24 18 0 #rect
>Proto Ur0 @|BIcon #fIcon
Ur0 f3 out f5 tail #connect
Ur0 f5 head f2 mainIn #connect
Ur0 f2 mainOut f4 tail #connect
Ur0 f4 head f3 mainIn #connect
Ur0 f0 mainOut f8 tail #connect
Ur0 f8 head f3 mainIn #connect
