[Ivy]
[>Created: Wed Jul 27 15:25:21 CEST 2011]
12DE1F4105113C25 3.15 #module
>Proto >Proto Collection #zClass
Cn0 ConfigurationAdmin Big #zClass
Cn0 B #cInfo
Cn0 #process
Cn0 @TextInP .resExport .resExport #zField
Cn0 @TextInP .type .type #zField
Cn0 @TextInP .processKind .processKind #zField
Cn0 @AnnotationInP-0n ai ai #zField
Cn0 @TextInP .xml .xml #zField
Cn0 @TextInP .responsibility .responsibility #zField
Cn0 @StartSub f0 '' #zField
Cn0 @EndSub f1 '' #zField
Cn0 @CallSub f4 '' #zField
Cn0 @Page f5 '' #zField
Cn0 @GridStep f8 '' #zField
Cn0 @GridStep f6 '' #zField
Cn0 @PushWFArc f7 '' #zField
Cn0 @PushWFArc f9 '' #zField
Cn0 @PushWFArc f11 '' #zField
Cn0 @PushWFArc f12 '' #zField
Cn0 @PushWFArc f14 '' #zField
Cn0 @PushWFArc f15 '' #zField
>Proto Cn0 Cn0 ConfigurationAdmin #zField
Cn0 f0 outParamDecl '<> result;
' #txt
Cn0 f0 actionDecl 'ch.soreco.orderbook.ui.ConfigurationAdmin out;
' #txt
Cn0 f0 callSignature call() #txt
Cn0 f0 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
Cn0 f0 91 35 26 26 14 0 #rect
Cn0 f0 @|StartSubIcon #fIcon
Cn0 f1 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f1 91 275 26 26 14 0 #rect
Cn0 f1 @|EndSubIcon #fIcon
Cn0 f4 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f4 processCall 'Business Processes/Generics/Entity/Table:table(java.lang.Class)' #txt
Cn0 f4 doCall true #txt
Cn0 f4 requestActionDecl '<java.lang.Class clazz> param;
' #txt
Cn0 f4 requestMappingAction 'param.clazz=in.selectedClass;
' #txt
Cn0 f4 responseActionDecl 'ch.soreco.orderbook.ui.ConfigurationAdmin out;
' #txt
Cn0 f4 responseMappingAction 'out=in;
' #txt
Cn0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Config</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Cn0 f4 246 236 36 24 20 -2 #rect
Cn0 f4 @|CallSubIcon #fIcon
Cn0 f5 outTypes "ch.soreco.orderbook.ui.ConfigurationAdmin","ch.soreco.orderbook.ui.ConfigurationAdmin" #txt
Cn0 f5 outLinks "getConfig.ivp","exit.ivp" #txt
Cn0 f5 template "ChooseConfig.ivc" #txt
Cn0 f5 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f5 skipLink skip.ivp #txt
Cn0 f5 sortLink sort.ivp #txt
Cn0 f5 templateWizard '#
#Tue Feb 01 17:48:07 CET 2011
' #txt
Cn0 f5 pageArchivingActivated false #txt
Cn0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show configs</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Cn0 f5 @C|.responsibility Everybody #txt
Cn0 f5 86 172 36 24 20 -2 #rect
Cn0 f5 @|PageIcon #fIcon
Cn0 f8 actionDecl 'ch.soreco.orderbook.ui.ConfigurationAdmin out;
' #txt
Cn0 f8 actionTable 'out=in;
' #txt
Cn0 f8 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeConstants;
import ch.soreco.webbies.common.html.generics.AttributeContent;
import ch.soreco.webbies.common.html.generics.ClassProperties;
import ch.soreco.webbies.common.db.EntityClasses;
import ch.soreco.webbies.common.util.XmlProperties;

List entities = EntityClasses.getEntityClasses();
in.configs = new Recordset();
in.configs.addColumn("Konfiguration",[]);
for(Class clazz:entities){
	XmlProperties props = new XmlProperties(clazz);
	if(props.hasProperties()
		&&props.getProperties().containsKey(ClassProperties.CONFIGURABLE.toString())){
			Boolean isConfig = props.getProperties().get(ClassProperties.CONFIGURABLE.toString()).toString().toLowerCase().equals("true");
			if(isConfig){
				in.classes.add(clazz);	
				AttributeContent ac = new AttributeContent(AttributeConstants.CONTENTPREFIX, clazz);
				in.configs.add([ac.getLabel()]);
				}
		}
	}
/*
import ch.soreco.orderbook.bo.SignatureRequest;
import ch.soreco.orderbook.bo.Contact;
import ch.soreco.orderbook.bo.Paper;
import ch.soreco.orderbook.bo.Order;

//in.classes.add(ch.soreco.orderbook.bo.TimeRemainingConfig.class);
in.classes.add(ch.soreco.orderbook.bo.ScanShare.class);
in.classes.add(ch.soreco.orderbook.bo.OrderMatching.class);
in.classes.add(ch.soreco.orderbook.bo.Order.class);
in.classes.add(Paper.class);
in.classes.add(SignatureRequest.class);
in.classes.add(Contact.class);

in.configs.clear();
List<String> configNames = new List<String>();
for(Class class:in.classes){
	configNames.add(class.getSimpleName());
	}
in.configs.addColumn("Konfiguration",configNames);

*/
' #txt
Cn0 f8 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init Configs</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Cn0 f8 86 100 36 24 20 -2 #rect
Cn0 f8 @|StepIcon #fIcon
Cn0 f6 actionDecl 'ch.soreco.orderbook.ui.ConfigurationAdmin out;
' #txt
Cn0 f6 actionTable 'out=in;
out.selectedClass=in.classes.get(in.i);
' #txt
Cn0 f6 type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
Cn0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getSelected Class</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Cn0 f6 246 172 36 24 20 -2 #rect
Cn0 f6 @|StepIcon #fIcon
Cn0 f7 expr out #txt
Cn0 f7 264 196 264 236 #arcP
Cn0 f9 expr data #txt
Cn0 f9 outCond ivp=="getConfig.ivp" #txt
Cn0 f9 122 184 246 184 #arcP
Cn0 f11 expr out #txt
Cn0 f11 104 124 104 172 #arcP
Cn0 f12 expr out #txt
Cn0 f12 246 241 122 191 #arcP
Cn0 f14 expr out #txt
Cn0 f14 104 61 104 100 #arcP
Cn0 f15 expr data #txt
Cn0 f15 outCond ivp=="exit.ivp" #txt
Cn0 f15 104 196 104 275 #arcP
>Proto Cn0 .type ch.soreco.orderbook.ui.ConfigurationAdmin #txt
>Proto Cn0 .processKind CALLABLE_SUB #txt
>Proto Cn0 0 0 32 24 18 0 #rect
>Proto Cn0 @|BIcon #fIcon
Cn0 f6 mainOut f7 tail #connect
Cn0 f7 head f4 mainIn #connect
Cn0 f5 out f9 tail #connect
Cn0 f9 head f6 mainIn #connect
Cn0 f8 mainOut f11 tail #connect
Cn0 f11 head f5 mainIn #connect
Cn0 f4 mainOut f12 tail #connect
Cn0 f12 head f5 mainIn #connect
Cn0 f0 mainOut f14 tail #connect
Cn0 f14 head f8 mainIn #connect
Cn0 f5 out f15 tail #connect
Cn0 f15 head f1 mainIn #connect
