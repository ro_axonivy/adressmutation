[Ivy]
[>Created: Tue Oct 14 13:43:29 CEST 2014]
12B57422D1AD2440 3.17 #module
>Proto >Proto Collection #zClass
cs0 chooseProcess Big #zClass
cs0 B #cInfo
cs0 #process
cs0 @TextInP .resExport .resExport #zField
cs0 @TextInP .type .type #zField
cs0 @TextInP .processKind .processKind #zField
cs0 @AnnotationInP-0n ai ai #zField
cs0 @TextInP .xml .xml #zField
cs0 @TextInP .responsibility .responsibility #zField
cs0 @EndSub f1 '' #zField
cs0 @Alternative f2 '' #zField
cs0 @GridStep f4 '' #zField
cs0 @CallSub f5 '' #zField
cs0 @PushWFArc f8 '' #zField
cs0 @CallSub f10 '' #zField
cs0 @PushWFArc f12 '' #zField
cs0 @GridStep f13 '' #zField
cs0 @GridStep f15 '' #zField
cs0 @PushWFArc f16 '' #zField
cs0 @CallSub f11 '' #zField
cs0 @PushWFArc f17 '' #zField
cs0 @CallSub f19 '' #zField
cs0 @PushWFArc f9 '' #zField
cs0 @GridStep f26 '' #zField
cs0 @CallSub f31 '' #zField
cs0 @PushWFArc f32 '' #zField
cs0 @Alternative f34 '' #zField
cs0 @PushWFArc f35 '' #zField
cs0 @PushWFArc f36 '' #zField
cs0 @CallSub f37 '' #zField
cs0 @PushWFArc f20 '' #zField
cs0 @PushWFArc f39 '' #zField
cs0 @Alternative f40 '' #zField
cs0 @PushWFArc f41 '' #zField
cs0 @PushWFArc f22 '' #zField
cs0 @PushWFArc f38 '' #zField
cs0 @GridStep f43 '' #zField
cs0 @PushWFArc f45 '' #zField
cs0 @PushWFArc f46 '' #zField
cs0 @CallSub f33 '' #zField
cs0 @PushWFArc f42 '' #zField
cs0 @StartSub f0 '' #zField
cs0 @PushWFArc f44 '' #zField
cs0 @Page f48 '' #zField
cs0 @PushWFArc f49 '' #zField
cs0 @GridStep f14 '' #zField
cs0 @GridStep f50 '' #zField
cs0 @PushWFArc f51 '' #zField
cs0 @PushWFArc f7 '' #zField
cs0 @StartEvent f30 '' #zField
cs0 @EndTask f53 '' #zField
cs0 @GridStep f54 '' #zField
cs0 @PushWFArc f55 '' #zField
cs0 @PushWFArc f56 '' #zField
cs0 @PushWFArc f25 '' #zField
cs0 @GridStep f57 '' #zField
cs0 @PushWFArc f58 '' #zField
cs0 @PushWFArc f21 '' #zField
cs0 @PushWFArc f59 '' #zField
cs0 @PushWFArc f18 '' #zField
cs0 @PushWFArc f3 '' #zField
cs0 @PushWFArc f27 '' #zField
cs0 @GridStep f6 '' #zField
cs0 @PushWFArc f24 '' #zField
cs0 @PushWFArc f47 '' #zField
>Proto cs0 cs0 chooseProcess #zField
cs0 f1 type ch.soreco.orderbook.ui.Order #txt
cs0 f1 235 875 26 26 14 0 #rect
cs0 f1 @|EndSubIcon #fIcon
cs0 f2 type ch.soreco.orderbook.ui.Order #txt
cs0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>switch by eViewType</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f2 234 314 28 28 11 -26 #rect
cs0 f2 @|AlternativeIcon #fIcon
cs0 f4 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f4 actionTable 'out=in;
' #txt
cs0 f4 type ch.soreco.orderbook.ui.Order #txt
cs0 f4 830 724 36 24 20 -2 #rect
cs0 f4 @|StepIcon #fIcon
cs0 f5 type ch.soreco.orderbook.ui.Order #txt
cs0 f5 processCall 'Business Processes/Adressmutation/Read:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
cs0 f5 doCall true #txt
cs0 f5 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
cs0 f5 requestMappingAction 'param.order=in.order;
param.viewType=in.eViewType;
' #txt
cs0 f5 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f5 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
cs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Adressmutation 
Read</name>
        <nameStyle>25
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f5 502 604 36 24 20 -2 #rect
cs0 f5 @|CallSubIcon #fIcon
cs0 f8 expr out #txt
cs0 f8 520 628 830 736 #arcP
cs0 f8 1 520 736 #addKink
cs0 f8 1 0.053735113619053025 0 0 #arcLabel
cs0 f10 type ch.soreco.orderbook.ui.Order #txt
cs0 f10 processCall 'Business Processes/Adressmutation/Edit:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
cs0 f10 doCall true #txt
cs0 f10 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
cs0 f10 requestMappingAction 'param.order=in.order;
param.viewType=in.eViewType;
' #txt
cs0 f10 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f10 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
cs0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation 
Edit</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f10 830 604 36 24 20 -2 #rect
cs0 f10 @|CallSubIcon #fIcon
cs0 f12 expr out #txt
cs0 f12 848 628 848 724 #arcP
cs0 f12 0 0.9196245018135206 0 0 #arcLabel
cs0 f13 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f13 actionTable 'out=in;
' #txt
cs0 f13 actionCode 'import ch.soreco.orderbook.ui.enums.ViewType;
import ch.soreco.webbies.common.html.form.RuleHandler;

out.eOrderType = null;
if(in.order.orderType.trim().length()>0){
	//set Enum DropDown
	try{
		out.eOrderType =ch.soreco.orderbook.enums.OrderType.valueOf(in.order.orderType);
		}
	catch (IllegalArgumentException e){
		ivy.log.info(e);
		out.eOrderType = ch.soreco.orderbook.enums.OrderType.Default;
		}
	catch (Exception e){
		ivy.log.error(e);
		out.eOrderType = ch.soreco.orderbook.enums.OrderType.Default;		
		}	
	}
	

out.eOrderState = null;
if(in.order.orderState.trim().length()>0){
	//set Enum DropDown
	try{
		out.eOrderState =ch.soreco.orderbook.enums.OrderState.valueOf(in.order.orderState);
		}
	catch (IllegalArgumentException e){
		ivy.log.info(e);
		out.eOrderState = ch.soreco.orderbook.enums.OrderState.Default;
		
		}
	catch (Exception e){
		ivy.log.error(e);
		}	
	}
Boolean canAccess = false;

if(in.eViewType.equals(ViewType.EDIT)) {
// everyone can edit, even if the user of the order is different
// mail from J. Chung 04.08.2011		
//	canAccess = RuleHandler.validateEdit(in.order.orderState,in.order.editorUserId); 
canAccess = true;
	}
else if(in.eViewType.equals(ViewType.CHECK)) {
	canAccess = RuleHandler.validateCheck(in.order.orderState,in.order.editorUserId); 
	}	
else if(in.eViewType.equals(ViewType.DISPATCH)) {
	canAccess = in.order.orderState.contains("Dispatching") ? true : false; 
	}	
if(!canAccess) {
	in.eViewType = ViewType.READ;
	}
' #txt
cs0 f13 type ch.soreco.orderbook.ui.Order #txt
cs0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Try to get Order type and state</name>
        <nameStyle>31
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f13 230 156 36 24 20 -2 #rect
cs0 f13 @|StepIcon #fIcon
cs0 f15 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f15 actionTable 'out=in;
' #txt
cs0 f15 type ch.soreco.orderbook.ui.Order #txt
cs0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>default 
Addressmutation</name>
        <nameStyle>24,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f15 334 604 36 24 20 -2 #rect
cs0 f15 @|StepIcon #fIcon
cs0 f16 expr out #txt
cs0 f16 370 616 502 616 #arcP
cs0 f11 type ch.soreco.orderbook.ui.Order #txt
cs0 f11 processCall 'Business Processes/Adressmutation/Check:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
cs0 f11 doCall true #txt
cs0 f11 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
cs0 f11 requestMappingAction 'param.order=in.order;
param.viewType=in.eViewType;
' #txt
cs0 f11 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f11 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
cs0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation
Check</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f11 1238 604 36 24 20 -2 #rect
cs0 f11 @|CallSubIcon #fIcon
cs0 f17 expr out #txt
cs0 f17 1256 628 866 736 #arcP
cs0 f17 1 1256 736 #addKink
cs0 f17 1 0.24456375508161565 0 0 #arcLabel
cs0 f19 type ch.soreco.orderbook.ui.Order #txt
cs0 f19 processCall 'Business Processes/Dispatching/DispatchOrder:call(String,Recordset,Integer,ch.soreco.orderbook.ui.enums.ViewType)' #txt
cs0 f19 doCall true #txt
cs0 f19 requestActionDecl '<java.lang.String orderBookType,ch.ivyteam.ivy.scripting.objects.Recordset orderBook,java.lang.Integer orderId,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
cs0 f19 requestMappingAction 'param.orderId=in.order.orderId;
param.viewType=in.eViewType;
' #txt
cs0 f19 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f19 responseMappingAction 'out=in;
' #txt
cs0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Manual Dispatching</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f19 1486 604 36 24 20 -2 #rect
cs0 f19 @|CallSubIcon #fIcon
cs0 f9 expr out #txt
cs0 f9 1504 628 866 736 #arcP
cs0 f9 1 1504 736 #addKink
cs0 f9 1 0.37162820457339507 0 0 #arcLabel
cs0 f26 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f26 actionTable 'out=in;
out.error="The Order Type is not properly specified - the order needs to be dispatched.";
out.success=false;
' #txt
cs0 f26 actionCode 'import ch.soreco.webbies.common.html.logger.HtmlLog;
in.error = "The Order Type is not properly specified - the order needs to be dispatched.";
HtmlLog.info(in.error);' #txt
cs0 f26 type ch.soreco.orderbook.ui.Order #txt
cs0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set error</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f26 78 476 36 24 20 -2 #rect
cs0 f26 @|StepIcon #fIcon
cs0 f31 type ch.soreco.orderbook.ui.Order #txt
cs0 f31 processCall 'Business Processes/Adressmutation/BPMatch:call(ch.soreco.orderbook.ui.Order)' #txt
cs0 f31 doCall true #txt
cs0 f31 requestActionDecl '<ch.soreco.orderbook.ui.Order orderUI> param;
' #txt
cs0 f31 requestMappingAction 'param.orderUI.eViewType=in.eViewType;
param.orderUI.order=in.order;
' #txt
cs0 f31 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f31 responseMappingAction 'out=in;
' #txt
cs0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation 
BPMatch</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f31 694 604 36 24 20 -2 #rect
cs0 f31 @|CallSubIcon #fIcon
cs0 f32 expr out #txt
cs0 f32 712 628 830 736 #arcP
cs0 f32 1 712 664 #addKink
cs0 f32 1 0.5042623082681326 0 0 #arcLabel
cs0 f34 type ch.soreco.orderbook.ui.Order #txt
cs0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Choose by State</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f34 834 458 28 28 13 -26 #rect
cs0 f34 @|AlternativeIcon #fIcon
cs0 f35 expr in #txt
cs0 f35 outCond in.eViewType.equals(ch.soreco.orderbook.ui.enums.ViewType.EDIT) #txt
cs0 f35 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>eViewType is EDIT</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f35 262 328 848 458 #arcP
cs0 f35 1 848 328 #addKink
cs0 f35 1 0.4888888888888889 55 0 #arcLabel
cs0 f36 expr in #txt
cs0 f36 outCond in.eOrderState.equals(ch.soreco.orderbook.enums.OrderState.Dispatched) #txt
cs0 f36 837 475 712 604 #arcP
cs0 f36 1 712 512 #addKink
cs0 f36 0 0.7161167747595336 0 0 #arcLabel
cs0 f37 type ch.soreco.orderbook.ui.Order #txt
cs0 f37 processCall 'Business Processes/Adressmutation/Pend:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
cs0 f37 doCall true #txt
cs0 f37 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
cs0 f37 requestMappingAction 'param.order=in.order;
param.viewType=in.eViewType;
' #txt
cs0 f37 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f37 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
cs0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation 
Pend</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f37 966 604 36 24 20 -2 #rect
cs0 f37 @|CallSubIcon #fIcon
cs0 f20 expr out #txt
cs0 f20 984 628 866 736 #arcP
cs0 f20 1 984 664 #addKink
cs0 f20 1 0.3334102701829342 0 0 #arcLabel
cs0 f39 expr in #txt
cs0 f39 848 486 848 604 #arcP
cs0 f39 0 0.5106600694698686 0 0 #arcLabel
cs0 f40 type ch.soreco.orderbook.ui.Order #txt
cs0 f40 1242 458 28 28 14 0 #rect
cs0 f40 @|AlternativeIcon #fIcon
cs0 f41 expr in #txt
cs0 f41 outCond in.eViewType.equals(ch.soreco.orderbook.ui.enums.ViewType.CHECK) #txt
cs0 f41 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>eViewType is CHECK</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f41 262 328 1256 458 #arcP
cs0 f41 1 1256 328 #addKink
cs0 f41 1 0.4888888888888889 63 0 #arcLabel
cs0 f22 expr in #txt
cs0 f22 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>default</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f22 256 334 352 604 #arcP
cs0 f22 1 352 400 #addKink
cs0 f22 1 0.13095238095238096 21 0 #arcLabel
cs0 f38 expr in #txt
cs0 f38 outCond 'in.eOrderState.equals(ch.soreco.orderbook.enums.OrderState.InPendencyRecording)
||in.eOrderState.equals(ch.soreco.orderbook.enums.OrderState.Pendant)' #txt
cs0 f38 859 475 984 604 #arcP
cs0 f38 1 984 512 #addKink
cs0 f38 0 0.6644344908448463 0 0 #arcLabel
cs0 f43 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f43 actionTable 'out=in;
' #txt
cs0 f43 type ch.soreco.orderbook.ui.Order #txt
cs0 f43 1134 532 36 24 20 -2 #rect
cs0 f43 @|StepIcon #fIcon
cs0 f45 expr out #txt
cs0 f45 1152 556 1238 616 #arcP
cs0 f45 1 1152 576 #addKink
cs0 f45 1 0.2756574713673568 0 0 #arcLabel
cs0 f46 expr in #txt
cs0 f46 outCond in.eOrderState.equals(ch.soreco.orderbook.enums.OrderState.InIdCheck) #txt
cs0 f46 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>InIdCheck</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f46 1244 474 1152 532 #arcP
cs0 f46 1 1152 488 #addKink
cs0 f46 0 1.0 -31 7 #arcLabel
cs0 f33 type ch.soreco.orderbook.ui.Order #txt
cs0 f33 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
cs0 f33 doCall true #txt
cs0 f33 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
cs0 f33 requestMappingAction 'param.filter.orderId=in.order.orderId;
' #txt
cs0 f33 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f33 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
cs0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getPlainOrder</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f33 230 108 36 24 20 -2 #rect
cs0 f33 @|CallSubIcon #fIcon
cs0 f42 expr out #txt
cs0 f42 248 132 248 156 #arcP
cs0 f0 inParamDecl '<java.lang.Integer orderId,ch.soreco.orderbook.ui.enums.ViewType viewType> param;' #txt
cs0 f0 inParamTable 'out.eViewType=param.viewType;
out.order.orderId=param.orderId;
' #txt
cs0 f0 outParamDecl '<java.lang.String error,java.lang.Boolean success> result;
' #txt
cs0 f0 outParamTable 'result.error=in.error;
result.success=in.success;
' #txt
cs0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f0 callSignature call(Integer,ch.soreco.orderbook.ui.enums.ViewType) #txt
cs0 f0 type ch.soreco.orderbook.ui.Order #txt
cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Integer,ViewType)</name>
        <nameStyle>22,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f0 235 59 26 26 14 0 #rect
cs0 f0 @|StartSubIcon #fIcon
cs0 f44 expr out #txt
cs0 f44 248 85 248 108 #arcP
cs0 f48 outTypes "ch.soreco.orderbook.ui.Order" #txt
cs0 f48 outLinks "submit.ivp" #txt
cs0 f48 template "/ProcessPages/chooseProcess/ErrorPage.ivc" #txt
cs0 f48 type ch.soreco.orderbook.ui.Order #txt
cs0 f48 skipLink skip.ivp #txt
cs0 f48 sortLink sort.ivp #txt
cs0 f48 templateWizard '#
#Thu Apr 14 20:22:42 CEST 2011
' #txt
cs0 f48 pageArchivingActivated false #txt
cs0 f48 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Error</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f48 @C|.responsibility Everybody #txt
cs0 f48 78 532 36 24 20 -2 #rect
cs0 f48 @|PageIcon #fIcon
cs0 f49 expr out #txt
cs0 f49 96 500 96 532 #arcP
cs0 f14 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f14 actionTable 'out=in;
' #txt
cs0 f14 actionCode 'import ch.ivyteam.ivy.ext.SessionLockManager;
import ch.soreco.orderbook.ui.enums.ViewType;
if(!in.eViewType.equals(ViewType.READ)){	
	SessionLockManager lockManager = SessionLockManager.getInstance();
	Boolean lockable = false;
	lockable = lockManager.lockIdForSession(in.order.orderId.toString(), ivy.session);
	if(!lockable) {
		ivy.log.info("Could''nt get lock for "+in.order.orderId+" by session id "+ivy.session.getIdentifier());		
		in.eViewType = ViewType.READ;
		}
	else {
		ivy.log.info("Locked "+in.order.orderId+" by session id "+ivy.session.getIdentifier()+" in application "+ivy.request.getApplication().getName());
		}
	}' #txt
cs0 f14 type ch.soreco.orderbook.ui.Order #txt
cs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Check Lockability</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f14 230 204 36 24 20 -2 #rect
cs0 f14 @|StepIcon #fIcon
cs0 f50 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f50 actionTable 'out=in;
' #txt
cs0 f50 actionCode 'import ch.ivyteam.ivy.ext.SessionLockManager;

SessionLockManager lockManager = SessionLockManager.getInstance();
if(lockManager.lockIdForSession(in.order.orderId.toString(),ivy.session)){
	lockManager.releaseLockId(in.order.orderId.toString());	
	ivy.log.info("Released lock for "+in.order.orderId+" by session id "+ivy.session.getIdentifier());
	}
	

' #txt
cs0 f50 type ch.soreco.orderbook.ui.Order #txt
cs0 f50 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>release lock</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f50 230 780 36 24 20 -2 #rect
cs0 f50 @|StepIcon #fIcon
cs0 f51 expr data #txt
cs0 f51 outCond ivp=="submit.ivp" #txt
cs0 f51 96 556 248 780 #arcP
cs0 f51 0 0.5153492014768365 0 0 #arcLabel
cs0 f7 expr out #txt
cs0 f7 248 804 248 875 #arcP
cs0 f30 outerBean "ch.ivyteam.ivy.process.eventstart.beans.AutoProcessStarterEventBean" #txt
cs0 f30 beanConfig "60" #txt
cs0 f30 outLink eventLink.ivp #txt
cs0 f30 type ch.soreco.orderbook.ui.Order #txt
cs0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Release Expired Session Locks
(every 1min)</name>
        <nameStyle>42
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f30 @C|.responsibility Everybody #txt
cs0 f30 539 67 26 26 14 0 #rect
cs0 f30 @|StartEventIcon #fIcon
cs0 f53 type ch.soreco.orderbook.ui.Order #txt
cs0 f53 539 187 26 26 14 0 #rect
cs0 f53 @|EndIcon #fIcon
cs0 f54 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f54 actionTable 'out=in;
' #txt
cs0 f54 actionCode 'import ch.ivyteam.ivy.ext.SessionLockManager;
SessionLockManager lockManager = SessionLockManager.getInstance();
//ivy.log.info("Releasing Session Locks for "+ivy.request.getApplication().getName());
lockManager.releaseExpiredSessionLocks(ivy.request.getApplication());' #txt
cs0 f54 type ch.soreco.orderbook.ui.Order #txt
cs0 f54 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>release locks</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f54 534 124 36 24 20 -2 #rect
cs0 f54 @|StepIcon #fIcon
cs0 f55 expr out #txt
cs0 f55 552 93 552 124 #arcP
cs0 f56 expr out #txt
cs0 f56 552 148 552 187 #arcP
cs0 f25 expr out #txt
cs0 f25 248 180 248 204 #arcP
cs0 f57 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f57 actionTable 'out=in;
' #txt
cs0 f57 actionCode 'import ch.soreco.orderbook.ui.enums.ViewType;
import ch.soreco.orderbook.enums.OrderState;

if(in.order.orderState.equalsIgnoreCase(OrderState.InDispatchingNew.name()) || in.order.orderState.equalsIgnoreCase(OrderState.InDispatchingOpen.name())){
	out.eViewType = ViewType.EDIT;
}

if(in.order.orderState.equalsIgnoreCase(OrderState.InDispatchingAbortCheck.name())){
	out.eViewType = ViewType.CHECK;
}' #txt
cs0 f57 type ch.soreco.orderbook.ui.Order #txt
cs0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set ViewType
by OrderState</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f57 1486 532 36 24 20 -2 #rect
cs0 f57 @|StepIcon #fIcon
cs0 f58 expr in #txt
cs0 f58 outCond in.eViewType.equals(ch.soreco.orderbook.ui.enums.ViewType.DISPATCH) #txt
cs0 f58 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>eViewType is DISPATCH</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f58 262 328 1504 532 #arcP
cs0 f58 1 1504 328 #addKink
cs0 f58 1 0.29411764705882354 76 0 #arcLabel
cs0 f21 expr out #txt
cs0 f21 1504 556 1504 604 #arcP
cs0 f59 expr in #txt
cs0 f59 outCond in.eOrderState.equals(ch.soreco.orderbook.enums.OrderState.InDispatchingAbortCheck) #txt
cs0 f59 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>dispatchingAbortCheck</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f59 1269 473 1489 532 #arcP
cs0 f59 1 1432 488 #addKink
cs0 f59 0 0.6531511053020401 -1 15 #arcLabel
cs0 f18 expr in #txt
cs0 f18 1256 486 1256 604 #arcP
cs0 f3 expr out #txt
cs0 f3 848 748 266 792 #arcP
cs0 f3 1 848 792 #addKink
cs0 f3 1 0.3594263332756882 0 0 #arcLabel
cs0 f27 expr in #txt
cs0 f27 outCond in.eViewType.equals(ch.soreco.orderbook.ui.enums.ViewType.READ) #txt
cs0 f27 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>eViewType is READ</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f27 262 328 520 604 #arcP
cs0 f27 1 520 328 #addKink
cs0 f27 1 0.7236842105263158 61 0 #arcLabel
cs0 f6 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
cs0 f6 actionTable 'out=in;
' #txt
cs0 f6 actionCode 'import ch.soreco.orderbook.order.audit.OrderAuditEventHandler;
import ch.soreco.orderbook.bo.OrderAuditEvent;

String viewType = "";
if(in.eViewType!=null){
	viewType = in.eViewType.name().toLowerCase();	
	viewType = viewType.substring(0, 1).toUpperCase() + viewType.substring(1);
} else {
	viewType = "Read";
}

OrderAuditEvent event = new OrderAuditEvent();
event.setOrderId(in.order.orderId);
event.setBpId(in.order.BPId);
event.setUserId(ivy.session.getSessionUserName());
event.setEventType("Open"+viewType+"OrderPage");
event.setEventDate(new java.util.Date());
OrderAuditEventHandler handler = new OrderAuditEventHandler();
handler.handle(event);
' #txt
cs0 f6 type ch.soreco.orderbook.ui.Order #txt
cs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Audit Log of Order View</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f6 230 252 36 24 20 -2 #rect
cs0 f6 @|StepIcon #fIcon
cs0 f24 expr out #txt
cs0 f24 248 276 248 314 #arcP
cs0 f24 0 0.9807281072260632 0 0 #arcLabel
cs0 f47 expr out #txt
cs0 f47 248 228 248 252 #arcP
cs0 f47 0 0.9807281072260633 0 0 #arcLabel
>Proto cs0 .type ch.soreco.orderbook.ui.Order #txt
>Proto cs0 .processKind CALLABLE_SUB #txt
>Proto cs0 0 0 32 24 18 0 #rect
>Proto cs0 @|BIcon #fIcon
cs0 f5 mainOut f8 tail #connect
cs0 f8 head f4 mainIn #connect
cs0 f10 mainOut f12 tail #connect
cs0 f12 head f4 mainIn #connect
cs0 f15 mainOut f16 tail #connect
cs0 f16 head f5 mainIn #connect
cs0 f11 mainOut f17 tail #connect
cs0 f17 head f4 mainIn #connect
cs0 f19 mainOut f9 tail #connect
cs0 f9 head f4 mainIn #connect
cs0 f31 mainOut f32 tail #connect
cs0 f32 head f4 mainIn #connect
cs0 f35 head f34 in #connect
cs0 f34 out f36 tail #connect
cs0 f36 head f31 mainIn #connect
cs0 f37 mainOut f20 tail #connect
cs0 f20 head f4 mainIn #connect
cs0 f39 head f10 mainIn #connect
cs0 f41 head f40 in #connect
cs0 f22 head f15 mainIn #connect
cs0 f34 out f38 tail #connect
cs0 f38 head f37 mainIn #connect
cs0 f34 out f39 tail #connect
cs0 f43 mainOut f45 tail #connect
cs0 f45 head f11 mainIn #connect
cs0 f40 out f46 tail #connect
cs0 f46 head f43 mainIn #connect
cs0 f33 mainOut f42 tail #connect
cs0 f42 head f13 mainIn #connect
cs0 f0 mainOut f44 tail #connect
cs0 f44 head f33 mainIn #connect
cs0 f26 mainOut f49 tail #connect
cs0 f49 head f48 mainIn #connect
cs0 f48 out f51 tail #connect
cs0 f51 head f50 mainIn #connect
cs0 f50 mainOut f7 tail #connect
cs0 f7 head f1 mainIn #connect
cs0 f30 mainOut f55 tail #connect
cs0 f55 head f54 mainIn #connect
cs0 f54 mainOut f56 tail #connect
cs0 f56 head f53 mainIn #connect
cs0 f13 mainOut f25 tail #connect
cs0 f25 head f14 mainIn #connect
cs0 f58 head f57 mainIn #connect
cs0 f57 mainOut f21 tail #connect
cs0 f21 head f19 mainIn #connect
cs0 f40 out f59 tail #connect
cs0 f59 head f57 mainIn #connect
cs0 f40 out f18 tail #connect
cs0 f18 head f11 mainIn #connect
cs0 f4 mainOut f3 tail #connect
cs0 f3 head f50 mainIn #connect
cs0 f2 out f27 tail #connect
cs0 f27 head f5 mainIn #connect
cs0 f2 out f35 tail #connect
cs0 f2 out f41 tail #connect
cs0 f2 out f58 tail #connect
cs0 f2 out f22 tail #connect
cs0 f6 mainOut f24 tail #connect
cs0 f24 head f2 in #connect
cs0 f14 mainOut f47 tail #connect
cs0 f47 head f6 mainIn #connect
