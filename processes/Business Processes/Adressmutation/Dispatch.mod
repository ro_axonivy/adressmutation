[Ivy]
[>Created: Tue Dec 14 12:06:31 CET 2010]
12B6743DE81DDE73 3.14 #module
>Proto >Proto Collection #zClass
Dh0 Dispatch Big #zClass
Dh0 B #cInfo
Dh0 #process
Dh0 @TextInP .resExport .resExport #zField
Dh0 @TextInP .type .type #zField
Dh0 @TextInP .processKind .processKind #zField
Dh0 @AnnotationInP-0n ai ai #zField
Dh0 @TextInP .xml .xml #zField
Dh0 @TextInP .responsibility .responsibility #zField
Dh0 @StartSub f0 '' #zField
Dh0 @EndSub f1 '' #zField
Dh0 @Page f2 '' #zField
Dh0 @PushWFArc f4 '' #zField
Dh0 @CallSub f8 '' #zField
Dh0 @PushWFArc f9 '' #zField
Dh0 @PushWFArc f6 '' #zField
Dh0 @CallSub f29 '' #zField
Dh0 @PushWFArc f3 '' #zField
>Proto Dh0 Dh0 Dispatch #zField
Dh0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;' #txt
Dh0 f0 inParamTable 'out.eViewType=param.viewType;
out.order=param.order;
' #txt
Dh0 f0 outParamDecl '<ch.soreco.orderbook.bo.Order order,java.lang.String error,java.lang.Boolean success> result;
' #txt
Dh0 f0 outParamTable 'result.order=in.order;
result.error=in.error;
result.success=in.success;
' #txt
Dh0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dh0 f0 callSignature call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType) #txt
Dh0 f0 type ch.soreco.orderbook.ui.Order #txt
Dh0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order,ViewType)</name>
    </language>
</elementInfo>
' #txt
Dh0 f0 87 37 26 26 14 0 #rect
Dh0 f0 @|StartSubIcon #fIcon
Dh0 f1 type ch.soreco.orderbook.ui.Order #txt
Dh0 f1 87 287 26 26 14 0 #rect
Dh0 f1 @|EndSubIcon #fIcon
Dh0 f2 outTypes "ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order" #txt
Dh0 f2 outLinks "exit.ivp","showScanFile.ivp" #txt
Dh0 f2 template "Adressmutation.ivd" #txt
Dh0 f2 type ch.soreco.orderbook.ui.Order #txt
Dh0 f2 skipLink skip.ivp #txt
Dh0 f2 sortLink sort.ivp #txt
Dh0 f2 templateWizard '#
#Sun Oct 17 19:15:59 CEST 2010
' #txt
Dh0 f2 pageArchivingActivated false #txt
Dh0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation Dispatch</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
Dh0 f2 @C|.responsibility Everybody #txt
Dh0 f2 82 176 36 24 20 -2 #rect
Dh0 f2 @|PageIcon #fIcon
Dh0 f4 expr data #txt
Dh0 f4 outCond ivp=="exit.ivp" #txt
Dh0 f4 100 200 100 287 #arcP
Dh0 f8 type ch.soreco.orderbook.ui.Order #txt
Dh0 f8 processCall 'Functional Processes/HTMLGet:call(Integer,ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
Dh0 f8 doCall true #txt
Dh0 f8 requestActionDecl '<java.lang.Integer currentChild,ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
Dh0 f8 requestMappingAction 'param.currentChild=in.currentChild;
param.order=in.order;
param.viewType=in.eViewType;
' #txt
Dh0 f8 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dh0 f8 responseMappingAction 'out=result.orderUI;
' #txt
Dh0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get HTML</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Dh0 f8 82 100 36 24 20 -2 #rect
Dh0 f8 @|CallSubIcon #fIcon
Dh0 f9 expr out #txt
Dh0 f9 100 63 100 100 #arcP
Dh0 f6 expr out #txt
Dh0 f6 100 124 100 176 #arcP
Dh0 f29 type ch.soreco.orderbook.ui.Order #txt
Dh0 f29 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Dh0 f29 doCall true #txt
Dh0 f29 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Dh0 f29 requestMappingAction 'param.filter.fileId=in.order.scanFileId;
' #txt
Dh0 f29 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dh0 f29 responseMappingAction 'out=in;
' #txt
Dh0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show ScanFile</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Dh0 f29 218 240 36 24 20 -2 #rect
Dh0 f29 @|CallSubIcon #fIcon
Dh0 f3 expr data #txt
Dh0 f3 outCond ivp=="showScanFile.ivp" #txt
Dh0 f3 118 196 218 244 #arcP
>Proto Dh0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Dh0 .processKind CALLABLE_SUB #txt
>Proto Dh0 0 0 32 24 18 0 #rect
>Proto Dh0 @|BIcon #fIcon
Dh0 f2 out f4 tail #connect
Dh0 f4 head f1 mainIn #connect
Dh0 f0 mainOut f9 tail #connect
Dh0 f9 head f8 mainIn #connect
Dh0 f8 mainOut f6 tail #connect
Dh0 f6 head f2 mainIn #connect
Dh0 f2 out f3 tail #connect
Dh0 f3 head f29 mainIn #connect
