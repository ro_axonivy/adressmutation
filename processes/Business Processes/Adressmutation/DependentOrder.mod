[Ivy]
[>Created: Tue Sep 10 11:50:21 CEST 2013]
131650E28FEB4BEB 3.15 #module
>Proto >Proto Collection #zClass
Dr0 DependentOrder Big #zClass
Dr0 B #cInfo
Dr0 #process
Dr0 @TextInP .resExport .resExport #zField
Dr0 @TextInP .type .type #zField
Dr0 @TextInP .processKind .processKind #zField
Dr0 @AnnotationInP-0n ai ai #zField
Dr0 @TextInP .xml .xml #zField
Dr0 @TextInP .responsibility .responsibility #zField
Dr0 @StartSub f0 '' #zField
Dr0 @EndSub f1 '' #zField
Dr0 @Alternative f70 '' #zField
Dr0 @CallSub f71 '' #zField
Dr0 @CallSub f75 '' #zField
Dr0 @PushWFArc f72 '' #zField
Dr0 @PushWFArc f76 '' #zField
Dr0 @Page f3 '' #zField
Dr0 @PushWFArc f2 '' #zField
Dr0 @GridStep f7 '' #zField
Dr0 @PushWFArc f8 '' #zField
Dr0 @PushWFArc f6 '' #zField
Dr0 @GridStep f12 '' #zField
Dr0 @PushWFArc f14 '' #zField
Dr0 @DBStep f109 '' #zField
Dr0 @PushWFArc f10 '' #zField
Dr0 @CallSub f5 '' #zField
Dr0 @PushWFArc f11 '' #zField
Dr0 @Alternative f17 '' #zField
Dr0 @CallSub f19 '' #zField
Dr0 @GridStep f9 '' #zField
Dr0 @PushWFArc f21 '' #zField
Dr0 @PushWFArc f13 '' #zField
Dr0 @GridStep f15 '' #zField
Dr0 @PushWFArc f16 '' #zField
Dr0 @PushWFArc f20 '' #zField
Dr0 @PushWFArc f22 '' #zField
Dr0 @PushWFArc f23 '' #zField
Dr0 @GridStep f24 '' #zField
Dr0 @PushWFArc f25 '' #zField
Dr0 @InfoButton f26 '' #zField
Dr0 @AnnotationArc f27 '' #zField
Dr0 @PushWFArc f18 '' #zField
Dr0 @PushWFArc f30 '' #zField
>Proto Dr0 Dr0 DependentOrder #zField
Dr0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order> param;' #txt
Dr0 f0 inParamTable 'out.order=param.order;
' #txt
Dr0 f0 outParamDecl '<java.lang.Boolean proceed,ch.soreco.orderbook.bo.Order order> result;
' #txt
Dr0 f0 outParamTable 'result.proceed=in.newOrder;
result.order=in.orderCopy;
' #txt
Dr0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f0 callSignature newDependentOrder(ch.soreco.orderbook.bo.Order) #txt
Dr0 f0 type ch.soreco.orderbook.ui.Order #txt
Dr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>newDependentOrder()</name>
        <nameStyle>19,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f0 147 35 26 26 14 0 #rect
Dr0 f0 @|StartSubIcon #fIcon
Dr0 f1 type ch.soreco.orderbook.ui.Order #txt
Dr0 f1 147 643 26 26 14 0 #rect
Dr0 f1 @|EndSubIcon #fIcon
Dr0 f70 type ch.soreco.orderbook.ui.Order #txt
Dr0 f70 146 250 28 28 14 0 #rect
Dr0 f70 @|AlternativeIcon #fIcon
Dr0 f71 type ch.soreco.orderbook.ui.Order #txt
Dr0 f71 processCall 'Functional Processes/orderbook/Order:set(ch.soreco.orderbook.bo.Order)' #txt
Dr0 f71 doCall true #txt
Dr0 f71 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Dr0 f71 requestMappingAction 'param.order.BPId=in.order.BPId;
param.order.checkerUserId=in.order.checkerUserId;
param.order.contacts=in.order.contacts;
param.order.dispatcherUserId=ivy.session.getSessionUserName();
param.order.dispatchingId=in.order.dispatchingId;
param.order.editorUserId=in.order.editorUserId;
param.order.orderQuantityType=in.orderCopy.orderQuantityType;
param.order.orderStart=in.order.orderStart;
param.order.orderState=ch.soreco.orderbook.enums.OrderState.Dispatched.toString();
param.order.orderType=in.orderCopy.orderType;
param.order.scanFileId=in.order.scanFileId;
param.order.scanId=in.order.scanId;
param.order.scanMonoFileId=in.order.scanMonoFileId;
param.order.scanningId=in.order.scanningId;
param.order.scannings=in.order.scannings;
' #txt
Dr0 f71 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f71 responseMappingAction 'out=in;
out.newOrder=result.success;
out.orderCopy=result.order;
' #txt
Dr0 f71 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f71 142 420 36 24 20 -2 #rect
Dr0 f71 @|CallSubIcon #fIcon
Dr0 f75 type ch.soreco.orderbook.ui.Order #txt
Dr0 f75 processCall 'Functional Processes/orderbook/Dispatching:set(ch.soreco.orderbook.bo.Dispatching)' #txt
Dr0 f75 doCall true #txt
Dr0 f75 requestActionDecl '<ch.soreco.orderbook.bo.Dispatching dispatching> param;
' #txt
Dr0 f75 requestMappingAction 'param.dispatching.assignments="";
param.dispatching.dispatchDateTime=new DateTime();
param.dispatching.dispatcherUserId=in.order.editorUserId;
param.dispatching.orderId=in.order.orderId;
' #txt
Dr0 f75 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f75 responseMappingAction 'out=in;
out.order.dispatcherUserId=result.dispatchingData.dispatcherUserId;
out.order.dispatchingId=result.dispatchingData.id;
' #txt
Dr0 f75 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Dispatching)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f75 142 300 36 24 20 -2 #rect
Dr0 f75 @|CallSubIcon #fIcon
Dr0 f72 expr in #txt
Dr0 f72 outCond 'in.order.dispatchingId > 0' #txt
Dr0 f72 174 264 178 432 #arcP
Dr0 f72 1 448 264 #addKink
Dr0 f72 2 448 432 #addKink
Dr0 f72 1 0.13581094547281886 0 0 #arcLabel
Dr0 f76 expr in #txt
Dr0 f76 160 278 160 300 #arcP
Dr0 f3 outTypes "ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order" #txt
Dr0 f3 outLinks "create.ivp","cancel.ivp" #txt
Dr0 f3 template "NewOrder.ivc" #txt
Dr0 f3 type ch.soreco.orderbook.ui.Order #txt
Dr0 f3 skipLink skip.ivp #txt
Dr0 f3 sortLink sort.ivp #txt
Dr0 f3 templateWizard '#
#Tue Sep 10 11:50:16 CEST 2013
' #txt
Dr0 f3 pageArchivingActivated false #txt
Dr0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>newOrder</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f3 @C|.responsibility Everybody #txt
Dr0 f3 142 204 36 24 20 -2 #rect
Dr0 f3 @|PageIcon #fIcon
Dr0 f2 expr data #txt
Dr0 f2 outCond ivp=="create.ivp" #txt
Dr0 f2 160 228 160 250 #arcP
Dr0 f7 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f7 actionTable 'out=in;
out.newOrder=false;
' #txt
Dr0 f7 type ch.soreco.orderbook.ui.Order #txt
Dr0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>cancel</name>
        <nameStyle>6
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f7 54 308 36 24 20 -2 #rect
Dr0 f7 @|StepIcon #fIcon
Dr0 f8 expr data #txt
Dr0 f8 outCond ivp=="cancel.ivp" #txt
Dr0 f8 142 216 72 308 #arcP
Dr0 f8 1 72 216 #addKink
Dr0 f8 1 0.25029321685086503 0 0 #arcLabel
Dr0 f6 expr out #txt
Dr0 f6 72 332 147 656 #arcP
Dr0 f6 1 72 656 #addKink
Dr0 f6 0 0.7389601374876841 0 0 #arcLabel
Dr0 f12 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f12 actionTable 'out=in;
' #txt
Dr0 f12 actionCode 'import ch.soreco.orderbook.authority.util.OrderTypeUtil;

out.officialTeamOrderTypes = OrderTypeUtil.getOfficialTeamOrderTypes();
out.orderQuantityTypes = OrderTypeUtil.getOrderQuantityTypes();
' #txt
Dr0 f12 type ch.soreco.orderbook.ui.Order #txt
Dr0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get  Order Types and Order Quantity Types 
from OrderTypeUtil</name>
        <nameStyle>61
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f12 142 92 36 24 20 -2 #rect
Dr0 f12 @|StepIcon #fIcon
Dr0 f14 expr out #txt
Dr0 f14 160 61 160 92 #arcP
Dr0 f109 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f109 actionTable 'out=in;
' #txt
Dr0 f109 dbSql '<?xml version=""1.0"" standalone=""no""?>
<!DOCTYPE ANY_SQL SYSTEM  ""sqlStatements.dtd"">
<ANY_SQL><Verbatim quote=''true''>update tbl_Scanning 
set 
	docType = &#39;IL&#39;,
	orderType = &#39;Order&#39;
where orderId = in.order.orderId
and isMain = 1</Verbatim></ANY_SQL>' #txt
Dr0 f109 dbUrl bzp_ivy_daten #txt
Dr0 f109 cache '{/cache false /invalidation false /scope 0 /groupname ""/lifetime_group "0"/invalidation_time_group ""/identifier ""/lifetime_entry "0"/invalidation_time_entry ""}' #txt
Dr0 f109 dbWizard 'update tbl_Scanning 
set 
	docType = ''IL'',
	orderType = ''Order''
where orderId = in.order.orderId
and isMain = 1' #txt
Dr0 f109 lotSize 2147483647 #txt
Dr0 f109 startIdx 0 #txt
Dr0 f109 type ch.soreco.orderbook.ui.Order #txt
Dr0 f109 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>fix docType</name>
        <nameStyle>11
</nameStyle>
        <desc>hotfix for bug 10.08.2011 / ZKEMA:
1. Kombiaufträge werden nicht als IL archiviert, ausser wenn als IL gescannt.
2. Einzelaufträge die Redispatched wurden, werden nicht mit der neuen (beim ReDisp. gesetzten) Auftragsart.
</desc>
    </language>
</elementInfo>
' #txt
Dr0 f109 142 468 36 24 20 -2 #rect
Dr0 f109 @|DBStepIcon #fIcon
Dr0 f10 expr out #txt
Dr0 f10 160 444 160 468 #arcP
Dr0 f5 type ch.soreco.orderbook.ui.Order #txt
Dr0 f5 processCall 'Functional Processes/orderbook/Order:setPlainOrder(ch.soreco.orderbook.bo.Order)' #txt
Dr0 f5 doCall true #txt
Dr0 f5 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Dr0 f5 requestMappingAction 'param.order=in.order;
' #txt
Dr0 f5 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f5 responseMappingAction 'out=in;
' #txt
Dr0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setPlainOrder(OriginalOrder)</name>
        <nameStyle>28,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f5 142 348 36 24 20 -2 #rect
Dr0 f5 @|CallSubIcon #fIcon
Dr0 f11 expr out #txt
Dr0 f11 160 324 160 348 #arcP
Dr0 f17 type ch.soreco.orderbook.ui.Order #txt
Dr0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>decrement i
to get contacts in correct order</name>
        <nameStyle>12,7
32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f17 146 570 28 28 14 0 #rect
Dr0 f17 @|AlternativeIcon #fIcon
Dr0 f19 type ch.soreco.orderbook.ui.Order #txt
Dr0 f19 processCall 'Functional Processes/orderbook/Contact:set(ch.soreco.orderbook.bo.Contact)' #txt
Dr0 f19 doCall true #txt
Dr0 f19 requestActionDecl '<ch.soreco.orderbook.bo.Contact contact> param;
' #txt
Dr0 f19 requestMappingAction 'param.contact=in.currentContact;
' #txt
Dr0 f19 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f19 responseMappingAction 'out=in;
' #txt
Dr0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set(Contact)</name>
    </language>
</elementInfo>
' #txt
Dr0 f19 502 572 36 24 20 -2 #rect
Dr0 f19 @|CallSubIcon #fIcon
Dr0 f9 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f9 actionTable 'out=in;
out.i=in.i - 1;
' #txt
Dr0 f9 type ch.soreco.orderbook.ui.Order #txt
Dr0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i--</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f9 502 628 36 24 20 -2 #rect
Dr0 f9 @|StepIcon #fIcon
Dr0 f21 expr out #txt
Dr0 f21 520 596 520 628 #arcP
Dr0 f13 expr out #txt
Dr0 f13 160 372 160 420 #arcP
Dr0 f15 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f15 actionTable 'out=in;
' #txt
Dr0 f15 actionCode 'import ch.soreco.orderbook.bo.Contact;
out.currentContact = in.order.contacts.get(in.i-1) as Contact;
out.currentContact.contactId = null;
//out.currentContact.contactMergedFromOrderId = in.orderCopy.orderId;
out.currentContact.contactOrderId = in.orderCopy.orderId;
' #txt
Dr0 f15 type ch.soreco.orderbook.ui.Order #txt
Dr0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set current contact</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f15 342 572 36 24 20 -2 #rect
Dr0 f15 @|StepIcon #fIcon
Dr0 f16 expr in #txt
Dr0 f16 outCond 'in.i >0' #txt
Dr0 f16 174 584 342 584 #arcP
Dr0 f20 expr out #txt
Dr0 f20 378 584 502 584 #arcP
Dr0 f22 expr out #txt
Dr0 f22 502 640 165 593 #arcP
Dr0 f22 1 192 640 #addKink
Dr0 f22 0 0.5532718430931863 0 0 #arcLabel
Dr0 f23 expr in #txt
Dr0 f23 160 598 160 643 #arcP
Dr0 f24 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Dr0 f24 actionTable 'out=in;
' #txt
Dr0 f24 actionCode 'out.i = in.order.contacts.size();' #txt
Dr0 f24 type ch.soreco.orderbook.ui.Order #txt
Dr0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set i = contacts.Size</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f24 142 516 36 24 20 -2 #rect
Dr0 f24 @|StepIcon #fIcon
Dr0 f25 expr out #txt
Dr0 f25 160 492 160 516 #arcP
Dr0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Fix for ITS-1052 / PB / 14.11.2011
copy all contacts</name>
        <nameStyle>52,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dr0 f26 642 554 60 60 -15 -15 #rect
Dr0 f26 @|IBIcon #fIcon
Dr0 f26 -1|-1|-16777216 #nodeStyle
Dr0 f27 642 584 538 584 #arcP
Dr0 f18 expr out #txt
Dr0 f18 160 540 160 570 #arcP
Dr0 f30 expr out #txt
Dr0 f30 160 116 160 204 #arcP
>Proto Dr0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Dr0 .processKind CALLABLE_SUB #txt
>Proto Dr0 0 0 32 24 18 0 #rect
>Proto Dr0 @|BIcon #fIcon
Dr0 f70 out f72 tail #connect
Dr0 f72 head f71 mainIn #connect
Dr0 f70 out f76 tail #connect
Dr0 f76 head f75 mainIn #connect
Dr0 f3 out f2 tail #connect
Dr0 f2 head f70 in #connect
Dr0 f3 out f8 tail #connect
Dr0 f8 head f7 mainIn #connect
Dr0 f7 mainOut f6 tail #connect
Dr0 f6 head f1 mainIn #connect
Dr0 f0 mainOut f14 tail #connect
Dr0 f14 head f12 mainIn #connect
Dr0 f71 mainOut f10 tail #connect
Dr0 f10 head f109 mainIn #connect
Dr0 f75 mainOut f11 tail #connect
Dr0 f11 head f5 mainIn #connect
Dr0 f19 mainOut f21 tail #connect
Dr0 f21 head f9 mainIn #connect
Dr0 f5 mainOut f13 tail #connect
Dr0 f13 head f71 mainIn #connect
Dr0 f17 out f16 tail #connect
Dr0 f16 head f15 mainIn #connect
Dr0 f15 mainOut f20 tail #connect
Dr0 f20 head f19 mainIn #connect
Dr0 f9 mainOut f22 tail #connect
Dr0 f22 head f17 in #connect
Dr0 f17 out f23 tail #connect
Dr0 f23 head f1 mainIn #connect
Dr0 f109 mainOut f25 tail #connect
Dr0 f25 head f24 mainIn #connect
Dr0 f26 ao f27 tail #connect
Dr0 f27 head f19 @CG|ai #connect
Dr0 f24 mainOut f18 tail #connect
Dr0 f18 head f17 in #connect
Dr0 f12 mainOut f30 tail #connect
Dr0 f30 head f3 mainIn #connect
