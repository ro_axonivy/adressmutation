[Ivy]
[>Created: Wed Nov 16 15:38:53 CET 2011]
12B5740D3E32F6C2 3.15 #module
>Proto >Proto Collection #zClass
Rd0 Read Big #zClass
Rd0 B #cInfo
Rd0 #process
Rd0 @InfoButton f7 '' #zField
Rd0 @TextInP .resExport .resExport #zField
Rd0 @TextInP .type .type #zField
Rd0 @TextInP .processKind .processKind #zField
Rd0 @AnnotationInP-0n ai ai #zField
Rd0 @TextInP .xml .xml #zField
Rd0 @TextInP .responsibility .responsibility #zField
Rd0 @StartSub f0 '' #zField
Rd0 @EndSub f1 '' #zField
Rd0 @Page f2 '' #zField
Rd0 @PushWFArc f4 '' #zField
Rd0 @CallSub f3 '' #zField
Rd0 @PushWFArc f5 '' #zField
Rd0 @CallSub f29 '' #zField
Rd0 @CallSub f14 '' #zField
Rd0 @PushWFArc f6 '' #zField
Rd0 @CallSub f9 '' #zField
Rd0 @PushWFArc f10 '' #zField
Rd0 @CallSub f8 '' #zField
Rd0 @PushWFArc f11 '' #zField
Rd0 @CallSub f87 '' #zField
Rd0 @PushWFArc f12 '' #zField
Rd0 @CallSub f105 '' #zField
Rd0 @PushWFArc f13 '' #zField
>Proto Rd0 Rd0 Read #zField
Rd0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>16.03.2011 / PB
old methods replaced 
by multi-doc view</name>
        <nameStyle>55
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f7 452 130 168 157 -37 -56 #rect
Rd0 f7 @|IBIcon #fIcon
Rd0 f7 -1|-1|-16777216 #nodeStyle
Rd0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;' #txt
Rd0 f0 inParamTable 'out.eViewType=param.viewType;
out.order=param.order;
' #txt
Rd0 f0 outParamDecl '<ch.soreco.orderbook.bo.Order order,java.lang.String error,java.lang.Boolean success> result;
' #txt
Rd0 f0 outParamTable 'result.order=in.order;
result.error=in.error;
result.success=in.success;
' #txt
Rd0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f0 callSignature call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType) #txt
Rd0 f0 type ch.soreco.orderbook.ui.Order #txt
Rd0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order,Boolean)</name>
        <nameStyle>19,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f0 87 37 26 26 14 0 #rect
Rd0 f0 @|StartSubIcon #fIcon
Rd0 f1 type ch.soreco.orderbook.ui.Order #txt
Rd0 f1 87 323 26 26 14 0 #rect
Rd0 f1 @|EndSubIcon #fIcon
Rd0 f2 outTypes "ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order" #txt
Rd0 f2 outLinks "exit.ivp","showScanFile.ivp","showContactFile.ivp","showRelatedOrder.ivp","showHistory.ivp" #txt
Rd0 f2 template "Addressmutation.ivc" #txt
Rd0 f2 type ch.soreco.orderbook.ui.Order #txt
Rd0 f2 skipLink skip.ivp #txt
Rd0 f2 sortLink sort.ivp #txt
Rd0 f2 templateWizard '#
#Wed Nov 16 15:38:52 CET 2011
' #txt
Rd0 f2 pageArchivingActivated false #txt
Rd0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>AddressmutationRead</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f2 @C|.responsibility Everybody #txt
Rd0 f2 82 164 36 24 20 -2 #rect
Rd0 f2 @|PageIcon #fIcon
Rd0 f4 expr data #txt
Rd0 f4 outCond ivp=="exit.ivp" #txt
Rd0 f4 100 188 100 323 #arcP
Rd0 f3 type ch.soreco.orderbook.ui.Order #txt
Rd0 f3 processCall 'Functional Processes/HTMLGet:call(Integer,ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
Rd0 f3 doCall true #txt
Rd0 f3 requestActionDecl '<java.lang.Integer currentChild,ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
Rd0 f3 requestMappingAction 'param.currentChild=in.currentChild;
param.order=in.order;
param.viewType=in.eViewType;
' #txt
Rd0 f3 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f3 responseMappingAction 'out=result.orderUI;
' #txt
Rd0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get HTML</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f3 82 100 36 24 20 -2 #rect
Rd0 f3 @|CallSubIcon #fIcon
Rd0 f5 expr out #txt
Rd0 f5 100 63 100 100 #arcP
Rd0 f5 0 0.5131129140619908 0 0 #arcLabel
Rd0 f29 type ch.soreco.orderbook.ui.Order #txt
Rd0 f29 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Rd0 f29 doCall true #txt
Rd0 f29 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Rd0 f29 requestMappingAction 'param.filter.fileId=in.order.scanFileId;
' #txt
Rd0 f29 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f29 responseMappingAction 'out=in;
' #txt
Rd0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show ScanFile</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f29 462 212 36 24 20 -2 #rect
Rd0 f29 @|CallSubIcon #fIcon
Rd0 f29 -65536|-1|-16777216 #nodeStyle
Rd0 f14 type ch.soreco.orderbook.ui.Order #txt
Rd0 f14 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Rd0 f14 doCall true #txt
Rd0 f14 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Rd0 f14 requestMappingAction 'param.filter.fileId=in.orderContacts.get(in.i).scanFileId;
' #txt
Rd0 f14 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f14 responseMappingAction 'out=in;
' #txt
Rd0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Contact File by i</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f14 462 252 36 24 20 -2 #rect
Rd0 f14 @|CallSubIcon #fIcon
Rd0 f14 -65536|-1|-16777216 #nodeStyle
Rd0 f6 expr out #txt
Rd0 f6 100 124 100 164 #arcP
Rd0 f9 type ch.soreco.orderbook.ui.Order #txt
Rd0 f9 processCall 'Start Processes/Archive/CheckDocuments:call(ch.soreco.orderbook.ui.enums.ViewType,Integer,String)' #txt
Rd0 f9 doCall true #txt
Rd0 f9 requestActionDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId,java.lang.String uiType> param;
' #txt
Rd0 f9 requestMappingAction 'param.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
param.orderId=in.orderContacts.get(in.i).contactMergedFromOrderId.intValue() > 0 ? in.orderContacts.get(in.i).contactMergedFromOrderId : in.orderContacts.get(in.i).contactOrderId;
param.uiType="SCANPLAIN";
' #txt
Rd0 f9 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f9 responseMappingAction 'out=in;
' #txt
Rd0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show files of contact</name>
        <nameStyle>21,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f9 246 252 36 24 20 -2 #rect
Rd0 f9 @|CallSubIcon #fIcon
Rd0 f10 expr data #txt
Rd0 f10 outCond ivp=="showContactFile.ivp" #txt
Rd0 f10 109 188 246 264 #arcP
Rd0 f10 1 168 264 #addKink
Rd0 f10 1 0.2336158700541377 0 0 #arcLabel
Rd0 f8 type ch.soreco.orderbook.ui.Order #txt
Rd0 f8 processCall 'Start Processes/Archive/CheckDocuments:call(ch.soreco.orderbook.ui.enums.ViewType,Integer,String)' #txt
Rd0 f8 doCall true #txt
Rd0 f8 requestActionDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId,java.lang.String uiType> param;
' #txt
Rd0 f8 requestMappingAction 'param.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
param.orderId=in.order.contacts.size() > 0 && in.orderContacts.get(0).contactMergedFromOrderId > 0 ?  in.orderContacts.get(0).contactMergedFromOrderId : in.order.orderId;
param.uiType="SCANPLAIN";
' #txt
Rd0 f8 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f8 responseMappingAction 'out=in;
' #txt
Rd0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show files of last order</name>
        <nameStyle>24,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f8 246 204 36 24 20 -2 #rect
Rd0 f8 @|CallSubIcon #fIcon
Rd0 f11 expr data #txt
Rd0 f11 outCond ivp=="showScanFile.ivp" #txt
Rd0 f11 118 187 246 216 #arcP
Rd0 f11 1 168 216 #addKink
Rd0 f11 1 0.046131817986106625 0 0 #arcLabel
Rd0 f87 type ch.soreco.orderbook.ui.Order #txt
Rd0 f87 processCall 'Business Processes/chooseProcess:call(Integer,ch.soreco.orderbook.ui.enums.ViewType)' #txt
Rd0 f87 doCall true #txt
Rd0 f87 requestActionDecl '<java.lang.Integer orderId,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
Rd0 f87 requestMappingAction 'param.orderId=in.order.orderId;
param.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
' #txt
Rd0 f87 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f87 responseMappingAction 'out=in;
' #txt
Rd0 f87 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show related Order</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f87 246 300 36 24 20 -2 #rect
Rd0 f87 @|CallSubIcon #fIcon
Rd0 f12 expr data #txt
Rd0 f12 outCond ivp=="showRelatedOrder.ivp" #txt
Rd0 f12 104 188 246 312 #arcP
Rd0 f12 1 144 312 #addKink
Rd0 f12 0 0.9180876074233169 0 0 #arcLabel
Rd0 f105 type ch.soreco.orderbook.ui.Order #txt
Rd0 f105 processCall 'Business Processes/History/ShowSimpleOrderHistory:showSimpleHistory(ch.soreco.orderbook.functional.History)' #txt
Rd0 f105 doCall true #txt
Rd0 f105 requestActionDecl '<ch.soreco.orderbook.functional.History history> param;
' #txt
Rd0 f105 requestMappingAction 'param.history.fetchId=in.order.orderId;
param.history.historyTable="tbl_Orders_History";
param.history.idName="orderId";
' #txt
Rd0 f105 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Rd0 f105 responseMappingAction 'out=in;
' #txt
Rd0 f105 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show history</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Rd0 f105 246 356 36 24 20 -2 #rect
Rd0 f105 @|CallSubIcon #fIcon
Rd0 f13 expr data #txt
Rd0 f13 outCond ivp=="showHistory.ivp" #txt
Rd0 f13 102 188 246 368 #arcP
Rd0 f13 1 136 368 #addKink
Rd0 f13 0 0.8260841522521171 0 0 #arcLabel
>Proto Rd0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Rd0 .processKind CALLABLE_SUB #txt
>Proto Rd0 0 0 32 24 18 0 #rect
>Proto Rd0 @|BIcon #fIcon
Rd0 f2 out f4 tail #connect
Rd0 f4 head f1 mainIn #connect
Rd0 f0 mainOut f5 tail #connect
Rd0 f5 head f3 mainIn #connect
Rd0 f3 mainOut f6 tail #connect
Rd0 f6 head f2 mainIn #connect
Rd0 f10 head f9 mainIn #connect
Rd0 f2 out f11 tail #connect
Rd0 f11 head f8 mainIn #connect
Rd0 f2 out f10 tail #connect
Rd0 f2 out f12 tail #connect
Rd0 f12 head f87 mainIn #connect
Rd0 f2 out f13 tail #connect
Rd0 f13 head f105 mainIn #connect
