[Ivy]
[>Created: Tue May 10 20:58:02 CEST 2016]
12C0B885F3D4A30A 3.17 #module
>Proto >Proto Collection #zClass
Bh0 BPMatch Big #zClass
Bh0 B #cInfo
Bh0 #process
Bh0 @TextInP .resExport .resExport #zField
Bh0 @TextInP .type .type #zField
Bh0 @TextInP .processKind .processKind #zField
Bh0 @AnnotationInP-0n ai ai #zField
Bh0 @TextInP .xml .xml #zField
Bh0 @TextInP .responsibility .responsibility #zField
Bh0 @StartSub f0 '' #zField
Bh0 @EndSub f1 '' #zField
Bh0 @GridStep f26 '' #zField
Bh0 @Page f36 '' #zField
Bh0 @CallSub f47 '' #zField
Bh0 @CallSub f46 '' #zField
Bh0 @GridStep f42 '' #zField
Bh0 @CallSub f38 '' #zField
Bh0 @GridStep f35 '' #zField
Bh0 @GridStep f49 '' #zField
Bh0 @PushWFArc f37 '' #zField
Bh0 @PushWFArc f39 '' #zField
Bh0 @PushWFArc f48 '' #zField
Bh0 @PushWFArc f51 '' #zField
Bh0 @PushWFArc f52 '' #zField
Bh0 @PushWFArc f54 '' #zField
Bh0 @PushWFArc f2 '' #zField
Bh0 @GridStep f4 '' #zField
Bh0 @PushWFArc f5 '' #zField
Bh0 @PushWFArc f6 '' #zField
Bh0 @GridStep f7 '' #zField
Bh0 @PushWFArc f8 '' #zField
Bh0 @PushWFArc f10 '' #zField
Bh0 @CallSub f9 '' #zField
Bh0 @PushWFArc f11 '' #zField
Bh0 @CallSub f29 '' #zField
Bh0 @GridStep f14 '' #zField
Bh0 @PushWFArc f15 '' #zField
Bh0 @GridStep f17 '' #zField
Bh0 @PushWFArc f16 '' #zField
Bh0 @GridStep f19 '' #zField
Bh0 @PushWFArc f21 '' #zField
Bh0 @PushWFArc f18 '' #zField
Bh0 @CallSub f22 '' #zField
Bh0 @PushWFArc f23 '' #zField
Bh0 @CallSub f25 '' #zField
Bh0 @CallSub f28 '' #zField
Bh0 @PushWFArc f27 '' #zField
Bh0 @PushWFArc f31 '' #zField
Bh0 @CallSub f32 '' #zField
Bh0 @PushWFArc f33 '' #zField
Bh0 @GridStep f13 '' #zField
Bh0 @PushWFArc f34 '' #zField
Bh0 @PushWFArc f30 '' #zField
Bh0 @GridStep f40 '' #zField
Bh0 @PushWFArc f41 '' #zField
Bh0 @PushWFArc f24 '' #zField
Bh0 @Alternative f12 '' #zField
Bh0 @GridStep f3 '' #zField
Bh0 @PushWFArc f45 '' #zField
Bh0 @PushWFArc f44 '' #zField
Bh0 @PushWFArc f50 '' #zField
Bh0 @CallSub f105 '' #zField
Bh0 @PushWFArc f53 '' #zField
Bh0 @PushWFArc f57 '' #zField
Bh0 @PushWFArc f20 '' #zField
Bh0 @InfoButton f43 '' #zField
Bh0 @AnnotationArc f55 '' #zField
Bh0 @GridStep f108 '' #zField
Bh0 @PushWFArc f56 '' #zField
Bh0 @PushWFArc f58 '' #zField
>Proto Bh0 Bh0 BPMatch #zField
Bh0 f0 inParamDecl '<ch.soreco.orderbook.ui.Order orderUI> param;' #txt
Bh0 f0 inParamTable 'out=param.orderUI;
' #txt
Bh0 f0 outParamDecl '<ch.soreco.orderbook.ui.Order orderUI> result;
' #txt
Bh0 f0 outParamTable 'result.orderUI=in;
' #txt
Bh0 f0 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f0 callSignature call(ch.soreco.orderbook.ui.Order) #txt
Bh0 f0 type ch.soreco.orderbook.ui.Order #txt
Bh0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
    </language>
</elementInfo>
' #txt
Bh0 f0 291 51 26 26 14 0 #rect
Bh0 f0 @|StartSubIcon #fIcon
Bh0 f1 type ch.soreco.orderbook.ui.Order #txt
Bh0 f1 307 955 26 26 14 0 #rect
Bh0 f1 @|EndSubIcon #fIcon
Bh0 f26 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f26 actionTable 'out=in;
' #txt
Bh0 f26 actionCode 'import ch.soreco.webbies.common.html.form.ClassConverter;
StringBuilder html = new StringBuilder("");
out.htmlBuild =  new StringBuilder("");


html.append("<h3><%=ivy.cms.co(\"/Content/Shared/Labels/General\")%></h3>");
html.append("<div id=''OrderGeneral''>");
html.append(ClassConverter.getClassHTML(in.order,"order", in.modify, false));
html.append("</div><!--in Order-->");

out.htmlBuild.append(html);
out.htmlBuild.insert(0,"<div id=\"AdressmutationAccordion\" class=\"ui-widget ui-widget-content\">\n");
out.htmlBuild.append("</div>");

out.html = out.htmlBuild.toString();' #txt
Bh0 f26 type ch.soreco.orderbook.ui.Order #txt
Bh0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>build General</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f26 286 444 36 24 20 -2 #rect
Bh0 f26 @|StepIcon #fIcon
Bh0 f36 outTypes "ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order","ch.soreco.orderbook.ui.Order" #txt
Bh0 f36 outLinks "match.ivp","bpmatch.ivp","bpnotmatchable.ivp","saveAndClose.ivp","close.ivp","save.ivp","showScanFile.ivp","showHistory.ivp","abortAndClose.ivp" #txt
Bh0 f36 template "/ProcessPages/BPMatch/DoMatch.ivc" #txt
Bh0 f36 type ch.soreco.orderbook.ui.Order #txt
Bh0 f36 skipLink skip.ivp #txt
Bh0 f36 sortLink sort.ivp #txt
Bh0 f36 templateWizard '#
#Thu Mar 24 14:44:24 CET 2016
' #txt
Bh0 f36 pageArchivingActivated false #txt
Bh0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>DoMatch</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f36 @C|.responsibility Everybody #txt
Bh0 f36 286 508 36 24 20 -2 #rect
Bh0 f36 @|PageIcon #fIcon
Bh0 f47 type ch.soreco.orderbook.ui.Order #txt
Bh0 f47 processCall 'Functional Processes/orderbook/Order:setPlainOrder(ch.soreco.orderbook.bo.Order)' #txt
Bh0 f47 doCall true #txt
Bh0 f47 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Bh0 f47 requestMappingAction 'param.order=in.order;
' #txt
Bh0 f47 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f47 responseMappingAction 'out=in;
out.error=result.error;
out.success=result.success;
' #txt
Bh0 f47 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save</name>
        <nameStyle>4
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f47 286 244 36 24 20 -2 #rect
Bh0 f47 @|CallSubIcon #fIcon
Bh0 f46 type ch.soreco.orderbook.ui.Order #txt
Bh0 f46 processCall 'Functional Processes/orderUISave:call(ch.soreco.orderbook.ui.Order)' #txt
Bh0 f46 doCall true #txt
Bh0 f46 requestActionDecl '<ch.soreco.orderbook.ui.Order orderUI> param;
' #txt
Bh0 f46 requestMappingAction 'param.orderUI=in;
' #txt
Bh0 f46 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f46 responseMappingAction 'out=result.orderUI;
' #txt
Bh0 f46 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save orderUI</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f46 438 796 36 24 20 -2 #rect
Bh0 f46 @|CallSubIcon #fIcon
Bh0 f42 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f42 actionTable 'out=in;
out.order.bpIdIsChecked=null;
out.order.matchedToOrder=null;
out.order.orderState=ch.soreco.orderbook.enums.OrderState.Recording.toString();
' #txt
Bh0 f42 type ch.soreco.orderbook.ui.Order #txt
Bh0 f42 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Recording</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f42 286 188 36 24 20 -2 #rect
Bh0 f42 @|StepIcon #fIcon
Bh0 f38 type ch.soreco.orderbook.ui.Order #txt
Bh0 f38 processCall 'Business Processes/Adressmutation/Match:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.bo.Order)' #txt
Bh0 f38 doCall true #txt
Bh0 f38 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.bo.Order filter> param;
' #txt
Bh0 f38 requestMappingAction 'param.order=in.order;
param.filter.BPId=in.order.BPId;
' #txt
Bh0 f38 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f38 responseMappingAction 'out=in;
out.order=result.order;
' #txt
Bh0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Match</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f38 110 508 36 24 20 -2 #rect
Bh0 f38 @|CallSubIcon #fIcon
Bh0 f35 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f35 actionTable 'out=in;
out.order.bpNotMatchable=false;
out.order.orderState=ch.soreco.orderbook.enums.OrderState.Recording.toString();
' #txt
Bh0 f35 type ch.soreco.orderbook.ui.Order #txt
Bh0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>BP festgestellt</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f35 198 676 36 24 20 -2 #rect
Bh0 f35 @|StepIcon #fIcon
Bh0 f49 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f49 actionTable 'out=in;
out.order.bpNotMatchable=true;
out.order.editorUserId=ivy.session.getSessionUserName();
out.order.orderState=ch.soreco.orderbook.enums.OrderState.InIdCheck.toString();
' #txt
Bh0 f49 type ch.soreco.orderbook.ui.Order #txt
Bh0 f49 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>BP nicht festgestellt
set Editor</name>
        <nameStyle>32
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f49 350 676 36 24 20 -2 #rect
Bh0 f49 @|StepIcon #fIcon
Bh0 f37 expr out #txt
Bh0 f37 304 468 304 508 #arcP
Bh0 f39 expr data #txt
Bh0 f39 outCond ivp=="match.ivp" #txt
Bh0 f39 286 520 146 520 #arcP
Bh0 f48 expr out #txt
Bh0 f48 304 212 304 244 #arcP
Bh0 f51 expr out #txt
Bh0 f51 368 700 441 796 #arcP
Bh0 f51 1 368 736 #addKink
Bh0 f51 1 0.3129673795899511 0 0 #arcLabel
Bh0 f52 expr data #txt
Bh0 f52 outCond ivp=="bpmatch.ivp" #txt
Bh0 f52 298 532 222 676 #arcP
Bh0 f54 expr data #txt
Bh0 f54 outCond ivp=="bpnotmatchable.ivp" #txt
Bh0 f54 309 532 363 676 #arcP
Bh0 f2 expr out #txt
Bh0 f2 446 820 328 958 #arcP
Bh0 f4 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f4 actionTable 'out=in;
out.order.editorUserId=ivy.session.getSessionUserName();
' #txt
Bh0 f4 type ch.soreco.orderbook.ui.Order #txt
Bh0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>save and close: set Editor</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f4 526 676 36 24 20 -2 #rect
Bh0 f4 @|StepIcon #fIcon
Bh0 f5 expr data #txt
Bh0 f5 outCond ivp=="saveAndClose.ivp" #txt
Bh0 f5 321 532 527 676 #arcP
Bh0 f6 expr out #txt
Bh0 f6 544 700 471 796 #arcP
Bh0 f6 1 544 736 #addKink
Bh0 f6 1 0.3129673795899511 0 0 #arcLabel
Bh0 f7 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f7 actionTable 'out=in;
' #txt
Bh0 f7 type ch.soreco.orderbook.ui.Order #txt
Bh0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f7 1006 676 36 24 20 -2 #rect
Bh0 f7 @|StepIcon #fIcon
Bh0 f8 expr data #txt
Bh0 f8 outCond ivp=="close.ivp" #txt
Bh0 f8 322 520 1006 688 #arcP
Bh0 f10 expr out #txt
Bh0 f10 1024 700 333 968 #arcP
Bh0 f10 1 1024 968 #addKink
Bh0 f10 1 0.23804597567448435 0 0 #arcLabel
Bh0 f9 type ch.soreco.orderbook.ui.Order #txt
Bh0 f9 processCall 'Functional Processes/orderUISave:call(ch.soreco.orderbook.ui.Order)' #txt
Bh0 f9 doCall true #txt
Bh0 f9 requestActionDecl '<ch.soreco.orderbook.ui.Order orderUI> param;
' #txt
Bh0 f9 requestMappingAction 'param.orderUI=in;
' #txt
Bh0 f9 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f9 responseMappingAction 'out=result.orderUI;
' #txt
Bh0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save orderUI</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f9 518 364 36 24 20 -2 #rect
Bh0 f9 @|CallSubIcon #fIcon
Bh0 f11 expr data #txt
Bh0 f11 outCond ivp=="save.ivp" #txt
Bh0 f11 322 520 536 388 #arcP
Bh0 f11 1 536 472 #addKink
Bh0 f11 0 0.6 0 0 #arcLabel
Bh0 f29 type ch.soreco.orderbook.ui.Order #txt
Bh0 f29 processCall 'Functional Processes/common/File:show(ch.soreco.common.bo.File)' #txt
Bh0 f29 doCall true #txt
Bh0 f29 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Bh0 f29 requestMappingAction 'param.filter.fileId=in.order.scanFileId;
' #txt
Bh0 f29 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f29 responseMappingAction 'out=in;
' #txt
Bh0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show ScanFile</name>
        <nameStyle>13
</nameStyle>
        <desc>16.03.2011 / PB
replaced by multi-doc view</desc>
    </language>
</elementInfo>
' #txt
Bh0 f29 86 612 36 24 20 -2 #rect
Bh0 f29 @|CallSubIcon #fIcon
Bh0 f29 -65536|-1|-16777216 #nodeStyle
Bh0 f14 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f14 actionTable 'out=in;
' #txt
Bh0 f14 actionCode 'import ch.soreco.orderbook.enums.DoubletteTypes;
import ch.soreco.orderbook.enums.CorrespondenceType;
import ch.soreco.orderbook.enums.BPLevel;
import ch.soreco.orderbook.enums.OrderIncomeType;
import ch.soreco.orderbook.ui.enums.ViewType;
import ch.soreco.orderbook.enums.StandardPaperType;
import ch.soreco.orderbook.enums.PaperType;
import ch.soreco.orderbook.enums.ReversType;
import ch.soreco.orderbook.enums.SignatureType;
import ch.soreco.common.enums.BooleanOption;
import ch.soreco.orderbook.enums.OrderState;
import ch.soreco.orderbook.enums.OrderType;
import ch.soreco.webbies.common.html.form.InputType;
import ch.soreco.webbies.common.html.form.ClassConverter;

ClassConverter.clear();
ClassConverter.putEnabledFields("");
ClassConverter.putInputType(InputType.SUPPRESSED, ["signatures", "recording", "papers"]);
ClassConverter.putInputType(InputType.SUPPRESSED, "comment");
ClassConverter.putInputType(InputType.SUPPRESSED, "discard");
ClassConverter.putInputType(InputType.SOURCED,"scanFileId");
ClassConverter.putInputType(InputType.TEXTAREA,"newAdressText");
ClassConverter.putInputType(InputType.SUPPRESSED, "nextDueDate");
ClassConverter.putInputType(InputType.SUPPRESSED, "lastEdit");
ClassConverter.putInputType(InputType.SUPPRESSED, "pendencyTicketType");


List<String> labelled = new List<String>();
labelled.add("orderId");
labelled.add("scanId");
labelled.add("editorUserId");
labelled.add("checkerUserId");
labelled.add("signatureRequestDate");
labelled.add("paperRequestDate");
ClassConverter.putInputType(InputType.LABEL,labelled);

List<String> hidden = new List<String>();

hidden.add("scanMonoFileId");
hidden.add("contactOrderNo");
hidden.add("matchedToOrder");
hidden.add("SignatureRequestOrderId");
hidden.add("newSignatureRequest");
hidden.add("timeRemaining");
hidden.add("contact");
hidden.add("contactOrderId");
hidden.add("signatures");
hidden.add("incomeDate");
hidden.add("scanning");
hidden.add("recording");
hidden.add("address");
hidden.add("contactId");
hidden.add("addressContactId");
hidden.add("paperOrderId");
hidden.add("paperId");
hidden.add("correspondenceId");
hidden.add("affirmationId");
hidden.add("businessPartnerId");
hidden.add("signatureRequestId");
hidden.add("domicileOrOwnerId");
hidden.add("lsvId");
hidden.add("correspondenceContainerId");
hidden.add("correspondence");
hidden.add("order");
hidden.add("bpNotMatchable");

hidden.add("containerCheckedBy");
hidden.add("contactCheckedBy");
hidden.add("addressCheckedBy");
hidden.add("paperCheckedBy");
hidden.add("signatureCheckedBy");
hidden.add("businessPartnerCheckedBy");
hidden.add("newAdressCheckedBy");
hidden.add("changeAddressCheckedBy");
hidden.add("affirmationCheckedBy");
hidden.add("domicileOrOwnerCheckedBy");
hidden.add("LSVCheckedBy");

ClassConverter.putInputType(InputType.HIDDEN,hidden);

ClassConverter.putHideenOnEmptyFields("matchedToOrder");
ClassConverter.putHideenOnEmptyFields("signatureRequestDate");
ClassConverter.putHideenOnEmptyFields("paperRequestDate");

if(!in.eViewType.equals(ViewType.DISPATCH)){
	ClassConverter.putInputType(InputType.HIDDEN,"orderType");
	ClassConverter.putInputType(InputType.HIDDEN,"orderState");
	ClassConverter.putInputType(InputType.HIDDEN,"orderStart");
	ClassConverter.putInputType(InputType.HIDDEN,"orderEnd");
	}

ClassConverter.putInputType(InputType.SELECT,"orderType",OrderType.values(), false);
ClassConverter.putInputType(InputType.SELECT,"orderState",OrderState.values(), false);
ClassConverter.putInputType(InputType.SELECT,"standardPaperType",StandardPaperType.values(), false);
ClassConverter.putInputType(InputType.SELECT,"bpLevel",BPLevel.values(), false);
	
//for Check View Type
List<String> isChecked = new List<String>();
isChecked.add("addressIsChecked");
isChecked.add("newAddressIsChecked");
isChecked.add("changeAddressIsChecked");
isChecked.add("containerIsChecked");
isChecked.add("bpIdIsChecked");
isChecked.add("signatureIsChecked");
isChecked.add("contactIsChecked");
isChecked.add("paperIsChecked");
isChecked.add("domicileOrOwnerIsChecked");
isChecked.add("LSVIsChecked");
isChecked.add("affirmationIsChecked");
isChecked.add("businessPartnerIsChecked");
ClassConverter.putInputType(InputType.RADIOS_RL,isChecked,BooleanOption.values());

ClassConverter.putInputType(InputType.RADIOS_RL,"doubletteType",DoubletteTypes.values(),false);
ClassConverter.putInputType(InputType.RADIOS_RL,"postalSignatureType",SignatureType.values(),false);
ClassConverter.putInputType(InputType.RADIOS_RL,"faxOrMailReversType",ReversType.values(),false);
ClassConverter.putInputType(InputType.RADIOS_RL,"faxSignatureType",SignatureType.values(),false);
ClassConverter.putInputType(InputType.RADIOS_RL,"faxReversType",ReversType.values(),false);

ClassConverter.putInputType(InputType.RADIOS_TD,"orderIncomeType",OrderIncomeType.values(),false);
ClassConverter.putAdjacentToOptionMap("orderIncomeTypea1Postal".toLowerCase(),["postalSignatureType"]);
ClassConverter.putAdjacentToOptionMap("orderIncomeTypea2FaxOrMail".toLowerCase(),["faxOrMailReversType"]);
ClassConverter.putAdjacentToOptionMap("orderIncomeTypea3Fax".toLowerCase(),["faxSignatureType", "faxReversType"]);

ClassConverter.putInputType(InputType.RADIOS_TD,"paperType",PaperType.values(),false);
ClassConverter.putNextToOptionMap("paperTypeIsStandard".toLowerCase(),["standardPaperType"]);
ClassConverter.putNextToOptionMap("paperTypeIsIndividual".toLowerCase(),["individualPaperType"]);

ClassConverter.putInputType(InputType.RADIOS_TD,"correspondenceType",CorrespondenceType.values(),false);
ClassConverter.putNextToOptionMap("correspondenceTypechangeAddress".toLowerCase(),["changeAddressOrderNo", "changeAddressIsChecked"]);
ClassConverter.putNextToOptionMap("correspondenceTypenewAddress".toLowerCase(),["newAddressOrderNo","newAddressIsChecked"]);




import ch.soreco.orderbook.enums.OrderState;

if(!in.order.orderState.equals(OrderState.InIdCheck.toString())){
	ClassConverter.putDisabledFields("bpIdIsChecked");
	}	
ClassConverter.putDisabledFields("domicileOrOwnerIsChecked");
ClassConverter.putDisabledFields("containerIsChecked");
ClassConverter.putDisabledFields("newAddressIsChecked");
ClassConverter.putDisabledFields("changeAddressIsChecked");
ClassConverter.putDisabledFields("isChecked");
ClassConverter.putDisabledFields("contactIsChecked");	
ClassConverter.putDisabledFields("paperIsChecked");	
ClassConverter.putDisabledFields("signatureIsChecked");	
ClassConverter.putDisabledFields("businessPartnerIsChecked");	
ClassConverter.putDisabledFields("affirmationIsChecked");	
ClassConverter.putDisabledFields("LSVIsChecked");	
ClassConverter.putDisabledFields("addressIsChecked");

' #txt
Bh0 f14 type ch.soreco.orderbook.ui.Order #txt
Bh0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set general Objects Meta</name>
        <nameStyle>24
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f14 286 300 36 24 20 -2 #rect
Bh0 f14 @|StepIcon #fIcon
Bh0 f15 expr out #txt
Bh0 f15 304 268 304 300 #arcP
Bh0 f17 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f17 actionTable 'out=in;
' #txt
Bh0 f17 actionCode 'import ch.soreco.webbies.common.html.form.ClassConverter;

ClassConverter.putAdjacentMap("orderId", ["scanFileId", "editorUserId"]);
ClassConverter.putAdjacentMap("scanId",["checkerUserId"]);
ClassConverter.putAdjacentMap("orderType",["orderState"]);
ClassConverter.putAdjacentMap("isDouble",["archiveId"]);
ClassConverter.putAdjacentMap("BPId",["bpIdIsChecked"]);
ClassConverter.putAdjacentMap("isNewAdress",["newAdressText"]);
ClassConverter.putAdjacentMap("bpOrderNo",["businessPartnerIsChecked"]);
ClassConverter.putAdjacentMap("orderNo",["affirmationIsChecked"]);
ClassConverter.putAdjacentMap("scanFileId",["contactIsChecked"]);
ClassConverter.putAdjacentMap("isPaper",["paperRequestDate", "paperIsChecked"]);
' #txt
Bh0 f17 type ch.soreco.orderbook.ui.Order #txt
Bh0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set adjacents</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f17 286 404 36 24 20 -2 #rect
Bh0 f17 @|StepIcon #fIcon
Bh0 f16 expr out #txt
Bh0 f16 304 428 304 444 #arcP
Bh0 f19 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f19 actionTable 'out=in;
' #txt
Bh0 f19 actionCode 'in.orderType = ivy.cms.co("/ch/soreco/orderbook/enums/OrderType/"+in.order.orderType+"/0_name");
in.orderState = ivy.cms.co("/ch/soreco/orderbook/enums/OrderState/"+in.order.orderState+"/0_name");' #txt
Bh0 f19 type ch.soreco.orderbook.ui.Order #txt
Bh0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order type and State Labels</name>
        <nameStyle>31,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f19 286 348 36 24 20 -2 #rect
Bh0 f19 @|StepIcon #fIcon
Bh0 f21 expr out #txt
Bh0 f21 304 324 304 348 #arcP
Bh0 f18 expr out #txt
Bh0 f18 304 372 304 404 #arcP
Bh0 f22 type ch.soreco.orderbook.ui.Order #txt
Bh0 f22 processCall 'Business Processes/Adressmutation/Match:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.bo.Order)' #txt
Bh0 f22 doCall true #txt
Bh0 f22 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.bo.Order filter> param;
' #txt
Bh0 f22 requestMappingAction 'param.order=in.order;
param.filter.BPId=in.order.BPId;
param.filter.orderType=in.order.orderType;
' #txt
Bh0 f22 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f22 responseMappingAction 'out=in;
out.order=result.order;
' #txt
Bh0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Match</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f22 198 732 36 24 20 -2 #rect
Bh0 f22 @|CallSubIcon #fIcon
Bh0 f23 expr out #txt
Bh0 f23 216 700 216 732 #arcP
Bh0 f23 0 0.5000000000000001 0 0 #arcLabel
Bh0 f25 type ch.soreco.orderbook.ui.Order #txt
Bh0 f25 processCall 'Business Processes/Adressmutation/Edit:call(ch.soreco.orderbook.bo.Order,ch.soreco.orderbook.ui.enums.ViewType)' #txt
Bh0 f25 doCall true #txt
Bh0 f25 requestActionDecl '<ch.soreco.orderbook.bo.Order order,ch.soreco.orderbook.ui.enums.ViewType viewType> param;
' #txt
Bh0 f25 requestMappingAction 'param.order=in.order;
param.viewType=in.eViewType;
' #txt
Bh0 f25 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f25 responseMappingAction 'out=in;
out.error=result.error;
out.order=result.order;
out.success=result.success;
' #txt
Bh0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Adressmutation 
Edit</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f25 198 900 36 24 20 -2 #rect
Bh0 f25 @|CallSubIcon #fIcon
Bh0 f28 type ch.soreco.orderbook.ui.Order #txt
Bh0 f28 processCall 'Functional Processes/orderUISave:call(ch.soreco.orderbook.ui.Order)' #txt
Bh0 f28 doCall true #txt
Bh0 f28 requestActionDecl '<ch.soreco.orderbook.ui.Order orderUI> param;
' #txt
Bh0 f28 requestMappingAction 'param.orderUI=in;
' #txt
Bh0 f28 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f28 responseMappingAction 'out=result.orderUI;
' #txt
Bh0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Save orderUI</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f28 198 844 36 24 20 -2 #rect
Bh0 f28 @|CallSubIcon #fIcon
Bh0 f27 expr out #txt
Bh0 f27 216 868 216 900 #arcP
Bh0 f31 expr out #txt
Bh0 f31 536 364 322 312 #arcP
Bh0 f31 1 536 312 #addKink
Bh0 f31 0 0.891849104287907 0 0 #arcLabel
Bh0 f32 type ch.soreco.orderbook.ui.Order #txt
Bh0 f32 processCall 'Start Processes/Archive/CheckDocuments:call(ch.soreco.orderbook.ui.enums.ViewType,Integer,String)' #txt
Bh0 f32 doCall true #txt
Bh0 f32 requestActionDecl '<ch.soreco.orderbook.ui.enums.ViewType viewType,java.lang.Integer orderId,java.lang.String uiType> param;
' #txt
Bh0 f32 requestMappingAction 'param.viewType=ch.soreco.orderbook.ui.enums.ViewType.READ;
param.orderId=in.order.orderId;
param.uiType="SCANPLAIN";
' #txt
Bh0 f32 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f32 responseMappingAction 'out=in;
' #txt
Bh0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show files of order</name>
        <nameStyle>19,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f32 86 564 36 24 20 -2 #rect
Bh0 f32 @|CallSubIcon #fIcon
Bh0 f33 expr data #txt
Bh0 f33 outCond ivp=="showScanFile.ivp" #txt
Bh0 f33 286 525 122 571 #arcP
Bh0 f13 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f13 actionTable 'out=in;
out.order.editorUserId=ivy.session.getSessionUserName();
' #txt
Bh0 f13 type ch.soreco.orderbook.ui.Order #txt
Bh0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>remove checker</name>
        <nameStyle>14
</nameStyle>
        <desc>PB: why remove checker? removed cause of business need
set editor on save</desc>
    </language>
</elementInfo>
' #txt
Bh0 f13 198 780 36 24 20 -2 #rect
Bh0 f13 @|StepIcon #fIcon
Bh0 f34 expr out #txt
Bh0 f34 216 756 216 780 #arcP
Bh0 f34 0 0.5000000000000001 0 0 #arcLabel
Bh0 f30 expr out #txt
Bh0 f30 216 804 216 844 #arcP
Bh0 f40 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f40 actionTable 'out=in;
' #txt
Bh0 f40 actionCode 'import ch.ivyteam.ivy.ext.SessionLockManager;

SessionLockManager lockManager = SessionLockManager.getInstance();
if(lockManager.lockIdForSession(in.order.orderId.toString(),ivy.session)){
	lockManager.releaseLockId(in.order.orderId.toString());	
	ivy.log.info("Released lock for "+in.order.orderId+" by session id "+ivy.session.getIdentifier());
	}
	

' #txt
Bh0 f40 type ch.soreco.orderbook.ui.Order #txt
Bh0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>release lock</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f40 198 956 36 24 20 -2 #rect
Bh0 f40 @|StepIcon #fIcon
Bh0 f41 expr out #txt
Bh0 f41 216 924 216 956 #arcP
Bh0 f41 0 0.5045754195490944 0 0 #arcLabel
Bh0 f24 expr out #txt
Bh0 f24 234 968 307 968 #arcP
Bh0 f12 type ch.soreco.orderbook.ui.Order #txt
Bh0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isAccountOpening?</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f12 290 114 28 28 14 0 #rect
Bh0 f12 @|AlternativeIcon #fIcon
Bh0 f3 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f3 actionTable 'out=in;
out.order.bpNotMatchable=null;
out.order.orderState=ch.soreco.orderbook.enums.OrderState.Recording.toString();
' #txt
Bh0 f3 type ch.soreco.orderbook.ui.Order #txt
Bh0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set State Erfassung</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f3 30 676 36 24 20 -2 #rect
Bh0 f3 @|StepIcon #fIcon
Bh0 f45 expr in #txt
Bh0 f45 outCond in.order.orderType.equals(ch.soreco.orderbook.enums.OrderType.Accountopening.toString()) #txt
Bh0 f45 290 128 48 676 #arcP
Bh0 f45 1 48 128 #addKink
Bh0 f45 1 0.35203310135445415 0 0 #arcLabel
Bh0 f44 expr out #txt
Bh0 f44 48 700 198 744 #arcP
Bh0 f44 1 48 744 #addKink
Bh0 f44 1 0.3009680173969628 0 0 #arcLabel
Bh0 f50 expr in #txt
Bh0 f50 304 142 304 188 #arcP
Bh0 f105 type ch.soreco.orderbook.ui.Order #txt
Bh0 f105 processCall 'Business Processes/History/ShowSimpleOrderHistory:showSimpleHistory(ch.soreco.orderbook.functional.History)' #txt
Bh0 f105 doCall true #txt
Bh0 f105 requestActionDecl '<ch.soreco.orderbook.functional.History history> param;
' #txt
Bh0 f105 requestMappingAction 'param.history.fetchId=in.order.orderId;
param.history.historyTable="tbl_Orders_History";
param.history.idName="orderId";
' #txt
Bh0 f105 responseActionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f105 responseMappingAction 'out=in;
' #txt
Bh0 f105 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>show history</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f105 566 500 36 24 20 -2 #rect
Bh0 f105 @|CallSubIcon #fIcon
Bh0 f53 expr data #txt
Bh0 f53 outCond ivp=="showHistory.ivp" #txt
Bh0 f53 322 520 566 512 #arcP
Bh0 f57 expr out #txt
Bh0 f57 128 508 286 312 #arcP
Bh0 f57 1 128 312 #addKink
Bh0 f57 1 0.11351184556535769 0 0 #arcLabel
Bh0 f20 expr out #txt
Bh0 f20 304 77 304 188 #arcP
Bh0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>PB: 21.03.2012
removed: Accountopening is now generic</name>
        <nameStyle>53,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f43 442 98 60 60 -14 -14 #rect
Bh0 f43 @|IBIcon #fIcon
Bh0 f43 -1|-1|-16777216 #nodeStyle
Bh0 f55 442 128 318 128 #arcP
Bh0 f108 actionDecl 'ch.soreco.orderbook.ui.Order out;
' #txt
Bh0 f108 actionTable 'out=in;
out.order.discard=true;
out.order.editorUserId=ivy.session.getSessionUserName();
out.order.orderState=ch.soreco.orderbook.enums.OrderState.InAbortCheck.toString();
' #txt
Bh0 f108 actionCode 'import ch.soreco.orderbook.util.Authority;

out.order.checkerUserId = Authority.getInstance().findTeamLeaderOf(in.order.teams, in.order.orderType,in.order.editorUserId, in.order.dispatcherUserId);
' #txt
Bh0 f108 type ch.soreco.orderbook.ui.Order #txt
Bh0 f108 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Abbruch: State is InAbortCheck
Checker is TeamLeader or Dispatcher</name>
        <nameStyle>66,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Bh0 f108 734 676 36 24 20 -2 #rect
Bh0 f108 @|StepIcon #fIcon
Bh0 f56 expr out #txt
Bh0 f56 752 700 474 808 #arcP
Bh0 f56 1 752 736 #addKink
Bh0 f56 1 0.41074347929786964 0 0 #arcLabel
Bh0 f58 expr data #txt
Bh0 f58 outCond ivp=="abortAndClose.ivp" #txt
Bh0 f58 322 520 734 688 #arcP
>Proto Bh0 .type ch.soreco.orderbook.ui.Order #txt
>Proto Bh0 .processKind CALLABLE_SUB #txt
>Proto Bh0 0 0 32 24 18 0 #rect
>Proto Bh0 @|BIcon #fIcon
Bh0 f26 mainOut f37 tail #connect
Bh0 f37 head f36 mainIn #connect
Bh0 f36 out f39 tail #connect
Bh0 f39 head f38 mainIn #connect
Bh0 f42 mainOut f48 tail #connect
Bh0 f48 head f47 mainIn #connect
Bh0 f49 mainOut f51 tail #connect
Bh0 f51 head f46 mainIn #connect
Bh0 f36 out f52 tail #connect
Bh0 f52 head f35 mainIn #connect
Bh0 f36 out f54 tail #connect
Bh0 f54 head f49 mainIn #connect
Bh0 f46 mainOut f2 tail #connect
Bh0 f2 head f1 mainIn #connect
Bh0 f36 out f5 tail #connect
Bh0 f5 head f4 mainIn #connect
Bh0 f4 mainOut f6 tail #connect
Bh0 f6 head f46 mainIn #connect
Bh0 f36 out f8 tail #connect
Bh0 f8 head f7 mainIn #connect
Bh0 f7 mainOut f10 tail #connect
Bh0 f10 head f1 mainIn #connect
Bh0 f36 out f11 tail #connect
Bh0 f11 head f9 mainIn #connect
Bh0 f47 mainOut f15 tail #connect
Bh0 f15 head f14 mainIn #connect
Bh0 f17 mainOut f16 tail #connect
Bh0 f16 head f26 mainIn #connect
Bh0 f14 mainOut f21 tail #connect
Bh0 f21 head f19 mainIn #connect
Bh0 f19 mainOut f18 tail #connect
Bh0 f18 head f17 mainIn #connect
Bh0 f35 mainOut f23 tail #connect
Bh0 f23 head f22 mainIn #connect
Bh0 f28 mainOut f27 tail #connect
Bh0 f27 head f25 mainIn #connect
Bh0 f9 mainOut f31 tail #connect
Bh0 f31 head f14 mainIn #connect
Bh0 f36 out f33 tail #connect
Bh0 f33 head f32 mainIn #connect
Bh0 f22 mainOut f34 tail #connect
Bh0 f34 head f13 mainIn #connect
Bh0 f13 mainOut f30 tail #connect
Bh0 f30 head f28 mainIn #connect
Bh0 f25 mainOut f41 tail #connect
Bh0 f41 head f40 mainIn #connect
Bh0 f40 mainOut f24 tail #connect
Bh0 f24 head f1 mainIn #connect
Bh0 f12 out f45 tail #connect
Bh0 f45 head f3 mainIn #connect
Bh0 f3 mainOut f44 tail #connect
Bh0 f44 head f22 mainIn #connect
Bh0 f12 out f50 tail #connect
Bh0 f50 head f42 mainIn #connect
Bh0 f36 out f53 tail #connect
Bh0 f53 head f105 mainIn #connect
Bh0 f38 mainOut f57 tail #connect
Bh0 f57 head f14 mainIn #connect
Bh0 f0 mainOut f20 tail #connect
Bh0 f20 head f42 mainIn #connect
Bh0 f43 ao f55 tail #connect
Bh0 f55 head f12 @CG|ai #connect
Bh0 f108 mainOut f56 tail #connect
Bh0 f56 head f46 mainIn #connect
Bh0 f36 out f58 tail #connect
Bh0 f58 head f108 mainIn #connect
