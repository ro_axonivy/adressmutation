[Ivy]
[>Created: Fri Aug 05 12:02:40 CEST 2011]
1307B071B9ECA90C 3.15 #module
>Proto >Proto Collection #zClass
Cs0 Classes Big #zClass
Cs0 B #cInfo
Cs0 #process
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @StartSub f0 '' #zField
Cs0 @EndSub f1 '' #zField
Cs0 @GridStep f5 '' #zField
Cs0 @Page f2 '' #zField
Cs0 @GridStep f7 '' #zField
Cs0 @GridStep f19 '' #zField
Cs0 @GridStep f25 '' #zField
Cs0 @GridStep f14 '' #zField
Cs0 @PushWFArc f3 '' #zField
Cs0 @PushWFArc f8 '' #zField
Cs0 @PushWFArc f15 '' #zField
Cs0 @PushWFArc f27 '' #zField
Cs0 @PushWFArc f4 '' #zField
Cs0 @PushWFArc f6 '' #zField
Cs0 @CallSub f9 '' #zField
Cs0 @CallSub f10 '' #zField
Cs0 @PushWFArc f11 '' #zField
Cs0 @PushWFArc f12 '' #zField
Cs0 @GridStep f13 '' #zField
Cs0 @PushWFArc f16 '' #zField
Cs0 @PushWFArc f17 '' #zField
Cs0 @PushWFArc f18 '' #zField
Cs0 @CallSub f20 '' #zField
Cs0 @PushWFArc f22 '' #zField
Cs0 @CallSub f24 '' #zField
Cs0 @PushWFArc f26 '' #zField
Cs0 @PushWFArc f28 '' #zField
Cs0 @GridStep f30 '' #zField
Cs0 @CallSub f31 '' #zField
Cs0 @PushWFArc f34 '' #zField
Cs0 @GridStep f29 '' #zField
Cs0 @PushWFArc f32 '' #zField
Cs0 @PushWFArc f33 '' #zField
Cs0 @PushWFArc f21 '' #zField
Cs0 @Page f35 '' #zField
Cs0 @PushWFArc f36 '' #zField
Cs0 @PushWFArc f23 '' #zField
Cs0 @GridStep f40 '' #zField
Cs0 @PushWFArc f41 '' #zField
Cs0 @PushWFArc f42 '' #zField
>Proto Cs0 Cs0 Classes #zField
Cs0 f0 outParamDecl '<> result;
' #txt
Cs0 f0 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f0 callSignature call() #txt
Cs0 f0 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 91 35 26 26 14 0 #rect
Cs0 f0 @|StartSubIcon #fIcon
Cs0 f1 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f1 91 287 26 26 14 0 #rect
Cs0 f1 @|EndSubIcon #fIcon
Cs0 f5 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f5 actionTable 'out=in;
' #txt
Cs0 f5 actionCode 'import ch.soreco.webbies.common.util.XmlProperties;
import ch.soreco.webbies.common.util.CompositeUtils;
import ch.soreco.webbies.common.db.PersistenceUtils;
import ch.soreco.webbies.common.eclipse.Classes;
import ch.soreco.webbies.common.db.EntityClasses;

Recordset rs = new Recordset();
List<Class> classes = new List<Class>();
//classes.addAll(EntityClasses.getEntityClasses());
classes.addAll(Classes.getAllClasses());
rs.addColumn("ClassType",[]);
rs.addColumn("ClassName",[]);
rs.addColumn("Classes",[]);
rs.addColumn("isConfigured",[]);

for(Class clazz:classes){
	String classType = "";

	if(clazz.isEnum()){
		classType = "Enum";
		}
	else if(PersistenceUtils.isEntityClass(clazz)){
		classType = "Entity";
		}
	else if(CompositeUtils.isCompositeClass(clazz)){
		classType = "";
		}
	XmlProperties props = new XmlProperties(clazz);

	if(!classType.equals("")){
		Record rec = new Record();
		rec.putField("ClassType",classType);
		rec.putField("Classes",clazz);
		rec.putField("ClassName",clazz.getName());
		rec.putField("isConfigured",props.hasProperties());
		rs.add(rec);
		}
	}
in.rsClasses = rs;
in.rsClasses.sort("ClassName");
in.rsClasses.sort("ClassType");
' #txt
Cs0 f5 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get List of Classes</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f5 86 116 36 24 20 -2 #rect
Cs0 f5 @|StepIcon #fIcon
Cs0 f2 outTypes "ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin" #txt
Cs0 f2 outLinks "gotoEntityTable.ivp","gotoFormAttributes.ivp","gotoEntityList.ivp","exit.ivp","excel.ivp","excelImport.ivp","gotoAdminClass.ivp" #txt
Cs0 f2 template "/ProcessPages/Classes/ClassesOverview.ivc" #txt
Cs0 f2 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f2 skipLink skip.ivp #txt
Cs0 f2 sortLink sort.ivp #txt
Cs0 f2 templateWizard '#
#Tue Jul 12 21:07:31 CEST 2011
' #txt
Cs0 f2 pageArchivingActivated false #txt
Cs0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Classes Overview</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f2 @C|.responsibility Everybody #txt
Cs0 f2 86 180 36 24 20 -2 #rect
Cs0 f2 @|PageIcon #fIcon
Cs0 f7 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f7 actionTable 'out=in;
out.clazz=in.rsClasses.getColumn("Classes").get(in.i) as java.lang.Class;
' #txt
Cs0 f7 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Class</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f7 606 236 36 24 20 -2 #rect
Cs0 f7 @|StepIcon #fIcon
Cs0 f19 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f19 actionTable 'out=in;
' #txt
Cs0 f19 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeContent;
in.clazz = in.rsClasses.getColumn("Classes").get(in.i) as java.lang.Class;
in.attributeContent = new AttributeContent(in.clazz);' #txt
Cs0 f19 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Class AttributeContent</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f19 910 236 36 24 20 -2 #rect
Cs0 f19 @|StepIcon #fIcon
Cs0 f25 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f25 actionTable 'out=in;
out.clazz=in.rsClasses.getColumn("Classes").get(in.i) as java.lang.Class;
' #txt
Cs0 f25 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Class</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f25 478 236 36 24 20 -2 #rect
Cs0 f25 @|StepIcon #fIcon
Cs0 f14 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f14 actionTable 'out=in;
out.clazz=in.rsClasses.getColumn("Classes").get(in.i) as java.lang.Class;
' #txt
Cs0 f14 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Class</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f14 742 236 36 24 20 -2 #rect
Cs0 f14 @|StepIcon #fIcon
Cs0 f3 expr out #txt
Cs0 f3 104 140 104 180 #arcP
Cs0 f8 expr data #txt
Cs0 f8 outCond ivp=="gotoEntityTable.ivp" #txt
Cs0 f8 122 192 624 236 #arcP
Cs0 f8 1 624 192 #addKink
Cs0 f8 0 0.601026814489164 0 0 #arcLabel
Cs0 f15 expr data #txt
Cs0 f15 outCond ivp=="gotoFormAttributes.ivp" #txt
Cs0 f15 122 192 760 236 #arcP
Cs0 f15 1 760 192 #addKink
Cs0 f15 0 0.57237600948133 0 0 #arcLabel
Cs0 f27 expr data #txt
Cs0 f27 outCond ivp=="gotoEntityList.ivp" #txt
Cs0 f27 122 192 496 236 #arcP
Cs0 f27 1 496 192 #addKink
Cs0 f27 0 0.6667470654492814 0 0 #arcLabel
Cs0 f4 expr out #txt
Cs0 f4 104 61 104 116 #arcP
Cs0 f6 expr data #txt
Cs0 f6 outCond ivp=="exit.ivp" #txt
Cs0 f6 104 204 104 287 #arcP
Cs0 f9 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f9 processCall 'Business Processes/Generics/Admin/Attributes:editAttributeContent(ch.soreco.webbies.common.html.generics.AttributeContent)' #txt
Cs0 f9 doCall true #txt
Cs0 f9 requestActionDecl '<ch.soreco.webbies.common.html.generics.AttributeContent attributeContent> param;
' #txt
Cs0 f9 requestMappingAction 'param.attributeContent=in.attributeContent;
' #txt
Cs0 f9 responseActionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f9 responseMappingAction 'out=in;
' #txt
Cs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>editAttributeContent(AttributeContent)</name>
        <nameStyle>38,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f9 910 300 36 24 20 -2 #rect
Cs0 f9 @|CallSubIcon #fIcon
Cs0 f10 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f10 processCall 'Business Processes/Generics/Admin/Attributes:call(java.lang.Class)' #txt
Cs0 f10 doCall true #txt
Cs0 f10 requestActionDecl '<java.lang.Class clazz> param;
' #txt
Cs0 f10 requestMappingAction 'param.clazz=in.clazz;
' #txt
Cs0 f10 responseActionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f10 responseMappingAction 'out=in;
' #txt
Cs0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Class)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f10 742 300 36 24 20 -2 #rect
Cs0 f10 @|CallSubIcon #fIcon
Cs0 f11 expr out #txt
Cs0 f11 760 260 760 300 #arcP
Cs0 f12 expr out #txt
Cs0 f12 928 260 928 300 #arcP
Cs0 f13 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f13 actionTable 'out=in;
' #txt
Cs0 f13 actionCode 'import ch.soreco.webbies.common.util.XmlProperties;
XmlProperties props = new XmlProperties(in.clazz);
if(props.hasProperties()){
	for(int i=0;i<in.rsClasses.size();i++){
		if(in.rsClasses.getField(i,"ClassName").toString().equals(in.clazz.getName())){
			in.rsClasses.putField(i,"isConfigured", true);
			}
		}
	}
' #txt
Cs0 f13 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set isConfigured</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f13 678 388 36 24 20 -2 #rect
Cs0 f13 @|StepIcon #fIcon
Cs0 f16 expr out #txt
Cs0 f16 760 324 714 391 #arcP
Cs0 f16 1 760 368 #addKink
Cs0 f16 1 0.1162992345951565 0 0 #arcLabel
Cs0 f17 expr out #txt
Cs0 f17 928 324 714 400 #arcP
Cs0 f17 1 928 400 #addKink
Cs0 f17 1 0.30623470757545485 0 0 #arcLabel
Cs0 f18 expr out #txt
Cs0 f18 678 400 86 192 #arcP
Cs0 f18 1 16 400 #addKink
Cs0 f18 2 16 192 #addKink
Cs0 f18 0 0.8913128223497456 0 0 #arcLabel
Cs0 f20 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f20 processCall 'Business Processes/Generics/Entity/Table:list(java.lang.Class)' #txt
Cs0 f20 doCall true #txt
Cs0 f20 requestActionDecl '<java.lang.Class clazz> param;
' #txt
Cs0 f20 requestMappingAction 'param.clazz=in.clazz;
' #txt
Cs0 f20 responseActionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f20 responseMappingAction 'out=in;
' #txt
Cs0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>goto List</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f20 478 300 36 24 20 -2 #rect
Cs0 f20 @|CallSubIcon #fIcon
Cs0 f22 expr out #txt
Cs0 f22 514 320 678 392 #arcP
Cs0 f24 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f24 processCall 'Business Processes/Generics/Entity/Table:table(java.lang.Class)' #txt
Cs0 f24 doCall true #txt
Cs0 f24 requestActionDecl '<java.lang.Class clazz> param;
' #txt
Cs0 f24 requestMappingAction 'param.clazz=in.clazz;
' #txt
Cs0 f24 responseActionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f24 responseMappingAction 'out=in;
' #txt
Cs0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>goto Table</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f24 606 300 36 24 20 -2 #rect
Cs0 f24 @|CallSubIcon #fIcon
Cs0 f26 expr out #txt
Cs0 f26 624 260 624 300 #arcP
Cs0 f28 expr out #txt
Cs0 f28 634 324 686 388 #arcP
Cs0 f30 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f30 actionTable 'out=in;
' #txt
Cs0 f30 actionCode 'import ch.soreco.webbies.common.excel.ColumnMeta;
import ch.soreco.webbies.common.excel.SheetMeta;
import ch.soreco.webbies.common.excel.ExcelWriter;
SheetMeta meta = new SheetMeta(in.rsExport);
//meta.caption = "Class Configurations";
meta.sheetName = "Configs";
ExcelWriter excel = new ExcelWriter("Excel.xls");


try {
excel.export(meta);
File excelFile = excel.getExcelFile();
out.excel = new java.io.File(excelFile.getJavaFile().getAbsolutePath());//new java.io.File(excelFile.getPath());
} catch (Exception e){
	ivy.log.error("Error during exporting to excel.", e);
	}' #txt
Cs0 f30 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Excel</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f30 190 292 36 24 20 -2 #rect
Cs0 f30 @|StepIcon #fIcon
Cs0 f31 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f31 processCall 'Functional Processes/common/File:show(java.io.File)' #txt
Cs0 f31 doCall true #txt
Cs0 f31 requestActionDecl '<java.io.File file> param;
' #txt
Cs0 f31 requestMappingAction 'param.file=in.excel;
' #txt
Cs0 f31 responseActionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f31 responseMappingAction 'out=in;
' #txt
Cs0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Excel</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f31 190 348 36 24 20 -2 #rect
Cs0 f31 @|CallSubIcon #fIcon
Cs0 f34 expr out #txt
Cs0 f34 208 316 208 348 #arcP
Cs0 f29 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f29 actionTable 'out=in;
' #txt
Cs0 f29 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClass;
List<Recordset> configs = new List<Recordset>();
List<String> fieldNames = new List<String>();


//Get the properties recordsets
for(int i=0;i<in.rsClasses.size();i++){
	Class clazz = in.rsClasses.getColumn("Classes").get(i) as java.lang.Class;
	AttributeClass ac = new AttributeClass(clazz);
	Recordset rs =ac.getPropertiesRecordset();
	
	configs.add(rs);
	//gather fieldnames of each recordset as rs.addAll(<Recordset>) does not attempt new fieldnames
	for(String key:rs.getKeys()){
		if(fieldNames.indexOf(key)==-1){
			fieldNames.add(key);
			}
		}
	}

//set columns	
in.rsExport = new Recordset();	
for(String columnName:fieldNames){
	in.rsExport.addColumn(columnName,[]);
	}

//add all Recordsets to Export Recordset
for(Recordset rs:configs){
	in.rsExport.addAll(rs);
	}' #txt
Cs0 f29 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Prepare Classes
Config Recordset</name>
        <nameStyle>32
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f29 190 236 36 24 20 -2 #rect
Cs0 f29 @|StepIcon #fIcon
Cs0 f32 expr data #txt
Cs0 f32 outCond ivp=="excel.ivp" #txt
Cs0 f32 122 202 190 238 #arcP
Cs0 f32 0 0.23550053834032444 0 0 #arcLabel
Cs0 f33 expr out #txt
Cs0 f33 208 260 208 292 #arcP
Cs0 f21 expr out #txt
Cs0 f21 496 260 496 300 #arcP
Cs0 f35 outTypes "ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin","ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin" #txt
Cs0 f35 outLinks "submit.ivp","import.ivp" #txt
Cs0 f35 template "/ProcessPages/Classes/ImportFile.ivc" #txt
Cs0 f35 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f35 skipLink skip.ivp #txt
Cs0 f35 sortLink sort.ivp #txt
Cs0 f35 templateWizard '#
#Fri Aug 05 12:01:51 CEST 2011
' #txt
Cs0 f35 pageArchivingActivated false #txt
Cs0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Import File</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f35 @C|.responsibility Everybody #txt
Cs0 f35 270 108 36 24 20 -2 #rect
Cs0 f35 @|PageIcon #fIcon
Cs0 f36 expr data #txt
Cs0 f36 outCond ivp=="excelImport.ivp" #txt
Cs0 f36 122 185 270 127 #arcP
Cs0 f23 expr data #txt
Cs0 f23 outCond ivp=="gotoAdminClass.ivp" #txt
Cs0 f23 122 192 928 236 #arcP
Cs0 f23 1 928 192 #addKink
Cs0 f23 0 0.5358998142954254 0 0 #arcLabel
Cs0 f40 actionDecl 'ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin out;
' #txt
Cs0 f40 actionTable 'out=in;
' #txt
Cs0 f40 actionCode 'import ch.soreco.webbies.common.util.XmlProperties;
//try to start excel import
import ch.soreco.webbies.common.excel.ExcelReader;

in.rsExport = ExcelReader.getExcelSheet(in.importFile.getJavaFile());
for(int i=0;i<in.rsExport.size();i++){
	XmlProperties prop = new XmlProperties(in.rsExport.getAt(i));
	}' #txt
Cs0 f40 type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
Cs0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Store Configs</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f40 270 60 36 24 20 -2 #rect
Cs0 f40 @|StepIcon #fIcon
Cs0 f41 expr data #txt
Cs0 f41 outCond ivp=="import.ivp" #txt
Cs0 f41 288 108 288 84 #arcP
Cs0 f42 expr out #txt
Cs0 f42 288 60 114 116 #arcP
Cs0 f42 1 288 40 #addKink
Cs0 f42 2 176 40 #addKink
Cs0 f42 1 0.6632325157509686 0 0 #arcLabel
>Proto Cs0 .type ch.soreco.common.webbies.entity.ui.admin.ClassesAdmin #txt
>Proto Cs0 .processKind CALLABLE_SUB #txt
>Proto Cs0 0 0 32 24 18 0 #rect
>Proto Cs0 @|BIcon #fIcon
Cs0 f5 mainOut f3 tail #connect
Cs0 f3 head f2 mainIn #connect
Cs0 f2 out f8 tail #connect
Cs0 f8 head f7 mainIn #connect
Cs0 f2 out f15 tail #connect
Cs0 f15 head f14 mainIn #connect
Cs0 f2 out f27 tail #connect
Cs0 f27 head f25 mainIn #connect
Cs0 f0 mainOut f4 tail #connect
Cs0 f4 head f5 mainIn #connect
Cs0 f2 out f6 tail #connect
Cs0 f6 head f1 mainIn #connect
Cs0 f14 mainOut f11 tail #connect
Cs0 f11 head f10 mainIn #connect
Cs0 f19 mainOut f12 tail #connect
Cs0 f12 head f9 mainIn #connect
Cs0 f10 mainOut f16 tail #connect
Cs0 f16 head f13 mainIn #connect
Cs0 f9 mainOut f17 tail #connect
Cs0 f17 head f13 mainIn #connect
Cs0 f13 mainOut f18 tail #connect
Cs0 f18 head f2 mainIn #connect
Cs0 f20 mainOut f22 tail #connect
Cs0 f22 head f13 mainIn #connect
Cs0 f7 mainOut f26 tail #connect
Cs0 f26 head f24 mainIn #connect
Cs0 f24 mainOut f28 tail #connect
Cs0 f28 head f13 mainIn #connect
Cs0 f30 mainOut f34 tail #connect
Cs0 f34 head f31 mainIn #connect
Cs0 f2 out f32 tail #connect
Cs0 f32 head f29 mainIn #connect
Cs0 f29 mainOut f33 tail #connect
Cs0 f33 head f30 mainIn #connect
Cs0 f25 mainOut f21 tail #connect
Cs0 f21 head f20 mainIn #connect
Cs0 f2 out f36 tail #connect
Cs0 f36 head f35 mainIn #connect
Cs0 f2 out f23 tail #connect
Cs0 f23 head f19 mainIn #connect
Cs0 f35 out f41 tail #connect
Cs0 f41 head f40 mainIn #connect
Cs0 f40 mainOut f42 tail #connect
Cs0 f42 head f5 mainIn #connect
