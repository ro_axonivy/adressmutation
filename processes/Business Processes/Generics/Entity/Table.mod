[Ivy]
13094CB5E75F0CFE 3.20 #module
>Proto >Proto Collection #zClass
Te0 Table Big #zClass
Te0 B #cInfo
Te0 #process
Te0 @TextInP .resExport .resExport #zField
Te0 @TextInP .type .type #zField
Te0 @TextInP .processKind .processKind #zField
Te0 @AnnotationInP-0n ai ai #zField
Te0 @TextInP .xml .xml #zField
Te0 @TextInP .responsibility .responsibility #zField
Te0 @GridStep f34 '' #zField
Te0 @CallSub f9 '' #zField
Te0 @GridStep f17 '' #zField
Te0 @StartSub f43 '' #zField
Te0 @StartSub f46 '' #zField
Te0 @CallSub f15 '' #zField
Te0 @GridStep f7 '' #zField
Te0 @CallSub f32 '' #zField
Te0 @EndSub f41 '' #zField
Te0 @GridStep f44 '' #zField
Te0 @GridStep f38 '' #zField
Te0 @GridStep f25 '' #zField
Te0 @StartSub f0 '' #zField
Te0 @GridStep f27 '' #zField
Te0 @EndSub f30 '' #zField
Te0 @Page f2 '' #zField
Te0 @GridStep f12 '' #zField
Te0 @GridStep f21 '' #zField
Te0 @StartSub f29 '' #zField
Te0 @EndSub f1 '' #zField
Te0 @GridStep f20 '' #zField
Te0 @GridStep f60 '' #zField
Te0 @GridStep f50 '' #zField
Te0 @GridStep f3 '' #zField
Te0 @EndSub f39 '' #zField
Te0 @GridStep f62 '' #zField
Te0 @GridStep f57 '' #zField
Te0 @Page f40 '' #zField
Te0 @CallSub f45 '' #zField
Te0 @GridStep f56 '' #zField
Te0 @GridStep f36 '' #zField
Te0 @PushWFArc f8 '' #zField
Te0 @PushWFArc f10 '' #zField
Te0 @PushWFArc f13 '' #zField
Te0 @PushWFArc f19 '' #zField
Te0 @PushWFArc f22 '' #zField
Te0 @PushWFArc f23 '' #zField
Te0 @PushWFArc f24 '' #zField
Te0 @PushWFArc f5 '' #zField
Te0 @PushWFArc f28 '' #zField
Te0 @PushWFArc f31 '' #zField
Te0 @PushWFArc f14 '' #zField
Te0 @PushWFArc f4 '' #zField
Te0 @PushWFArc f33 '' #zField
Te0 @PushWFArc f6 '' #zField
Te0 @PushWFArc f35 '' #zField
Te0 @PushWFArc f11 '' #zField
Te0 @PushWFArc f37 '' #zField
Te0 @PushWFArc f18 '' #zField
Te0 @PushWFArc f42 '' #zField
Te0 @PushWFArc f47 '' #zField
Te0 @PushWFArc f48 '' #zField
Te0 @PushWFArc f49 '' #zField
Te0 @PushWFArc f54 '' #zField
Te0 @PushWFArc f51 '' #zField
Te0 @PushWFArc f52 '' #zField
Te0 @PushWFArc f58 '' #zField
Te0 @PushWFArc f59 '' #zField
Te0 @PushWFArc f61 '' #zField
Te0 @PushWFArc f55 '' #zField
Te0 @PushWFArc f63 '' #zField
Te0 @PushWFArc f53 '' #zField
Te0 @CallSub f66 '' #zField
Te0 @GridStep f68 '' #zField
Te0 @GridStep f69 '' #zField
Te0 @PushWFArc f70 '' #zField
Te0 @PushWFArc f71 '' #zField
Te0 @PushWFArc f72 '' #zField
Te0 @GridStep f67 '' #zField
Te0 @Page f73 '' #zField
Te0 @PushWFArc f74 '' #zField
Te0 @PushWFArc f75 '' #zField
Te0 @PushWFArc f16 '' #zField
Te0 @PushWFArc f64 '' #zField
Te0 @GridStep f65 '' #zField
Te0 @PushWFArc f76 '' #zField
Te0 @PushWFArc f77 '' #zField
Te0 @GridStep f78 '' #zField
Te0 @Alternative f80 '' #zField
Te0 @PushWFArc f81 '' #zField
Te0 @PushWFArc f79 '' #zField
Te0 @PushWFArc f82 '' #zField
Te0 @GridStep f83 '' #zField
Te0 @PushWFArc f84 '' #zField
Te0 @PushWFArc f26 '' #zField
>Proto Te0 Te0 Table #zField
Te0 f34 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f34 actionTable 'out=in;
' #txt
Te0 f34 actionCode 'if(in.entity!=null){
	in.entities.add(in.entity);
	}' #txt
Te0 f34 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add @entities if set</name>
        <nameStyle>20
</nameStyle>
        <desc>Map the result only if entity is set (User may exited)</desc>
    </language>
</elementInfo>
' #txt
Te0 f34 1054 500 36 24 20 -2 #rect
Te0 f34 @|StepIcon #fIcon
Te0 f34 -13016147|-1|-16777216 #nodeStyle
Te0 f9 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f9 processCall 'Business Processes/Generics/Entity/Edit:edit(ch.ivyteam.ivy.scripting.objects.CompositeObject,ch.ivyteam.ivy.scripting.objects.CompositeObject)' #txt
Te0 f9 doCall true #txt
Te0 f9 requestActionDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> param;
' #txt
Te0 f9 requestMappingAction 'param.entityOld=in.entity.deepClone();
param.entity=in.entity;
' #txt
Te0 f9 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f9 responseMappingAction 'out=in;
out.entity=result.entity;
' #txt
Te0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>goto Entity Edit</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f9 766 444 36 24 20 -2 #rect
Te0 f9 @|CallSubIcon #fIcon
Te0 f17 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f17 actionTable 'out=in;
' #txt
Te0 f17 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f17 902 548 36 24 20 -2 #rect
Te0 f17 @|StepIcon #fIcon
Te0 f43 inParamDecl '<java.lang.Class clazz,List<ch.ivyteam.ivy.scripting.objects.CompositeObject> entities> param;' #txt
Te0 f43 inParamTable 'out.clazz=param.clazz;
out.entities=param.entities;
' #txt
Te0 f43 outParamDecl '<> result;
' #txt
Te0 f43 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f43 callSignature list(java.lang.Class,List<ch.ivyteam.ivy.scripting.objects.CompositeObject>) #txt
Te0 f43 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>list(Class,List&lt;CompositeObject&gt;)</name>
    </language>
</elementInfo>
' #txt
Te0 f43 211 627 26 26 14 0 #rect
Te0 f43 @|StartSubIcon #fIcon
Te0 f46 inParamDecl '<java.lang.Class clazz> param;' #txt
Te0 f46 inParamTable 'out.clazz=param.clazz;
' #txt
Te0 f46 outParamDecl '<> result;
' #txt
Te0 f46 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f46 callSignature list(java.lang.Class) #txt
Te0 f46 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f46 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>list(Class)</name>
    </language>
</elementInfo>
' #txt
Te0 f46 43 619 26 26 14 0 #rect
Te0 f46 @|StartSubIcon #fIcon
Te0 f15 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f15 processCall 'Business Processes/Generics/Entity/Edit:add(java.lang.Class)' #txt
Te0 f15 doCall true #txt
Te0 f15 requestActionDecl '<java.lang.Class clazz> param;
' #txt
Te0 f15 requestMappingAction 'param.clazz=in.clazz;
' #txt
Te0 f15 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f15 responseMappingAction 'out=in;
out.entity=result.entity;
' #txt
Te0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>goTo Entity Add</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f15 1054 444 36 24 20 -2 #rect
Te0 f15 @|CallSubIcon #fIcon
Te0 f7 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f7 actionTable 'out=in;
out.entity=in.entities.get(in.i);
' #txt
Te0 f7 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Composite</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f7 766 396 36 24 20 -2 #rect
Te0 f7 @|StepIcon #fIcon
Te0 f32 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f32 processCall 'Business Processes/Generics/Entity/Table:table(java.lang.Class,List<ch.ivyteam.ivy.scripting.objects.CompositeObject>)' #txt
Te0 f32 doCall true #txt
Te0 f32 requestActionDecl '<java.lang.Class clazz,List<ch.ivyteam.ivy.scripting.objects.CompositeObject> entities> param;
' #txt
Te0 f32 requestMappingAction 'param.clazz=in.clazz;
param.entities=in.entities;
' #txt
Te0 f32 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f32 responseMappingAction 'out=in;
' #txt
Te0 f32 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call Table</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f32 54 148 36 24 20 -2 #rect
Te0 f32 @|CallSubIcon #fIcon
Te0 f41 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f41 43 811 26 26 14 0 #rect
Te0 f41 @|EndSubIcon #fIcon
Te0 f44 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f44 actionTable 'out=in;
' #txt
Te0 f44 actionCode 'import ch.soreco.webbies.common.util.CompositeUtils;
for(int i = 0;i<in.rs.size();i++){
	in.entity = CompositeUtils.mapRecordTo(in.rs.getRecords().get(i),in.clazz);
	in.entities.set(i,in.entity);
	}
	
' #txt
Te0 f44 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f44 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>map recordset 
to entities</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f44 542 836 36 24 20 -2 #rect
Te0 f44 @|StepIcon #fIcon
Te0 f38 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f38 actionTable 'out=in;
' #txt
Te0 f38 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;
//get all Entities of present clazz
String unit = PersistenceUtils.getPersistenceUnitNameOf(in.clazz);
if(!unit.equals("")){
	in.entities = ivy.persistence.get(unit).findAll(in.clazz);	
	}
' #txt
Te0 f38 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f38 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>find all of clazz</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f38 38 668 36 24 20 -2 #rect
Te0 f38 @|StepIcon #fIcon
Te0 f25 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f25 actionTable 'out=in;
' #txt
Te0 f25 actionCode '//as Casting from CompositeObject to Record fails if Columns are initialized
//Edit and Delete Column need to be added separatly	
if(in.canDelete) {
	in.rs.addColumn("canDelete",[]);
	}
if(in.canEdit) {
	in.rs.addColumn("canEdit",[]);
	}
for(int i=0;i<in.rs.size();i++){
		if(in.canDelete) {
			in.rs.putField(i,"canDelete","<%=ivy.cms.co(\"/Content/Shared/Images/delete\")%>");
		}
		if(in.canEdit) {
			in.rs.putField(i,"canEdit","<%=ivy.cms.co(\"/Content/Shared/Images/edit\")%>");
		}
	}' #txt
Te0 f25 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add Delete/Edit Column</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f25 478 300 36 24 20 -2 #rect
Te0 f25 @|StepIcon #fIcon
Te0 f0 inParamDecl '<java.lang.Class clazz> param;' #txt
Te0 f0 inParamTable 'out.clazz=param.clazz;
' #txt
Te0 f0 outParamDecl '<> result;
' #txt
Te0 f0 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f0 callSignature table(java.lang.Class) #txt
Te0 f0 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>table(Class)</name>
    </language>
</elementInfo>
' #txt
Te0 f0 59 59 26 26 14 0 #rect
Te0 f0 @|StartSubIcon #fIcon
Te0 f27 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f27 actionTable 'out=in;
' #txt
Te0 f27 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;
//get all Entities of present clazz
String unit = PersistenceUtils.getPersistenceUnitNameOf(in.clazz);
if(!unit.equals("")){
	in.entities = ivy.persistence.get(unit).findAll(in.clazz);	
	}
' #txt
Te0 f27 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>find all of clazz</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f27 54 108 36 24 20 -2 #rect
Te0 f27 @|StepIcon #fIcon
Te0 f30 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f30 483 523 26 26 14 0 #rect
Te0 f30 @|EndSubIcon #fIcon
Te0 f2 outTypes "ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable" #txt
Te0 f2 outLinks "submit.ivp","edit.ivp","exit.ivp","add.ivp","remove.ivp","exportEntities.ivp","importEntities.ivp","filter.ivp" #txt
Te0 f2 template "/ProcessPages/Table/EntityTable.ivc" #txt
Te0 f2 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f2 skipLink skip.ivp #txt
Te0 f2 sortLink sort.ivp #txt
Te0 f2 templateWizard '#
#Tue Jul 26 18:40:14 CEST 2011
' #txt
Te0 f2 pageArchivingActivated false #txt
Te0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Table</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f2 @C|.responsibility Everybody #txt
Te0 f2 478 396 36 24 20 -2 #rect
Te0 f2 @|PageIcon #fIcon
Te0 f12 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f12 actionTable 'out=in;
' #txt
Te0 f12 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f12 430 460 36 24 20 -2 #rect
Te0 f12 @|StepIcon #fIcon
Te0 f21 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f21 actionTable 'out=in;
' #txt
Te0 f21 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;
String unit = PersistenceUtils.getPersistenceUnitNameOf(in.clazz);
ivy.persistence.get(unit).remove(in.entity);
in.entities.removeAt(in.i);' #txt
Te0 f21 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>remove 
@persistence and 
@entities</name>
        <nameStyle>35
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f21 902 500 36 24 21 -12 #rect
Te0 f21 @|StepIcon #fIcon
Te0 f29 inParamDecl '<java.lang.Class clazz,List<ch.ivyteam.ivy.scripting.objects.CompositeObject> entities> param;' #txt
Te0 f29 inParamTable 'out.clazz=param.clazz;
out.entities=param.entities;
' #txt
Te0 f29 outParamDecl '<> result;
' #txt
Te0 f29 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f29 callSignature table(java.lang.Class,List<ch.ivyteam.ivy.scripting.objects.CompositeObject>) #txt
Te0 f29 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>table(Class,List&lt;CompositeObject&gt;)</name>
    </language>
</elementInfo>
' #txt
Te0 f29 483 59 26 26 14 0 #rect
Te0 f29 @|StartSubIcon #fIcon
Te0 f1 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f1 59 251 26 26 14 0 #rect
Te0 f1 @|EndSubIcon #fIcon
Te0 f20 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f20 actionTable 'out=in;
out.entity=in.entities.get(in.i);
' #txt
Te0 f20 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Composite</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f20 902 396 36 24 25 -7 #rect
Te0 f20 @|StepIcon #fIcon
Te0 f60 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f60 actionTable 'out=in;
' #txt
Te0 f60 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;

for(int i = 0;i<in.entities.size();i++){
	in.entity = in.entities.get(i) as CompositeObject;
	in.entity = PersistenceUtils.merge(in.entity); 
	in.entities.set(i,in.entity);
	}
	' #txt
Te0 f60 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f60 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Persist Entities</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f60 542 892 36 24 20 -2 #rect
Te0 f60 @|StepIcon #fIcon
Te0 f50 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f50 actionTable 'out=in;
' #txt
Te0 f50 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClass;
import ch.soreco.webbies.common.html.generics.InputType;
import ch.soreco.webbies.common.html.generics.AttributeContent;
import ch.soreco.webbies.common.html.generics.AttributeClassHtml;

//initialize Recordset, AttributeClass, and EntityList
AttributeClass attributeClass = new AttributeClass(in.clazz, in.view);
in.classLabel = attributeClass.getClassContent().getLabel();

in.listHtml = new StringBuilder("");	
//Map Found List to Recordset and Entities List<CompositeObject>
if(in.entities!=null) {
	for(int i=0;i<in.entities.size();i++){
		in.entity = in.entities.get(i) as CompositeObject;
		in.rs.add(in.entity);
		AttributeClassHtml acc = new AttributeClassHtml(in.view, in.entity, "rs", i);
		in.listHtml.append(acc.toHtml());
		}
	}

' #txt
Te0 f50 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f50 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Map Entities to HtmlList &amp; Recordset</name>
        <nameStyle>36
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f50 206 724 36 24 20 -2 #rect
Te0 f50 @|StepIcon #fIcon
Te0 f3 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f3 actionTable 'out=in;
' #txt
Te0 f3 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClassRecordset;
import ch.soreco.webbies.common.html.generics.AttributeClass;
import ch.soreco.webbies.common.html.generics.ViewProperties;

//initialize Recordset, AttributeClass, and EntityList
AttributeClass attributeClass = new AttributeClass(in.clazz, in.view);
in.classLabel = attributeClass.getClassContent().getLabel();
in.canDelete = attributeClass.getClassContent().getProperty(ViewProperties.CAN_BE_DELETED).toLowerCase().equals("true");
in.canEdit = attributeClass.getClassContent().getProperty(ViewProperties.CAN_BE_EDITED).toLowerCase().equals("true");
in.canExport = attributeClass.getClassContent().getProperty(ViewProperties.CAN_EXPORT_ALL).toLowerCase().equals("true");
in.canImport = attributeClass.getClassContent().getProperty(ViewProperties.CAN_IMPORT_ALL).toLowerCase().equals("true");
in.canFilter = attributeClass.getClassContent().getProperty(ViewProperties.CAN_FILTER).trim().length()>0;
if(in.canFilter){
	in.filterView =  attributeClass.getClassContent().getProperty(ViewProperties.CAN_FILTER).trim();
	}
in.rs = AttributeClassRecordset.getRecordset(in.clazz,in.view,in.entities);
' #txt
Te0 f3 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init View Properties 
and Map Entities to table</name>
        <nameStyle>47
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f3 478 108 36 24 20 -2 #rect
Te0 f3 @|StepIcon #fIcon
Te0 f39 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f39 211 907 26 26 14 0 #rect
Te0 f39 @|EndSubIcon #fIcon
Te0 f62 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f62 actionTable 'out=in;
out.view=ch.soreco.webbies.common.html.generics.AttributeConstants.CONTENTPREFIX;
' #txt
Te0 f62 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f62 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Default View</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f62 206 676 36 24 20 -2 #rect
Te0 f62 @|StepIcon #fIcon
Te0 f57 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f57 actionTable 'out=in;
' #txt
Te0 f57 actionCode 'in.entity = in.clazz.newInstance() as CompositeObject;
in.entities.add(in.entity);
' #txt
Te0 f57 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f57 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add new entity</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f57 406 836 36 24 20 -2 #rect
Te0 f57 @|StepIcon #fIcon
Te0 f40 outTypes "ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable","ch.soreco.common.webbies.entity.ui.functional.EntityTable" #txt
Te0 f40 outLinks "exit.ivp","submit.ivp","addNew.ivp" #txt
Te0 f40 template "/ProcessPages/Table/EntityList.ivc" #txt
Te0 f40 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f40 skipLink skip.ivp #txt
Te0 f40 sortLink sort.ivp #txt
Te0 f40 templateWizard '#
#Mon Jun 06 16:52:45 CEST 2011
' #txt
Te0 f40 pageArchivingActivated false #txt
Te0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>List</name>
        <nameStyle>4
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f40 @C|.responsibility Everybody #txt
Te0 f40 206 836 36 24 20 -2 #rect
Te0 f40 @|PageIcon #fIcon
Te0 f45 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f45 processCall 'Business Processes/Generics/Entity/Table:list(java.lang.Class,List<ch.ivyteam.ivy.scripting.objects.CompositeObject>)' #txt
Te0 f45 doCall true #txt
Te0 f45 requestActionDecl '<java.lang.Class clazz,List<ch.ivyteam.ivy.scripting.objects.CompositeObject> entities> param;
' #txt
Te0 f45 requestMappingAction 'param.clazz=in.clazz;
param.entities=in.entities;
' #txt
Te0 f45 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f45 responseMappingAction 'out=in;
' #txt
Te0 f45 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call List</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f45 38 708 36 24 20 -2 #rect
Te0 f45 @|CallSubIcon #fIcon
Te0 f56 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f56 actionTable 'out=in;
' #txt
Te0 f56 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f56 406 980 36 24 20 -2 #rect
Te0 f56 @|StepIcon #fIcon
Te0 f36 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f36 actionTable 'out=in;
out.entities=in.entities.set(in.i,in.entity);
' #txt
Te0 f36 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set @entities</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f36 766 500 36 24 20 -2 #rect
Te0 f36 @|StepIcon #fIcon
Te0 f8 expr data #txt
Te0 f8 outCond ivp=="edit.ivp" #txt
Te0 f8 514 400 784 396 #arcP
Te0 f8 1 584 368 #addKink
Te0 f8 2 784 368 #addKink
Te0 f8 0 0.9575856372140799 0 0 #arcLabel
Te0 f10 expr out #txt
Te0 f10 784 420 784 444 #arcP
Te0 f13 expr data #txt
Te0 f13 outCond ivp=="exit.ivp" #txt
Te0 f13 487 420 457 460 #arcP
Te0 f19 expr out #txt
Te0 f19 920 572 514 120 #arcP
Te0 f19 1 920 608 #addKink
Te0 f19 2 1224 608 #addKink
Te0 f19 3 1224 120 #addKink
Te0 f19 2 0.42566415486744014 0 0 #arcLabel
Te0 f22 expr out #txt
Te0 f22 920 420 920 500 #arcP
Te0 f23 expr out #txt
Te0 f23 920 524 920 548 #arcP
Te0 f24 expr data #txt
Te0 f24 outCond ivp=="remove.ivp" #txt
Te0 f24 513 396 920 396 #arcP
Te0 f24 1 584 344 #addKink
Te0 f24 2 920 344 #addKink
Te0 f24 1 0.4484214051598991 0 0 #arcLabel
Te0 f5 expr out #txt
Te0 f5 496 324 496 396 #arcP
Te0 f28 expr out #txt
Te0 f28 72 85 72 108 #arcP
Te0 f31 expr out #txt
Te0 f31 457 484 488 525 #arcP
Te0 f14 expr data #txt
Te0 f14 outCond ivp=="submit.ivp" #txt
Te0 f14 496 420 496 523 #arcP
Te0 f4 expr out #txt
Te0 f4 496 85 496 108 #arcP
Te0 f33 expr out #txt
Te0 f33 72 132 72 148 #arcP
Te0 f6 expr out #txt
Te0 f6 72 172 72 251 #arcP
Te0 f35 expr out #txt
Te0 f35 1072 468 1072 500 #arcP
Te0 f11 expr out #txt
Te0 f11 1072 524 938 560 #arcP
Te0 f11 1 1072 560 #addKink
Te0 f11 1 0.3296682806408887 0 0 #arcLabel
Te0 f37 expr out #txt
Te0 f37 784 468 784 500 #arcP
Te0 f18 expr out #txt
Te0 f18 784 524 902 553 #arcP
Te0 f18 1 784 544 #addKink
Te0 f18 2 880 544 #addKink
Te0 f18 1 0.46603301798962726 0 0 #arcLabel
Te0 f42 expr data #txt
Te0 f42 outCond ivp=="exit.ivp" #txt
Te0 f42 224 860 224 907 #arcP
Te0 f47 expr out #txt
Te0 f47 56 645 56 668 #arcP
Te0 f48 expr out #txt
Te0 f48 56 692 56 708 #arcP
Te0 f49 expr out #txt
Te0 f49 56 732 56 811 #arcP
Te0 f54 expr out #txt
Te0 f54 224 748 224 836 #arcP
Te0 f51 expr data #txt
Te0 f51 outCond ivp=="submit.ivp" #txt
Te0 f51 239 836 560 836 #arcP
Te0 f51 1 296 792 #addKink
Te0 f51 2 560 792 #addKink
Te0 f51 1 0.4738885302594898 0 0 #arcLabel
Te0 f52 expr out #txt
Te0 f52 442 992 242 736 #arcP
Te0 f52 1 664 992 #addKink
Te0 f52 2 664 736 #addKink
Te0 f52 2 0.09572328778246582 0 0 #arcLabel
Te0 f58 expr data #txt
Te0 f58 outCond ivp=="addNew.ivp" #txt
Te0 f58 242 848 406 848 #arcP
Te0 f59 expr out #txt
Te0 f59 424 860 424 980 #arcP
Te0 f61 expr out #txt
Te0 f61 560 860 560 892 #arcP
Te0 f55 expr out #txt
Te0 f55 542 916 442 980 #arcP
Te0 f63 expr out #txt
Te0 f63 224 653 224 676 #arcP
Te0 f53 expr out #txt
Te0 f53 224 700 224 724 #arcP
Te0 f66 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f66 processCall 'Functional Processes/common/File:show(java.io.File)' #txt
Te0 f66 doCall true #txt
Te0 f66 requestActionDecl '<java.io.File file> param;
' #txt
Te0 f66 requestMappingAction 'param.file=in.excel;
' #txt
Te0 f66 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f66 responseMappingAction 'out=in;
' #txt
Te0 f66 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show Excel</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f66 334 572 36 24 20 -2 #rect
Te0 f66 @|CallSubIcon #fIcon
Te0 f68 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f68 actionTable 'out=in;
' #txt
Te0 f68 actionCode 'import ch.soreco.webbies.common.excel.ColumnMeta;
import ch.soreco.webbies.common.excel.SheetMeta;
import ch.soreco.webbies.common.excel.ExcelWriter;
SheetMeta meta = new SheetMeta(in.rs);
//meta.caption = "Class Configurations";
meta.sheetName = in.classLabel;
ExcelWriter excel = new ExcelWriter(in.classLabel.replace(" ","_")+".xls");


try {
excel.export(meta);
File excelFile = excel.getExcelFile();
out.excel = new java.io.File(excelFile.getJavaFile().getAbsolutePath());//new java.io.File(excelFile.getPath());
} catch (Exception e){
	ivy.log.error("Error during exporting to excel.", e);
	}' #txt
Te0 f68 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f68 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Excel</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f68 334 508 36 24 20 -2 #rect
Te0 f68 @|StepIcon #fIcon
Te0 f69 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f69 actionTable 'out=in;
' #txt
Te0 f69 actionCode 'import ch.soreco.webbies.common.util.ListToRecordset;

in.rs=ListToRecordset.getRecordset(in.entities,in.clazz, false, true);
' #txt
Te0 f69 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f69 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Prepare Export</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f69 334 428 36 24 20 -2 #rect
Te0 f69 @|StepIcon #fIcon
Te0 f70 expr out #txt
Te0 f70 352 452 352 508 #arcP
Te0 f71 expr out #txt
Te0 f71 352 532 352 572 #arcP
Te0 f72 expr data #txt
Te0 f72 outCond ivp=="exportEntities.ivp" #txt
Te0 f72 478 408 352 428 #arcP
Te0 f72 1 352 408 #addKink
Te0 f72 0 0.6374368541872553 0 0 #arcLabel
Te0 f67 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f67 actionTable 'out=in;
' #txt
Te0 f67 actionCode 'import ch.soreco.webbies.common.util.CompositeUtils;
import ch.soreco.webbies.common.excel.ExcelReader;
import ch.soreco.webbies.common.db.PersistenceUtils;

try {
Recordset rs = ExcelReader.getExcelSheet(in.importFile.getJavaFile());
for(int i=0;i<rs.size();i++){
	ivy.log.info("Imported Record from Excel at "+i+" is "+rs.getAt(i).toString());
	in.entity = CompositeUtils.mapRecordTo(rs.getAt(i),in.clazz);
	ivy.log.info("Mapping Record to "+i+" is "+in.entity.toString());
	}
List<CompositeObject> entities = CompositeUtils.recordsetToList(rs,in.clazz); 
 
for(int i=0;i<entities.size();i++){
	in.entity = entities.get(i);
	ivy.log.info("Start to Merge "+in.entity.toString());
	in.entity = PersistenceUtils.merge(in.entity);
	ivy.log.info("End of Merge "+in.entity.toString());
	entities.set(i,in.entity);
	}
in.entities = entities;
}
catch(Exception e){
	ivy.log.error(e);
	}' #txt
Te0 f67 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f67 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Store Configs</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f67 630 500 36 24 20 -2 #rect
Te0 f67 @|StepIcon #fIcon
Te0 f73 outTypes "ch.soreco.common.webbies.entity.ui.functional.EntityTable" #txt
Te0 f73 outLinks "submit.ivp" #txt
Te0 f73 template "/ProcessPages/Classes/ImportFile.ivc" #txt
Te0 f73 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f73 skipLink skip.ivp #txt
Te0 f73 sortLink sort.ivp #txt
Te0 f73 templateWizard '#
#Thu Jul 07 09:03:54 CEST 2011
' #txt
Te0 f73 pageArchivingActivated false #txt
Te0 f73 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Import File</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f73 @C|.responsibility Everybody #txt
Te0 f73 630 444 36 24 20 -2 #rect
Te0 f73 @|PageIcon #fIcon
Te0 f74 expr data #txt
Te0 f74 outCond ivp=="submit.ivp" #txt
Te0 f74 648 468 648 500 #arcP
Te0 f75 expr data #txt
Te0 f75 outCond ivp=="add.ivp" #txt
Te0 f75 509 396 1072 444 #arcP
Te0 f75 1 584 328 #addKink
Te0 f75 2 1072 328 #addKink
Te0 f75 1 0.474075949421673 0 0 #arcLabel
Te0 f16 expr data #txt
Te0 f16 outCond ivp=="importEntities.ivp" #txt
Te0 f16 514 403 648 444 #arcP
Te0 f16 1 584 384 #addKink
Te0 f16 2 648 384 #addKink
Te0 f16 1 0.100234602660223 0 0 #arcLabel
Te0 f64 expr out #txt
Te0 f64 648 524 902 560 #arcP
Te0 f64 1 648 560 #addKink
Te0 f64 1 0.40460019079942755 0 0 #arcLabel
Te0 f65 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f65 actionTable 'out=in;
' #txt
Te0 f65 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;

in.entities = PersistenceUtils.searchBy(in.filter,in.filterLotSize);
' #txt
Te0 f65 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f65 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>searchBy filter</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f65 310 140 36 24 20 -2 #rect
Te0 f65 @|StepIcon #fIcon
Te0 f76 expr data #txt
Te0 f76 outCond ivp=="filter.ivp" #txt
Te0 f76 488 396 336 164 #arcP
Te0 f77 expr out #txt
Te0 f77 346 149 478 123 #arcP
Te0 f78 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f78 actionTable 'out=in;
' #txt
Te0 f78 actionCode 'if(in.#filter==null){
	Object obj = in.clazz.newInstance();
	in.filter = obj as CompositeObject;
}	' #txt
Te0 f78 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f78 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initialize Filter Object</name>
        <nameStyle>24
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f78 566 196 36 24 20 -2 #rect
Te0 f78 @|StepIcon #fIcon
Te0 f80 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f80 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>canFilter?</name>
        <nameStyle>10
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f80 482 154 28 28 14 0 #rect
Te0 f80 @|AlternativeIcon #fIcon
Te0 f81 expr out #txt
Te0 f81 496 132 496 154 #arcP
Te0 f79 expr in #txt
Te0 f79 506 172 566 200 #arcP
Te0 f82 expr in #txt
Te0 f82 496 182 496 300 #arcP
Te0 f83 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityTable out;
' #txt
Te0 f83 actionTable 'out=in;
' #txt
Te0 f83 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClass;
import ch.soreco.webbies.common.html.generics.AttributeClassHtml;

AttributeClassHtml acc = new AttributeClassHtml(in.filterView, in.filter, "filter");
in.filterFormHtml = acc.toHtml();

' #txt
Te0 f83 type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
Te0 f83 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Filter Form</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Te0 f83 566 244 36 24 20 -2 #rect
Te0 f83 @|StepIcon #fIcon
Te0 f84 expr out #txt
Te0 f84 584 220 584 244 #arcP
Te0 f26 expr out #txt
Te0 f26 566 267 514 301 #arcP
>Proto Te0 .type ch.soreco.common.webbies.entity.ui.functional.EntityTable #txt
>Proto Te0 .processKind CALLABLE_SUB #txt
>Proto Te0 0 0 32 24 18 0 #rect
>Proto Te0 @|BIcon #fIcon
Te0 f8 head f7 mainIn #connect
Te0 f7 mainOut f10 tail #connect
Te0 f10 head f9 mainIn #connect
Te0 f13 head f12 mainIn #connect
Te0 f17 mainOut f19 tail #connect
Te0 f19 head f3 mainIn #connect
Te0 f20 mainOut f22 tail #connect
Te0 f22 head f21 mainIn #connect
Te0 f21 mainOut f23 tail #connect
Te0 f23 head f17 mainIn #connect
Te0 f24 head f20 mainIn #connect
Te0 f25 mainOut f5 tail #connect
Te0 f5 head f2 mainIn #connect
Te0 f0 mainOut f28 tail #connect
Te0 f28 head f27 mainIn #connect
Te0 f12 mainOut f31 tail #connect
Te0 f31 head f30 mainIn #connect
Te0 f2 out f14 tail #connect
Te0 f14 head f30 mainIn #connect
Te0 f2 out f8 tail #connect
Te0 f2 out f13 tail #connect
Te0 f29 mainOut f4 tail #connect
Te0 f4 head f3 mainIn #connect
Te0 f27 mainOut f33 tail #connect
Te0 f33 head f32 mainIn #connect
Te0 f32 mainOut f6 tail #connect
Te0 f6 head f1 mainIn #connect
Te0 f15 mainOut f35 tail #connect
Te0 f35 head f34 mainIn #connect
Te0 f34 mainOut f11 tail #connect
Te0 f11 head f17 mainIn #connect
Te0 f9 mainOut f37 tail #connect
Te0 f37 head f36 mainIn #connect
Te0 f36 mainOut f18 tail #connect
Te0 f18 head f17 mainIn #connect
Te0 f40 out f42 tail #connect
Te0 f42 head f39 mainIn #connect
Te0 f46 mainOut f47 tail #connect
Te0 f47 head f38 mainIn #connect
Te0 f38 mainOut f48 tail #connect
Te0 f48 head f45 mainIn #connect
Te0 f45 mainOut f49 tail #connect
Te0 f49 head f41 mainIn #connect
Te0 f50 mainOut f54 tail #connect
Te0 f54 head f40 mainIn #connect
Te0 f40 out f51 tail #connect
Te0 f51 head f44 mainIn #connect
Te0 f56 mainOut f52 tail #connect
Te0 f52 head f50 mainIn #connect
Te0 f40 out f58 tail #connect
Te0 f58 head f57 mainIn #connect
Te0 f57 mainOut f59 tail #connect
Te0 f59 head f56 mainIn #connect
Te0 f44 mainOut f61 tail #connect
Te0 f61 head f60 mainIn #connect
Te0 f60 mainOut f55 tail #connect
Te0 f55 head f56 mainIn #connect
Te0 f43 mainOut f63 tail #connect
Te0 f63 head f62 mainIn #connect
Te0 f62 mainOut f53 tail #connect
Te0 f53 head f50 mainIn #connect
Te0 f69 mainOut f70 tail #connect
Te0 f70 head f68 mainIn #connect
Te0 f68 mainOut f71 tail #connect
Te0 f71 head f66 mainIn #connect
Te0 f72 head f69 mainIn #connect
Te0 f73 out f74 tail #connect
Te0 f74 head f67 mainIn #connect
Te0 f2 out f75 tail #connect
Te0 f75 head f15 mainIn #connect
Te0 f2 out f24 tail #connect
Te0 f2 out f72 tail #connect
Te0 f2 out f16 tail #connect
Te0 f16 head f73 mainIn #connect
Te0 f67 mainOut f64 tail #connect
Te0 f64 head f17 mainIn #connect
Te0 f2 out f76 tail #connect
Te0 f76 head f65 mainIn #connect
Te0 f65 mainOut f77 tail #connect
Te0 f77 head f3 mainIn #connect
Te0 f3 mainOut f81 tail #connect
Te0 f81 head f80 in #connect
Te0 f80 out f79 tail #connect
Te0 f79 head f78 mainIn #connect
Te0 f80 out f82 tail #connect
Te0 f82 head f25 mainIn #connect
Te0 f78 mainOut f84 tail #connect
Te0 f84 head f83 mainIn #connect
Te0 f83 mainOut f26 tail #connect
Te0 f26 head f25 mainIn #connect
