[Ivy]
130982C4203A2C60 3.20 #module
>Proto >Proto Collection #zClass
Et0 Edit Big #zClass
Et0 B #cInfo
Et0 #process
Et0 @TextInP .resExport .resExport #zField
Et0 @TextInP .type .type #zField
Et0 @TextInP .processKind .processKind #zField
Et0 @AnnotationInP-0n ai ai #zField
Et0 @TextInP .xml .xml #zField
Et0 @TextInP .responsibility .responsibility #zField
Et0 @StartSub f0 '' #zField
Et0 @EndSub f1 '' #zField
Et0 @GridStep f3 '' #zField
Et0 @StartSub f7 '' #zField
Et0 @StartSub f15 '' #zField
Et0 @GridStep f16 '' #zField
Et0 @CallSub f22 '' #zField
Et0 @GridStep f35 '' #zField
Et0 @Page f14 '' #zField
Et0 @CallSub f19 '' #zField
Et0 @GridStep f12 '' #zField
Et0 @CallSub f5 '' #zField
Et0 @EndSub f13 '' #zField
Et0 @GridStep f31 '' #zField
Et0 @EndSub f6 '' #zField
Et0 @GridStep f27 '' #zField
Et0 @GridStep f33 '' #zField
Et0 @GridStep f2 '' #zField
Et0 @GridStep f17 '' #zField
Et0 @PushWFArc f4 '' #zField
Et0 @PushWFArc f18 '' #zField
Et0 @PushWFArc f21 '' #zField
Et0 @PushWFArc f23 '' #zField
Et0 @PushWFArc f11 '' #zField
Et0 @PushWFArc f8 '' #zField
Et0 @PushWFArc f9 '' #zField
Et0 @PushWFArc f20 '' #zField
Et0 @PushWFArc f25 '' #zField
Et0 @PushWFArc f28 '' #zField
Et0 @PushWFArc f29 '' #zField
Et0 @PushWFArc f32 '' #zField
Et0 @PushWFArc f30 '' #zField
Et0 @PushWFArc f26 '' #zField
Et0 @PushWFArc f34 '' #zField
Et0 @PushWFArc f10 '' #zField
Et0 @PushWFArc f36 '' #zField
Et0 @PushWFArc f24 '' #zField
>Proto Et0 Et0 Edit #zField
Et0 f0 inParamDecl '<java.lang.Class clazz,java.lang.Integer key> param;' #txt
Et0 f0 inParamTable 'out.className=param.clazz.getName();
out.clazz=param.clazz;
out.key=param.key;
' #txt
Et0 f0 outParamDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entity> result;
' #txt
Et0 f0 outParamTable 'result.entity=in.entity;
' #txt
Et0 f0 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f0 callSignature edit(java.lang.Class,Integer) #txt
Et0 f0 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>edit(Class,Integer)</name>
        <nameStyle>19,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f0 483 83 26 26 14 0 #rect
Et0 f0 @|StartSubIcon #fIcon
Et0 f1 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f1 483 331 26 26 14 0 #rect
Et0 f1 @|EndSubIcon #fIcon
Et0 f3 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f3 actionTable 'out=in;
' #txt
Et0 f3 actionCode 'import ch.soreco.webbies.common.db.PersistenceUtils;
if(in.key>0) {
	String unit = PersistenceUtils.getPersistenceUnitNameOf(in.clazz);
	try {
		Object found = ivy.persistence.get(unit).find(in.clazz, in.key); 
		in.entity = found as CompositeObject;
	} catch (Exception e) {
		ivy.log.error(e);
		}	
	}
else {
	Object obj = in.clazz.newInstance();
	in.entity = obj as CompositeObject;	
	in.entityOld = in.entity.deepClone();
	}
	
	' #txt
Et0 f3 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Entity</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f3 478 140 36 24 20 -2 #rect
Et0 f3 @|StepIcon #fIcon
Et0 f7 inParamDecl '<java.lang.Class clazz> param;' #txt
Et0 f7 inParamTable 'out.className=param.clazz.getName();
out.clazz=param.clazz;
' #txt
Et0 f7 outParamDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entity> result;
' #txt
Et0 f7 outParamTable 'result.entity=in.entity;
' #txt
Et0 f7 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f7 callSignature add(java.lang.Class) #txt
Et0 f7 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add(Class)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f7 619 83 26 26 14 0 #rect
Et0 f7 @|StartSubIcon #fIcon
Et0 f15 inParamDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> param;' #txt
Et0 f15 inParamTable 'out.className=param.entity.getClass().getName();
out.clazz=param.entity.getClass();
out.entity=param.entity;
out.entityOld=param.entityOld;
' #txt
Et0 f15 outParamDecl '<java.lang.Boolean isSet,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> result;
' #txt
Et0 f15 outParamTable 'result.isSet=in.isSet;
result.entity=in.entity;
' #txt
Et0 f15 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f15 callSignature edit(ch.ivyteam.ivy.scripting.objects.CompositeObject,ch.ivyteam.ivy.scripting.objects.CompositeObject) #txt
Et0 f15 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>edit(CompositeObject)</name>
        <nameStyle>21,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f15 107 91 26 26 14 0 #rect
Et0 f15 @|StartSubIcon #fIcon
Et0 f16 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f16 actionTable 'out=in;
' #txt
Et0 f16 actionCode 'import ch.soreco.webbies.common.util.ConfigurationEditLogger;
import ch.soreco.webbies.common.db.PersistenceUtils;

in.entity = PersistenceUtils.merge(in.entity);

ConfigurationEditLogger.logConfigurationEdit(in.clazz, in.entityOld, in.entity);

in.entityOld = in.entity.deepClone();' #txt
Et0 f16 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge entity</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f16 214 268 36 24 20 -2 #rect
Et0 f16 @|StepIcon #fIcon
Et0 f22 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f22 processCall 'Business Processes/Generics/Entity/Edit:edit(ch.ivyteam.ivy.scripting.objects.CompositeObject,ch.ivyteam.ivy.scripting.objects.CompositeObject)' #txt
Et0 f22 doCall true #txt
Et0 f22 requestActionDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> param;
' #txt
Et0 f22 requestMappingAction 'param.entityOld=in.entityOld;
param.entity=in.entity;
' #txt
Et0 f22 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f22 responseMappingAction 'out=in;
out.entity=result.entity;
' #txt
Et0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>edit Form</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f22 478 188 36 24 20 -2 #rect
Et0 f22 @|CallSubIcon #fIcon
Et0 f35 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f35 actionTable 'out=in;
' #txt
Et0 f35 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeConstants;

if(in.view.equals("")){
	in.view = AttributeConstants.CONTENTPREFIX;
	}' #txt
Et0 f35 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set default View</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f35 102 156 36 24 20 -2 #rect
Et0 f35 @|StepIcon #fIcon
Et0 f14 outTypes "ch.soreco.common.webbies.entity.ui.functional.EntityEdit","ch.soreco.common.webbies.entity.ui.functional.EntityEdit","ch.soreco.common.webbies.entity.ui.functional.EntityEdit" #txt
Et0 f14 outLinks "submit.ivp","exit.ivp","addAssociation.ivp" #txt
Et0 f14 template "/ProcessPages/Edit/Edit.ivc" #txt
Et0 f14 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f14 skipLink skip.ivp #txt
Et0 f14 sortLink sort.ivp #txt
Et0 f14 templateWizard '#
#Wed Jun 15 21:35:10 CEST 2011
' #txt
Et0 f14 pageArchivingActivated false #txt
Et0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Add/Edit</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f14 @C|.responsibility Everybody #txt
Et0 f14 102 268 36 24 20 -2 #rect
Et0 f14 @|PageIcon #fIcon
Et0 f19 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f19 processCall 'Business Processes/Generics/Entity/Edit:edit(ch.ivyteam.ivy.scripting.objects.CompositeObject,ch.ivyteam.ivy.scripting.objects.CompositeObject)' #txt
Et0 f19 doCall true #txt
Et0 f19 requestActionDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> param;
' #txt
Et0 f19 requestMappingAction 'param.entityOld=in.associateEntity.deepClone();
param.entity=in.associateEntity;
' #txt
Et0 f19 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f19 responseMappingAction 'out=in;
' #txt
Et0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Entity to Append</name>
        <nameStyle>20,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f19 310 324 36 24 20 -2 #rect
Et0 f19 @|CallSubIcon #fIcon
Et0 f12 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f12 actionTable 'out=in;
' #txt
Et0 f12 actionCode 'import ch.soreco.webbies.common.html.generics.AttributeClass;
import ch.soreco.webbies.common.html.generics.AttributeClassHtml;

AttributeClassHtml acc = new AttributeClassHtml(in.view, in.entity, "entity");
in.html = acc.toHtml();

in.classLabel = acc.getClassLabel();
in.associatableClasses = acc.getAssociatables();

if(in.associatableClasses.size()>0){
	for(int i=0;i<in.associatableClasses.size();i++){
		AttributeClass attributeClass = new AttributeClass(in.associatableClasses.get(i), in.view);
		String label = attributeClass.getClassContent().getLabel();
		in.html = in.html+"<input type=''Button'' class=''Associatable'' value=''Add "+label+"'' onclick=\"this.form.action=''<%=ivy.html.ref(\"addAssociation.ivp\")%>&i="+i+"'';this.form.submit();\">";
		}
	}

in.html = in.html;
' #txt
Et0 f12 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Form</name>
        <nameStyle>8,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f12 102 212 36 24 20 -2 #rect
Et0 f12 @|StepIcon #fIcon
Et0 f5 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f5 processCall 'Business Processes/Generics/Entity/Edit:edit(ch.ivyteam.ivy.scripting.objects.CompositeObject,ch.ivyteam.ivy.scripting.objects.CompositeObject)' #txt
Et0 f5 doCall true #txt
Et0 f5 requestActionDecl '<ch.ivyteam.ivy.scripting.objects.CompositeObject entityOld,ch.ivyteam.ivy.scripting.objects.CompositeObject entity> param;
' #txt
Et0 f5 requestMappingAction 'param.entityOld=null;
param.entity=in.entity;
' #txt
Et0 f5 responseActionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f5 responseMappingAction 'out=in;
out.entity=result.entity;
out.isSet=result.isSet;
' #txt
Et0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>edit Form</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f5 614 188 36 24 20 -2 #rect
Et0 f5 @|CallSubIcon #fIcon
Et0 f13 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f13 107 395 26 26 14 0 #rect
Et0 f13 @|EndSubIcon #fIcon
Et0 f31 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f31 actionTable 'out=in;
out.isSet=true;
' #txt
Et0 f31 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set isSet:true</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f31 270 212 36 24 20 -2 #rect
Et0 f31 @|StepIcon #fIcon
Et0 f6 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f6 619 331 26 26 14 0 #rect
Et0 f6 @|EndSubIcon #fIcon
Et0 f27 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f27 actionTable 'out=in;
' #txt
Et0 f27 actionCode 'import java.lang.reflect.Field;
import ch.soreco.webbies.common.db.PersistenceUtils;
in.entity = PersistenceUtils.merge(in.entity);

Class clazz = in.associatableClasses.get(in.i) as Class;
in.associateEntity = clazz.newInstance() as CompositeObject;
String fieldName = "";
for(Field field:in.associateEntity.getClass().getDeclaredFields()){
	if(field.getType().getName().equals(in.clazz.getName())){
		fieldName = field.getName();
		break;
		}
	}
if(!fieldName.equals("")){
	in.html = fieldName;
	in.associateEntity.set(fieldName,in.entity);
	}' #txt
Et0 f27 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>merge entity 
and get 
Appending class</name>
        <nameStyle>38
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f27 214 324 36 24 -16 15 #rect
Et0 f27 @|StepIcon #fIcon
Et0 f33 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f33 actionTable 'out=in;
' #txt
Et0 f33 actionCode 'if(!in.isSet){
	in.entity = null;
	}' #txt
Et0 f33 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>if(isSet:false) set result Entity to null</name>
        <nameStyle>41
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f33 614 252 36 24 20 -2 #rect
Et0 f33 @|StepIcon #fIcon
Et0 f2 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f2 actionTable 'out=in;
' #txt
Et0 f2 actionCode 'Object obj = in.clazz.newInstance();
in.entity = obj as CompositeObject;	
	
	' #txt
Et0 f2 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initialize Entity</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Et0 f2 614 140 36 24 20 -2 #rect
Et0 f2 @|StepIcon #fIcon
Et0 f17 actionDecl 'ch.soreco.common.webbies.entity.ui.functional.EntityEdit out;
' #txt
Et0 f17 actionTable 'out=in;
' #txt
Et0 f17 type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
Et0 f17 102 332 36 24 20 -2 #rect
Et0 f17 @|StepIcon #fIcon
Et0 f4 expr out #txt
Et0 f4 496 109 496 140 #arcP
Et0 f18 expr data #txt
Et0 f18 outCond ivp=="submit.ivp" #txt
Et0 f18 138 280 214 280 #arcP
Et0 f21 expr out #txt
Et0 f21 120 236 120 268 #arcP
Et0 f23 expr out #txt
Et0 f23 496 164 496 188 #arcP
Et0 f11 expr out #txt
Et0 f11 496 212 496 331 #arcP
Et0 f8 expr out #txt
Et0 f8 632 109 632 140 #arcP
Et0 f9 expr out #txt
Et0 f9 632 164 632 188 #arcP
Et0 f20 expr data #txt
Et0 f20 outCond ivp=="exit.ivp" #txt
Et0 f20 120 292 120 332 #arcP
Et0 f25 expr out #txt
Et0 f25 120 356 120 395 #arcP
Et0 f28 expr data #txt
Et0 f28 outCond ivp=="addAssociation.ivp" #txt
Et0 f28 138 289 214 327 #arcP
Et0 f29 expr out #txt
Et0 f29 250 336 310 336 #arcP
Et0 f32 expr out #txt
Et0 f32 324 324 292 236 #arcP
Et0 f30 expr out #txt
Et0 f30 244 268 276 236 #arcP
Et0 f26 expr out #txt
Et0 f26 270 224 138 224 #arcP
Et0 f34 expr out #txt
Et0 f34 632 212 632 252 #arcP
Et0 f10 expr out #txt
Et0 f10 632 276 632 331 #arcP
Et0 f36 expr out #txt
Et0 f36 120 117 120 156 #arcP
Et0 f24 expr out #txt
Et0 f24 120 180 120 212 #arcP
>Proto Et0 .type ch.soreco.common.webbies.entity.ui.functional.EntityEdit #txt
>Proto Et0 .processKind CALLABLE_SUB #txt
>Proto Et0 0 0 32 24 18 0 #rect
>Proto Et0 @|BIcon #fIcon
Et0 f0 mainOut f4 tail #connect
Et0 f4 head f3 mainIn #connect
Et0 f14 out f18 tail #connect
Et0 f18 head f16 mainIn #connect
Et0 f12 mainOut f21 tail #connect
Et0 f21 head f14 mainIn #connect
Et0 f3 mainOut f23 tail #connect
Et0 f23 head f22 mainIn #connect
Et0 f22 mainOut f11 tail #connect
Et0 f11 head f1 mainIn #connect
Et0 f7 mainOut f8 tail #connect
Et0 f8 head f2 mainIn #connect
Et0 f2 mainOut f9 tail #connect
Et0 f9 head f5 mainIn #connect
Et0 f14 out f20 tail #connect
Et0 f20 head f17 mainIn #connect
Et0 f17 mainOut f25 tail #connect
Et0 f25 head f13 mainIn #connect
Et0 f14 out f28 tail #connect
Et0 f28 head f27 mainIn #connect
Et0 f27 mainOut f29 tail #connect
Et0 f29 head f19 mainIn #connect
Et0 f19 mainOut f32 tail #connect
Et0 f32 head f31 mainIn #connect
Et0 f16 mainOut f30 tail #connect
Et0 f30 head f31 mainIn #connect
Et0 f31 mainOut f26 tail #connect
Et0 f26 head f12 mainIn #connect
Et0 f5 mainOut f34 tail #connect
Et0 f34 head f33 mainIn #connect
Et0 f33 mainOut f10 tail #connect
Et0 f10 head f6 mainIn #connect
Et0 f15 mainOut f36 tail #connect
Et0 f36 head f35 mainIn #connect
Et0 f35 mainOut f24 tail #connect
Et0 f24 head f12 mainIn #connect
