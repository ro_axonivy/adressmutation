[Ivy]
[>Created: Wed Jul 20 12:10:05 CEST 2011]
13146FE0F55DF26C 3.15 #module
>Proto >Proto Collection #zClass
Ud0 Upload Big #zClass
Ud0 B #cInfo
Ud0 #process
Ud0 @TextInP .resExport .resExport #zField
Ud0 @TextInP .type .type #zField
Ud0 @TextInP .processKind .processKind #zField
Ud0 @AnnotationInP-0n ai ai #zField
Ud0 @TextInP .xml .xml #zField
Ud0 @TextInP .responsibility .responsibility #zField
Ud0 @StartSub f0 '' #zField
Ud0 @EndSub f1 '' #zField
Ud0 @Page f3 '' #zField
Ud0 @PushWFArc f4 '' #zField
Ud0 @GridStep f8 '' #zField
Ud0 @PushWFArc f9 '' #zField
Ud0 @PushWFArc f2 '' #zField
Ud0 @PushWFArc f10 '' #zField
>Proto Ud0 Ud0 Upload #zField
Ud0 f0 inParamDecl '<ch.ivyteam.ivy.cm.CoType coType,java.lang.String targetUri> param;' #txt
Ud0 f0 inParamTable 'out.coType=param.coType;
out.targetUri=param.targetUri;
' #txt
Ud0 f0 outParamDecl '<> result;
' #txt
Ud0 f0 actionDecl 'ch.soreco.common.webbies.cms.ui.functional.Upload out;
' #txt
Ud0 f0 callSignature call(ch.ivyteam.ivy.cm.CoType,String) #txt
Ud0 f0 type ch.soreco.common.webbies.cms.ui.functional.Upload #txt
Ud0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(CoType,String)</name>
    </language>
</elementInfo>
' #txt
Ud0 f0 87 37 26 26 14 0 #rect
Ud0 f0 @|StartSubIcon #fIcon
Ud0 f1 type ch.soreco.common.webbies.cms.ui.functional.Upload #txt
Ud0 f1 87 287 26 26 14 0 #rect
Ud0 f1 @|EndSubIcon #fIcon
Ud0 f3 outTypes "ch.soreco.common.webbies.cms.ui.functional.Upload","ch.soreco.common.webbies.cms.ui.functional.Upload" #txt
Ud0 f3 outLinks "submit.ivp","exit.ivp" #txt
Ud0 f3 template "ContentFileUpload.ivc" #txt
Ud0 f3 type ch.soreco.common.webbies.cms.ui.functional.Upload #txt
Ud0 f3 skipLink skip.ivp #txt
Ud0 f3 sortLink sort.ivp #txt
Ud0 f3 templateWizard '#
#Wed Jul 20 12:06:06 CEST 2011
' #txt
Ud0 f3 pageArchivingActivated false #txt
Ud0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get The File</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Ud0 f3 @C|.responsibility Everybody #txt
Ud0 f3 82 148 36 24 20 -2 #rect
Ud0 f3 @|PageIcon #fIcon
Ud0 f4 expr out #txt
Ud0 f4 100 63 100 148 #arcP
Ud0 f8 actionDecl 'ch.soreco.common.webbies.cms.ui.functional.Upload out;
' #txt
Ud0 f8 actionTable 'out=in;
' #txt
Ud0 f8 actionCode 'import ch.soreco.webbies.common.cms.CMSUtils;
CMSUtils.mkdirs(in.targetUri);' #txt
Ud0 f8 type ch.soreco.common.webbies.cms.ui.functional.Upload #txt
Ud0 f8 182 220 36 24 20 -2 #rect
Ud0 f8 @|StepIcon #fIcon
Ud0 f9 expr data #txt
Ud0 f9 outCond ivp=="submit.ivp" #txt
Ud0 f9 117 172 183 220 #arcP
Ud0 f2 expr out #txt
Ud0 f2 182 244 110 292 #arcP
Ud0 f10 expr data #txt
Ud0 f10 outCond ivp=="exit.ivp" #txt
Ud0 f10 100 172 100 287 #arcP
>Proto Ud0 .type ch.soreco.common.webbies.cms.ui.functional.Upload #txt
>Proto Ud0 .processKind CALLABLE_SUB #txt
>Proto Ud0 0 0 32 24 18 0 #rect
>Proto Ud0 @|BIcon #fIcon
Ud0 f0 mainOut f4 tail #connect
Ud0 f4 head f3 mainIn #connect
Ud0 f3 out f9 tail #connect
Ud0 f9 head f8 mainIn #connect
Ud0 f8 mainOut f2 tail #connect
Ud0 f2 head f1 mainIn #connect
Ud0 f3 out f10 tail #connect
Ud0 f10 head f1 mainIn #connect
