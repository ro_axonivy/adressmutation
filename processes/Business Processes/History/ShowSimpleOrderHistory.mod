[Ivy]
[>Created: Fri Mar 16 14:20:20 CET 2012]
133AC78DFF5FB5C2 3.15 #module
>Proto >Proto Collection #zClass
Sy0 ShowSimpleOrderHistory Big #zClass
Sy0 B #cInfo
Sy0 #process
Sy0 @TextInP .resExport .resExport #zField
Sy0 @TextInP .type .type #zField
Sy0 @TextInP .processKind .processKind #zField
Sy0 @AnnotationInP-0n ai ai #zField
Sy0 @TextInP .xml .xml #zField
Sy0 @TextInP .responsibility .responsibility #zField
Sy0 @StartSub f0 '' #zField
Sy0 @EndSub f1 '' #zField
Sy0 @CallSub f2 '' #zField
Sy0 @Page f4 '' #zField
Sy0 @PushWFArc f5 '' #zField
Sy0 @Page f6 '' #zField
Sy0 @PushWFArc f7 '' #zField
Sy0 @PushWFArc f3 '' #zField
>Proto Sy0 Sy0 ShowSimpleOrderHistory #zField
Sy0 f0 inParamDecl '<ch.soreco.orderbook.functional.History history> param;' #txt
Sy0 f0 inParamTable 'out=param.history;
' #txt
Sy0 f0 outParamDecl '<> result;
' #txt
Sy0 f0 actionDecl 'ch.soreco.orderbook.functional.History out;
' #txt
Sy0 f0 callSignature showSimpleHistory(ch.soreco.orderbook.functional.History) #txt
Sy0 f0 type ch.soreco.orderbook.functional.History #txt
Sy0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>showSimpleHistory(History)</name>
        <nameStyle>26,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Sy0 f0 87 37 26 26 14 0 #rect
Sy0 f0 @|StartSubIcon #fIcon
Sy0 f1 type ch.soreco.orderbook.functional.History #txt
Sy0 f1 87 287 26 26 14 0 #rect
Sy0 f1 @|EndSubIcon #fIcon
Sy0 f2 type ch.soreco.orderbook.functional.History #txt
Sy0 f2 processCall 'Functional Processes/history/History:getHistoryDataById(String,Number,String)' #txt
Sy0 f2 doCall true #txt
Sy0 f2 requestActionDecl '<java.lang.String idName,java.lang.Number fetchId,java.lang.String historyTable> param;
' #txt
Sy0 f2 requestMappingAction 'param.idName=in.idName;
param.fetchId=in.fetchId;
param.historyTable=in.historyTable;
' #txt
Sy0 f2 responseActionDecl 'ch.soreco.orderbook.functional.History out;
' #txt
Sy0 f2 responseMappingAction 'out=in;
out.historyRS=result.historyData;
' #txt
Sy0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getHistoryDataById(String,Number,String)</name>
    </language>
</elementInfo>
' #txt
Sy0 f2 82 140 36 24 20 -2 #rect
Sy0 f2 @|CallSubIcon #fIcon
Sy0 f4 template "showSimpleOrderHistory.ivc" #txt
Sy0 f4 type ch.soreco.orderbook.functional.History #txt
Sy0 f4 skipLink skip.ivp #txt
Sy0 f4 sortLink sort.ivp #txt
Sy0 f4 templateWizard '#
#Thu Nov 17 16:15:36 CET 2011
' #txt
Sy0 f4 pageArchivingActivated false #txt
Sy0 f4 @C|.responsibility Everybody #txt
Sy0 f4 82 204 36 24 20 -2 #rect
Sy0 f4 @|PageIcon #fIcon
Sy0 f5 expr out #txt
Sy0 f5 100 164 100 204 #arcP
Sy0 f6 outTypes "ch.soreco.orderbook.functional.History" #txt
Sy0 f6 outLinks "LinkA.ivp" #txt
Sy0 f6 template "redirectHistory.jsp" #txt
Sy0 f6 type ch.soreco.orderbook.functional.History #txt
Sy0 f6 skipLink skip.ivp #txt
Sy0 f6 sortLink sort.ivp #txt
Sy0 f6 templateWizard '#
#Fri Mar 16 14:10:09 CET 2012
' #txt
Sy0 f6 pageArchivingActivated false #txt
Sy0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>redirect</name>
        <nameStyle>8,7
</nameStyle>
        <desc>IE is sometimes stupid and cannot load the page</desc>
    </language>
</elementInfo>
' #txt
Sy0 f6 @C|.responsibility Everybody #txt
Sy0 f6 82 92 36 24 20 -2 #rect
Sy0 f6 @|PageIcon #fIcon
Sy0 f7 expr out #txt
Sy0 f7 100 63 100 92 #arcP
Sy0 f3 expr data #txt
Sy0 f3 outCond ivp=="LinkA.ivp" #txt
Sy0 f3 100 116 100 140 #arcP
>Proto Sy0 .type ch.soreco.orderbook.functional.History #txt
>Proto Sy0 .processKind CALLABLE_SUB #txt
>Proto Sy0 0 0 32 24 18 0 #rect
>Proto Sy0 @|BIcon #fIcon
Sy0 f2 mainOut f5 tail #connect
Sy0 f5 head f4 mainIn #connect
Sy0 f0 mainOut f7 tail #connect
Sy0 f7 head f6 mainIn #connect
Sy0 f6 out f3 tail #connect
Sy0 f3 head f2 mainIn #connect
